var gulp = require('gulp');

var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var stylus = require('gulp-stylus');
var myth = require('gulp-myth');
var nib = require('nib');
var del = require('del');

const typescript = require('gulp-typescript');
const tscConfig = require('./tsconfig.json');

// var elixir = require('laravel-elixir');
// var elixirJade = require('laravel-elixir-jade');

gulp.task('lint', function() {
    gulp.src('./resources/assets/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('minify', function(){
    gulp.src('./resources/assets/js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./public/static'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/static'));
});

gulp.task('stylus', function () {
    gulp.src('./resources/assets/stylus/screen.styl')
        .pipe(stylus({
            use: nib(),
            compress: false
        }))
        // .pipe(myth())
        .pipe(gulp.dest('./public/static'));

    gulp.src('./resources/assets/stylus/screen.styl')
        .pipe(stylus({
            use: nib(),
            compress: true
        }))
        // .pipe(myth())
        .pipe(rename('screen.min.css'))
        .pipe(gulp.dest('./public/static'));
});

gulp.task('stylusadmin', function () {
    gulp.src('./resources/assets/stylusadmin/admin.styl')
        .pipe(stylus({
            use: nib(),
            compress: false
        }))
        // .pipe(myth())
        .pipe(gulp.dest('./public/static'));

    gulp.src('./resources/assets/stylusadmin/admin.styl')
        .pipe(stylus({
            use: nib(),
            compress: true
        }))
        // .pipe(myth({
        //     sourcemap: true
        // }))
        .pipe(rename('admin.min.css'))
        .pipe(gulp.dest('./public/static'));
});

gulp.task('clean', function () {
  return del('./public/app/*');
});

gulp.task('angular', ['clean'], function () {
    return gulp
        .src('./resources/assets/angular/app/**/*.ts')
        .pipe(typescript(tscConfig.compilerOptions))
        // .pipe(concat('all.js'))
        // .pipe(gulp.dest('./public/app'))
        // .pipe(rename('all.min.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('./public/app'))
        ;
});

// gulp.task('jade', function () {
//     elixir(function(mix) {
//         mix.jade({
//             // search: '*.jade',
//             // src: '/jade/'
//         });
//     });
// });

gulp.task('default', ['lint', 'minify', 'stylus', 'stylusadmin', 'angular'], function () {
    gulp.watch('./resources/assets/js/*.js', ['lint', 'minify']);
    gulp.watch('./resources/assets/stylus/*.styl', ['stylus']);
    gulp.watch('./resources/assets/stylusadmin/*.styl', ['stylusadmin']);
    gulp.watch('./resources/assets/angular/app/**/*.ts', ['angular']);
    // gulp.watch('./resources/jade/*.jade', ['jade']);
});
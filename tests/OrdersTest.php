<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrdersTest extends TestCase
{
    public $user;
    public $profile;
    public $city;

    public function setUp()
    {
        parent::setUp();

        $this->city = App\City::where('public', 1)->first();
        $this->user = factory(App\User::class, 'licensed')->create();
        $this->profile = factory(App\Profile::class)->create([
            'user_id' => $this->user->id,
        ]);

        $this->actingAs($this->user)
             ->withSession(['foo' => 'bar'])
             ->visit('/')
             ->see($this->profile->displayName());
    }

    public function testCreateOrderForm()
    {
        $this->visit('/ads/create/order')
             ->see('Создать заказ');
    }

    public function testCreateOrderPostFailed() {
        $countBefore = App\Order::count();

        $this->visit('/ads/create/order')
            ->press('Отправить')
            ->seePageIs('/ads/create/order');

        $countAfter = App\Order::count();

        $this->assertEquals($countBefore, $countAfter);
    }

    public function testCreateOrderPost()
    {
        $countBefore = App\Order::count();
        $title = 'Hello this is the task';
        $content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

        // echo csrf_token();
        // echo $this->call('POST', '/ads/create/order', [
        //     'city_id' => 4400,
        //     'title' => $title,
        //     'content' => $content,
        //     'category_id' => 1,
        //     '_token' => csrf_token(),
        // ]);
        // return;

        $this->visit('/ads/create/order')
            ->type($title, 'title')
            ->type($content, 'content')
            ->select($this->city->city_id, 'city_id')
            ->type(1, 'category_id')
            ->press('Отправить')
            // ->seePageIs('/');
            ->see('Редактирование заказа');

        $countAfter = App\Order::count();

        $this->assertEquals($countBefore + 1, $countAfter);
    }
}

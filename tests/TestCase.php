<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        Artisan::call('migrate:reset');
        Artisan::call('migrate');
        Artisan::call('db:seed');

        App\City::where('public', 1)->update(['public' => 0]);
        App\City::where('country_id', config('app.default_country_id'))->take(10)->update(['public' => 1]);
        
        $this->firstRun = false;

        return $app;
    }
}

<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiTest extends TestCase
{
    public $user;
    public $profile;
    public $city;

    public function setUp()
    {
        parent::setUp();

        $this->city = App\City::where('public', 1)->first();
        $this->user = factory(App\User::class, 'licensed')->create();
        $this->profile = factory(App\Profile::class)->create([
            'user_id' => $this->user->id,
        ]);

        $this->actingAs($this->user)
             ->withSession(['foo' => 'bar'])
             ->visit('/')
             ->see($this->profile->displayName());
    }

    public function testNotificationsCheck()
    {
        $lastId = 0;

        $this->json('GET', '/webapi/notifications/check/' . $lastId)
            ->seeJson([
                'code' => 200,
                'data' => [],
            ]);

        App\Notification::noteAddOrderRequest(
            0,
            0,
            $this->user->id,
            $this->user->id,
            App\Order::IS_OFFER
        );

        $this->json('GET', '/webapi/notifications/check/' . $lastId)
            ->seeJsonStructure([
                'code',
                'data' => [
                    '*' => [
                        'id',
                        'data',
                        'date'
                    ],
                ],
            ]);

        $data = json_decode($this->call('GET', '/webapi/notifications/check/' . $lastId)->content(), true);

        foreach ($data['data'] as $item) {
            $lastId = $item['id'];
            break;
        }

        $this->json('GET', '/webapi/notifications/check/' . $lastId)
            ->seeJson([
                'code' => 200,
                'data' => [],
            ]);
    }

    public function testUsernameCheck()
    {
        $name = $this->profile->name;
        $default = $this->profile->name;

        // echo "NAME IS: " . $name;

        $this->json('GET', '/webapi/username/check/' . $name . '/' . $default)
            ->seeJson([
                'code' => 200,
                'data' => [
                    'busy' => false,
                    'clean' => true,
                ],
            ]);

        $name = 'aaa';

        $this->json('GET', '/webapi/username/check/' . $name . '/' . $default)
            ->seeJson([
                'code' => 200,
                'data' => [
                    'busy' => false,
                    'clean' => true,
                ],
            ]);

        $default = 'aaa';

        $this->json('GET', '/webapi/username/check/' . $name . '/' . $default)
            ->seeJson([
                'code' => 200,
                'data' => [
                    'busy' => false,
                    'clean' => true,
                ],
            ]);

        $name = 'aaa&';

        // echo $this->call('GET', '/webapi/username/check/' . $name . '/' . $default)->content();
        $this->json('GET', '/webapi/username/check/' . $name . '/' . $default)
            ->seeJson([
                'code' => 200,
                'data' => [
                    'busy' => false,
                    'clean' => false,
                ],
            ]);
    }
}

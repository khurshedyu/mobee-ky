var app = require('express')();
var request = require('request');
var config = require('./.app.json');

if(config.mode == 'http')
{
    var http = require('http').Server(app);
    var io = require('socket.io')(http);
    http.listen(3000, function(){
        console.log('Starting http server on 0.0.0.0:3000');
    });
}
else
{
    try
    {
        var fs = require('fs');
        var https = require('https');
        var httpsOptions = {
            key: fs.readFileSync(config.sslprivatekey),
            cert: fs.readFileSync(config.sslpublickey)
        };
        var server = https.createServer(httpsOptions, app).listen(3000, function(){
            console.log('Starting https server on 0.0.0.0:3000');
        });
        var io = require('socket.io')(server);

    }
    catch(e)
    {
        console.log(e.message);
    }
}

// Middleware to check apikey
// @todo check post params too
// app.use(function(req, res, next){
//     if(req.query.apikey != 'AhQue8WoB2aiph8zeicahHooraf5goeh')
//         return res.status(403).send({
//             'error': 'Please provide correct apikey'
//         });
//     else
//         next();
// });

// Notify user to check his notifications
app.get('/nodejs/reloadnotifications', function(req, res)
{
    // If userid given
    if(req.query.userid != undefined)
    {
        // At this point try to go through all connected clients
        console.log('Given user who should be notified', req.query.userid);

        // Go through all global Profiles and search for given userid
        var globalProfiles = Dialog.globalProfiles;
        Object.keys(globalProfiles).forEach(function(profile_id, index) {
            if(globalProfiles[profile_id].globalUserId == req.query.userid)
            {
                console.log('I found user, who should be notified', globalProfiles[profile_id]);
                // Emit to user
                io.to(globalProfiles[profile_id].socket).emit('check_for_new_notifications');
            }
        });

        res.send({
            'success': true
        });
    }
});

var Api = {
    baseUri: null,
    get: function (uri, callback) {
        var me = this;
        var getUri = me.baseUri + uri;
        console.log('GET URI', getUri);
        request.get({
            uri: getUri
        }, function (error, response, body) {
            console.log('GET', getUri, error, body);
            if (!error && body) {
                body = JSON.parse(body);
            } else {
                body = {data: null, code: -1};
            }
            callback(body);
            // console.log(body.data);
        });
    },
    post: function (uri, data, callback) {
        var me = this;
        var getUri = me.baseUri + uri;

        request.post({
            uri: getUri,
            form: data
        }, function (error, response, body) {
            console.log('POST', uri, body);
            body = JSON.parse(body);
            callback(body);
        });
    },
    run: function () {
        var me = this;

        me.baseUri = config.baseurl;
    }
};

Api.run();

var TokenAuth = {
    tokens: {},
    setToken: function (id, token) {
        console.log('SET TOKEN', id, token);
        this.tokens[id] = token;
    },
    hasToken: function (id) {
        return (this.tokens[id] !== undefined && this.tokens[id]);
    },
    getToken: function (id) {
        console.log('GET TOKEN', id, this.tokens[id]);
        return this.hasToken(id) ? this.tokens[id] : null;
    },
    removeToken: function (id) {
        console.log('REMOVE TOKEN', id);
        if (this.tokens[id] !== undefined) {
            delete this.tokens[id];
        }
    },
    run: function () {

    }
};

TokenAuth.run();

var Dialog = {
    dialogs: {},
    sockets: {},
    profiles: {},
    globalProfiles: {},
    dialogMembers: {},
    initDialog: function (id, socketId, profileId) {
        console.log('DIALOG INIT', this.dialogs);
        console.log('DIALOG USERS', this.sockets);
        if (this.hasDialog(id)) {
            this.addUser(id, socketId, profileId);
            return false;
        }

        this.dialogs[id] = {
            price: {},
            users: {},
            deal: null
        };

        this.addUser(id, socketId, profileId);

        return true;
    },
    hasDialog: function (id) {
        return this.dialogs[id] !== undefined;
    },
    users: function (id) {
        if (!this.hasDialog(id)) {
            return false;
        }

        var users = [];

        for (var i in this.dialogs[id].users) {
            users.push(i);
        }

        delete i;
        return users;
    },
    addUser: function (id, socketId, profileId) {
        var me = this;

        if (!me.hasDialog(id)) {
            return false;
        }

        if (me.sockets[socketId]) {
            return false;
        }

        var url = '/api/chat/member/' + id + '/' + profileId;
        url += '?token=' + TokenAuth.getToken(socketId);

        Api.get(url, function (res) {
            console.log('ADD USER', res);
            if (res.code == 0) {
                me.dialogs[id].users[socketId] = profileId;
                me.sockets[socketId] = id;
                me.profiles[socketId] = res.data.profile;
                console.log('PROFILE', res.data.profile);
                console.log('DIALOG IS', me.dialogs, me.sockets);
            }
        });

        delete url;
    },
    removeUser: function (socketId) {
        console.log('DIALOG REMOVE USER', socketId);
        if (!this.sockets[socketId]) {
            return false;
        }

        if (!this.hasDialog(this.sockets[socketId])) {
            return false;
        }

        delete this.dialogs[this.sockets[socketId]].users[socketId];
        delete this.sockets[socketId];
        return true;
    },

    // Is called on connection of a client
    // Used for remembering who is all connected to node js
    addGlobalProfile: function (data) { // @m working now, globalProfiles[profile_id]
        var me = this;
        me.globalProfiles[data.globalProfileId] = data;
        console.log('GlobalProfileData: ', me.globalProfiles[data.globalProfileId]);
        return true;
    },
    // Is called on retrieving old chat messages
    // Used for remembering by retrieving old chat messages who are members of this dialog
    addDialogMembers: function (data) { // @m working now, dialogMembers[dialog_id]
        var me = this;
        me.dialogMembers[data.dialog_id] = data.members;
        console.log('DialogMembers: ', data);
        return true;
    },

    sendMessage: function (message, socketId, callback) {
        console.log('DIALOG SEND MESSAGE', socketId, message);
        var me = this;
        var dialog_id = me.sockets[socketId];

        if (!me.hasDialog(dialog_id)) {
            return false;
        }

        var profile_id = me.dialogs[dialog_id].users[socketId]; // profile_id
        var userProfile = me.profiles[socketId];

        // Date
        var d = new Date();
        var month = [];
        month[0] = "января";
        month[1] = "февраля";
        month[2] = "марта";
        month[3] = "апреля";
        month[4] = "мая";
        month[5] = "июня";
        month[6] = "июля";
        month[7] = "августа";
        month[8] = "сентября";
        month[9] = "октября";
        month[10] = "ноября";
        month[11] = "декабря";
        var monthname = month[d.getMonth()];

        var dataToSend = {
            data: {
                message: {
                    id: 1, // hardcoded 1, dont know why we need this... it came from old programmer
                    avatar: userProfile.avatar,
                    name: userProfile.name,
                    nameUrl: userProfile.nameUrl,
                    content: message,
                    date: d.getDate() + ' ' + monthname + ' ' + d.getFullYear() + ', в ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds(),
                    type: 'message'
                }
            },
            code: 0
        };

        // Send to users and to myself, additionally pass to callback dialog and profile
        // To inform users that they should check if they have unread messages
        callback(dataToSend, me.dialogs[dialog_id], dialog_id, profile_id);

        Api.post('/api/chat/send', {
            dialog: dialog_id,
            profile: profile_id,
            message: message
        }, function (res) {
            console.log('MESSAGE WRITTEN TO DB', res);
            //callback(res, me.dialogs[dialog]);
        });
    },
    run: function () {

    }
};

Dialog.run();

// Api.get('/api/chat/messages/6/19');

io.on('connection', function(socket, data){
    console.log('Client connected, socket.id:', socket.id);
    console.log('Socket data from leftpanel: ', socket.handshake.query);
    socket.handshake.query.socket = socket.id;
    Dialog.addGlobalProfile(socket.handshake.query);
    socket.emit('need_token');

    socket.on('token', function (token) {
        if (token) {
            TokenAuth.setToken(socket.id, token);
            TokenAuth.getToken(socket.id);
            socket.emit('token_success');
        } else {
            socket.emit('token_fails');
        }
    });

    socket.on('disconnect', function() {
        console.log('USER DISCONNECTED');
        Dialog.removeUser(socket.id);
        TokenAuth.removeToken(socket.id);
        TokenAuth.getToken(socket.id);
    });

    socket.on('init_dialog', function (id, profileId) {
        console.log('TRY INIT DIALOG', id);
        if (!TokenAuth.hasToken(socket.id)) {
            socket.emit('need_token');
            return false;
        }

        var inited = Dialog.initDialog(id, socket.id, profileId);
        console.log('DIALOG INITED', inited);
        socket.emit('dialog_inited');
        delete inited;
    });

    socket.on('old_messages', function (data) {
        if (!TokenAuth.hasToken(socket.id)) {
            return false;
        }

        var url = '/api/chat/messages/' + data.dialog + '/' + data.from;
        url += '?token=' + TokenAuth.getToken(socket.id);

        console.log('OLD MESSAGES: ', url);

        Api.get(url, function (data) {
            if (data.code === 0) {
                // Send old messages to clients
                socket.emit('messages', data.data.messages);

                // Now remember who are members of this dialog
                Dialog.addDialogMembers(data.data.members);
            }
        });

        delete url;
    });

    socket.on('send_message', function(data)
    {
        Dialog.sendMessage(data.message, socket.id, function(res, dialog, dialog_id, profile_id)
        {
            if(res.code === 0)
            {
                // Count how many users are in dialog right now
                var countusers = 0;
                // Send message to users
                for(var socketId in dialog.users)
                {
                    // Message
                    io.to(socketId).emit('message', res.data.message);
                    countusers++;
                }

                // Inform other user only if they arent in dialog right now
                if(countusers == 2)
                    return true;

                // Get dialog members
                var members = Dialog.dialogMembers[dialog_id];

                // Go through all members and determine if one of them is online
                // If yes, tell him to check his unread messages
                members.forEach(function(dialogMembersProfileId, index){
                    // Check if there is a dialog member globally connected to website
                    // And send message only to him
                    if(Dialog.globalProfiles[dialogMembersProfileId] != undefined && profile_id != dialogMembersProfileId)
                    {
                        console.log('I would send to.............. ', Dialog.globalProfiles[dialogMembersProfileId]);
                        io.to(Dialog.globalProfiles[dialogMembersProfileId].socket).emit('check_for_new_chat_messages');
                    }
                });
            }
        });
    });

    socket.on('deal_price', function (price) {
        console.log('DEAL PRICE', price);
        console.log('DIALOG', Dialog.sockets);
        var dialogId = Dialog.sockets[socket.id];
        var users = Dialog.dialogs[dialogId].users;
        console.log('DIALOG', dialogId);
        console.log('USERS', users);

        Dialog.dialogs[dialogId].price = {};

        for (var socketId in users) {
            if (socketId != socket.id) {
                io.to(socketId).emit('deal_price_sync', price);
            }
        }

        delete dialogId;
        delete users;
    });

    socket.on('deal_price_confirm', function (price) {
        var dialogId = Dialog.sockets[socket.id];
        var users = Dialog.dialogs[dialogId].users;
        var userId = users[socket.id];
        var closeDeal = function () {
            console.log('DEAL YEAAAAAAAAAAAAAAAAH');
            for (var socketId in users) {
                io.to(socketId).emit('deal_close', price);
            }

            Api.post('/api/deal/create/' + dialogId + '/' + userId, {
                token: TokenAuth.getToken(socket.id),
                price: price
            }, function (res) {
                if (res.code == 0) {
                    for (var socketId in users) {
                        io.to(socketId).emit('message', res.data.message);
                    }
                }
            });
        };

        console.log('CONFIRM', dialogId, users, userId);

        Dialog.dialogs[dialogId].price[userId] = parseFloat(price);

        var prePrice = null;
        for (var priceUserId in Dialog.dialogs[dialogId].price) {
            if (prePrice === null) {
                prePrice = Dialog.dialogs[dialogId].price[priceUserId];
            } else {
                if (prePrice === Dialog.dialogs[dialogId].price[priceUserId]) {
                    closeDeal();
                }
            }
        }

        delete dialogId;
        delete users;
    });

    // io.emit('system', 'SERVER io ' + socket.id);
    // socket.emit('system', 'SERVER socket ' + socket.id);
});
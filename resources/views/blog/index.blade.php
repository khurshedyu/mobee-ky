@extends('layouts.page')

@section('title', 'Блог')

@section('content')
    <div class="content-element-box">
        <h2>Блог</h2>
    </div>
    <div class="content-element-box">
        @foreach ($news as $newsItem)
            <div class="content-element-box-item">
                <h2><a href="{{ route('blog::view', ['id' => $newsItem->id]) }}">{{ $newsItem->title }}</a></h2>
                <br />
                <div>{!! Html::t($newsItem->teaser, true) !!}</div>
                <br />
                <small class="blend-text">{{ Html::formatDateTime($newsItem->published_at) }}</small>
            </div>
        @endforeach
    </div>
@endsection
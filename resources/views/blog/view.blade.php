@extends('layouts.page')

@section('title', $item->title . ' – Блог')

@section('content')
    <div class="content-element-box">
        <h2>{{ $item->title }}</h2>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            <div>{!! Html::t($item->content, true) !!}</div>
            <br />
            <small class="blend-text">{{ Html::formatDateTime($item->published_at) }}</small>
        </div>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h3>Другие новости</h3>
        </div>
        @foreach ($news as $newsItem)
            <div class="content-element-box-item">
                <a href="{{ route('blog::view', ['id' => $newsItem->id]) }}">{{ $newsItem->title }}</a>
            </div>
        @endforeach
    </div>
@endsection
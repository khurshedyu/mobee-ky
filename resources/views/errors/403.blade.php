@extends('layouts.page')

@section('title', $exception->getStatusCode() . ' – Ошибка доступа')

@section('content')
    <div class="content-element-box">
        <h2>{{ $exception->getStatusCode() }} – Ошибка доступа</h2>
    </div>
    <div class="content-element-box">
        {{ $exception->getMessage() }}
    </div>
@endsection
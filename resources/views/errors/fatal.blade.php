@extends('layouts.page')

@section('title', 'Ошибка')

@section('content')
    <div class="content-element-box">
        <div class="center">
            <img src="{{ url('static/images/mobee-remont.png') }}" style="max-height:200px; max-width:100%" />
            <br />
            <br />
            <h2>Ой! Что-то пошло не так...</h2>
            <p>Толковый моби-бот уже выехал на ремонт.</p>
        </div>
    </div>
@endsection
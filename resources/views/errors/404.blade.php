@extends('layouts.page')

@section('title', $exception->getStatusCode() . ' – Ошибка')

@section('content')
    <div class="center">
        <div class="content-element-box">

            <img src="{{ url('static/images/mobee-remont.png') }}" style="max-height:200px; max-width:100%" />
            <br />
            <br />
            <h2>{{ $exception->getStatusCode() }} – Ошибка</h2>
        </div>
        <div class="content-element-box">
            {{ $exception->getMessage() }}
        </div>
    </div>
@endsection
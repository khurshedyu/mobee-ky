<div class="profile-userbar">
    <div class="profile-userbar-avatar">
        <img src="{{ $profile->avatarUrl() }}" />
    </div>
    <div class="profile-userbar-info">
        <div class="profile-userbar-info-name">
            <a href="{{ route('profile::view', ['name' => $profile->linkName()]) }}">{{ $profile->displayName() }}</a>
        </div>
        <div class="profile-userbar-info-description blend-text">
            <div class="pull-right">
                {{-- Свой {{ $profile->experience() }} – --}}
                Качество профиля {{ $profile->quality() }}%,
                Рейтинг профиля {{ $profile->countRating() }},
                Регистрировался {{ $profile->created_at->format('d/m/y') }}
            </div>
            {{ $profile->profileTitle() }}
            @if ($profile->isChecked())
                &nbsp;
                <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
            @endif
            @if ($profile->isSuper())
                &nbsp;
                <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
            @endif
        </div>
    </div>
</div>
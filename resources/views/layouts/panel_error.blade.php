@if (count($errors))
    <div class="alert alert-danger" role="alert">
        @if (isset($panelError))
            {{ $panelError }}
        @else
            Ошибка сохранения данных
        @endif
    </div>
    <?php /*
    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title">Ошибка сохранения данных</h3>
        </div>
        <div class="panel-body">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
    */ ?>
@endif
<table class="table table-striped">
    <thead>
        @foreach ($columns as $n => $column)
            @if (isset($column['type']) AND $column['type'] == 'checkbox')
                <th class="{{ $tableId }}_checkbox" data-class="{{ $tableId }}_{{ $n }}"><input type="checkbox" /></th>
            @else
                <th>{{ $column['title'] }}</th>
            @endif
        @endforeach
    </thead>
    <tbody>
        @foreach ($items as $item)
            <tr>
                @foreach ($columns as $n => $column)
                    @if (isset($column['type']) AND $column['type'] == 'actions')
                        <td class="text-right">
                            <a class="edit btn btn-sm btn-default" href="{{ route($column['routes'] . '::edit', ['id' => $item->id]) }}"><i class="icon-note"></i></a>
                            <a class="delete btn btn-sm btn-danger" href="{{ route($column['routes'] . '::delete', ['id' => $item->id]) }}"><i class="icons-office-52"></i></a>
                        </td>
                    @else
                        <td align="{{ isset($column['align']) ? $column['align'] : null }}" class="{{ $tableId }}_{{ $n }}">
                            @if (!isset($column['visible']) OR $column['visible']($item[$column['name']], $item))
                                @if (isset($column['render']))
                                    {!! $column['render']($item[$column['name']], $item) !!}
                                @else
                                    {{ $item[$column['name']] }}
                                @endif
                            @endif
                        </td>
                    @endif
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>

{{--
<script type="text/javascript">
$(document).ready(function () {
    $('.{{ $tableId }}_checkbox').click(function () {
        var me = $(this);
        $('.' + me.data('class') + ' input').prop('checked', me.find('input').prop('checked'));
    });
});
</script>
--}}
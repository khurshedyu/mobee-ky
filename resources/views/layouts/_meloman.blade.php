<div class="row">
    <div class="col-xs-12 col-sm-12">
        <h6>Сайты, подключенные к Me ID {!! Html::plus('me-id') !!}</h6>
        <table class="table">
            <tbody>
                <tr>
                    <td width="33%">
                        <div class="meloman-item">
                            <a href="https://mobee.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/mobee-logo.png') }}" />
                                </div>
                                Моби
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="meloman-item">
                            <a href="http://www.marwin.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/marwin-logo.png') }}" />
                                </div>
                                Марвин
                            </a>
                        </div>
                    </td>
                    <td width="33%">
                        <div class="meloman-item">
                            <a href="http://www.meloman.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/meloman-logo.png') }}" />
                                </div>
                                Меломан
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="meloman-item">
                            <a href="http://www.komfort.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/komfort-logo.png') }}" />
                                </div>
                                Комфорт
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="meloman-item">
                            <a href="http://privezi.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/privezi-logo.png') }}" />
                                </div>
                                Привези
                            </a>
                        </div>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12 col-sm-12">
        <a href="http://masa.meloman.kz/p/" target="_blank" class="btn btn-mobee col-xs-12 col-sm-12">Открыть мой Me ID</a>
        {{--
        <br />
        <br />
        <hr />
        --}}
    </div>
    {{--
    <div class="col-xs-12 col-sm-12">
        <h6>Другие интересные проекты</h6>
        <table class="table">
            <tbody>
                <tr>
                    <td width="33%">
                        <div class="meloman-item">
                            <a href="http://news.meloman.kz/ru/afisha/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/mobee-logo.png') }}" />
                                </div>
                                Афиша
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="meloman-item">
                            <a href="http://news.meloman.kz/ru/card/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/mobee-logo.png') }}" />
                                </div>
                                Бонусы
                            </a>
                        </div>
                    </td>
                    <td width="33%">
                        <div class="meloman-item">
                            <a href="http://www.arsenal.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/arsenal-logo.png') }}" />
                                </div>
                                Арсенал
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="meloman-item">
                            <a href="http://legoeducation.marwin.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/lego-edu-logo.png') }}" />
                                </div>
                                Лего Edu
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="meloman-item">
                            <a href="http://questeria.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/questeria-logo.png') }}" />
                                </div>
                                Квестерия
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="meloman-item">
                            <a href="http://sunrise.meloman.kz/" target="_blank">
                                <div class="meloman-item-img">
                                    <img src="{{ url('static/images/meloman/sunrise-logo.png') }}" />
                                </div>
                                Санрайз
                            </a>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    --}}
</div>
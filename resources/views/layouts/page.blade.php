@extends('layouts.layout')

@section('page')
    <div id="modals" class="mobeemodals">
        <div class="mobeemodal-loader">
            <div class="mobeemodal-loader-text">
                Загрузка
            </div>
        </div>
        <ul></ul>
        <div id="mobeemodal-template" class="mobeemodal-template">
            <div class="mobeemodal-rails">
                <div class="mobeemodal-window">
                    <div class="mobeemodal-header">
                        <div class="mobeemodal-header-title">[[title]]</div>
                        <div class="mobeemodal-header-close"><span class="mobeemodal-header-close-btn">Закрыть</span></div>
                    </div>
                    <div class="mobeemodal-content">
                        [[content]]
                    </div>
                </div>
            </div>
            <div class="mobeemodal-bg"></div>
        </div>
    </div>
    <div id="wrap" class="wrap">
        <aside id="wrap-left" class="sidebar sidebar-left wrap-item">
            @if (Auth::check())
                @include('layouts.dashboard_left')
            @else
                @include('layouts.dashboard_left_noauth')
                {{-- Design here was broken --}}
                {{--@include('layouts.dashboard_demo_button')--}}
            @endif
        </aside>
        <main id="wrap-main" class="main wrap-item wrap-active">
            <div class="inner">
                @include('layouts.header')
                <content class="content">
                    @yield('content')
                </content>
                @yield('after_content')
                @include('layouts.footer')
            </div>
        </main>
        <aside id="wrap-right" class="sidebar sidebar-right wrap-item">
            @if (Auth::check())
                @include('layouts.dashboard_right')
            @else
                @include('layouts.dashboard_right_noauth')
                @include('layouts.dashboard_demo_button')
            @endif
        </aside>
    </div>
    <div id="controls" class="controls">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <a id="controls-left" data-wrap="left" href="#" class="">{{ Auth::check() ? 'Режимы' : 'Регистрация' }}</a>
                </td>
                <td>
                    <a id="controls-main" data-wrap="main" href="#" class=" active">Платформа</a>
                </td>
                <td>
                    <a id="controls-right" data-wrap="right" href="#" class="">{{ Auth::check() ? 'Аккаунт' : 'Вход' }}</a>
                </td>
            </tr>
        </table>
    </div>
@endsection
<div class="chatitem">
    <div class="chatitem-avatar">
        @if (isset($item))
            <img src="{{ $item->profile->avatarUrl() }}" />
        @endif
    </div>
    <div class="chatitem-content">
        <div class="chatitem-content-name">
            @if (isset($item))
                <a href="{{ route('profile::view', ['name' => $item->profile->linkName()]) }}">{{ $item->profile->displayName() }}</a>
            @endif
        </div>
        <div class="chatitem-content-message">
            {{ isset($item) ? $item->content : null }}
        </div>
        <div class="chatitem-content-date">
            <small class="blend-text">{{ isset($item) ? Html::formatDateTime($item->created_at) : null }}</small>
        </div>
    </div>
    <div class="chatitem-clear"></div>
</div>
{{-- Audio --}}
<audio id="sound1" src="/static/sound/M.mp3"></audio>

<div class="dashboard">
    <div class="dashboard-header">
        <div class="dropdown">
            <a href="#" class="dashboard-header-mode active font-condensed" id="modeMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <div class="dashboard-header-mode-inner">
                    <div class="dashboard-left-full">
                        Текущий режим — {{ App\Components\SessionWorker::instance()->currentProfile()->profileTitle() }}
                        <i class="mobee-down-open-1 pull-right"></i>
                    </div>
                    <div class="dashboard-left-small center">
                        <i class="mobee-down-open-1"></i>
                    </div>
                </div>
            </a>
            <ul class="dropdown-menu" aria-labelledby="modeMenuButton">
                @foreach (App\Components\SessionWorker::instance()->availableProfileList() as $item)
                    <li><a href="{{ route('profile::change', ['id' => $item['id']]) }}">{{ $item['title'] }} — {{ $item['name'] }}</a></li>
                @endforeach
                <li role="separator" class="divider"></li>
                <li><a href="{{ route('profile::create') }}">Добавить компанию</a></li>
            </ul>
        </div>
    </div>
    <div class="dashboard-content nano">
        <div class="nano-content">



            <div class="dashboard-content-element">
                <div class="dashboard-left-full">
                    <a href="{{ route('orders::create') }}" class="btn btn-default btn-lg dashboard-content-orderbtn font-condensed">
                        <i class="mobee-plus-3"></i>
                        &nbsp;
                        Создать
                    </a>
                </div>
                <div class="dashboard-left-small">
                    <a href="{{ route('orders::create') }}" class="dashboard-left-btn dashboard-left-small-orderbtn">
                        <i class="mobee-plus-1"></i>
                    </a>
                    <a href="{{ route('profile::view', ['name' => App\Components\SessionWorker::instance()->currentProfile()->linkName()]) }}" class="dashboard-avatar">
                        <img src="{{ App\Components\SessionWorker::instance()->currentProfile()->avatarUrl() }}" />
                    </a>
                </div>
            </div>



            <div class="dashboard-left-full">
                <div class="dashboard-content-element">
                    <div class="dashboard-content-box">
                        <a href="{{ route('profile::view', ['name' => App\Components\SessionWorker::instance()->currentProfile()->linkName()]) }}" class="dashboard-avatar">
                            <img src="{{ App\Components\SessionWorker::instance()->currentProfile()->avatarUrl() }}" />
                        </a>
                        <div class="row">
                            <div class="col-xs-9 col-sm-9">
                                <div class="height-btn">
                                    <i class="mobee-user-6"></i> &nbsp; {{ App\Components\SessionWorker::instance()->currentProfile()->displayName() }}
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <a href="{{ route('profile::index') }}" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-cog-6"></i></a>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="dashboard-content-element">
                    <div class="dashboard-content-box">
                        <div class="dashboard-content-box-header">
                            Качество профиля
                            <span class="plus" data-key="quality"><i class="mobee-plus-1"></i></span>
                        </div>
                        <div class="dashboard-content-box-content">
                            <div class="row">
                                <div class="col-xs-9 col-sm-9">
                                    <div class="progress progress-dashboard">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ App\Components\SessionWorker::instance()->currentProfile()->quality() }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ App\Components\SessionWorker::instance()->currentProfile()->quality() }}%;">
                                            {{ App\Components\SessionWorker::instance()->currentProfile()->quality() }}%
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-3 col-sm-3">
                                    <a href="{{ route('page::show', ['name' => 'profile-quality']) }}" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-cog-6"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="dashboard-content-element">
                    <div class="dashboard-content-box">
                        <div class="dashboard-content-box-header">
                            Личное меню
                            <span class="plus" data-key="menu-dashboard"><i class="mobee-plus-1"></i></span>
                        </div>
                        <div class="dashboard-content-box-content">
                            <div class="dashboard-usermenu">
                                <ul>
                                    <?php
                                        $dashboardUsermenu = [
                                            [
                                                'icon' => 'mobee-pencil-5',
                                                'title' => 'Заказы',
                                                'link' => route('profile::orders'),
                                                'count' => App\Components\SessionWorker::instance()->currentProfile()->countOrders(),
                                            ],
                                            [
                                                'icon' => 'mobee-suitcase',
                                                'title' => 'Предложения',
                                                'link' => route('profile::offers'),
                                                'count' => App\Components\SessionWorker::instance()->currentProfile()->countOffers(),
                                            ],
                                            [
                                                'class' => 'deals',
                                                'icon' => 'mobee-shield',
                                                'title' => 'Сделки',
                                                'link' => route('profile::deals'),
                                                'notification' => App\Components\SessionWorker::instance()->currentProfile()->countNewMessages(),
                                                'count' =>
                                                    (App\Components\SessionWorker::instance()->currentProfile()->countDialogs() - App\Components\SessionWorker::instance()->currentProfile()->countDeals())
                                                    . ' / ' .
                                                    App\Components\SessionWorker::instance()->currentProfile()->countDeals(),
                                            ],
                                            [
                                                'icon' => 'mobee-link-4',
                                                'title' => 'Связи',
                                                'link' => route('profile::connections'),
                                                'count' => App\Components\SessionWorker::instance()->currentProfile()->countConnections(),
                                            ],
                                            [
                                                'icon' => 'mobee-comment-2',
                                                'title' => 'Отзывы',
                                                'link' => route('profile::reviews'),
                                                'count' => App\Components\SessionWorker::instance()->currentProfile()->countReviews(),
                                            ],
                                            // [
                                            //     'icon' => 'mobee-chart-line',
                                            //     'title' => 'Посещения',
                                            //     'link' => route('profile::visits'),
                                            //     'count' => App\Components\SessionWorker::instance()->currentProfile()->countVisits(),
                                            // ],
                                            // [
                                            //     'icon' => 'mobee-chat-1',
                                            //     'title' => 'Поддержка',
                                            //     'link' => route('profile::support'),
                                            //     'count' => App\Components\SessionWorker::instance()->currentProfile()->countSupport(),
                                            // ],
                                        ];
                                    ?>
                                    @foreach ($dashboardUsermenu as $item)
                                        <li @if(isset($item['class']))class="{{ $item['class'] }}"@endif>
                                            <a href="{{ $item['link'] }}">
                                                <span>{{ $item['count'] }}</span>
                                                @if (isset($item['notification']) AND $item['notification'])
                                                    <span id="messagescount" class="dashboard-usermenu-notification">+{{ $item['notification'] }}</span>
                                                @else
                                                    <span id="messagescount" class="dashboard-usermenu-notification" style="display: none">0</span>
                                                @endif
                                                <i class="{{ $item['icon'] }}"></i> &nbsp; {{ $item['title'] }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="dashboard-content-element">
                    <div class="dashboard-content-box">
                        <div class="dashboard-content-box-header">
                            Текущий рейтинг
                            <span class="plus" data-key="rating"><i class="mobee-plus-1"></i></span>
                        </div>
                        <div class="dashboard-content-box-content">
                            <div class="row">
                                <div class="col-xs-9 col-sm-9">
                                    <div class="height-btn">
                                        <i class="mobee-chart"></i> &nbsp;
                                        {{ \App\Components\SessionWorker::instance()->currentProfile()->countRating() }}
                                        баллов
                                    </div>
                                </div>
                                <div class="col-xs-3 col-sm-3">
                                    <a href="{{ route('page::show', ['name' => 'profile-rating']) }}" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-cog-6"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="dashboard-left-small">
                <div class="dashboard-content-element">
                    @foreach ($dashboardUsermenu as $item)
                        <a href="{{ $item['link'] }}" class="dashboard-left-btn" title="{{ $item['title'] }}">
                            <!-- <span>{{ $item['count'] }}</span> -->
                            @if (isset($item['notification']) AND $item['notification'])
                                <span class="dashboard-left-small-notification">{{ $item['notification'] }}</span>
                            @endif
                            <i class="{{ $item['icon'] }}"></i>
                        </a>
                    @endforeach
                </div>
            </div>



        </div>
    </div>
</div>

@section('script')
<script type="text/javascript">
    $(document).ready(function()
    {
        socket.on('check_for_new_chat_messages', function(data)
        {
            console.log('I should check for new messages');

            // Play Sound
            $('#sound1')[0].play();

            var messagescountobj = $('.deals').find('#messagescount');
            var messagescount = parseInt(messagescountobj.html());

            if(messagescountobj.html() == 0)
            {
                messagescountobj.show();
                messagescount++;
                messagescountobj.html('+'+messagescount);
            }
            else
            {
                var messagescount = parseInt(messagescountobj.html().replace('+', ''));
                messagescount++;
                messagescountobj.html('+'+messagescount);
            }
        });
    });
</script>
@append
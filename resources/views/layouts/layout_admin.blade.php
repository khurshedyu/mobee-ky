<!DOCTYPE html>
<html class="" lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="admin-themes-lab">
  <meta name="author" content="themes-lab">
  <link rel="shortcut icon" href="assets/images/favicon.png" type="image/png">
  <title>Make Admin Template - Themes Lab</title>
  <!-- <link href="http://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet" type="text/css"> -->
  <link href="{{ url('assets/css/style.css') }}" rel="stylesheet"> <!-- MANDATORY -->
  <link href="{{ url('assets/css/theme.css') }}" rel="stylesheet"> <!-- MANDATORY -->
  <link href="{{ url('assets/css/ui.css') }}" rel="stylesheet"> <!-- MANDATORY -->
  <link href="{{ url('assets/plugins/datatables/dataTables.min.css') }}" rel="stylesheet">
  <!-- <link href="{{ url('assets/page-builder/plugins/slider-pips/jquery-ui-slider-pips.css') }}" rel="stylesheet"> -->
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <![endif]-->
</head>
<!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
<!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
<!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
<!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
<!-- LAYOUT: Apply "submenu-hover" class to body element to show sidebar submenu on mouse hover -->
<!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
<!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
<!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
<!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->
<!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
<!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->
<!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
<!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
<!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
<!-- THEME COLOR: Apply "color-default" for green color: #18A689 -->
<!-- THEME COLOR: Apply "color-default" for orange color: #B66D39 -->
<!-- THEME COLOR: Apply "color-default" for purple color: #6E62B5 -->
<!-- THEME COLOR: Apply "color-default" for blue color: #4A89DC -->
<!-- BEGIN BODY -->
<body class="fixed-topbar theme-sdtl fixed-sidebar color-default bg-light-blue">

@yield('page')

<!-- Preloader -->
<div class="loader-overlay">
  <div class="spinner">
    <div class="bounce1"></div>
    <div class="bounce2"></div>
    <div class="bounce3"></div>
  </div>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>

{{--
<script src="{{ url('assets/plugins/gsap/main-gsap.min.js') }}"></script> <!-- HTML Animations -->
<script src="{{ url('assets/plugins/switchery/switchery.min.js') }}"></script> <!-- IOS Switch -->
<script src="{{ url('assets/plugins/charts-sparkline/sparkline.min.js') }}"></script> <!-- Charts Sparkline -->
<script src="{{ url('assets/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script> <!-- Animated Progress Bar -->
<script src="{{ url('assets/plugins/select2/select2.min.js') }}"></script> <!-- Select Inputs -->
<script src="{{ url('assets/plugins/retina/retina.min.js') }}"></script>  <!-- Retina Display -->
<script src="{{ url('assets/plugins/touchspin/jquery.bootstrap-touchspin.min.js') }}"></script> <!-- A mobile and touch friendly input spinner component for Bootstrap -->
<script src="{{ url('assets/plugins/icheck/icheck.min.js') }}"></script> <!-- Icheck -->
<script src="{{ url('assets/plugins/simple-weather/jquery.simpleWeather.min.js') }}"></script>
<script src="{{ url('assets/js/widgets/widget_weather.js') }}"></script>

<script src="{{ url('assets/page-builder/plugins/slider-pips/jquery-ui-slider-pips.min.js') }}"></script>
<script src="{{ url('assets/page-builder/plugins/saveas/saveAs.js') }}"></script>
<script src="{{ url('assets/page-builder/js/builder_page.js') }}"></script>

<script src="{{ url('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script> <!-- Tables Filtering, Sorting & Editing -->
<script src="{{ url('assets/plugins/noty/jquery.noty.packaged.min.js') }}"></script>  <!-- Notifications -->

--}}

<script src="{{ url('assets/plugins/jquery/jquery-1.11.1.min.js') }}"></script>
<script src="{{ url('assets/plugins/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ url('assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js') }}"></script>
<script src="{{ url('assets/plugins/jquery-block-ui/jquery.blockUI.min.js') }}"></script> <!-- simulate synchronous behavior when using AJAX -->
<script src="{{ url('assets/plugins/bootbox/bootbox.min.js') }}"></script>
<script src="{{ url('assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script> <!-- Custom Scrollbar sidebar -->
<script src="{{ url('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js') }}"></script> <!-- Show Dropdown on Mouseover -->
<script src="{{ url('assets/plugins/jquery-cookies/jquery.cookies.js') }}"></script> <!-- Jquery Cookies, for theme -->
<script src="{{ url('assets/plugins/bootstrap/js/jasny-bootstrap.min.js') }}"></script> <!-- File Upload and Input Masks -->
<script src="{{ url('assets/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js') }}"></script> <!-- Select Inputs -->
<script src="{{ url('assets/plugins/bootstrap-loading/lada.min.js') }}"></script> <!-- Buttons Loading State -->
<script src="{{ url('assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js') }}"></script> <!-- Time Picker -->
<script src="{{ url('assets/plugins/multidatepicker/multidatespicker.min.js') }}"></script> <!-- Multi dates Picker -->
<script src="{{ url('assets/plugins/colorpicker/spectrum.min.js') }}"></script> <!-- Color Picker -->
<script src="{{ url('assets/plugins/autosize/autosize.min.js') }}"></script> <!-- Textarea autoresize -->
<script src="{{ url('assets/plugins/bootstrap-editable/js/bootstrap-editable.min.js') }}"></script> <!-- Inline Edition X-editable -->
<script src="{{ url('assets/plugins/bootstrap-context-menu/bootstrap-contextmenu.min.js') }}"></script> <!-- File Upload and Input Masks -->
<script src="{{ url('assets/plugins/prettify/prettify.min.js') }}"></script> <!-- Show html code -->
<script src="{{ url('assets/plugins/slick/slick.min.js') }}"></script> <!-- Slider -->
<script src="{{ url('assets/plugins/countup/countUp.min.js') }}"></script> <!-- Animated Counter Number -->
<script src="{{ url('assets/plugins/backstretch/backstretch.min.js') }}"></script> <!-- Background Image -->
<script src="{{ url('assets/plugins/charts-chartjs/Chart.min.js') }}"></script>  <!-- ChartJS Chart -->
<script src="{{ url('assets/plugins/bootstrap-slider/bootstrap-slider.js') }}"></script> <!-- Bootstrap Input Slider -->
<script src="{{ url('assets/plugins/visible/jquery.visible.min.js') }}"></script> <!-- Visible in Viewport -->
<script src="{{ url('assets/js/layout.js') }}"></script>
<script src="{{ url('assets/js/builder.js') }}"></script>
<script src="{{ url('assets/js/sidebar_hover.js') }}"></script>
<script src="{{ url('assets/js/application.js') }}"></script> <!-- Main Application Script -->
<script src="{{ url('assets/js/plugins.js') }}"></script> <!-- Main Plugin Initialization Script -->
<script src="{{ url('assets/js/widgets/notes.js') }}"></script>
<script src="{{ url('assets/js/quickview.js') }}"></script> <!-- Quickview Script -->
<script src="{{ url('assets/js/pages/search.js') }}"></script> <!-- Search Script -->
<!-- BEGIN PAGE SCRIPTS -->
<script src="{{ url('assets/plugins/summernote/summernote.js') }}"></script>
<script src="{{ url('assets/plugins/skycons/skycons.js') }}"></script>
<script src="{{ url('assets/js/widgets/todo_list.js') }}"></script>
<!-- END PAGE SCRIPTS -->
</body>
</html>
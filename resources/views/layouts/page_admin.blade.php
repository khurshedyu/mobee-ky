@extends('layouts.layout_admin')

@section('page')



<section>
    @include('admin._sidebar')
    <div class="main-content">
        @include('admin._topbar')

        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content">
            @yield('content')
            {{--
            <div class="custom-page m-t-60 text-center">
                <h2 class="m-b-20">Do you want to create your page with Page <strong>Builder</strong>?</h2>
                <a href="#" id="no-page-builder" class="btn btn-dark btn-lg btn-square">No, thanks</a>
                <a href="#" id="import-page-builder" class="btn btn-primary btn-lg btn-square">Import Page Builder</a>
            </div>
            --}}

            {{--
            <div class="footer">
                <div class="copyright">
                    <p class="pull-left sm-pull-reset"> <span>Copyright <span class="copyright">©</span> 2014 </span> <span>THEMES LAB</span>. <span>All rights reserved. </span> </p>
                    <p class="pull-right sm-pull-reset"> <span><a href="#" class="m-r-10">Support</a> | <a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span> </p>
                </div>
            </div>
            --}}
        </div>
    </div>
</section>



@include('admin._quickbar')
{{--
@include('admin._morphsearch')
--}}

@endsection
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <!-- Bootstrap -->
        <link href="/static/bootstrap/css/bootstrap.min.css?{{ config('app.build') }}" rel="stylesheet">
        <link href="/static/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css?{{ config('app.build') }}" rel="stylesheet">
        {{--<link href="/static/screen.css?{{ config('app.build') }}" rel="stylesheet">--}}
        <link href="/static/screen.css" rel="stylesheet"> {{-- @todo --}}
        <link href="/static/fontello/css/fontello.css?{{ config('app.build') }}" rel="stylesheet">
        <link href="/static/bootstrap-formhelpers/css/bootstrap-formhelpers.min.css?{{ config('app.build') }}" rel="stylesheet">
        <!-- <link href="/static/clearsans/clearsans.css?{{ config('app.build') }}" rel="stylesheet"> -->

        <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:400,300,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'> -->
        <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'> -->
        <!-- <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'> -->
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700|Roboto+Condensed:400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <title>@yield('title') – Mobee.kz</title>
        <script src="/static/jquery.min.js?{{ config('app.build') }}"></script>
    </head>
    <body class="@yield('body_class')">
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TWR6WR"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TWR6WR');</script>
        <!-- End Google Tag Manager -->

        @yield('page')

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/static/jquery.nanoscroller.min.js?{{ config('app.build') }}"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/static/bootstrap/js/bootstrap.min.js?{{ config('app.build') }}"></script>
        <script src="/static/bootstrap-switch/js/bootstrap-switch.min.js?{{ config('app.build') }}"></script>
        <script src="/static/bootstrap-formhelpers/js/bootstrap-formhelpers.min.js?{{ config('app.build') }}"></script>
        <script src="/static//moment.js?{{ config('app.build') }}"></script>
        <script src="/static//Chart.js?{{ config('app.build') }}"></script>
        <script src="/static/all.js?{{ config('app.build') }}"></script>

        <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>

        <script src="/static/angular/es6-shim.js?{{ config('app.build') }}"></script>
        <script src="/static/angular/angular2-polyfills.js?{{ config('app.build') }}"></script>
        <script src="/static/angular/system.src.js?{{ config('app.build') }}"></script>
        <script src="/static/angular/Rx.js?{{ config('app.build') }}"></script>
        <script src="/static/angular/angular2.js?{{ config('app.build') }}"></script>

        @if(Auth::check())
            <script type="text/javascript">
                // At this point we get some data from server to know who we are and in which dialogs we take part
                var globalUserToken = "{{ \App\UserToken::tokenByUserId(Auth::user()->id) }}";
                var globalUserId = "{{ Auth::user()->id }}";
                var globalProfileId = "{{ App\Components\SessionWorker::instance()->currentProfile()->id }}";

                var socket = io('{{ \App\Helpers\GeneralHelper::getNodeJsConfig()['baseurl'] }}:3000', { query: "globalUserToken="+globalUserToken+"&globalUserId="+globalUserId+"&globalProfileId="+globalProfileId });

                System.config({
                    // defaultJSExtensions: true,
                    // baseURL: '/app',
                    map: {
                        app: '/app'
                    },
                    packages: {
                        app: {
                            format: 'register',
                            defaultExtension: 'js'
                        }
                    }
                });
            </script>
        @endif
        @yield('script')

    </body>
</html>
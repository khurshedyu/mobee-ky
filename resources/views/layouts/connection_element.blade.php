<div class="content-element-box-item">
    <div class="row">
        <div class="col-md-5">
            <div class="profile-userbar">
                <div class="profile-userbar-avatar">
                    <img src="{{ $connection->toProfile->avatarUrl() }}" />
                </div>
                <div class="profile-userbar-info">
                    <div class="profile-userbar-info-name">
                        <a href="{{ route('profile::view', ['name' => $connection->toProfile->linkName()]) }}">{{ $connection->toProfile->displayName() }}</a>
                    </div>
                    <div class="profile-userbar-info-description blend-text">
                        {{ $connection->toProfile->profileTitle() }}
                        @if ($connection->toProfile->isChecked())
                            &nbsp;
                            <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                        @endif
                        @if ($connection->toProfile->isSuper())
                            &nbsp;
                            <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="connection-list-link">
                <i class="mobee-anchor-1"></i>
            </div>
        </div>
        <div class="col-md-5">
            <div class="profile-userbar profile-userbar-right">
                <div class="profile-userbar-avatar">
                    <img src="{{ $connection->fromProfile->avatarUrl() }}" />
                </div>
                <div class="profile-userbar-info">
                    <div class="profile-userbar-info-name">
                        <a href="{{ route('profile::view', ['name' => $connection->fromProfile->linkName()]) }}">{{ $connection->fromProfile->displayName() }}</a>
                    </div>
                    <div class="profile-userbar-info-description blend-text">
                        {{ $connection->fromProfile->profileTitle() }}
                        @if ($connection->fromProfile->isChecked())
                            &nbsp;
                            <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                        @endif
                        @if ($connection->fromProfile->isSuper())
                            &nbsp;
                            <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--
    {!! view('layouts.profile_userbar', ['profile' => $connection->fromProfile]) !!}
    <br />
    {!! view('layouts.profile_userbar', ['profile' => $connection->toProfile]) !!}
    --}}
</div>
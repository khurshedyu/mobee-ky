<div class="dashboard dashboard-noauth-main">
    <div class="dashboard-header">
        <div class="dashboard-header-mode font-condensed purple">
            <div class="dashboard-header-mode-inner">
                <div class="dashboard-left-full">
                    Регистрация <span class="plus" data-key="register"><i class="mobee-plus-1"></i></span>
                </div>
                <div class="dashboard-left-small center">
                    Рег.
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-content nano">
        <div class="nano-content">



            <div class="dashboard-left-small">
                <div class="dashboard-content-element">
                        <a href="{{ route('faq::index') }}" class="dashboard-left-btn" title="">
                            <i class="glyphicon glyphicon-question-sign"></i>
                        </a>

                        <a href="{{ route('main::register') }}" class="dashboard-left-btn" title="">
                            <i class="mobee-user-plus"></i>
                        </a>
                </div>
            </div>



            <div class="dashboard-left-full">
                <div class="dashboard-content-fakebox">
                    <form method="POST" action="{{ route('main::register') }}">
                        <div class="dashboard-content-element">
                            <input type="text" name="email" value="" placeholder="Укажите e-mail" class="dashboard-form-textfield" />
                        </div>
                        <div class="dashboard-content-element">
                            <input type="password" name="password" value="" placeholder="Придумайте пароль" class="dashboard-form-textfield" />
                        </div>
                        <div class="dashboard-content-element">
                            {{ csrf_field() }}
                            <button class="btn btn-default btn-lg dashboard-content-orderbtn font-condensed"><i class="mobee-user-2"></i> &nbsp; Зарегистрироваться</button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>

<div class="dashboard dashboard-noauth-demo">
    <div class="dashboard-header">
        <div class="dropdown">
            <a href="#" class="dashboard-header-mode active font-condensed" id="modeMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <div class="dashboard-header-mode-inner">
                    Текущий режим – Компания
                    <i class="mobee-down-open-1 pull-right"></i>
                </div>
            </a>
            <ul class="dropdown-menu" aria-labelledby="modeMenuButton">
                <li><a href="#">Частное лицо – Моби-бот</a></li>
                <li><a href="#">Компания – Платформа Моби</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Добавить компанию</a></li>
            </ul>
        </div>
    </div>
    <div class="dashboard-content nano">
        <div class="nano-content">
            <div class="dashboard-content-element">
                <a href="#" class="btn btn-default btn-lg dashboard-content-orderbtn font-condensed"><i class="mobee-plus-3"></i> &nbsp; Создать</a>
            </div>
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <a href="#" class="dashboard-avatar">
                        <img src="{{ url('/static/images/avatar_default.png') }}" />
                    </a>
                    <div class="row">
                        <div class="col-xs-9 col-sm-9">
                            <div class="height-btn">
                                <i class="mobee-user-6"></i> &nbsp; Платформа Моби
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3">
                            <a href="#" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-cog-6"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        Качество профиля
                        <span class="plus" data-key="quality"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="row">
                            <div class="col-xs-9 col-sm-9">
                                <div class="progress progress-dashboard">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                        100%
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <a href="" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-cog-6"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        Личное меню
                        <span class="plus" data-key="menu-dashboard"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="dashboard-usermenu">
                            <ul>
                                <?php
                                    $dashboardUsermenu = [
                                        [
                                            'icon' => 'mobee-pencil-5',
                                            'title' => 'Заказы',
                                            'link' => '#',
                                            'count' => 26,
                                        ],
                                        [
                                            'icon' => 'mobee-suitcase',
                                            'title' => 'Предложения',
                                            'link' => '#',
                                            'count' => 8,
                                        ],
                                        [
                                            'icon' => 'mobee-shield',
                                            'title' => 'Сделки',
                                            'link' => '#',
                                            'count' => '20 / 14',
                                            'notification' => 3,
                                        ],
                                        [
                                            'icon' => 'mobee-link-4',
                                            'title' => 'Связи',
                                            'link' => '#',
                                            'count' => 12,
                                        ],
                                        [
                                            'icon' => 'mobee-comment-2',
                                            'title' => 'Отзывы',
                                            'link' => '#',
                                            'count' => 12,
                                        ],
                                        // [
                                        //     'icon' => 'mobee-chart-line',
                                        //     'title' => 'Посещения',
                                        //     'link' => '#',
                                        //     'count' => 165,
                                        // ],
                                        // [
                                        //     'icon' => 'mobee-chat-1',
                                        //     'title' => 'Поддержка',
                                        //     'link' => '#',
                                        //     'count' => 2,
                                        // ],
                                    ];
                                ?>
                                @foreach ($dashboardUsermenu as $item)
                                    <li>
                                        <a href="{{ $item['link'] }}">
                                            <span>{{ $item['count'] }}</span>
                                            @if (isset($item['notification']) AND $item['notification'])
                                                <span class="dashboard-usermenu-notification">+{{ $item['notification'] }}</span>
                                            @endif
                                            <i class="{{ $item['icon'] }}"></i> &nbsp; {{ $item['title'] }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        Текущий рейтинг
                        <span class="plus" data-key="rating"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="row">
                            <div class="col-xs-9 col-sm-9">
                                <div class="height-btn">
                                    <i class="mobee-chart"></i> &nbsp; 220 баллов
                                </div>
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <a href="" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-cog-6"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </div>
</div>
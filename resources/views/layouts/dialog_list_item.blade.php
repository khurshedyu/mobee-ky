<div class="profile-userbar">
    <div class="profile-userbar-avatar">
        <img src="{{ $dialog->avatarUrl() }}" />
    </div>
    <div class="profile-userbar-info">
        <div class="profile-userbar-info-name">
            <a href="{{ route('profile::deals::dialog', ['id' => $dialog->id]) }}">{{ $dialog->title() }}</a>
        </div>
        <div class="profile-userbar-info-description blend-text">
            <div class="pull-right">
                @if ($dialog->lastMessage())
                    {{ Html::formatDateTime($dialog->lastMessage()->created_at) }}
                @else
                    {{ Html::formatDateTime($dialog->created_at) }}
                @endif
            </div>
            @if ($dialogMember->count_messages)
                <span class="badge">{{ $dialogMember->count_messages }}</span>
            @endif
            @if ($dialog->lastMessage())
                {{ $dialog->lastMessage()->profile->displayName() }}: {{ $dialog->lastMessage()->content }}
            @else
                Нет сообщений
            @endif
        </div>
    </div>
</div>
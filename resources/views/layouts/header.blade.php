<header class="header">
    <div class="navigation">
        <div class="logo">
            <a href="{{ route('main::index') }}"></a>
        </div>
        <div class="links">
            <ul class="links-ul" id="links-ul">
                <!-- <li><a href="" class="plus">+</a></li> -->
                <li style="float:right;"><a href="{{ route('faq::index') }}" class="{{ \App\Components\ActiveMenu::instance()->active('faq_index') }}">Центр помощи</a></li>
                <li style="float:right;"><a href="{{ route('blog::index') }}" class="{{ \App\Components\ActiveMenu::instance()->active('blog_index') }}">Блог</a></li>

                <li><a href="{{ route('main::profiles') }}" class="{{ \App\Components\ActiveMenu::instance()->active('main_profiles') }}">Все профили</a></li>
                <li><a href="{{ route('main::connections') }}" class="{{ \App\Components\ActiveMenu::instance()->active('main_connections') }}">Связи</a></li>
                <li><a href="{{ route('main::reviews') }}" class="{{ \App\Components\ActiveMenu::instance()->active('main_reviews') }}">Отзывы</a></li>

                {{--
                @foreach (Html::headerLinks() as $link)
                    <li><a href="{{ route('page::show', ['name' => $link->name]) }}">{{ $link->title }}</a></li>
                @endforeach
                --}}
                <?php /*
                <li><a href="{{ route('page::show', ['name' => 'mobee']) }}">Чем уникален Mobee?</a></li>
                <li><a href="{{ route('page::show', ['name' => 'features']) }}">Улучшения</a></li>
                <li><a href="{{ route('page::show', ['name' => 'capabilities']) }}">Возможности</a></li>
                <li><a href="{{ route('page::show', ['name' => 'principles']) }}">Принципы</a></li>
                <li><a href="{{ route('page::show', ['name' => 'rules']) }}">Правила</a></li>
                <li><a href="{{ route('page::show', ['name' => 'advice']) }}">Советы</a></li>
                <li><a href="{{ route('page::show', ['name' => 'help']) }}">Помощь</a></li>
                */ ?>
            </ul>
            <div id="links-more" class="links-more">
                <a href="#" id="links-more-btn" class="links-more-btn">
                    <i class="mobee-menu-2"></i> Меню
                </a>
                <div id="links-more-list" class="links-more-list">
                    <div class="links-more-list-inner">
                        <ul>
                            <li><a href="{{ route('main::profiles') }}">Все профили</a></li>
                            <li><a href="{{ route('main::connections') }}">Связи</a></li>
                            <li><a href="{{ route('main::reviews') }}">Отзывы</a></li>
                        </ul>
                        <ul>
                            <li><a href="{{ route('faq::index') }}">Центр помощи</a></li>
                            <li><a href="{{ route('blog::index') }}">Блог</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            {{--
            <div class="links-more">
                <div class="dropdown">
                    <a href="#" id="headerLinksButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="mobee-menu-2"></i></a>
                    <ul class="dropdown-menu" aria-labelledby="headerLinksButton">
                        <li><a href="{{ route('blog::index') }}">Блог</a></li>
                        <li><a href="{{ route('faq::index') }}">База знаний</a></li>
                        @foreach (Html::headerLinks() as $link)
                            <li><a href="{{ route('page::show', ['name' => $link->name]) }}">{{ $link->title }}</a></li>
                        @endforeach;
                    </ul>
                </div>
            </div>
            <div class="links-shadow"></div>
            --}}
        </div>
        <div class="meloman">
            <!-- <a href="#">Meloman</a> -->
            <a href="#" id="header-links-more"><i class="mobee-th-3"></i></a>
            <div id="header-links-more-content" class="header-links-more-content">
                {{-- @include('layouts._meloman') --}}
            </div>
        </div>
        <div id="meloman-list" class="meloman-list">
            <div class="meloman-list-arr"></div>
            <div class="meloman-list-inner">
                @include('layouts._meloman')
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="menu-more">
            <div class="menu-more-shadow"></div>
            <a href="#" id="header-menu-more"><i class="mobee-down-open-1"></i></a>
        </div>
        <div class="menu-list">
            <ul>
                <!-- <li><a href="#" class="plus">+</a></li> -->
                @foreach (Html::subheaderLinks() as $link)
                    <li><a href="{{ route('orders::section', ['name' => $link->name]) }}" class="{{ (isset($section) AND $section->id == $link->id) ? 'active' : null }}">{{ $link->title }}</a></li>
                @endforeach
                {{--
                <li><a href="{{ route('orders::index') }}">Заказы</a></li>
                <li><a href="{{ route('main::connections') }}">Связи</a></li>
                <li><a href="{{ route('main::clients') }}">Клиенты</a></li>
                <li><a href="{{ route('main::companies') }}">Исполнители</a></li>
                <li><a href="{{ route('main::reviews') }}">Отзывы</a></li>
                --}}
            </ul>
        </div>
        <div class="menu-down" id="menu-down">
            <div class="menu-down-inner">
                <div class="row">
                    @foreach (Html::subheaderLinks() as $link)
                        <div class="menu-down-inner-item col-xs-6 col-sm-6 col-md-4 col-lg-3">
                            <strong>{{ $link->title }}</strong>
                            <ul>
                                @foreach ($link->listRoot(5) as $category)
                                    <li><a href="{{ route('orders::category', ['name' => $link->name, 'categoryname' => $category->name]) }}">{{ $category->title }}</a></li>
                                @endforeach
                            </ul>
                            <strong><a href="{{ route('orders::section', ['name' => $link->name]) }}">Все категории</a></strong>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</header>
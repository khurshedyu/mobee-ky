{{-- Audio --}}
<audio id="sound1" src="/static/sound/M.mp3"></audio>

<div class="dashboard">
    <div class="dashboard-header">
        <div class="dropdown">
            <a href="#" class="dashboard-header-mode active font-condensed" id="modeMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <div class="dashboard-header-mode-inner">
                    Аккаунт
                    <i class="mobee-down-open-1 pull-right"></i>
                </div>
            </a>
            <ul class="dropdown-menu" aria-labelledby="modeMenuButton">
                <li><a href="http://masa.meloman.kz/p/change_email" target="_blank">Изменить E-Mail</a></li>
                <li><a href="http://masa.meloman.kz/p/change_password" target="_blank">Изменить пароль</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="{{ route('main::logout') }}">Выход</a></li>
            </ul>
        </div>
        {{--
        <div class="dashboard-header-mode purple">
            <div class="dashboard-header-mode-inner font-condensed pull-right">
                <div class="dropdown">
                    <a href="#" class="" id="rightSettingsButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="mobee-dot-3 rotate-90"></i> &nbsp; Настройки</a>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="rightSettingsButton">
                        <li><a href="http://masa.meloman.kz/p/change_email" target="_blank">Изменить E-Mail</a></li>
                        <li><a href="http://masa.meloman.kz/p/change_password" target="_blank">Изменить пароль</a></li>
                        <li><a href="{{ route('account::index') }}"><i class="mobee-mail-7"></i> Аккаунт</a></li>
                        <li><a href="{{ route('account::password') }}"><i class="mobee-key-5"></i> Пароль</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('main::logout') }}"><i class="mobee-off"></i> Выход</a></li>
                        <li><a href="{{ route('main::logout') }}">Выход</a></li>
                    </ul>
                </div>
            </div>
            <div class="dashboard-header-mode-inner font-condensed">
                <i class="mobee-lock-open-5"></i> &nbsp;
                Аккаунт
            </div>
        </div>
        --}}
    </div>
    <div class="dashboard-content nano">
        <div class="nano-content">
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        @if (Auth::user()->isSuper())
                            Текущий аккаунт – Супер
                        @else
                            Текущий аккаунт – Обычный
                        @endif
                        <span class="plus" data-key="account-types"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="row">
                            <div class="col-xs-9 col-sm-9">
                                @if (Auth::user()->isSuper())
                                    <div class="progress progress-dashboard super-progress">
                                        <div class="super-progress-title">
                                            <div class="height-btn">
                                                <i class="mobee-rocket"></i> &nbsp; Суперсила
                                                @if(Auth::user()->superUntil()['days'] == 0 && Auth::user()->superUntil()['minutes'] > 0)
                                                    {{ Auth::user()->superUntil()['minutes'] }} {{ \App\Helpers\GeneralHelper::generateRussianWordendingForNumbers((int)Auth::user()->superUntil()['minutes'], ['минута', 'минуты', 'минут']) }}
                                                @else
                                                    {{ Auth::user()->superUntil()['days'] }} д.
                                                @endif
                                            </div>
                                        </div>
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ Auth::user()->superUntil()['percent'] }}" aria-valuemin="0" aria-valuemax="{{ Auth::user()->superUntil()['percent'] }}" style="width: {{ Auth::user()->superUntil()['percent'] }}%;">
                                        </div>
                                    </div>
                                @else
                                    <div class="progress progress-dashboard">
                                        <div class="height-btn">
                                            &nbsp; &nbsp; <i class="mobee-rocket"></i> &nbsp; Суперсилы нет
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <a href="{{ route('account::super') }}" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-cog-6"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        Общий счет
                        <span class="plus" data-key="bill"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="height-btn">
                                    <i class="mobee-database-1"></i> &nbsp; {{ Html::formatNum(Auth::user()->balance, 0) }} ед.
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <a href="{{ route('account::billing') }}" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-plus-2"></i> &nbsp; Пополнить</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        Новые события
                        <span class="plus" data-key="notices"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="dashboard-notifications" id="dashboard-notifications" data-last="{{ Auth::user()->lastNotificationId() }}">
                            <ul>
                                <li>
                                    <a href="{{ route('faq::message') }}" class="dashboard-notifications-item warning">
                                        <div class="message">Внимание! Сейчас наш сайт находится в режиме «beta», в связи с чем на нём могут появляться ошибки и недочёты. Просим сообщать, если Вы что-то заметили.</div>
                                    </a>
                                </li>
                            </ul>
                            <ul id="usernotifications">
                                @foreach (Auth::user()->lastNotifications() as $notification)
                                    <li>
                                        <a href="{{ $notification->displayUrl() }}" class="dashboard-notifications-item" data-id="{{ $notification->id }}">
                                            <div class="message">
                                                @if($notification->read)
                                                    {!! $notification->displayMessage() !!}
                                                @else
                                                    <span class="label label-danger">Новое</span> {!! $notification->displayMessage() !!}
                                                @endif
                                            </div>
                                            <small class="blend-text">{{ Html::formatDateTime($notification->created_at) }}</small>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="height-btn">
                            <a href="{{ route('account::notifications') }}"><i class="mobee-bell-4"></i> &nbsp; Все события</a>
                        </div>
                        <?php /*
                        <div class="height-btn font-s">
                            5 самых последних
                        </div>
                        */ ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="widget-notifications">
    <ul>
        <?php /*
        <li>
            <div class="dashboard-notifications-item">
                <a href="#" class="closebtn"><i class="mobee-cancel-2"></i></a>
                <a href="#" class="message">Message message message message message message message</a>
                <small class="blend-text">date</small>
            </div>
        </li>
        */ ?>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        socket.on('check_for_new_notifications', function(data)
        {
            console.log('I should check for new notifications');
            var usernotifications = $('#usernotifications');

            // Get user notifications from server
            $.ajax({
                type: 'GET',
                url: '/api/notifications/last',
                data: {},
                dataType: 'json',
                beforeSend: function()
                {
                },
                success: function(data)
                {
                    // Play Sound
                    $('#sound1')[0].play();
                    console.log(data);
                    usernotifications.html('');
                    $.each(data.data.notifications, function(index, value){
                        var li = $('<li>');

                        console.log(value);

                        if(value.read == 0)
                        {
                            var a = $('<a>', {
                                href: value.displayUrl,
                                class: 'dashboard-notifications-item blinknew',
                                'data-id': value.id
                            });

                            var cmessage = $('<div>', { class: 'message' }).html('&nbsp;'+value.displayMessage);
                            var span = $('<span>', { class: 'label label-danger' }).html('Новое');
                            cmessage.prepend(span);
                        }
                        else
                        {
                            var a = $('<a>', {
                                href: value.displayUrl,
                                class: 'dashboard-notifications-item',
                                'data-id': value.id
                            });

                            var cmessage = $('<div>', { class: 'message' }).html('&nbsp;'+value.displayMessage);
                        }

                        var small = $('<small>', { class: 'blend-text' }).html(value.date);
                        a.append(cmessage);
                        a.append(small);
                        li.append(a);

                        usernotifications.append(li);
                    });
                },
                error: function(request, status, error)
                {
                }
            });
        });
    });
</script>
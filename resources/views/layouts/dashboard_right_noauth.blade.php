<div class="dashboard dashboard-noauth-main">
    <div class="dashboard-header">
        <div class="dashboard-header-mode font-condensed purple">
            <div class="dashboard-header-mode-inner">
                Вход <span class="plus" data-key="login"><i class="mobee-plus-1"></i></span>
            </div>
        </div>
    </div>
    <div class="dashboard-content nano">
        <div class="nano-content">
            <div class="dashboard-content-fakebox">
                <form method="POST" action="{{ route('main::login') }}">
                    <div class="dashboard-content-element">
                        <input type="text" name="email" value="" placeholder="Введите e-mail" class="dashboard-form-textfield" />
                    </div>
                    <div class="dashboard-content-element">
                        <input type="password" name="password" value="" placeholder="Введите пароль" class="dashboard-form-textfield" />
                    </div>
                    <div class="dashboard-content-element">
                        <button class="btn btn-default btn-lg dashboard-content-orderbtn font-condensed"><i class="mobee-key"></i> &nbsp; Подключиться</button>
                    </div>
                    <div class="dashboard-content-element">
                        <div class="height-btn center">
                            <a href="http://masa.meloman.kz/p/#rem_form" target="_blank">Восстановить пароль</a>
                        </div>
                        <div class="height-btn center">
                            <a href="{{ route('main::register') }}">Еще на зарегистрированы?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="dashboard dashboard-noauth-demo">
    <div class="dashboard-header">
        <div class="dropdown">
            <a href="#" class="dashboard-header-mode active font-condensed" id="modeMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <div class="dashboard-header-mode-inner">
                    Аккаунт
                    <i class="mobee-down-open-1 pull-right"></i>
                </div>
            </a>
            <ul class="dropdown-menu" aria-labelledby="modeMenuButton">
                <li><a href="#">Изменить E-Mail</a></li>
                <li><a href="#">Изменить пароль</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="#">Выход</a></li>
            </ul>
        </div>
    </div>
    <div class="dashboard-content nano">
        <div class="nano-content">
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        Текущий аккаунт – Супер
                        <span class="plus" data-key="account-types"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="row">
                            <div class="col-xs-9 col-sm-9">
                                    <div class="progress progress-dashboard super-progress">
                                        <div class="super-progress-title">
                                            <div class="height-btn">
                                                <i class="mobee-rocket"></i> &nbsp; Суперсила
                                                15 д.
                                            </div>
                                        </div>
                                        <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="90" style="width: 90%;">
                                        </div>
                                    </div>
                            </div>
                            <div class="col-xs-3 col-sm-3">
                                <a href="#" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-cog-6"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        Общий счет
                        <span class="plus" data-key="bill"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="row">
                            <div class="col-xs-6 col-sm-6">
                                <div class="height-btn">
                                    <i class="mobee-database-1"></i> &nbsp; 4000 ед.
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6">
                                <a href="#" class="btn btn-dashboard col-xs-12 col-sm-12"><i class="mobee-plus-2"></i> &nbsp; Пополнить</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dashboard-content-element">
                <div class="dashboard-content-box">
                    <div class="dashboard-content-box-header">
                        Новые события
                        <span class="plus" data-key="notices"><i class="mobee-plus-1"></i></span>
                    </div>
                    <div class="dashboard-content-box-content">
                        <div class="dashboard-notifications" id="dashboard-notifications" data-last="0">
                            <ul>
                                <li>
                                    <a href="{{ route('page::show', ['name' => 'welcome']) }}" class="dashboard-notifications-item warning">
                                        <div class="message">Внимание! Сейчас наш сайт находится в режиме «beta», в связи с чем на нём могут появляться ошибки и недочёты. Просим сообщать, если Вы что-то заметили.</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('page::show', ['name' => 'welcome']) }}" class="dashboard-notifications-item" data-id="0">
                                        <div class="message">{!! trans('messages.hello_new_user') !!}</div>
                                        <small class="blend-text">{{ Html::formatDateTime(date('Y-m-d H:i:s')) }}</small>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="height-btn">
                            <a href="#"><i class="mobee-bell-4"></i> &nbsp; Все события</a>
                        </div>
                        <?php /*
                        <div class="height-btn font-s">
                            5 самых последних
                        </div>
                        */ ?>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <br />
        </div>
    </div>
</div>
@extends('layouts.page_admin')

@section('title', 'Панель управления')

@section('content')

<div class="header">
    <h2><strong>Список</strong> заказов и предложений</h2>
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
            <li>
                Заказы и предложения
            </li>
            <li class="active">
                Список
            </li>
        </ol>
    </div>
</div>

{{--
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-content">
        <h1><strong>Simple</strong> Portlets</h1><p>Example of panel with no header and footer.<br>You can edit content by clicking on text.</p>
      </div>
    </div>
  </div>
</div>
--}}

<div class="row">
    <div class="col-md-9">
        <div class="panel">
            {{--
            <div class="panel-header">
                <h3><i class="fa fa-table"></i> <strong>Striped rows</strong> Table</h3>
            </div>
            --}}

            <div class="panel-content">
                {!! Html::table($orders)->columns([
                    [
                        'name' => 'id',
                        'title' => 'ID',
                    ],
                    [
                        'name' => 'title',
                        'title' => 'Заголовок',
                    ],
                    [
                        'name' => 'id',
                        'title' => 'Автор',
                        'render' => function ($value, $item) {
                            return $item->profile->displayName();
                        },
                    ],
                    [
                        'name' => 'id',
                        'title' => 'Статус',
                        'render' => function ($value, $item) {
                            return $item->statusTitle();
                        },
                    ],
                    [
                        'name' => 'id',
                        'title' => 'Действия',
                        'type' => 'actions',
                        'routes' => 'admin::orders'
                    ],
                ])->html(); !!}

                @if ($orders->lastPage() > 1)
                    {!! $orders->appends($filters->values())->links() !!}
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel">
            <div class="panel-header">
                <h3>Фильтры</h3>
            </div>
            <div class="panel-content">
                <form method="GET">
                    @foreach ($filters->filters() as $filter)
                        @if ($filter['type'] == 'order')
                            <div class="form-group">
                                <label>{{ $filter['label'] }}</label>
                                <select name="{{ $filter['name'] }}" class="form-control">
                                    @foreach ($filter['map'] as $key => $val)
                                        <option value="{{ $key }}" {{ $filter['value'] == $key ? 'selected' : '' }}>{{ $val['title'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @elseif ($filter['type'] == 'select')
                            <div class="form-group">
                                <label>{{ $filter['label'] }}</label>
                                <select name="{{ $filter['name'] }}" class="form-control">
                                    @foreach ($filter['map'] as $key => $val)
                                        <option value="{{ $key }}" {{ $filter['value'] == $key ? 'selected' : '' }}>{{ $val['title'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @elseif ($filter['type'] == 'text')
                            <div class="form-group">
                                <label>{{ $filter['label'] }}</label>
                                <input type="text" name="{{ $filter['name'] }}" value="{{ $filter['value'] }}" class="form-control" />
                            </div>
                        @endif
                    @endforeach
                    
                    <button class="btn btn-primary">Применить</button>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection
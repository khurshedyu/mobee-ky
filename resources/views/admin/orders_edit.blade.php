@extends('layouts.page_admin')

@section('title', 'Панель управления')

@section('content')

<div class="header">
    <h2><strong>Редактирование</strong> {{ $order->isoffer ? 'предложения' : 'заказа' }}</h2>
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
            <li>
                Заказы и предложения
            </li>
            <li>
                <a href="{{ route('admin::orders') }}">Список</a>
            </li>
            <li class="active">
                Редактирование
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-9">
        <div class="panel">
            <div class="panel-content">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                @include('admin.orders_form')
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel">
            <div class="panel-header">
                <h3>Управление</h3>
            </div>
            <div class="panel-content">
                <div>
                    <strong>Статус:</strong>
                    <br />
                    {{ $order->statusTitle() }}
                    <br />
                    <br />
                    @if (!$order->statusIsPublic())
                        <a href="{{ route('admin::orders::status::public', ['id' => $order->id]) }}" class="btn btn-success">Публик.</a>
                    @endif
                    @if (!$order->statusIsBlock())
                        <a href="{{ route('admin::orders::status::block', ['id' => $order->id]) }}" class="btn btn-danger">Блок.</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
<form method="POST">
    <div class="radio">
        <label>
            <input type="radio" name="isoffer" value="0" {{ (request()->old('isoffer') ? request()->old('isoffer') : $order->isoffer) ? '' : 'checked' }} />
            Заказ
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" name="isoffer" value="1" {{ (request()->old('isoffer') ? request()->old('isoffer') : $order->isoffer) ? 'checked' : '' }} />
            Предложение
        </label>
    </div>



    {!! Html::formGroup('Город', 'city_id', request()->old('city_id') ? request()->old('city_id') : $order->city_id, 'select', ['errors' => $errors, 'list' => $cities, 'required' => true]) !!}

    {!! Html::formGroup('Категория', 'category_id', request()->old('category_id') ? request()->old('category_id') : $order->category_id, 'select', ['errors' => $errors, 'list' => $categoriesList, 'required' => true]) !!}

    {!! Html::formGroup('Заголовок', 'title', request()->old('title') ? request()->old('title') : $order->title, 'text', ['errors' => $errors, 'required' => true]) !!}

    {!! Html::formGroup('Задание', 'content', request()->old('content') ? request()->old('content') : $order->content, 'textarea', ['errors' => $errors, 'required' => true]) !!}



    {!! Html::formGroup('Бюджет (тг.)', 'price', request()->old('price') ? request()->old('price') : $order->price, 'text', ['errors' => $errors]) !!}

    {!! Html::formGroup('Запрос цены', 'price_request', request()->old('price_request') ? request()->old('price_request') : $order->price_request, 'checkbox', ['errors' => $errors]) !!}



    @if ($order->image)
        <img src="{{ $order->imageUrl() }}" width="300" />
        <br />
    @endif



    {!! Html::formGroup('Только для Супер', 'only_super', request()->old('only_super') ? request()->old('only_super') : $order->only_super, 'checkbox', ['errors' => $errors]) !!}



    {!! Html::formGroup('Отправить', null, null, 'save') !!}
</form>
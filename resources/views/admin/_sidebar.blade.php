<!-- BEGIN SIDEBAR -->
<div class="sidebar">
    <div class="logopanel">
        <h1><a href="{{ route('admin::index') }}">&nbsp;</a></h1>
    </div>
    <div class="sidebar-inner">
        <div class="sidebar-top small-img clearfix">
            <div class="user-image">
                <img src="{{ App\Components\SessionWorker::instance()->currentProfile()->avatarUrl() }}" class="img-responsive img-circle">
            </div>
            <div class="user-details">
                <h4>{{ App\Components\SessionWorker::instance()->currentProfile()->displayName() }}</h4>
                <div class="dropdown user-login">
                    <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300">
                        <i class="online"></i> Available <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="busy"></i> Busy</a></li>
                        <li><a href="#"><i class="turquoise"></i> Invisible</a></li>
                        <li><a href="#"><i class="away"></i> Away</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="menu-title">
            <span>Навигация</span>
            {{--
            <div class="pull-right menu-settings">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300"> 
                <i class="icon-settings"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#" id="reorder-menu" class="reorder-menu">Reorder menu</a></li>
                    <li><a href="#" id="remove-menu" class="remove-menu">Remove elements</a></li>
                    <li><a href="#" id="hide-top-sidebar" class="hide-top-sidebar">Hide user &amp; search</a></li>
                </ul>
            </div>
            --}}
        </div>
        <?php
            $navItems = [
                [
                    'title' => 'Дашборд',
                    'link' => route('admin::index'),
                    // 'active' => true,
                    'icon' => 'icon-home',
                ],
                [
                    'title' => 'Заказы и предложения',
                    'icon' => 'icon-home',
                    'items' => [
                        [
                            'title' => 'Список',
                            'link' => route('admin::orders'),
                            'icon' => 'icon-home',
                        ],
                    ],
                ],
            ];
        ?>
        <ul class="nav nav-sidebar">
            @foreach ($navItems as $item)
                @if (isset($item['items']) AND count($item['items']))
                    <li class="tm nav-parent">
                        <a href="#">
                            @if (isset($item['icon']))
                                <i class="{{ $item['icon'] }}"></i>
                            @endif
                            <span>{{ $item['title'] }}</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="children collapse">
                            @foreach ($item['items'] as $itemItem)
                                <li><a href="{{ $itemItem['link'] }}">{{ $itemItem['title'] }}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @else
                    <li class="tm nav-active {{ isset($item['active']) ? 'active' : null }}">
                        <a href="{{ isset($item['link']) ? $item['link'] : '#' }}">
                            @if (isset($item['icon']))
                                <i class="{{ $item['icon'] }}"></i>
                            @endif
                            <span>{{ $item['title'] }}</span>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
        {{--
        <div class="sidebar-widgets">
            <p class="menu-title widget-title"><span>Folders</span> <span class="pull-right"><a href="#" class="new-folder"> <i class="icon-plus"></i></a></span></p>
            <ul class="folders">
                <li>
                    <a href="#"><i class="icon-doc c-primary"></i><span>My documents</span></a> 
                </li>
                <li>
                    <a href="#"><i class="icon-picture"></i><span>My images</span></a> 
                </li>
                <li><a href="#"><i class="icon-lock"></i><span>Secure data</span></a> 
                </li>
                <li class="add-folder">
                    <input type="text" placeholder="Folder's name..." class="form-control input-sm">
                </li>
            </ul>
        </div>
        --}}
        <div class="sidebar-footer clearfix" style="">
            <a class="pull-left footer-settings" href="#" data-rel="tooltip" data-placement="top" data-original-title="Settings">
            <i class="icon-settings"></i></a>
            <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top" data-original-title="Fullscreen">
            <i class="icon-size-fullscreen"></i></a>
            <a class="pull-left" href="#" data-rel="tooltip" data-placement="top" data-original-title="Lockscreen">
            <i class="icon-lock"></i></a>
            <a class="pull-left btn-effect" href="#" data-modal="modal-1" data-rel="tooltip" data-placement="top" data-original-title="Logout">
            <i class="icon-power"></i></a>
        </div>
    </div>
</div>
<!-- END SIDEBAR -->
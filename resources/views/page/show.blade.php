@extends('layouts.page')

@section('title', $page->title)

@section('content')
    <div class="content-element-box">
        <h2>{{ $page->title }}</h2>
    </div>
    <div class="content-element-box">
        <div class="page-content">
            {!! Html::t($page->content, true) !!}
        </div>
    </div>
@endsection
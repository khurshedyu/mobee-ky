<div class="content-element-box">
    @if ($reviews AND count($reviews))
        @foreach ($reviews as $review)
            <div class="content-element-box-item">
                {!! view('main._review_profiles_element', [
                    'from' => $review->author,
                    'to' => $review->profile,
                    'connector' => '&rarr;',
                    'isoffer' => $review->type,
                ]) !!}

                <div class="row">
                    <div class="col-md-4">
                        <div class="review-item-param">
                            {{ \App\Review::reviewTitle(\App\Review::REVIEW_ITEM_QUALITY, $review->type) }}: <br />
                            {{ $review->qualityTitle() }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="review-item-param">
                            {{ \App\Review::reviewTitle(\App\Review::REVIEW_ITEM_TIME, $review->type) }}: <br />
                            {{ $review->timeTitle() }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="review-item-param">
                            {{ \App\Review::reviewTitle(\App\Review::REVIEW_ITEM_CIVILITY, $review->type) }}: <br />
                            {{ $review->civilityTitle() }}
                        </div>
                    </div>
                </div>
                <div>
                    <div class="content-element-box-header">
                        Оценка: {{ $review->ratingTitle() }}
                    </div>
                    <div class="progress progress-content">
                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ $review->ratingPercent() }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $review->ratingPercent() }}%;">
                            {{ $review->rating() }} баллов из 6
                        </div>
                    </div>
                </div>
                <br />
                <div>
                    {{ $review->content }}
                </div>
                <br />
                @if ($review->deal->dialog->dialogable)
                    <div>
                        <a href="{{ route('orders::view', ['id' => $review->deal->dialog->dialogable->order->id]) }}">Посмотреть {{ $review->deal->dialog->dialogable->order->isoffer ? 'Предложение' : 'Заказ' }}</a>
                    </div>
                    <br />
                @endif
                <small class="blend-text">{{ Html::formatDateTime($review->created_at) }}</small>
            </div>
        @endforeach
    @else
        <div class="center">
            <img src="{{ url('/static/images/features-reviews.png') }}" style="height: 300px" />
            <br />
            <br />
            <h3>Отзывов ещё нет.</h3>
        </div>
    @endif
</div>
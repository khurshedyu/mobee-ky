@extends('layouts.page')

@section('title', 'Моби — бесплатный суперпомощник!')

@section('content')
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h2 class="center">Моби — бесплатный суперпомощник!</h2>
            <?php
                //use App\Components\SessionWorker;
                //var_dump(Auth::user()->listProfiles());
                //var_dump(SessionWorker::instance()->currentProfile());
            ?>
            <br />
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-12 col-md-offset-0 col-xs-12">
                    <a href="#" id="main-slider" class="btn btn-lg btn-mobee font-condensed col-sm-12 col-xs-12"><i class="mobee-play-1"></i> Давайте знакомиться</a>
                </div>
            </div>
            <div id="main-slider-content" style="display: none;">
                @include('main._slider')
            </div>
        </div>
    </div>
    {{--
    <div class="content-element-box">
        @include('main.amcharts')
    </div>
    --}}
    @include('main._bots')
    {{--
    <div class="content-element-box dark">
        @include('main.charts_simple')
    </div>
    --}}
    {{--
    <div class="content-element-clear">
        <img src="{{ url('static/images/services-main-bg-2.jpg') }}" style="width:100%" />
    </div>
    --}}
    {{--
    <div class="content-element-box">
        <div class="content-element-box-item">
            Mobee помогает установить «избранные связи <span class="plus">+</span>» между <a href="#">клиентами</a> и <a href="">исполнителями</a>.
            <br />
            Понравилось? Расскажи друзьям:
        </div>
    </div>
    --}}
@endsection
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

<div id="chartdiv"></div>

<script type="text/javascript">
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "serial",
  "addClassNames": true,
  "theme": "light",
  "autoMargins": false,
  "marginLeft": 40,
  "marginRight": 40,
  "marginTop": 10,
  "marginBottom": 26,
  "balloon": {
    "adjustBorderColor": false,
    "horizontalPadding": 10,
    "verticalPadding": 8,
    "color": "#ffffff"
  },

  "dataProvider": {!! $chartData !!},
  "valueAxes": [ {
    "axisAlpha": 0,
    "position": "left"
  }, {
    "id": "durationAxis",
    "axisAlpha": 0,
    "gridAlpha": 0,
    // "inside": true,
    "position": "right"
  } ],
  "startDuration": 1,
  "graphs": [ {
    "alphaField": "alpha",
    "balloonText": "<span style='font-size:12px;'>[[title]] на [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
    "fillAlphas": 1,
    "title": "Профилей",
    "type": "column",
    "valueField": "profiles",
    "lineColor": "#474e6b",
    "dashLengthField": "dashLengthColumn"
  }, {
    "id": "graph_orders",
    "balloonText": "<span style='font-size:12px;'>[[title]] за [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
    "bullet": "round",
    "lineThickness": 3,
    "bulletSize": 7,
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "useLineColorForBulletBorder": true,
    "bulletBorderThickness": 3,
    "fillAlphas": 0,
    "lineAlpha": 1,
    "title": "Заказов",
    "valueField": "orders",
    "valueAxis": "durationAxis",
    "lineColor": "#99e8f4",
    "dashLengthField": "dashLengthLine"
  }, {
    "id": "graph_offers",
    "balloonText": "<span style='font-size:12px;'>[[title]] за [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
    "bullet": "round",
    "lineThickness": 3,
    "bulletSize": 7,
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "useLineColorForBulletBorder": true,
    "bulletBorderThickness": 3,
    "fillAlphas": 0,
    "lineAlpha": 1,
    "title": "Предложений",
    "valueField": "offers",
    "valueAxis": "durationAxis",
    "lineColor": "#9d99f4",
    "dashLengthField": "dashLengthLine"
  }, {
    "id": "graph_deals",
    "balloonText": "<span style='font-size:12px;'>[[title]] за [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
    "bullet": "round",
    "lineThickness": 3,
    "bulletSize": 7,
    "bulletBorderAlpha": 1,
    "bulletColor": "#FFFFFF",
    "useLineColorForBulletBorder": true,
    "bulletBorderThickness": 3,
    "fillAlphas": 0,
    "lineAlpha": 1,
    "title": "Сделок",
    "valueField": "deals",
    "valueAxis": "durationAxis",
    "lineColor": "#cff499",
    "dashLengthField": "dashLengthLine"
  } ],
  "categoryField": "date",
  "categoryAxis": {
    "gridPosition": "start",
    "axisAlpha": 0,
    "tickLength": 0
  },
  "export": {
    "enabled": true
  }
} );
</script>
<div class="content-element-box">
    <div class="main-features">
        <h3 class="center">Всё для первоклассной работы и даже больше...</h3>
        <?php
            $mainFeaturesItems = [
                [
                    'title' => 'Заказы',
                    'plus' => 'orders',
                    'image' => url('static/images/features-orders.png'),
                    'content' => 'Создание заказов для сбора предложений.',
                ],
                [
                    'title' => 'Предложения',
                    'plus' => 'offers',
                    'image' => url('static/images/features-offers.png'),
                    'content' => 'Готовые предложения для заказчиков.',
                ],
                [
                    'title' => 'Сделки',
                    'plus' => 'deals',
                    'image' => url('static/images/features-deals.png'),
                    'content' => 'Создание сделок в виде чата.',
                ],
                [
                    'title' => 'Мультиаккаунт',
                    'plus' => 'profiles',
                    'image' => url('static/images/features-profiles.png'),
                    'content' => 'Режимы частного лица и компании в одном аккаунте.',
                ],
                [
                    'title' => 'Супераккаунт',
                    'plus' => 'super',
                    'image' => url('static/images/features-super.png'),
                    'content' => 'Суперсила для сверхвозможностей.',
                ],
                [
                    'title' => 'Честные отзывы',
                    'plus' => 'reviews',
                    'image' => url('static/images/features-reviews.png'),
                    'content' => '«Живые отзывы» по реальным зслугам.',
                ],
                [
                    'title' => 'События',
                    'plus' => 'notices',
                    'image' => url('static/images/features-notices.png'),
                    'content' => 'Оповещения для новых заявок и сообщений.',
                ],
                [
                    'title' => 'Сейф',
                    'plus' => 'safe',
                    'image' => url('static/images/features-safe.png'),
                    'content' => 'Хранение и накапливание заказов, предложений, сделок и др.',
                ],
                [
                    'title' => 'Рейтинг',
                    'plus' => 'rating',
                    'image' => url('static/images/features-rating.png'),
                    'content' => 'Автоматическая система рейтинга.',
                ],
                [
                    'title' => 'Микро-подсказки',
                    'plus' => 'plus',
                    'image' => url('static/images/features-plus.png'),
                    'content' => 'Подсказки в нужном месте и нужное время.',
                ],
                [
                    'title' => 'Деловые связи',
                    'plus' => 'connections',
                    'image' => url('static/images/features-connections.png'),
                    'content' => 'Генерация «деловых связей»..',
                ],
                [
                    'title' => 'База знаний',
                    'plus' => 'support',
                    'image' => url('static/images/features-support.png'),
                    'content' => 'Ответы на вопросы и инструкции.',
                ],
                [
                    'title' => 'Общий счет',
                    'plus' => 'bill',
                    'image' => url('static/images/features-bill.png'),
                    'content' => 'Один счёт для всех профилей/режимов.',
                ],
                [
                    'title' => 'Профиль',
                    'plus' => 'public',
                    'image' => url('static/images/features-public.png'),
                    'content' => 'Публичный профиль с уникальной ссылкой.',
                ],
                [
                    'title' => 'Как до луны и обратно',
                    'plus' => 'love',
                    'image' => url('static/images/features-love.png'),
                    'content' => 'Непрерывный апгрейд платформы.',
                ],
            ];
        ?>
        <div class="main-features-items">
            <div class="row main-features-row">
                @foreach ($mainFeaturesItems as $item)
                    <div class="col-md-4 main-features-col">
                        <div class="main-features-items-item">
                            @if (isset($item['image']))
                                <div class="main-features-image"><img src="{{ $item['image'] }}" /></div>
                            @endif
                            <div class="main-features-title">
                                {{ $item['title'] }}
                                @if (isset($item['plus']))
                                    <span class="plus" data-key="{{ $item['plus'] }}"><i class="mobee-plus-1"></i></span>
                                @endif
                            </div>
                            <div class="main-features-content">{!! $item['content'] !!}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="content-element-box gray">
    <div class="main-features">
        <h3 class="center">В разработке...</h3>
        <?php
            $mainFeaturesItems = [
                [
                    'title' => 'Поддержка',
                    'plus' => 'techsupport',
                    'image' => url('static/images/features-techsupport.png'),
                    'content' => 'Чат-поддержка в личном меню.',
                ],
                [
                    'title' => 'Статистика посещений',
                    'plus' => 'visits',
                    'image' => url('static/images/features-visits.png'),
                    'content' => 'Посещения гостей и пользователей.',
                ],
                [
                    'title' => 'Верификация',
                    'plus' => 'verification',
                    'image' => url('static/images/features-verification.png'),
                    'content' => 'Первичная проверка данных.',
                ],
                [
                    'title' => 'Награды',
                    'plus' => 'trophies',
                    'image' => url('static/images/features-trophies.png'),
                    'content' => 'Всеобщее признание заслуг.',
                ],
                [
                    'title' => 'Звания',
                    'plus' => 'ranks',
                    'image' => url('static/images/features-ranks.png'),
                    'content' => 'Вес и репутация на платформе.',
                ],
                [
                    'title' => 'Новая функция',
                    'plus' => 'features',
                    'image' => url('static/images/features.png'),
                    'content' => 'Новый полезный модуль уже в планах.',
                ],
            ];
        ?>
        <div class="main-features-items">
            <div class="row main-features-row">
                @foreach ($mainFeaturesItems as $item)
                    <div class="col-md-4 main-features-col">
                        <div class="main-features-items-item">
                            @if (isset($item['image']))
                                <div class="main-features-image"><img src="{{ $item['image'] }}" /></div>
                            @endif
                            <div class="main-features-title">
                                {!! $item['title'] !!}
                                @if (isset($item['plus']))
                                    <span class="plus" data-key="{{ $item['plus'] }}"><i class="mobee-plus-1"></i></span>
                                @endif
                            </div>
                            <div class="main-features-content">{!! $item['content'] !!}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
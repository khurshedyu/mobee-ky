@extends('layouts.page')

@section('title', 'Исполнители')

@section('content')
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h2>Клиенты</h2>
        </div>
    </div>
    <div class="content-element-box">
        @foreach ($clients as $profile)
            <div class="content-element-box-item">
                @include('main._profile_list_item')
            </div>
        @endforeach
    </div>
    @if ($clients->hasMorePages())
        <div class="content-element-box">
            {!! $clients->appends(['cobra' => 2])->links() !!}
        </div>
    @endif
@endsection
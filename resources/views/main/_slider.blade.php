<div class="mobee-main-slider">
    <div id="carousel-example-generic" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            @foreach ($slides as $key => $slide)
                <li data-target="#carousel-example-generic" data-slide-to="{{ $key }}" class="{{ $key ? null : 'active' }}"></li>
            @endforeach
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            @foreach ($slides as $key => $slide)
                <div class="item {{ $key ? null : 'active' }}">
                    <div class="mobee-main-slider-item">
                        <h2>{{ $slide->title }}</h2>
                        <br />
                        <p><img src="{{ url($slide->image) }}" /></p>
                        <p>{!! $slide->content !!}</p>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
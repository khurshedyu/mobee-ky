<div class="profile-list-item">
    {!! view('layouts.profile_userbar', ['profile' => $profile]) !!}
    <br />
    <div class="row">
        <div class="col-xs-3 col-sm-3">
            <div class="well well-sm">
                <div class="profile-list-box-header">
                    Сделки
                </div>
                {{ $profile->countDeals() }}
            </div>
        </div>
        <div class="col-xs-3 col-sm-3">
            <div class="well well-sm">
                <div class="profile-list-box-header">
                    Отзывы
                </div>
                {{ $profile->countComments() }}
            </div>
        </div>
        <div class="col-xs-3 col-sm-3">
            <div class="well well-sm">
                <div class="profile-list-box-header">
                    Заказы
                </div>
                {{ $profile->countOrders() }}
            </div>
        </div>
        <div class="col-xs-3 col-sm-3">
            <div class="well well-sm">
                <div class="profile-list-box-header">
                    Город
                </div>
                {{ $profile->cityName() }}
            </div>
        </div>
        {{-- <div class="col-xs-2 col-sm-2 col-md-2">
            <a href="{{ route('profile::view', ['name' => $profile->linkName()]) }}" class="avatar" target="_blank">
                <img src="{{ $profile->avatarUrl() }}" class="img-circle" />
            </a>
        </div> --}}
        {{-- <div class="col-xs-10 col-sm-10 col-md-3">
            <a href="{{ route('profile::view', ['name' => $profile->linkName()]) }}" target="_blank">{{ $profile->displayName() }}</a>
                @if ($profile->isChecked())
                    &nbsp;
                    <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                @endif
                @if ($profile->isSuper())
                    &nbsp;
                    <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                @endif
        </div> --}}
        {{-- <div class="col-xs-12 col-md-7">
            <div class="row">
                <div class="col-xs-4 col-sm-4">
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Сделки
                        </div>
                        {{ $profile->countDeals() }}
                    </div>
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Отзывы
                        </div>
                        {{ $profile->countComments() }}
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4">
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Заказы
                        </div>
                        {{ $profile->countOrders() }}
                    </div>
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Город
                        </div>
                        {{ $profile->cityName() }}
                    </div>
                </div>
                <div class="col-xs-4 col-sm-4">
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Рейтинг
                        </div>
                        {{ $profile->countDeals() }}
                    </div>
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Свой в доску
                        </div>
                        {{ $profile->experience() }}
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>
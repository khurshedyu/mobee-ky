@extends('layouts.page')

@section('title', 'Исполнители')

@section('content')
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h2>Исполнители</h2>
        </div>
    </div>
    <div class="content-element-box">
        @foreach ($companies as $profile)
            <div class="content-element-box-item">
                @include('main._profile_list_item')
            </div>
        @endforeach
    </div>
    @if ($companies->hasMorePages())
        <div class="content-element-box">
            {!! $companies->appends(['cobra' => 2])->links() !!}
        </div>
    @endif
@endsection
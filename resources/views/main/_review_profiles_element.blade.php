<div class="row">
    <div class="col-md-5">
        <div class="profile-userbar">
            <div class="profile-userbar-avatar">
                <img src="{{ $from->avatarUrl() }}" />
            </div>
            <div class="profile-userbar-info">
                <div class="profile-userbar-info-name">
                    <a href="{{ route('profile::view', ['name' => $from->linkName()]) }}">{{ $from->displayName() }}</a>
                </div>
                <div class="profile-userbar-info-description blend-text">
                    {{ $isoffer ? 'Исполнитель' : 'Заказчик' }}
                    @if ($from->isChecked())
                        &nbsp;
                        <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                    @endif
                    @if ($from->isSuper())
                        &nbsp;
                        <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="connection-list-link">
            {!! $connector !!}
        </div>
    </div>
    <div class="col-md-5">
        <div class="profile-userbar profile-userbar-right">
            <div class="profile-userbar-avatar">
                <img src="{{ $to->avatarUrl() }}" />
            </div>
            <div class="profile-userbar-info">
                <div class="profile-userbar-info-name">
                    <a href="{{ route('profile::view', ['name' => $to->linkName()]) }}">{{ $to->displayName() }}</a>
                </div>
                <div class="profile-userbar-info-description blend-text">
                    {{ $isoffer ? 'Заказчик' : 'Исполнитель' }}
                    @if ($to->isChecked())
                        &nbsp;
                        <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                    @endif
                    @if ($to->isSuper())
                        &nbsp;
                        <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

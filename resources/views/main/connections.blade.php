@extends('layouts.page')

@section('title', 'Связи')

@section('content')
    <div class="content-element-box">
        <h2>Связи</h2>
    </div>
    <div class="content-element-box">
        @foreach ($connections as $connection)
            @include('layouts.connection_element')
        @endforeach
    </div>
    @if ($connections->lastPage() > 1)
        <div class="content-element-box">
            {!! $connections->links() !!}
        </div>
    @endif
@endsection
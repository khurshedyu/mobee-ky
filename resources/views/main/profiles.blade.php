@extends('layouts.page')

@section('title', 'Все профили')

@section('content')
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h2>Все профили</h2>
        </div>
    </div>
    <div class="content-element-box">
        @foreach ($profiles as $profile)
            <div class="content-element-box-item">
                @include('main._profile_list_item')
            </div>
        @endforeach
    </div>
    @if ($profiles->lastPage() > 1)
        <div class="content-element-box">
            {!! $profiles->links() !!}
        </div>
    @endif
@endsection
@extends('layouts.page')

@section('title', 'Отзывы')

@section('content')
    <div class="content-element-box">
        <h2>Отзывы</h2>
    </div>
    @include ('main._list_reviews')
    @if ($reviews->lastPage() > 1)
        <div class="content-element-box">
            {!! $reviews->links() !!}
        </div>
    @endif
@endsection
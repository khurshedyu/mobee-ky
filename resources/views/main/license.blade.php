@extends('layouts.page')

@section('title', 'Пользовательское соглашение')

@section('content')
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h2>Пользовательское соглашение</h2>
        </div>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            @if ($page)
                {!! $page->contentFormatted() !!}
            @endif
        </div>
        <div class="content-element-box-item">
            <form method="POST">
                {{ csrf_field() }}
                <button class="btn btn-mobee">Принять соглашение</button>
                &nbsp;
                &nbsp;
                <a href="{{ route('main::logout') }}" class="btn btn-mobee">Отказаться и выйти</a>
            </form>
        </div>
    </div>
@endsection
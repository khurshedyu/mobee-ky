@extends('layouts.page')

@section('title', 'Заказы')

@section('content')
    <div class="content-element-box">
        <h2>{{ $section->title }}</h2>
    </div>
    <div class="content-element-box">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        @include ('orders._all_orders_offers')
                        @include ('orders._filters')
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-8">
                        @include ('orders._orders')
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                {{--
                @if ($filters['show'] == 'offers')
                    <h3>Интересные заказы</h3>
                @elseif ($filters['show'] == 'orders')
                    <h3>Интересные предложения</h3>
                @else
                    <h3>Интересное</h3>
                @endif
                --}}
                @include ('orders._widget_orders')
            </div>
        </div>
    </div>
@endsection
@extends('layouts.page')

@section('title', 'Заказы')

@section('content')
    <div class="content-element-box">
        <h2>Заказы</h2>
    </div>
    <div class="content-element-box">
        @foreach ($orders as $order)
            <div class="content-element-box-item">
                <h3>
                    <a href="{{ route('orders::view', ['id' => $order->id]) }}" target="_blank">{{ $order->title }}</a>
                </h3>
                <div>
                    <small>{{ Html::formatDateTime($order->created_at) }}</small>
                </div>
                <div>
                    Автор:
                    <a href="{{ route('profile::view', ['name' => $order->profile->linkName()]) }}">{{ $order->profile->displayName() }}</a>
                </div>
                @if ($order->only_checked)
                    <div>
                        <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                        Только для проверенных
                    </div>
                @endif
                @if ($order->only_super)
                    <div>
                        <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                        Только для супер
                    </div>
                @endif
                @can('orders-edit', $order)
                    <div>
                        <a href="{{ route('orders::edit', ['id' => $order->id]) }}">Редактировать</a>
                    </div>
                @endcan
            </div>
        @endforeach
    </div>
    @if ($orders->lastPage() > 1)
        <div class="content-element-box">
            {!! $orders->links() !!}
        </div>
    @endif
@endsection
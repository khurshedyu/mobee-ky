@extends('layouts.page')

@section('title', 'Создать')

@section('content')
    <div class="content-element-box">
        <h2>Создать</h2>
    </div>
    <div class="content-element-box">
        <div class="row order-create-buttons">
            <div class="col-md-6">
                <a href="{{ route('orders::create::order') }}">
                    <img src="{{ url('static/images/features-orders.png') }}">
                    <h3>Создать заказ</h3>
                    <p>Заказ – это рассказ о том, что такое заказ и все такое.</p>
                </a>
            </div>
            <div class="col-md-6">
                <a href="{{ route('orders::create::offer') }}">
                    <img src="{{ url('static/images/features-offers.png') }}">
                    <h3>Создать предложение</h3>
                    <p>Предложение – такая штука, которая обрана заказу.</p>
                </a>
            </div>
        </div>
    </div>
    {{--
    <div class="content-element-box">
        @include('layouts.panel_success')
        @include('layouts.panel_error')
        @include('orders._form_order')
    </div>
    --}}
@endsection
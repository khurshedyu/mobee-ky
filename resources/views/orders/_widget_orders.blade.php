@foreach ($widgetOrders as $order)
    <div class="order-item">
        <div class="order-item-data">
            <h4><a href="{{ route('orders::view', ['id' => $order->id]) }}" target="_blank">{{ $order->title }}</a></h4>
            @if ($order->only_checked)
                <div>
                    <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                    Только для проверенных
                </div>
            @endif
            @if ($order->only_super)
                <div>
                    <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                    Только для супер
                </div>
            @endif
            <div class="blend-text">{{ Html::formatDateTime($order->created_at) }}</div>
        </div>
    </div>
@endforeach
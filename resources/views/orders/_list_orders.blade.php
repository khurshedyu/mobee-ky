<div class="content-element-box">
    @if ($orders AND count($orders))
        @foreach ($orders as $order)
            <div class="content-element-box-item">
                <h3>
                    <a href="{{ route('orders::view', ['id' => $order->id]) }}" target="_blank">{{ $order->title }}</a>
                </h3>
                <div>
                    Автор:
                    <a href="{{ route('profile::view', ['name' => $order->profile->linkName()]) }}">{{ $order->profile->displayName() }}</a>
                </div>
                @can('orders-edit', $order)
                    <div>
                        <a href="{{ route('orders::edit', ['id' => $order->id]) }}">Редактировать</a>
                    </div>
                @endcan
            </div>
        @endforeach
    @else
        @if (isset($isoffer) AND $isoffer)
            <div class="center">
                <img src="{{ url('/static/images/features-offers.png') }}" style="height: 300px" />
                <br />
                <br />
                <h3>Предложений ещё нет.</h3>
            </div>
        @else
            <div class="center">
                <img src="{{ url('/static/images/features-orders.png') }}" style="height: 300px" />
                <br />
                <br />
                <h3>Заказов ещё нет.</h3>
            </div>
        @endif
    @endif
</div>
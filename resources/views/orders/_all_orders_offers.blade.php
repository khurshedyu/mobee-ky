<div class="order-filters-verticalselect">
    <div class="row">
        <div class="btn-group-vertical col-xs-12 col-sm-12" role="group">
            <a href="{{ Html::sectionCategoryOrderOfferCheckLink($sectionname, $categoryname, $filters, ['show' => 'all']) }}" class="btn {{ $filters['show'] == 'all' ? 'btn-success' : 'btn-default' }}">Все</a>
            <a href="{{ Html::sectionCategoryOrderOfferCheckLink($sectionname, $categoryname, $filters, ['show' => 'orders']) }}" class="btn {{ $filters['show'] == 'orders' ? 'btn-success' : 'btn-default' }}">Заказы</a>
            <a href="{{ Html::sectionCategoryOrderOfferCheckLink($sectionname, $categoryname, $filters, ['show' => 'offers']) }}" class="btn {{ $filters['show'] == 'offers' ? 'btn-success' : 'btn-default' }}">Предложения</a>
        </div>
    </div>
</div>
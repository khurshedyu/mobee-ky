@extends('layouts.page')

<?php $title = 'Создать ' . ($order->typeIsOrder() ? 'заказ' : 'предложение'); ?>
@section('title', $title)

@section('content')
    <div class="content-element-box">
        <h2>{{ $title }}</h2>
    </div>
    <div class="content-element-box">
        <div class="row">
            <div class="col-md-8">
                @include('layouts.panel_success')
                <?php $panelError = $order->typeIsOrder() ? 'Пожалуйста, заполните все поля со звёздочкой, чтобы ваш заказ был более информативным.' : 'Пожалуйста, заполните все поля со звёздочкой, чтобы ваше предложение было более информативным.'; ?>
                @include('layouts.panel_error')
                @include('orders._form_order')
            </div>
            <div class="col-md-4">
                <div class="well">
                    @if ($orderCreate)
                        <h3>{!! $orderCreate->title !!}</h3>
                        <br />
                        {!! $orderCreate->contentFormatted() !!}
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
{!! view('layouts.profile_userbar', ['profile' => $orderRequest->profile]) !!}
<br />
<div>
    {{ $orderRequest->content }}
</div>
<br />
<div>
    @can ('order-request-create-dialog', $orderRequest)
        <div class="pull-right" style="margin-top:-5px">
            <a href="{{ route('orders::process', ['id' => $orderRequest->id]) }}" class="btn btn-mobee btn-mobee-xs">Обсудить сделку</a>
        </div>
    @endcan
    <small class="blend-text">{{ Html::formatDateTime($orderRequest->created_at) }}</small>
</div>
@extends('layouts.page')

@section('title', 'Редактирование')

@section('content')
    <div class="content-element-box">
        <h2>Редактирование {{ $order->isoffer ? 'предложения' : 'заказа' }} #{{ $order->id }}</h2>
    </div>
    <div class="content-element-box">
        @include('layouts.panel_success')
        @include('layouts.panel_error')
        @include('orders._form_order')
    </div>
@endsection
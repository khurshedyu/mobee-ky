@if (count($orders))
    @foreach ($orders as $order)
        <div class="order-item">
            <div class="order-item-img">
                <a href="{{ route('orders::view', ['id' => $order->id]) }}" target="_blank"><img src="{{ $order->imageUrl() }}" /></a>
            </div>
            <div class="order-item-data">
                <h3><a href="{{ route('orders::view', ['id' => $order->id]) }}" target="_blank">{{ $order->title }}</a></h3>
                @if ($order->only_checked)
                    <div>
                        <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                        Только для проверенных
                    </div>
                @endif
                @if ($order->only_super)
                    <div>
                        <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                        Только для супер
                    </div>
                @endif
                <div class="blend-text">{{ Html::formatDateTime($order->created_at) }}</div>
            </div>
        </div>
    @endforeach
@else
    <div class="center">
        @if ($filters['show'] == 'offers')
            <h3>Актуальных предложений ещё нет.</h3>
        @elseif ($filters['show'] == 'orders')
            <h3>Актуальных заказов ещё нет.</h3>
        @else
            <h3>Актуальных заказов или предложений ещё нет.</h3>
        @endif
        <br />
        <img src="{{ url('static/images/features-search.png') }}" style="max-width: 90%; max-height:300px;" />
    </div>
@endif
@extends('layouts.page')

@section('title', 'Заказ')

@section('content')
    <div class="content-element-box">
        <h2>{{ $order->title }}</h2>
        <div class="padding-vertical">
            @if ($order->category)
                <a href="{{ route('orders::section', ['name' => $order->category->section->name]) }}">{{ $order->category->section->title }}</a> /
            @endif
            @if ($order->category AND $order->category->parent)
                <a href="{{ route('orders::category', ['name' => $order->category->parent->section->name, 'categoryname' => $order->category->parent->name]) }}">{{ $order->category->parent->title }}</a> /
            @endif
            @if ($order->category)
                <a href="{{ route('orders::category', ['name' => $order->category->section->name, 'categoryname' => $order->category->name]) }}">{{ $order->category->title }}</a> /
            @endif
            {{ $order->isoffer ? 'Предложение' : 'Заказ' }}
            #{{ $order->id }}
        </div>
    </div>
    <div class="content-element-box">
        <div class="row">
            <div class="col-md-6">
                <div class="order-view-img">
                    <img src="{{ $order->imageUrl() }}" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="content-element-box-item">
                    Город:
                    {{ $order->city->name }}
                    <br />
                    Цена:
                    @if ($order->price_request)
                        Запрос цены
                    @else
                        {{ Html::formatNum($order->price, 0) }} тг.
                    @endif
                </div>
                <div class="content-element-box-item">
                    {{ $order->content }}
                </div>
                <div class="content-element-box-item">
                    @if ($order->only_checked)
                        <div>
                            <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                            Только для проверенных
                        </div>
                    @endif
                    @if ($order->only_super)
                        <div>
                            <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                            Только для супер
                        </div>
                    @endif
                    <div>
                        Автор:
                        <a href="{{ route('profile::view', ['name' => $order->profile->linkName()]) }}">{{ $order->profile->displayName() }}</a>
                    </div>
                    <div>
                        <span class="blend-text">{{ Html::formatDateTime($order->created_at) }}</span>
                    </div>
                    @can('orders-edit', $order)
                        <div>
                            <a href="{{ route('orders::edit', ['id' => $order->id]) }}">Редактировать</a>
                        </div>
                    @endcan
                </div>
            </div>
        </div>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            <div class="pull-right">
                Количество: {{ count($orderRequests) }}
            </div>
            <h3>Заявки</h3>
        </div>
        @if (count($orderRequests))
            @foreach ($orderRequests as $orderRequest)
                <div class="content-element-box-item">
                    {!! view('orders._order_request', ['orderRequest' => $orderRequest]) !!}
                </div>
            @endforeach
        @else
            <div class="content-element-box-item">
                <div class="content-cleaner center">
                    Ещё не оставлено ни одной заявки.
                    <br />
                    Вы будете первым.
                    {{-- <br />
                    <br />
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                            <a href="#" class="btn btn-mobee col-xs-12 col-sm-12">Оставить заявку</a>
                        </div>
                    </div> --}}
                </div>
            </div>
        @endif
        @can ('order-create-request', $order)
            <div class="content-element-box-item">
                <h3>Оставить заявку</h3>
                <br />
                <form method="POST">
                    {!! Html::formGroup('Профиль', 'profile_id', $profile->id, 'select', ['errors' => $errors, 'list' => $user->listProfiles(), 'required' => true]) !!}
                    {!! Html::formGroup('Предложение', 'content', '', 'textarea', ['errors' => $errors]) !!}
                    {!! Html::formGroup('Отправить', null, null, 'save') !!}
                </form>
            </div>
        @endcan
        @if ($order->only_super AND ($user AND !$user->isSuper()))
            <div class="content-element-box-item center">
                <h4>Заявки в этом {{ $order->isoffer ? 'предложении' : 'заказе' }} могут оставлять только Супер пользователи</h4>
                <br />
                <a href="{{ route('account::super') }}" class="btn btn-success">Получить Суперсилу!</a>
            </div>
        @endif
    </div>
@endsection
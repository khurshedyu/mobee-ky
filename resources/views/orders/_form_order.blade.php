<form class="form" method="POST" enctype="multipart/form-data">
    {{--
    <div class="content-element-box-header">
        Тип объявления
    </div>
    <div class="content-element-box-item">
        <div class="radio">
            <label>
                <input type="radio" name="isoffer" value="0" {{ (request()->old('isoffer') ? request()->old('isoffer') : $order->isoffer) ? '' : 'checked' }} />
                Заказ
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="isoffer" value="1" {{ (request()->old('isoffer') ? request()->old('isoffer') : $order->isoffer) ? 'checked' : '' }} />
                Предложение
            </label>
        </div>
    </div>
    --}}
    <div class="content-element-box-header">
        Основные данные
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Город', 'city_id', request()->old('city_id') ? request()->old('city_id') : $order->city_id, 'select', ['errors' => $errors, 'list' => $cities, 'required' => true]) !!}

        {!! Html::formGroup('Заголовок', 'title', request()->old('title') ? request()->old('title') : $order->title, 'text', ['errors' => $errors, 'required' => true, 'placeholder' => 'Введите краткое описание']) !!}
        <p class="help-block help-block-mobee">— заголовок должен быть наполнен смыслом, коротко о главном</p>

        {!! Html::formGroup('Описание ' . ($order->typeIsOrder() ? 'заказа' : 'предложения'), 'content', request()->old('content') ? request()->old('content') : $order->content, 'textarea', ['errors' => $errors, 'required' => true, 'placeholder' => 'Введите подробное описание']) !!}
    </div>
    <div class="content-element-box-header">
        Размещение
    </div>
    <div class="content-element-box-item order-category-select">

        {{--
        {!! Html::formGroup('Категория', 'category_id', request()->old('category_id') ? request()->old('category_id') : $order->category_id, 'select', ['errors' => $errors, 'list' => $categoriesList, 'required' => true]) !!}
        --}}

        <div class="form-group order-category-select-section <?php if ($errors->get('category_id')): ?>has-error<?php endif; ?>">
            <label>Сфера <span style="color:red">*</span></label>
            <select class="form-control">
                <option value="0">Укажите сферу</option>
                @foreach ($sections as $key => $val)
                    <option value="{{ $key }}">{{ $val }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group order-category-select-category <?php if ($errors->get('category_id')): ?>has-error<?php endif; ?>" style="display:none;">
            <label>Категория <span style="color:red">*</span></label>
            <select class="form-control">
                <option value="0">Укажите категорию</option>
            </select>
        </div>

        <div class="form-group order-category-select-category-sub" style="display:none;">
            <label>Подкатегория</label>
            <select class="form-control">
                <option value="0">Укажите подкатегорию</option>
            </select>
        </div>

        @if ($errors->get('category_id'))
            <div class="form-group has-error">
                @foreach ($errors->get('category_id') as $error)
                    <div class="help-block help-block-mobee">
                        @if ($order->isoffer)
                            Выберите подходящую категорию, тогда потенциальные заказчики смогут найти ваше предложение.
                        @else
                            Выберите подходящую категорию, тогда потенциальные исполнители смогут найти ваш заказ.
                        @endif
                    </div>
                @endforeach
            </div>
        @endif

        <input type="hidden" name="category_id" value="{{ request()->old('category_id') ? request()->old('category_id') : $order->category_id }}" class="order-category-select-input" />
    </div>
    <div class="content-element-box-header">
        Оплата
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Бюджет (тг.)', 'price', request()->old('price') ? request()->old('price') : $order->price, 'text', ['errors' => $errors, 'class' => "", 'data-format' => "ddddddddddddddd", 'placeholder' => '0', 'id' => 'order_price_field']) !!}

        {!! Html::formGroup('Запрос цены', 'price_request', request()->old('price_request') ? request()->old('price_request') : $order->price_request, 'checkbox', ['errors' => $errors, 'id' => 'order_price_request_field']) !!}

        <p class="help-block help-block-mobee">— назначайте свою цену или запрашивайте, как в тендере</p>
    </div>
    <div class="content-element-box-header">
        Главное изображение
    </div>
    <div class="content-element-box-item">
        <img src="{{ $order->imageUrl() }}" class="order-form-image" id="order_image_prev" width="300" style="{{ !$order->image ? 'display:none;' : null }}" />
        {!! Html::formGroup('Загрузить изображение', 'image_id', request()->old('image_id') ? request()->old('image_id') : $order->image_id, 'image_ajax', ['errors' => $errors, 'img' => 'order_image_prev']) !!}

        <p class="help-block help-block-mobee">— jpg, png, gif размером до 5 мегабайт</p>
    </div>
    <div class="content-element-box-header">
        Дополнительно {!! Html::plus('super') !!}
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('<span class="mobee-profile-super"><i class="mobee-rocket"></i></span>&nbsp; Только для Супер', 'only_super', request()->old('only_super') ? request()->old('only_super') : $order->only_super, 'checkbox', ['errors' => $errors]) !!}

        <p class="help-block help-block-mobee">— подавать заявки смогут только суперпрофили</p>

        {{--
        {!! Html::formGroup('<span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>&nbsp; Только для Проверенных', 'only_checked', request()->old('only_checked') ? request()->old('only_checked') : $order->only_checked, 'checkbox', ['errors' => $errors]) !!}
        --}}
    </div>
    <div class="content-element-box-item">
        <input type="hidden" name="isoffer" value="{{ $order->isoffer }}" />
        {!! Html::formGroup('Отправить', null, null, 'save') !!}
    </div>
</form>
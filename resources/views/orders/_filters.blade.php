<div class="order-filters-verticalselect">
    <div class="row">
        <div class="btn-group-vertical col-xs-12 col-sm-12" role="group">
            @foreach ($rootCategories as $categoryItem)
                <a href="{{ Html::sectionCategoryOrderOfferCheckLink($sectionname, $categoryItem->name, $filters) }}" class="btn {{ $categoryname == $categoryItem->name ? 'btn-success' : 'btn-default' }}">{{ $categoryItem->title }}</a>
                @if ($category)
                    @if ($category->id == $categoryItem->id OR ($category->parent AND $category->parent->id == $categoryItem->id))
                        @foreach ($categoryItem->categoriesOrdered() as $categoryItemChild)
                            <a href="{{ Html::sectionCategoryOrderOfferCheckLink($sectionname, $categoryItemChild->name, $filters) }}" class="btn {{ $categoryname == $categoryItemChild->name ? 'btn-success' : 'btn-default' }}">- {{ $categoryItemChild->title }}</a>
                        @endforeach
                    @endif
                @endif
            @endforeach
        </div>
    </div>
</div>
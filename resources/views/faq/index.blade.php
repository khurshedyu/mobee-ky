@extends('layouts.page')

@section('title', 'Помощь')

@section('content')
    <div class="content-element-box">
        <h2>Помощь</h2>
        <div class="padding-vertical">
            <a href="{{ route('faq::index') }}">База знаний</a>
        </div>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="{{ route('faq::index') }}">База знаний</a></li>
            <li role="presentation"><a href="{{ route('faq::message') }}">Создать обращение</a></li>
        </ul>
        <br />
        <div class="row">
            <div class="col-md-4">
                {!! view('faq._categories', ['categories' => $categories, 'currentId' => 0]) !!}
            </div>
            <div class="col-md-8">
                @if ($page)
                    {!! $page->content !!}
                @else
                    Ни одна категория не выбрана
                @endif
            </div>
        </div>
        {{--
        <div class="row">
            @foreach ($categories as $category)
                <div class="col-md-6">
                    <a href="{{ route('faq::category', ['id' => $category->id]) }}" class="faq-list-category center">
                        <div class="faq-list-category-title font-condensed">
                            {{ $category->title }}
                        </div>
                        <div class="faq-list-category-description">
                            {{ $category->description }}
                        </div>
                        <div class="faq-list-category-count">
                            Записей: {{ $category->items->count() }}
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        --}}
    </div>
@endsection
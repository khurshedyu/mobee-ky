@extends('layouts.page')

@section('title', $item->title . ' – Помощь')

@section('content')
    <div class="content-element-box">
        <h2>Помощь</h2>
        <div class="padding-vertical">
            <a href="{{ route('faq::index') }}">База знаний</a> /
            <a href="{{ route('faq::category', ['name' => $item->category->name]) }}">{{ $item->category->title }}</a> /
            {{ $item->title }}
        </div>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="{{ route('faq::index') }}">База знаний</a></li>
            <li role="presentation"><a href="{{ route('faq::message') }}">Создать обращение</a></li>
        </ul>
        <br />
        <div class="row">
            <div class="col-md-4">
                {!! view('faq._categories', ['categories' => $categories, 'currentId' => $item->category->id]) !!}
            </div>
            <div class="col-md-8">
                <div class="content-element-box-item">
                    {{--
                    <div>
                        {!! Html::t($item->description, true) !!}
                    </div>
                    --}}
                    <div>
                        {!! Html::t($item->content, true) !!}
                    </div>
                    <small class="blend-text">Редакция от — {{ Html::formatDate($item->updated_at) }}</small>
                </div>
                @foreach ($incatItems as $incatItem)
                    @if ($incatItem->id != $item->id)
                        <div class="content-element-box-item">
                            <a href="{{ route('faq::item', ['name' => $incatItem->name]) }}" class="faq-list-item-title font-condensed">{{ $incatItem->title }}</a>
                            <div>
                                {!! Html::t($incatItem->description, true) !!}
                            </div>
                            <small class="blend-text">Редакция от — {{ Html::formatDate($incatItem->updated_at) }}</small>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
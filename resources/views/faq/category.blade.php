@extends('layouts.page')

@section('title', $category->title . ' – База знаний')

@section('content')
    <div class="content-element-box">
        <h2>Помощь</h2>
        <div class="padding-vertical">
            <a href="{{ route('faq::index') }}">База знаний</a> /
            {{ $category->title }}
        </div>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="{{ route('faq::index') }}">База знаний</a></li>
            <li role="presentation"><a href="{{ route('faq::message') }}">Создать обращение</a></li>
        </ul>
        <br />
        <div class="row">
            <div class="col-md-4">
                {!! view('faq._categories', ['categories' => $categories, 'currentId' => $category->id]) !!}
            </div>
            <div class="col-md-8">
                @foreach ($items as $item)
                    <div class="content-element-box-item">
                        <a href="{{ route('faq::item', ['name' => $item->name]) }}" class="faq-list-item-title font-condensed">{{ $item->title }}</a>
                        <div>
                            {!! Html::t($item->description, true) !!}
                        </div>
                        <small class="blend-text">Редакция от — {{ Html::formatDate($item->updated_at) }}</small>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
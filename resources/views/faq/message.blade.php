@extends('layouts.page')

@section('title', 'Создать обращение – Помощь')

@section('content')
    <div class="content-element-box">
        <h2>Помощь</h2>
        <div class="padding-vertical">
            <a href="{{ route('faq::message') }}">Создать обращение</a>
        </div>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="{{ route('faq::index') }}">База знаний</a></li>
            <li role="presentation" class="active"><a href="{{ route('faq::message') }}">Создать обращение</a></li>
        </ul>
        <br />
        <div class="content-element-box-item">
            @include('layouts.panel_success')
            @if (count($errors))
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Ошибка Отправки</h3>
                    </div>
                    <div class="panel-body">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            <form method="POST">
                <p>Постарайтесь подробно описать интересующую вас тему или ошибку.</p>
                <textarea class="form-control" name="message" rows="5">{{ request()->old('message') }}</textarea>
                <br />
                <p>Выберите тип обращения</p>
                <div class="radio">
                    <label>
                        <input type="radio" name="message_type" id="message_type_error" value="1">
                        Ошибка
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="message_type" id="message_type_ask" value="2">
                        Вопрос
                    </label>
                </div>
                <div class="radio disabled">
                    <label>
                        <input type="radio" name="message_type" id="message_type_idea" value="3">
                        Идея
                    </label>
                </div>
                <br />
                {{ csrf_field() }}
                <button class="btn-mobee">Отправить</button>
            </form>
        </div>
    </div>
@endsection
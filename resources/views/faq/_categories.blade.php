@foreach ($categories as $cat)
    <a href="{{ route('faq::category', ['name' => $cat->name]) }}" class="faq-list-category {{ ($cat->id == $currentId) ? 'active' : null }}">
        <div class="faq-list-category-title font-condensed">
            {{ $cat->title }}
        </div>
        {{--
        <div class="faq-list-category-description">
            {{ $cat->description }}
        </div>
        --}}
        <div class="faq-list-category-count">
            Ответов: {{ $cat->items->count() }}
        </div>
    </a>
@endforeach
@extends('layouts.page')

@section('title', 'Супераккаунт')

@section('content')
    <div class="content-element-box">
        <h2>Супераккаунт</h2>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="{{ route('account::super') }}">Суперсила</a></li>
            <li role="presentation"><a href="{{ route('account::super::info') }}">Преимущества</a></li>
        </ul>
        <br />
        @if ($user->isSuper())
            <div class="content-element-box-item">
                <h3 class="center">Супераккаунт действует до {{ Html::formatDate($user->superUntil()['end_at']) }} ({{ $user->superUntil()['days'] }} дней)</h3>
                <div class="center blend-text">Добавить еще:</div>
            </div>
        @else
            <div class="content-element-box-item">
                <h3 class="center">У вас еще нет аккаунта Супер</h3>
                <div class="center blend-text">Ознакомьтесь с тарифами:</div>
            </div>
        @endif
        <div class="content-element-box-item">
            <div class="row">
                {{--
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="super-item">
                        <div class="super-item-price">2 400 тг./мес.</div>
                        <div class="super-item-desc">2 года</div>
                        <div class="super-item-cost">57 600 тг.</div>
                        <div class="">
                            <a href="{{ route('account::super::buy', ['id' => \App\Super::SUPER_PACK_24]) }}" class="btn btn-lg btn-success col-xs-12 col-sm-12">Получить</a>
                        </div>
                    </div>
                </div>
                --}}
                {{--
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="super-item">
                        <div class="super-item-price">2 800 тг./мес.</div>
                        <div class="super-item-desc">1 год</div>
                        <div class="super-item-cost">33 600 тг.</div>
                        <div class="">
                            <a href="{{ route('account::super::buy', ['id' => \App\Super::SUPER_PACK_12]) }}" class="btn btn-lg btn-success col-xs-12 col-sm-12">Получить</a>
                        </div>
                    </div>
                </div>
                --}}
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="super-item">
                        <div class="super-item-price">3 200 тг./мес.</div>
                        <div class="super-item-desc">6 месяцев</div>
                        <div class="super-item-cost">19 200 тг.</div>
                        <div class="">
                            <a href="{{ route('account::super::buy', ['id' => \App\Super::SUPER_PACK_6]) }}" class="btn btn-lg btn-success col-xs-12 col-sm-12">Получить</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="super-item">
                        <div class="super-item-price">3 600 тг./мес.</div>
                        <div class="super-item-desc">3 месяца</div>
                        <div class="super-item-cost">10 800 тг.</div>
                        <div class="">
                            <a href="{{ route('account::super::buy', ['id' => \App\Super::SUPER_PACK_3]) }}" class="btn btn-lg btn-success col-xs-12 col-sm-12">Получить</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-6">
                    <div class="super-item">
                        <div class="super-item-price">4 000 тг./мес.</div>
                        <div class="super-item-desc">1 месяц</div>
                        <div class="super-item-cost">4 000 тг.</div>
                        <div class="">
                            <a href="{{ route('account::super::buy', ['id' => \App\Super::SUPER_PACK_1]) }}" class="btn btn-lg btn-success col-xs-12 col-sm-12">Получить</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
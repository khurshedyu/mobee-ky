@extends('layouts.page')

@section('title', 'Общий счет')

@section('content')
    <div class="content-element-box">
        <h2>Общий счет</h2>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="{{ route('account::billing') }}">Пополнение баланса</a></li>
            <li role="presentation"><a href="{{ route('account::billing::invoices') }}">Счета</a></li>
            <li role="presentation" class="active"><a href="{{ route('account::billing::history') }}">История</a></li>
        </ul>
        <div class="content-element-box-item">
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Изменение</th>
                    <th>Описание</th>
                    <th>Дата</th>
                </thead>
                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>{{ $transaction->id }}</td>
                            <td>
                                @if ($transaction->money > 0)
                                    <span style="color:green">{{ $transaction->money }}</span>
                                @elseif ($transaction->money < 0)
                                    <span style="color:red">{{ $transaction->money }}</span>
                                @else
                                    {{ $transaction->money }}
                                @endif
                                
                            </td>
                            <td>{{ $transaction->typeString() }}</td>
                            <td>{{ Html::formatDateTime($transaction->created_at) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if ($transactions->hasPages())
            <div class="content-element-box-item">
                {!! $transactions->links() !!}
            </div>
        @endif
    </div>
@endsection
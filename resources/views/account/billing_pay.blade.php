@extends('layouts.page')

@section('title', 'Оплата счета')

@section('content')
    <div class="content-element-box">
        <h2>Оплата счета # {{ $invoice->id }}</h2>
    </div>
    <div class="content-element-box">
        <form id="SendOrder" name="SendOrder" method="post" action="{!! $payment['url'] !!}" target="_blank">
            <h3>К оплате</h3>
            {{ $invoice->money }} тг.
            <br />
            {{--
            <a href="{{ route('account::billing::pay::success', ['id' => $invoice->id]) }}">Применить мимо сиистемы оплаты</a>
            --}}
            <br />
            <input type="hidden" name="Signed_Order_B64" value='{!! $payment['xml'] !!}'>
            <input type="hidden" name="email" size=50 maxlength=50  value="{{ Auth::user()->email }}" readonly>
            <input type="hidden" name="Language" value="rus">
            <input type="hidden" name="BackLink" value="https://mobee.kz/payerror">
            <input type="hidden" name="PostLink" value="https://mobee.kz/payinfo">
            <button class="btn btn-mobee">Оплатить</button>
        </form>
    </div>
@endsection
@extends('layouts.page')

@section('title', 'Общий счет')

@section('content')
    <div class="content-element-box">
        <h2>Общий счет</h2>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="{{ route('account::billing') }}">Пополнение баланса</a></li>
            <li role="presentation"><a href="{{ route('account::billing::invoices') }}">Счета</a></li>
            <li role="presentation"><a href="{{ route('account::billing::history') }}">История</a></li>
        </ul>
        <br />
        @if (count($errors))
            <div class="alert alert-danger" role="alert">{{ $errors->first() }}</div>
        @endif
        <form class="form" method="POST">
            {!! Html::formGroup('Сумма для пополнения (тг.)', 'money', null, 'text', ['errors' => $errors, 'required' => true]) !!}
            <!-- <h3>Метод оплаты</h3>
            <div class="radio">
                <label>
                    <input type="radio" name="bill_type" value="{{ \App\Review::PARAM_BAD }}" />
                    Платежные карты VISA
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="bill_type" value="{{ \App\Review::PARAM_BAD }}" />
                    Платежные карты MasterCard
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="bill_type" value="{{ \App\Review::PARAM_BAD }}" />
                    Платежные карты Maestro
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="bill_type" value="{{ \App\Review::PARAM_BAD }}" />
                    American Express
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="bill_type" value="{{ \App\Review::PARAM_BAD }}" />
                    Cirrus
                </label>
            </div> -->
            <br />
            {!! Html::formGroup('Продолжить', null, null, 'save') !!}
        </form>
    </div>
@endsection
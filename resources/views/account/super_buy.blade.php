@extends('layouts.page')

@section('title', 'Приобретение супераккаунта')

@section('content')
    <div class="content-element-box">
        <h2>Приобретение супераккаунта</h2>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            @if ($canBuy)
                <h3>Супераккаунт на {{ $months }} месяцев</h3>
                <br />
                <form class="form" method="POST">
                    {!! Html::formGroup('Оплатить', null, null, 'save') !!}
                </form>
            @else
                <h3>У вас недостаточно средств для приобретения супераккаунта</h3>
                <br />
                <a href="{{ route('account::billing') }}" class="btn btn-success">Пополнить баланс</a>
            @endif
        </div>
    </div>
@endsection
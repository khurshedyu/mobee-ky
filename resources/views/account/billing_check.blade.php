@extends('layouts.page')

@section('title', 'Статус счета')

@section('content')
    <div class="content-element-box">
        <h2>Статус счета # {{ $invoice->id }}</h2>
    </div>
    <div class="content-element-box">
        {{ $paymentCheck->status() }}
        <br />
        <br />
        <a href="{{ route('account::billing::invoices') }}" class="btn btn-mobee">Мои счета</a>
    </div>
@endsection
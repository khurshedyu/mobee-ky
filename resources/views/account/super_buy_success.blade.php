@extends('layouts.page')

@section('title', 'Супераккаунт приобретен!')

@section('content')
    <div class="content-element-box">
        <h2>Супераккаунт приобретен!</h2>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            Операция прошла успешно.
        </div>
    </div>
@endsection
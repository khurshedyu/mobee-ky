@extends('layouts.page')

@section('title', 'События')

@section('content')
    <div class="content-element-box">
        <h2>События</h2>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" {!! $type == 0 ? 'class="active"' : null !!}><a href="{{ route('account::notifications') }}">Все события</a></li>
            <li role="presentation" {!! $type == 1 ? 'class="active"' : null !!}><a href="{{ route('account::notifications::my') }}">Мои действия</a></li>
            <li role="presentation" {!! $type == 2 ? 'class="active"' : null !!}><a href="{{ route('account::notifications::others') }}">Действия других</a></li>
        </ul>
        <div class="content-element-box-item">
            <table class="table">
                <thead>
                    <th>Описание</th>
                    <th width="30%">Дата</th>
                </thead>
                <tbody>
                    @foreach ($notifications as $notification)
                        <tr>
                            <td>
                                @if(!$notification->read)
                                    <a href="{{ $notification->displayUrl() }}"><span class="label label-danger">Новое</span> {!! $notification->displayMessage() !!}</a>
                                @else
                                    <a href="{{ $notification->displayUrl() }}">{!! $notification->displayMessage() !!}</a>
                                @endif
                            </td>
                            <td>{{ Html::formatDateTime($notification->created_at) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if ($notifications->hasPages())
            <div class="content-element-box-item">
                {!! $notifications->links() !!}
            </div>
        @endif
    </div>
@endsection
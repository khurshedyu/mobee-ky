@extends('layouts.page')

@section('title', 'Общий счет')

@section('content')
    <div class="content-element-box">
        <h2>Общий счет</h2>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="{{ route('account::billing') }}">Пополнение баланса</a></li>
            <li role="presentation" class="active"><a href="{{ route('account::billing::invoices') }}">Счета</a></li>
            <li role="presentation"><a href="{{ route('account::billing::history') }}">История</a></li>
        </ul>
        <div class="content-element-box-item">
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>Сумма</th>
                    <th>Описание</th>
                    <th>Дата</th>
                    <th>Опции</th>
                </thead>
                <tbody>
                    @foreach ($invoices as $invoice)
                        <tr>
                            <td>{{ $invoice->id }}</td>
                            <td>
                                @if ($invoice->money > 0)
                                    <span style="color:green">{{ $invoice->money }}</span>
                                @elseif ($invoice->money < 0)
                                    <span style="color:red">{{ $invoice->money }}</span>
                                @else
                                    {{ $invoice->money }}
                                @endif
                                
                            </td>
                            <td>{{ $invoice->typeString() }}</td>
                            <td>{{ Html::formatDateTime($invoice->created_at) }}</td>
                            <td>
                                @if ($invoice->isPayd())
                                @else
                                    <a href="{{ route('account::billing::check', ['id' => $invoice->id]) }}" class="btn btn-xs btn-primary">Проверить статус</a>
                                    <a href="{{ route('account::billing::pay', ['id' => $invoice->id]) }}" class="btn btn-xs btn-success">Оплатить</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if ($invoices->lastPage() > 1)
            <div class="content-element-box-item">
                {!! $invoices->links() !!}
            </div>
        @endif
    </div>
@endsection
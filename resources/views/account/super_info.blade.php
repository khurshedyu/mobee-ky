@extends('layouts.page')

@section('title', 'Супер аккаунт')

@section('content')
    <div class="content-element-box">
        <h2>Супер аккаунт</h2>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="{{ route('account::super') }}">Суперсила</a></li>
            <li role="presentation" class="active"><a href="{{ route('account::super::info') }}">Преимущества</a></li>
        </ul>
        <br />
        <div class="content-element-box-item">
            @if ($page)
                {!! $page->contentFormatted() !!}
            @endif
        </div>
    </div>
@endsection
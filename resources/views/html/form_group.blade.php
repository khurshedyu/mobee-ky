<div class="form-group <?php if ($errors->get($name)): ?>has-error<?php endif; ?>">
    {!! $element !!}
</div>
<label for="form_element_{{ $options['id'] }}" class="control-label">
    {!! $title !!}
    @if (isset($options['required']))
        <span style="color:red">*</span>
    @endif
</label>
{{-- <label for="form_element_{{ $options['id'] }}" class="col-md-3 col-lg-2 control-label">{{ $title }}</label> --}}
{{-- <div class="col-md-6 col-lg-8"> --}}
    <select id="form_element_{{ $options['id'] }}" name="{{ $name }}" class="form-control">
        @foreach ($options['list'] as $listKey => $listValue)
            <option value="{{ $listKey }}" <?php if ($value == $listKey): ?>selected="selected"<?php endif; ?>>{{ $listValue }}</option>
        @endforeach
    </select>
    @if ($errors->get($name))
        @foreach ($errors->get($name) as $error)
            <div class="help-block help-block-mobee">{{ $error }}</div>
        @endforeach
    @endif
{{-- </div> --}}
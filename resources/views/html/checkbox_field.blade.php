<div>
    <div class="checkbox">
        <label for="form_element_{{ $options['id'] }}">
            <input type="checkbox" name="{{ $name }}" id="form_element_{{ $options['id'] }}" value="1" <?php if($value): ?>checked="checked"<?php endif; ?> />
            {!! $title !!}
            @if (isset($options['required']))
                <span style="color:red">*</span>
            @endif
        </label>
    </div>
</div>

@if ($errors->get($name))
    @foreach ($errors->get($name) as $error)
        <div class="help-block help-block-mobee">{{ $error }}</div>
    @endforeach
@endif
<div class="image-ajax-box">
    <input type="file" name="image_ajax" data-img="{{ isset($options['img']) ? $options['img'] : null }}" data-hidden="image_ajax_hidden_{{ $options['id'] }}" data-btn="image_ajax_btn_{{ $options['id'] }}" class="image-ajax-input" id="image_ajax_{{ $options['id'] }}" />
</div>
<a href="#" class="btn btn-mobee image-ajax-btn" id="image_ajax_btn_{{ $options['id'] }}" data-id="image_ajax_{{ $options['id'] }}">{{ $title }}</a>
<input type="hidden" name="{{ $name }}" value="{{ $value }}" id="image_ajax_hidden_{{ $options['id'] }}" />
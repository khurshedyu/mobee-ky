<label for="form_element_{{ $options['id'] }}" class="control-label">
    {!! $title !!}
    @if (isset($options['required']))
        <span style="color:red">*</span>
    @endif
</label>
<input type="{{ $type }}" name="{{ $name }}" value="{{ $value }}" class="form-control" id="form_element_{{ $options['id'] }}" <?php if (isset($options['placeholder'])): ?>placeholder="{{ $options['placeholder'] }}"<?php endif; ?> />
{{--
<div class="row">
    <div class="col-xs-4 col-sm-4">
        <select name="{{ $name }}" class="form-control col-md-4">
            @for ($i = 1; $i <= 31; $i++)
                <option value="{{ $i }}">{{ $i }}</option>
            @endfor
        </select>
    </div>
</div>
--}}
@if ($errors->get($name))
    @foreach ($errors->get($name) as $error)
        <div class="help-block help-block-mobee">{{ $error }}</div>
    @endforeach
@endif
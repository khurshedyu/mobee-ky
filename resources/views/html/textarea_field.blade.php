<label for="form_element_{{ $options['id'] }}" class="control-label">
    {!! $title !!}
    @if (isset($options['required']))
        <span style="color:red">*</span>
    @endif
</label>
<textarea type="{{ $type }}" name="{{ $name }}" class="form-control" id="form_element_{{ $options['id'] }}" style="max-width:100%;" rows="3" <?php if (isset($options['placeholder'])): ?>placeholder="{{ $options['placeholder'] }}"<?php endif; ?> />{{ $value }}</textarea>
@if ($errors->get($name))
    @foreach ($errors->get($name) as $error)
        <div class="help-block help-block-mobee">{{ $error }}</div>
    @endforeach
@endif
<label for="form_element_{{ $options['id'] }}" class="control-label">
    {!! $title !!}
    @if (isset($options['required']))
        <span style="color:red">*</span>
    @endif
    @if (isset($options['plus']))
        <span class="plus" data-key="{{ $options['plus'] }}"><i class="mobee-plus-1"></i></span>
    @endif
</label>
<input type="{{ $type }}" name="{{ $name }}" value="{{ $value }}" class="form-control <?php if (isset($options['class'])): ?>{{ $options['class'] }}<?php endif; ?>" id="form_element_{{ $options['id'] }}" <?php if (isset($options['placeholder'])): ?>placeholder="{{ $options['placeholder'] }}"<?php endif; ?> <?php if (isset($options['data-format'])): ?>data-format="{{ $options['data-format'] }}"<?php endif; ?> />
@if ($errors->get($name))
    @foreach ($errors->get($name) as $error)
        <div class="help-block help-block-mobee">{{ $error }}</div>
    @endforeach
@endif
@extends('layouts.page')

@section('title', 'Регистрация')

@section('content')
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h2>Регистрация</h2>
        </div>
        <div class="content-element-box-item">
            <p>На ваш Email {{ $email }} отправлено письмо со ссылкой для активации.</p>
            <a href="{{ route('main::index') }}" class="btn btn-mobee">Готово</a>
        </div>
    </div>
@endsection
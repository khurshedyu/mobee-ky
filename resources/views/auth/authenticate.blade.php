@extends('layouts.page')

@section('title', 'Авторизация')

@section('content')
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h2>Авторизация</h2>
        </div>
        <div class="content-element-box-item">
            <div class="row">
                <div class="col-lg-offset-2 col-lg-8">
                    @if (count($errors))
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" class="form-horizontal">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail" class="col-md-2 control-label">Email</label>
                                <div class="col-md-10">
                                    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Введите ваш Email" value="{{ request()->old('email') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-md-2 control-label">Пароль</label>
                                <div class="col-md-10">
                                    <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Введите ваш пароль" value="{{ request()->old('password') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    <div class="checkbox">
                                        <label>
                                            <input name="remember" type="checkbox"> Не запоминать меня
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-2 col-md-10">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-mobee">Авторизоваться</button>
                                </div>
                            </div>
                        </form>
                    </form>
                    <div class="col-md-offset-2 col-md-10">
                        <a href="{{ route('main::register') }}">Регистрация</a>
                        <br />
                        <br />
                        <a href="http://masa.meloman.kz/p/#rem_form" target="_blank">Восстановить пароль</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
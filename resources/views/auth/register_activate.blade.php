@extends('layouts.page')

@section('title', 'Активация')

@section('content')
    <div class="content-element-box">
        <div class="content-element-box-item">
            <h2>Активация</h2>
        </div>
        <div class="content-element-box-item">
            @if (count($errors))
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @else
                @if (isset($fail))
                    <p>{{ $fail }}</p>
                @else
                    <p>Активация прошла успешно.</p>
                @endif
                <a href="{{ route('main::index') }}" class="btn btn-mobee">Готово</a>
            @endif
        </div>
    </div>
@endsection
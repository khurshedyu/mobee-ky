@extends('layouts.page')

@section('title', 'Диалог')

@section('content')
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="{{ route('profile::deals::dialog', ['id' => $dialog->id]) }}">Чат</a></li>
            <li role="presentation" class="active"><a href="{{ route('profile::deals::history', ['id' => $dialog->id]) }}">История</a></li>
            @if ($dialog->deal AND $dialog->deal->closed())
                <li role="presentation"><a href="{{ route('profile::deals::review', ['id' => $dialog->id]) }}">Отзыв</a></li>
            @endif
        </ul>
        <br />
        <h2>{{ $dialog->title() }}</h2>
    </div>
    <div class="content-element-box">
        @foreach ($items as $item)
            <div class="content-element-box-item">
                <div class="pull-right blend-text">
                    {{ Html::formatDateTime($item->created_at) }}
                </div>
                {{ $item->title() }}
            </div>
        @endforeach
    </div>

@endsection

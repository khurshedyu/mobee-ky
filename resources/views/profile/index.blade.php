@extends('layouts.page')

@section('title', 'Профиль')

@section('content')
    <div class="content-element-box">
        <h2>Профиль</h2>
    </div>
    <div class="content-element-box">
        @include('layouts.panel_success')
        @include('layouts.panel_error')
        @include('profile._form_user')
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#form_element_username').checkUsername();
        });
    </script>
@endsection
<div class="content-element-box" style="background: #111 url('{{ url('/static/images/bg_default.jpg')  }}') no-repeat center center; background-size: cover;">
    <div class="content-profile-header content-profile-gradient">
        <a href="{{ route('profile::view', ['name' => $profile->linkName()]) }}" class="content-profile-avatar">
            <img src="{{ $profile->avatarUrl() }}" />
        </a>
        @if ($profile->isCompany())
            <h2>
                {{ $profile->displayEntityType() }}
                {{ $profile->prefix }}
                «{{ $profile->displayName() }}»
            </h2>
        @else
            <h2>{{ $profile->displayName() }}</h2>
        @endif
        <p>
            {{ $profile->isCompany() ? 'Компания' :  'Частное лицо' }}
            @if ($profile->isChecked())
                &nbsp;
                <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
            @endif
            @if ($profile->isSuper())
                &nbsp;
                <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
            @endif
            <!-- <br /> -->
            <!-- <small><i class="fa fa-circle"></i> Нет на сайте</small> -->
        </p>
        <div class="content-profile-navigation">
            <ul>
                <li>
                    <a href="{{ route('profile::view', ['name' => $profile->linkName()]) }}" class="{{ $currentPage == 'view' ? 'active' : null }}"><i class="mobee-vcard-1"></i><span> &nbsp; Профиль</span></a>
                </li>
                <li>
                    <a href="{{ route('profile::view::orders', ['name' => $profile->linkName()]) }}" class="{{ $currentPage == 'orders' ? 'active' : null }}"><i class="mobee-pencil-alt"></i><span> &nbsp; Заказы</span></a>
                </li>
                <li>
                    <a href="{{ route('profile::view::offers', ['name' => $profile->linkName()]) }}" class="{{ $currentPage == 'offers' ? 'active' : null }}"><i class="mobee-suitcase"></i><span> &nbsp; Предложения</span></a>
                </li>
                <li>
                    <a href="{{ route('profile::view::reviews', ['name' => $profile->linkName()]) }}" class="{{ $currentPage == 'reviews' ? 'active' : null }}"><i class="mobee-comment-2"></i><span> &nbsp; Отзывы</span></a>
                </li>
                <li>
                    <a href="{{ route('profile::view::connections', ['name' => $profile->linkName()]) }}" class="{{ $currentPage == 'connections' ? 'active' : null }}"><i class="mobee-link-4"></i><span> &nbsp; Связи</span></a>
                </li>
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
<form class="form" method="POST" enctype="multipart/form-data">
    <div class="content-element-box-header">
        Основные данные
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Город', 'city_id', $profile->city_id, 'select', ['errors' => $errors, 'list' => $cities, 'required' => true]) !!}
        {!! Html::formGroup('Фамилия', 'lastname', $profile->lastname, 'text', ['errors' => $errors, 'required' => true]) !!}
        {!! Html::formGroup('Имя', 'firstname', $profile->firstname, 'text', ['errors' => $errors, 'required' => true]) !!}
        {!! Html::formGroup('Отчество', 'secondname', $profile->secondname, 'text', ['errors' => $errors]) !!}
    </div>
    <div class="content-element-box-header">
        Аватар
    </div>
    <div class="content-element-box-item">
        <img src="{{ $profile->avatarUrl() }}" class="order-form-image" id="avatar_image_prev" width="300" style="{{ !$profile->avatar_id ? 'display:none;' : null }}" />
        {!! Html::formGroup('Загрузить изображение', 'avatar_id', request()->old('avatar_id') ? request()->old('avatar_id') : $profile->avatar_id, 'image_ajax', ['errors' => $errors, 'img' => 'avatar_image_prev']) !!}
    </div>
    <div class="content-element-box-header">
        Дополнительно
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Юзернейм', 'name', $profile->name, 'text', ['errors' => $errors, 'id' => 'username', 'data-format' => $profile->name, 'plus' => 'username']) !!}

        <p class="help-block help-block-mobee">— может состоять из латинских букв, цифр и символов _ -</p>

        {!! Html::formGroup('Дата рождения', 'birth_date', $profile->birth_date, 'date', ['errors' => $errors]) !!}
        {!! Html::formGroup('Пол', 'sex', $profile->sex, 'select', ['errors' => $errors, 'list' => \App\Profile::listSex()]) !!}
        {!! Html::formGroup('Телефон', 'phone', $profile->phone, 'text', ['errors' => $errors, 'class' => "input-medium bfh-phone", 'data-format' => "+d (ddd) ddd-dddd"]) !!}
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Сохранить', null, null, 'save') !!}
    </div>
</form>
@extends('layouts.page')

@section('title', 'Просмотр профиля')

@section('content')
    @include ('profile._profile_header')
    <div class="content-element-box">
        {{--
        <div class="content-element-box-header">
            Информация
        </div>
        --}}
        <div class="content-element-box-item">
            <div class="row">
                <div class="col-xs-3 col-sm-3">
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Сделки
                        </div>
                        {{ $profile->countDeals() }}
                    </div>
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Отзывы
                        </div>
                        {{ $profile->countComments() }}
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3">
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Заказы
                        </div>
                        {{ $profile->countOrders() }}
                    </div>
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Связи
                        </div>
                        {{ $profile->countConnections() }}
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3">
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Предложения
                        </div>
                        {{ $profile->countOffers() }}
                    </div>
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Город
                        </div>
                        {{ $profile->cityName() }}
                    </div>
                </div>
                <div class="col-xs-3 col-sm-3">
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Рейтинг
                        </div>
                        {{ $profile->countRating() }}
                    </div>
                    <div class="profile-list-box-item">
                        <div class="profile-list-box-header">
                            Свой в доску
                        </div>
                        {{ $profile->experience() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-header">
            Качество профиля
        </div>
        <div class="content-element-box-item">
            <div class="progress progress-content">
                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $profile->quality() }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $profile->quality() }}%;">
                    {{ $profile->quality() }}%
                </div>
            </div>
        </div>
    </div>
@endsection
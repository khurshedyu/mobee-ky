@extends('layouts.page')

@section('title', 'Просмотр профиля')

@section('content')
    <div class="content-element-box" style="background: url('{{ url('/static/images/bg_default.jpg')  }}') no-repeat center center; background-size: cover;">
        <div class="content-profile-header content-profile-gradient">
            <a href="#" class="content-profile-avatar">
                <img src="{{ $profile->avatarUrl() }}" />
            </a>
            <h2>{{ $profile->displayName() }}</h2>
            <p>
                Частное лицо
                @if ($profile->isChecked())
                    &nbsp;
                    <span class="mobee-profile-checked"><i class="mobee-ok-4"></i></span>
                @endif
                @if ($profile->isSuper())
                    &nbsp;
                    <span class="mobee-profile-super"><i class="mobee-rocket"></i></span>
                @endif
                <!-- <br /> -->
                <!-- <small><i class="fa fa-circle"></i> Нет на сайте</small> -->
            </p>
            <div class="content-profile-navigation">
                <ul>
                    <li>
                        <a href="{{ route('profile::view', ['name' => $profile->linkName()]) }}"><i class="mobee-vcard-1"></i><span> &nbsp; Профиль</span></a>
                    </li>
                    <li>
                        <a href="{{ route('profile::view::orders', ['name' => $profile->linkName()]) }}"><i class="mobee-pencil-alt"></i><span> &nbsp; Заказы</span></a>
                    </li>
                    <li>
                        <a href="{{ route('profile::view::offers', ['name' => $profile->linkName()]) }}"><i class="mobee-suitcase"></i><span> &nbsp; Предложения</span></a>
                    </li>
                    <li>
                        <a href="{{ route('profile::view::reviews', ['name' => $profile->linkName()]) }}"><i class="mobee-comment-2"></i><span> &nbsp; Отзывы</span></a>
                    </li>
                    <li>
                        <a href="{{ route('profile::view::connections', ['name' => $profile->linkName()]) }}"><i class="mobee-star-empty-2"></i><span> &nbsp; Связи</span></a>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-header">
            Информация
        </div>
        <div class="content-element-box-item">
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <div class="profile-list-box-item">
                                        <div class="profile-list-box-header">
                                            Сделки
                                        </div>
                                        {{ $profile->countDeals() }}
                                    </div>
                                    <div class="profile-list-box-item">
                                        <div class="profile-list-box-header">
                                            Отзывы
                                        </div>
                                        {{ $profile->countComments() }}
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <div class="profile-list-box-item">
                                        <div class="profile-list-box-header">
                                            Заказы
                                        </div>
                                        {{ $profile->countOrders() }}
                                    </div>
                                    <div class="profile-list-box-item">
                                        <div class="profile-list-box-header">
                                            Город
                                        </div>
                                        {{ $profile->cityName() }}
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4">
                                    <div class="profile-list-box-item">
                                        <div class="profile-list-box-header">
                                            Рейтинг
                                        </div>
                                        {{ $profile->countDeals() }}
                                    </div>
                                    <div class="profile-list-box-item">
                                        <div class="profile-list-box-header">
                                            Свой в доску
                                        </div>
                                        {{ $profile->experience() }}
                                    </div>
                                </div>
                            </div>
        </div>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-header">
            Качество профиля
        </div>
        <div class="content-element-box-item">
            <div class="progress progress-content">
                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $profile->quality() }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $profile->quality() }}%;">
                    {{ $profile->quality() }}%
                </div>
            </div>
        </div>
    </div>
@endsection
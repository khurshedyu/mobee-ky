@extends('layouts.page')

@section('title', 'Диалог')
@section('body_class', 'body-chat')

@section('content')
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="{{ route('profile::deals::dialog', ['id' => $dialog->id]) }}">Чат</a></li>
            <li role="presentation"><a href="{{ route('profile::deals::history', ['id' => $dialog->id]) }}">История</a></li>
            @if ($dialog->deal AND $dialog->deal->closed())
                <li role="presentation"><a href="{{ route('profile::deals::review', ['id' => $dialog->id]) }}">Отзыв</a></li>
            @endif
        </ul>
        <br />
        <h2>{{ $dialog->title() }}</h2>
    </div>
    <div class="content-element-box">
        @if ($dialog->dialogable_type == 'order_request')
            <div class="content-element-box-item">
                {!! view('orders._order_request', ['orderRequest' => $dialog->dialogable]) !!}
            </div>
        @endif
    </div>
    <app-dialog></app-dialog>
    <?php /*
    @if (count($items))
        <div class="content-element-box" id="chat-messages" data-last="{{ $lastItem->id }}">
            @foreach ($items as $item)
                <div class="content-element-box-item">
                    {!! view('layouts.chat_item', ['item' => $item]) !!}
                </div>
            @endforeach
        </div>
    @endif
    <div class="chat-message-spacer"></div>
    <div class="chat-clean-item">
        {!! view('layouts.chat_item') !!}
    </div>
    <div class="chat-message-box">
        <form method="POST" id="chat-form" data-dialog="{{ $dialog->id }}">
            <div class="chat-message-box-field">
                <textarea id="chat-form-field" class="form-control" name="message"></textarea>
            </div>
            <button id="chat-form-send" class="chat-message-box-send">
                <i class="mobee-mail-7"></i>
            </button>
            {{ csrf_field() }}
        </form>
    </div>
    */ ?>
    {{--<div class="content-element-box">
        <form method="POST">
            {!! Html::formGroup('Сообщение', 'message', null, 'textarea', ['errors' => $errors]) !!}
            {!! Html::formGroup('Отправить', null, null, 'save') !!}
        </form>
    </div>--}}
@endsection

@section('after_content')
    <div class="chat-message-spacer"></div>
@endsection

@section('script')
    <script type="text/javascript">
        window.jsonData = {!! $jsonData !!};
        System.import('app/dialog.component').then(null, console.error.bind(console));
    </script>
@endsection
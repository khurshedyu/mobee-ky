@extends('layouts.page')

@section('title', 'Сделки')

@section('content')
    <div class="content-element-box">
        <h2>Мои сделки</h2>
    </div>
    <div class="content-element-box">
        @if ($dialogMember AND count($dialogMember))
            @foreach ($dialogMember as $dialogMemberItem)
                <div class="content-element-box-item">
                    {!! view('layouts.dialog_list_item', [
                        'dialog' => $dialogMemberItem->dialog,
                        'dialogMember' => $dialogMemberItem
                    ]) !!}
                    {{--
                    <br />
                    @if ($dialogMemberItem->dialog->deal)
                        <a href="{{ route('profile::review', ['id' => $dialogMemberItem->dialog->deal->id]) }}">Оставить отзыв</a>
                    @endif
                    --}}
                </div>
            @endforeach
        @else
            <div class="center">
                <img src="{{ url('/static/images/features-deals.png') }}" style="height: 300px" />
                <br />
                <br />
                <h3>Сделок ещё нет.</h3>
            </div>
        @endif
    </div>
@endsection
@extends('layouts.page')

@section('title', 'Просмотр профиля')

@section('content')
    @include ('profile._profile_header')
    <?php $isoffer = true; ?>
    @include ('orders._list_orders')
    @if ($orders->lastPage() > 1)
        <div class="content-element-box">
            {!! $orders->links() !!}
        </div>
    @endif
@endsection
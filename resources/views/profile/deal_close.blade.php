@extends('layouts.page')

@section('title', 'Завершение сделки')

@section('content')
    <div class="content-element-box">
        <h2>Завершение сделки</h2>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            <ul>
                <li>
                    Исполнитель: <a href="{{ route('profile::view', ['name' => $performer->linkName()]) }}">{{ $performer->displayName() }}</a>
                </li>
                <li>
                    Стоимость сделки: {{ $deal->price }} тг.
                </li>
            </ul>
        </div>
        @include('layouts.panel_success')
        @include('layouts.panel_error')
        <form class="form" method="POST">
            <div class="content-element-box-item">
                Вы уверены, что хотите завершить эту сделку?
            </div>
            <div class="content-element-box-item">
                {!! Html::formGroup('Продолжить', null, null, 'save') !!}
            </div>
        </form>
    </div>
@endsection
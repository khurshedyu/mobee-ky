@extends('layouts.page')

@section('title', 'Просмотр профиля')

@section('content')
    @include ('profile._profile_header')
    <div class="content-element-box">
        @if ($connections AND count($connections))
            @foreach ($connections as $connection)
                @include('layouts.connection_element')
            @endforeach
        @else
            <div class="center">
                <img src="{{ url('/static/images/features-connections.png') }}" style="height: 300px" />
                <br />
                <br />
                <h3>Связей ещё нет.</h3>
            </div>
        @endif
    </div>
    @if ($connections->lastPage() > 1)
        <div class="content-element-box">
            {!! $connections->links() !!}
        </div>
    @endif
@endsection
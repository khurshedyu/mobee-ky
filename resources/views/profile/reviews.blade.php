@extends('layouts.page')

@section('title', 'Мои отзывы')

@section('content')
    <div class="content-element-box">
        <h2>Мои отзывы</h2>
    </div>
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" class="{{ $currentTab == 'about' ? 'active' : null }}"><a href="{{ route('profile::reviews') }}">Отзывы обо мне ( {{ $countAbout }} )</a></li>
            <li role="presentation" class="{{ $currentTab == 'my' ? 'active' : null }}"><a href="{{ route('profile::reviews::my') }}">Мои отзывы ( {{ $countMy }} )</a></li>
        </ul>
    </div>
    @include ('main._list_reviews')
    @if ($reviews->lastPage() > 1)
        <div class="content-element-box">
            {!! $reviews->links() !!}
        </div>
    @endif
@endsection
@extends('layouts.page')

@section('title', 'Мои предложения')

@section('content')
    <div class="content-element-box">
        <h2>Мои предложения</h2>
    </div>
    <?php $isoffer = true; ?>
    @include ('orders._list_orders')
    @if ($orders->lastPage() > 1)
        <div class="content-element-box">
            {!! $orders->links() !!}
        </div>
    @endif
@endsection
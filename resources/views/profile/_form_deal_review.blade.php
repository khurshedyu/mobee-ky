<form class="form" method="POST">
    <div class="content-element-box-item">
        <h3>{{ \App\Review::reviewTitle(\App\Review::REVIEW_ITEM_QUALITY, $isoffer) }}</h3>
        <div class="radio">
            <label>
                <input type="radio" name="param_quality" value="{{ \App\Review::PARAM_BAD }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_BAD, \App\Review::REVIEW_ITEM_QUALITY, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_quality" value="{{ \App\Review::PARAM_MIDDLING }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_MIDDLING, \App\Review::REVIEW_ITEM_QUALITY, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_quality" value="{{ \App\Review::PARAM_GOOD }}" checked />
                {{ \App\Review::reviewItem(\App\Review::PARAM_GOOD, \App\Review::REVIEW_ITEM_QUALITY, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_quality" value="{{ \App\Review::PARAM_SUPER }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_SUPER, \App\Review::REVIEW_ITEM_QUALITY, $isoffer) }}
            </label>
        </div>
    </div>
    <div class="content-element-box-item">
        <h3>{{ \App\Review::reviewTitle(\App\Review::REVIEW_ITEM_TIME, $isoffer) }}</h3>
        <div class="radio">
            <label>
                <input type="radio" name="param_time" value="{{ \App\Review::PARAM_BAD }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_BAD, \App\Review::REVIEW_ITEM_TIME, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_time" value="{{ \App\Review::PARAM_MIDDLING }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_MIDDLING, \App\Review::REVIEW_ITEM_TIME, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_time" value="{{ \App\Review::PARAM_GOOD }}" checked />
                {{ \App\Review::reviewItem(\App\Review::PARAM_GOOD, \App\Review::REVIEW_ITEM_TIME, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_time" value="{{ \App\Review::PARAM_SUPER }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_SUPER, \App\Review::REVIEW_ITEM_TIME, $isoffer) }}
            </label>
        </div>
    </div>
    <div class="content-element-box-item">
        <h3>{{ \App\Review::reviewTitle(\App\Review::REVIEW_ITEM_CIVILITY, $isoffer) }}</h3>
        <div class="radio">
            <label>
                <input type="radio" name="param_civility" value="{{ \App\Review::PARAM_BAD }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_BAD, \App\Review::REVIEW_ITEM_CIVILITY, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_civility" value="{{ \App\Review::PARAM_MIDDLING }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_MIDDLING, \App\Review::REVIEW_ITEM_CIVILITY, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_civility" value="{{ \App\Review::PARAM_GOOD }}" checked />
                {{ \App\Review::reviewItem(\App\Review::PARAM_GOOD, \App\Review::REVIEW_ITEM_CIVILITY, $isoffer) }}
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="param_civility" value="{{ \App\Review::PARAM_SUPER }}" />
                {{ \App\Review::reviewItem(\App\Review::PARAM_SUPER, \App\Review::REVIEW_ITEM_CIVILITY, $isoffer) }}
            </label>
        </div>
    </div>
    <div class="content-element-box-item">
    {!! Html::formGroup('Комментарий', 'content', request()->old('content'), 'textarea', ['errors' => $errors, 'required' => true]) !!}
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Сохранить', null, null, 'save') !!}
    </div>
</form>
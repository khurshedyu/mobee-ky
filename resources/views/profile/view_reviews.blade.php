@extends('layouts.page')

@section('title', 'Просмотр профиля')

@section('content')
    @include ('profile._profile_header')
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation" class="{{ $currentTab == 'about' ? 'active' : null }}"><a href="{{ route('profile::view::reviews', ['name' => $profile->linkName()]) }}">Отзывы обо мне ( {{ $countAbout }} )</a></li>
            <li role="presentation" class="{{ $currentTab == 'my' ? 'active' : null }}"><a href="{{ route('profile::view::reviews::my', ['name' => $profile->linkName()]) }}">Мои отзывы ( {{ $countMy }} )</a></li>
        </ul>
    </div>
    @include ('main._list_reviews')
    @if ($reviews->lastPage() > 1)
        <div class="content-element-box">
            {!! $reviews->links() !!}
        </div>
    @endif
@endsection
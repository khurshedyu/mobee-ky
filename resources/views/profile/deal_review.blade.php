@extends('layouts.page')

@section('title', 'Отзыв')

@section('content')
    <div class="content-element-box">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="{{ route('profile::deals::dialog', ['id' => $dialog->id]) }}">Чат</a></li>
            <li role="presentation"><a href="{{ route('profile::deals::history', ['id' => $dialog->id]) }}">История</a></li>
            @if ($dialog->deal AND $dialog->deal->closed())
                <li role="presentation" class="active"><a href="{{ route('profile::deals::review', ['id' => $dialog->id]) }}">Отзыв</a></li>
            @endif
        </ul>
        <br />
        <h2>Отзыв</h2>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            <ul class="deal-review-summary">
                <li>
                    Автор объявления: <a href="{{ route('profile::view', ['name' => $starter->linkName()]) }}">{{ $starter->displayName() }}</a>
                </li>
                <li>
                    Автор отзыва: <a href="{{ route('profile::view', ['name' => $author->linkName()]) }}">{{ $author->displayName() }}</a>
                </li>
                <li>
                    {{ $isoffer ? 'Клиент' : 'Исполнитель' }}: <a href="{{ route('profile::view', ['name' => $respondent->linkName()]) }}">{{ $respondent->displayName() }}</a>
                </li>
                <li>
                    {{ $isoffer ? 'Исполнитель' : 'Клиент' }}: <a href="{{ route('profile::view', ['name' => $author->linkName()]) }}">{{ $author->displayName() }}</a>
                </li>
                <li>
                    Стоимость сделки: {{ $deal->price }} тг.
                </li>
            </ul>
        </div>
        @include('layouts.panel_success')
        @include('layouts.panel_error')
        @if ($checkReview)
            <div class="content-cleaner center">
                <h4>Вы уже оставили отзыв на эту сделку.</h4>
            </div>
        @else
            @include('profile._form_deal_review')
        @endif
    </div>
@endsection
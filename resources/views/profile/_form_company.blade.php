<form class="form" method="POST" enctype="multipart/form-data">
    <div class="content-element-box-header">
        Основные данные
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Город', 'city_id', $profile->city_id, 'select', ['errors' => $errors, 'list' => $cities, 'required' => true]) !!}
        {!! Html::formGroup('Тип бизнеса', 'entity_type', $profile->entity_type, 'select', ['errors' => $errors, 'list' => $entityTypes]) !!}
        {!! Html::formGroup('Префикс названия', 'prefix', $profile->prefix, 'text', ['errors' => $errors]) !!}
        {!! Html::formGroup('Название', 'firstname', $profile->firstname, 'text', ['errors' => $errors, 'required' => true]) !!}
        {!! Html::formGroup('Юзернейм', 'name', $profile->name, 'text', ['errors' => $errors, 'id' => 'username', 'data-format' => $profile->name, 'plus' => 'username']) !!}
        {!! Html::formGroup('Сфера деятельности', 'section_id', $profile->section_id, 'select', ['errors' => $errors, 'list' => $sections, 'required' => true]) !!}
    </div>
    <div class="content-element-box-header">
        Аватар
    </div>
    <div class="content-element-box-item">
        <input name="avatar" type="file" />
    </div>
    <div class="content-element-box-header">
        Дополнительно
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Дата основания', 'birth_date', $profile->birth_date, 'date', ['errors' => $errors]) !!}
        {!! Html::formGroup('Количество сотрудников', 'employees', $profile->employees, 'select', ['errors' => $errors, 'list' => $employeesList]) !!}
        {!! Html::formGroup('Адрес', 'address', $profile->address, 'text', ['errors' => $errors]) !!}
        {!! Html::formGroup('Телефон', 'phone', $profile->phone, 'text', ['errors' => $errors, 'class' => "input-medium bfh-phone", 'data-format' => "+d (ddd) ddd-dddd"]) !!}
    </div>
    <div class="content-element-box-item">
        {!! Html::formGroup('Сохранить', null, null, 'save') !!}
    </div>
</form>
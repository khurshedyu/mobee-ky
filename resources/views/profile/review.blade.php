@extends('layouts.page')

@section('title', 'Отзыв')

@section('content')
    <div class="content-element-box">
        <h2>Отзыв</h2>
    </div>
    <div class="content-element-box">
        <div class="content-element-box-item">
            <ul>
                <li>
                    Исполнитель: <a href="{{ route('profile::view', ['name' => $performer->linkName()]) }}">{{ $performer->displayName() }}</a>
                </li>
                <li>
                    Стоимость сделки: {{ $deal->price }} тг.
                </li>
            </ul>
        </div>
        @include('layouts.panel_success')
        @include('layouts.panel_error')
        <form class="form" method="POST">
            <div class="content-element-box-item">
                <h3>Качество исполнения</h3>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_quality" value="{{ \App\Review::PARAM_BAD }}" />
                        Плохо
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_quality" value="{{ \App\Review::PARAM_MIDDLING }}" />
                        Удовлетворительно
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_quality" value="{{ \App\Review::PARAM_GOOD }}" checked />
                        Качественно
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_quality" value="{{ \App\Review::PARAM_SUPER }}" />
                        Супер! Лучше или больше, чем договаривались.
                    </label>
                </div>
            </div>
            <div class="content-element-box-item">
                <h3>Срок</h3>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_time" value="{{ \App\Review::PARAM_BAD }}" />
                        Срок сорван
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_time" value="{{ \App\Review::PARAM_MIDDLING }}" />
                        Незначительное опоздание
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_time" value="{{ \App\Review::PARAM_GOOD }}" checked />
                        Точно в срок
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_time" value="{{ \App\Review::PARAM_SUPER }}" />
                        Раньше срока
                    </label>
                </div>
            </div>
            <div class="content-element-box-item">
                <h3>Уровень вежливости</h3>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_civility" value="{{ \App\Review::PARAM_BAD }}" />
                        Нецензурная или грубая лексика
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_civility" value="{{ \App\Review::PARAM_MIDDLING }}" />
                        Нейтрально
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_civility" value="{{ \App\Review::PARAM_GOOD }}" checked />
                        Доброжелательно
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="param_civility" value="{{ \App\Review::PARAM_SUPER }}" />
                        Супервежливо
                    </label>
                </div>
            </div>
            <div class="content-element-box-item">
            {!! Html::formGroup('Комментарий', 'content', request()->old('content'), 'textarea', ['errors' => $errors, 'required' => true]) !!}
            </div>
            <div class="content-element-box-item">
                {!! Html::formGroup('Сохранить', null, null, 'save') !!}
            </div>
        </form>
    </div>
@endsection
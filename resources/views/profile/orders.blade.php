@extends('layouts.page')

@section('title', 'Мои заказы')

@section('content')
    <div class="content-element-box">
        <h2>Мои заказы</h2>
    </div>
    @include ('orders._list_orders')
    @if ($orders->lastPage() > 1)
        <div class="content-element-box">
            {!! $orders->links() !!}
        </div>
    @endif
@endsection
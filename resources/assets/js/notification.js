var Notification = {
    lastId: null,
    checkInterval: null,
    soundS: null,
    soundM: null,
    init: function () {
        var me = this;
        
        me.lastId = $('#dashboard-notifications').data('last');

        if (me.lastId) {
            // console.log('LAST ID', me.lastId);

            me.checkInterval = setTimeout(function () {
                // console.log('check by id', me.lastId);
                $.ajax('/webapi/notifications/check/' + me.lastId)
                    .done(function (res) {
                        if (!AjaxResponse.isValid(res)) {
                            return false;
                        }

                        var data = res.data;

                        if (data.length) {
                            me.soundS.play();
                        }

                        for (var i = 0; i < data.length; i++) {
                            if (data[i].id > me.lastId) {
                                me.lastId = data[i].id;
                            }

                            var item = '<li><div class="dashboard-notifications-item"><a href="#" class="closebtn"><i class="mobee-cancel-2"></i></a><a href="' + data[i].data.url + '" class="message">' + data[i].data.message + '</a><small class="blend-text">' + data[i].date + '</small></div></li>';
                            $('.widget-notifications ul').append(item);
                        }
                    })
                    .fail(function (data) {
                        console.log('FAIL', data);
                    });
            }, 3000);
        }

        me.soundS = new Audio('/static/sound/S.mp3');
        me.soundM = new Audio('/static/sound/M.mp3');
    },
    bind: function () {
        var me = this;

        $('.widget-notifications ul').on('click', '.closebtn', function (e) {
            e.preventDefault();
            $(this).parent().parent().remove();
        });
    },
    run: function () {
        // console.log('RUN NOTIFICATION');
        var me = this;

        me.init();
        me.bind();
    }
};

$(document).ready(function () {
    // Notification.run();
});
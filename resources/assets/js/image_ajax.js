var ImageAjax = {
    imageUploaded: function (res, dataImg, dataHidden) {
        console.log('IMAGE UPLOADED', res, dataImg, dataHidden);

        if (res.error === undefined || res.error !== 0) {
            return false;
        }

        var imgId = '#' + dataImg;
        var imgField = $(imgId);
        
        var hiddenId = '#' + dataHidden;
        var hiddenField = $(hiddenId);

        hiddenField.val(res.data.id);
        
        imgField.attr('src', res.data.path);
        imgField.show();

        return true;
    },
    bind: function () {
        var me = this;

        $('.image-ajax-input').on('change', function (e) {
            if (!$(this)[0].files[0]) {
                return false;
            }

            var dataImg = $(this).data('img');
            var dataBtn = $(this).data('btn');
            var dataHidden = $(this).data('hidden');

            var formData = new FormData();
            formData.append('file', $(this)[0].files[0]);

            var btn = $('#' + dataBtn);
            btn.hide();

            $.ajax({
                url : '/uploadimg',
                type : 'POST',
                data : formData,
                processData: false,
                contentType: false
            }).done(function(res) {
                me.imageUploaded(res, dataImg, dataHidden);
            }).fail(function(res) {
            }).always(function(res) {
                console.log('IMG RESUEST END');
                btn.show();
            });
        });

        $('.image-ajax-btn').on('click', function (e) {
            e.preventDefault();

            var input = $('#' + $(this).data('id'));
            input.click();

            return false;
        });
    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    ImageAjax.run();
});
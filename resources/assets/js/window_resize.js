var WindowResize = {
    subscribes: [],
    subscribe: function (callback) {
        var me = this;

        me.subscribes.push(callback);
    },
    run: function () {
        var me = this;
        var params = {
            width: $(window).width(),
            height: $(window).height()
        };

        for (var i = 0; i < me.subscribes.length; i++) {
            me.subscribes[i](params);
        }

        // console.log('WINDOW RESIZE RUN', params);
    }
};

$(document).ready(function () {
    WindowResize.run();
    $(window).resize(function () {
        WindowResize.run();
    });
});
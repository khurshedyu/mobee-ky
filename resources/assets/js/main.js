var Main = {
    priceRequestDisabler: function () {
        var element = $('#form_element_order_price_request_field');
        var field = $('#form_element_order_price_field');

        if (element.length && field.length) {
            if (element[0].checked) {
                field[0].disabled = true;
            } else {
                field[0].disabled = false;
            }
        }
    },
    orderCategorySelectItem: function (item) {
        var me = this;
        var box = $(item);
        var input = box.find('.order-category-select-input');
        var currentId = input.val();

        var updateCategories = function (categoryId) {
            box.loadBlockStart();

            $.ajax('/ads/ordersectioninfo/' + categoryId).done(function (res) {
                if (!AjaxResponse.isValid(res)) {
                    return false;
                }

                box.find('.order-category-select-section select').val(res.data.section_id);

                box.find('.order-category-select-category select').selectLoad(res.data.categories);
                box.find('.order-category-select-category select').val(res.data.category_id);
                box.find('.order-category-select-category').show();

                box.find('.order-category-select-category-sub select').selectLoad(res.data.sub);
                box.find('.order-category-select-category-sub select').val(res.data.sub_id);
                box.find('.order-category-select-category-sub').show();

                if (res.data.sub.length) {
                    box.find('.order-category-select-category-sub').hide();
                }
            }).fail(function (res) {
                alert('Ошибка загрузки категорий');
            }).always(function (res) {
                box.loadBlockStop();
            });
        };

        if (currentId) {
            updateCategories(currentId);
        }

        box.find('.order-category-select-section select').on('change', function () {
            if (!$(this)) {
                return false;
            }

            var select = $(this);
            var currentSection = select.val();

            input.val(0);

            box.loadBlockStart();

            $.ajax('/ads/ordersection/' + currentSection).done(function (res) {
                if (!AjaxResponse.isValid(res)) {
                    return false;
                }

                box.find('.order-category-select-category select').selectLoad(res.data.categories);
                box.find('.order-category-select-category select').val(0);
                box.find('.order-category-select-category').show();

                if (res.data.categories.length) {
                    box.find('.order-category-select-category').hide();
                }

                box.find('.order-category-select-category-sub select').selectLoad(res.data.sub);
                box.find('.order-category-select-category-sub select').val(0);
                box.find('.order-category-select-category-sub').hide();
            }).fail(function (res) {
                alert('Ошибка загрузки категорий');
            }).always(function (res) {
                box.loadBlockStop();
            });
        });

        box.find('.order-category-select-category select').on('change', function () {
            var select = $(this);
            var currentCategory = parseInt(select.val());

            console.log('CAT CHANGE', currentCategory);
            input.val(currentCategory);

            if (!currentCategory) {
                box.find('.order-category-select-category-sub').hide();
                return false;
            }

            updateCategories(currentCategory);
        });

        box.find('.order-category-select-category-sub select').on('change', function () {
            var select = $(this);
            var currentCategory = parseInt(select.val());

            if (!currentCategory) {
                currentCategory = box.find('.order-category-select-category select').val();
            }

            console.log('SUB CHANGE', currentCategory);
            input.val(currentCategory);

            // updateCategories(currentCategory);
        });
    },
    orderCategorySelect: function () {
        var me = this;
        var box = $('.order-category-select');

        for (var i = 0; i < box.length; i++) {
            me.orderCategorySelectItem(box[i]);
        }
    },
    bind: function () {
        var me = this;

        $('#main-slider').on('click', function (e) {
            e.preventDefault();

            var content = $('#main-slider-content');

            Modal.sizeL('Давайте знакомиться', content.html());
        });

        $('#form_element_order_price_request_field').on('change', function (e) {
            me.priceRequestDisabler();
        });

        me.orderCategorySelect();
    },
    run: function () {
        var me = this;

        me.bind();
        me.priceRequestDisabler();
    }
};

$(document).ready(function () {
    Main.run();
});
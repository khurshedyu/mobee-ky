var Wrap = {
    select: function (wrap) {
        var me = this;
        
        $('#controls a').removeClass('active');
        $('#controls a#controls-' + wrap).addClass('active');
        $('#wrap .wrap-item').removeClass('wrap-active');
        $('#wrap #wrap-' + wrap).addClass('wrap-active');
    },
    bind: function () {
        var me = this;

        $('#controls a').on('click', function (e) {
            e.preventDefault();
            me.select($(this).data('wrap'));
        });

        $(".nano").nanoScroller();
    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    Wrap.run();
});
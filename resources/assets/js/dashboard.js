var Dashboard = {
    init: function () {
        var me = this;

        $('.dashboard-demo-button a').on('click', function (e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).html($(this).data('active'));
                $(this).parent().parent().find('.dashboard-noauth-main').hide();
                $(this).parent().parent().find('.dashboard-noauth-demo').show();
            } else {
                $(this).addClass('active');
                $(this).html($(this).data('unactive'));
                $(this).parent().parent().find('.dashboard-noauth-main').show();
                $(this).parent().parent().find('.dashboard-noauth-demo').hide();
            }
        });
    }, 
    run: function () {
        var me = this;

        me.init();
    }
};

$(document).ready(function () {
    Dashboard.run();
});
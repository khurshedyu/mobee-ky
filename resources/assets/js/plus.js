var Plus = {
    openPlus: function (key) {
        // console.log('OPEN PLUS', key);
        Modal.loader(true);
        $.ajax('/plus/' + key)
            .done(function (data) {
                Modal.loader(false);
                // console.log('GET PLUS', data);

                if (data.title && data.content) {
                    Modal.sizeM(data.title, data.content);
                }
            })
            .fail(function (data) {
                Modal.loader(false);
                // console.log('GET PLUS', data);
            });

        //Modal.sizeM(key, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,vquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
    },
    bind: function () {
        var me = this;

        $('body').on('click', '.plus', function (e) {
            e.preventDefault();
            var key = $(this).data('key');
            if (key) {
                me.openPlus(key);
            }
        });
    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    Plus.run();
});
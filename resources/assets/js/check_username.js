$.fn.checkUsername = function () {
    var element = $(this);
    var label = element.parent().find('label');
    var lastKeyUp = Date.now();
    var lastUpdate = Date.now();
    var delay = 500;
    var defaultName = element.data('format');
    var queryCount = 0;

    var minSymbol = function () {
        label.find('span.check-username').remove();
        label.append('<span class="check-username check-username-busy">(минимум 5 символов)</span>');
        return;
    };

    element.on('keyup', function (e) {
        console.log('ELEMENT LENGTH', element.val().length);

        if (element.val().length < 5) {
            return minSymbol();
        }

        lastKeyUp = Date.now();

        setTimeout(function () {
            var now = Date.now();

            if ((now - lastKeyUp) > delay && (now - lastUpdate) > delay) {
                lastUpdate = Date.now();
                var value = element.val();
                console.log('GO', value);

                if (element.val().length < 5) {
                    return minSymbol();
                }

                label.find('span.check-username').remove();
                label.append('<span class="check-username check-username-process">(проверка)</span>');

                queryCount++;
                $.ajax('/webapi/username/check/' + value + '/' + defaultName)
                    .done(function (res) {
                        queryCount--;

                        if (queryCount) {
                            return;
                        }

                        label.find('span.check-username').remove();
                        if (AjaxResponse.isValid(res)) {
                            if (element.val().length < 5) {
                                return minSymbol();
                            }
                            
                            if (res.data.busy) {
                                label.append('<span class="check-username check-username-busy">(занят)</span>');
                            } else if (!res.data.clean) {
                                label.append('<span class="check-username check-username-busy">(недопустимые символы)</span>');
                            } else {
                                label.append('<span class="check-username check-username-free">(свободен)</span>');
                            }
                        } else {
                            label.append('<span class="check-username check-username-busy">(недопустимые символы)</span>');
                        }
                    })
                    .fail(function () {
                        queryCount--;
                        label.find('span.check-username').remove();
                        label.append('<span class="check-username check-username-busy">(недопустимые символы)</span>');
                    });
            }
        }, delay + 50);

        // console.log('CHANHGE', $(this).val());
    });
};
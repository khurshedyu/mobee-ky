var Work = {
    substr: function (string, key, val) {
        return string.split(key).join(val);
    }
};

$.fn.selectLoad = function (data) {
    var element = $(this);

    element.html('');

    for (var key in data) {
        element.append('<option value="' + key + '">' + data[key] + '</option>');
    }
};

$.fn.loadBlockStart = function (params) {
    var element = $(this);

    element.prepend('<div style="position:absolute; top:0; left:0; right:0; bottom:0; background:rgba(255,255,255,0.7); z-index:1000;" class="load-block"><div style="position:absolute; top:50%; left:0; right:0; text-align:center; margin-top:-20px; line-height:40px; color:#333;">Загрузка...</div></div>');
};

$.fn.loadBlockStop = function () {
    var element = $(this);

    element.find('.load-block').remove();
};
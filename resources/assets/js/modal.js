var Modal = {
    MODAL_TYPE_S: 's',
    MODAL_TYPE_M: 'm',
    MODAL_TYPE_L: 'l',
    loader: function (show) {
        if (show) {
            $('.mobeemodal-loader').show();
            $('.mobeemodals > ul').hide();
        } else {
            $('.mobeemodal-loader').hide();
            $('.mobeemodals > ul').show();
        }
    },
    checkBodyBlock: function () {
        var me = this;
        var container = $('#modals > ul > li');

        if (container.length) {
            $('body').addClass('blocked');
        } else {
            $('body').removeClass('blocked');
        }

        // console.log('CHECK', container.length);
    },
    showModal: function (type, title, content, options) {
        var me = this;
        var data = {};

        if (typeof options == 'object') {
            for (var key in options) {
                data[key] = options[key];
            }
        }

        var container = $('#modals > ul');
        var modal = $('#mobeemodal-template').clone().html();
        var li = $('<li></li>');

        // console.log('ASDASDASD', type);

        modal = Work.substr(modal, '[[title]]', title);
        modal = Work.substr(modal, '[[content]]', content);
        // console.log('MODAL', modal);

        modal = $(modal);
        modal.find('.mobeemodal-window').addClass('mobeemodal-window-' + type);
        // console.log('MODAL', modal);

        li.append(modal);
        container.prepend(li);

        me.checkBodyBlock();
    },
    sizeS: function (title, content, options) {
        var me = this;

        me.showModal(me.MODAL_TYPE_S, title, content, options);
    },
    sizeM: function (title, content, options) {
        var me = this;

        me.showModal(me.MODAL_TYPE_M, title, content, options);
    },
    sizeL: function (title, content, options) {
        var me = this;

        me.showModal(me.MODAL_TYPE_L, title, content, options);
    },
    bind: function () {
        var me = this;

        $('#modals > ul').on('click', 'li .mobeemodal-header-close-btn', function (e) {
            e.preventDefault();
            $(this).parent().parent().parent().parent().parent().remove();
            me.checkBodyBlock();
        });

        // WindowResize.subscribe(function (params) {
        //     $('.mobeemodal-bg').width(params.width);
        //     $('.mobeemodal-bg').height(params.height);
        //     $('.mobeemodals li').width(params.width);
        //     $('.mobeemodals li').height(params.height);
        // });

    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    Modal.run();
});
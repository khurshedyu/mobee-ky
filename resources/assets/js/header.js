var Header = {
    bind: function () {
        var me = this;

        $('#header-links-more').on('click', function (e) {
            e.preventDefault();

            $('#meloman-list').toggle();

            // var content = $('#header-links-more-content');

            // Modal.sizeS('Все проекты', content.html());
        });

        $('#header-menu-more').on('click', function (e) {
            e.preventDefault();

            $('#menu-down').toggle();

            // alert('WUT');
            return false;

            // var liList = $(this).parent().parent().find('ul li a');

            // var content = '<div class="row"><div class="btn-group-vertical col-xs-12 col-sm-12" role="group">';
            // for (var i = 0; i < liList.length; i++) {
            //     var item = $(liList[i]);
            //     content += '<a href="' + item.attr('href') + '" class="btn btn-lg btn-default">' + item.html() + '</a>';
            // }
            // content +='</div></div>';

            // Modal.sizeS('Все сферы', content);
        });

        $('#links-more-btn').on('click', function (e) {
            e.preventDefault();

            $('#links-more-list').toggle();
        });

        // console.log('BIND LINKS UL RESIZE');
        WindowResize.subscribe(function (params) {
            // console.log('LINKS UL RESIZE');
            if (params.width < 1150) {
                $('#links-ul').hide();
                $('#links-more').show();
            } else {
                $('#links-ul').show();
                $('#links-more').hide();
            }
        });
    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    Header.run();
});
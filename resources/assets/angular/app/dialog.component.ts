import {Component, Input, Directive, Attribute, NgZone} from 'angular2/core';
import {NgClass} from 'angular2/common';
// import {Http, Headers} from 'angular2/http';
// import {LifeCycle} from 'angular2/core';
// import {Injectable} from 'angular2/core';
// import {Http, HTTP_PROVIDERS} from "angular2/http";
import {bootstrap} from 'angular2/platform/browser';

interface Message {
    id: number;
    avatar: string;
    name: string;
    nameUrl: string;
    content: string;
    date: any;
    type: string;
    params: any;
}

interface ProfileData {
    id: number;
    avatar: string;
    name: string;
    nameUrl: string;
    url: string;
}

var socket: any;
declare var io: any;
declare var window: any;
declare var $: any;

@Component({
    selector: 'app-dialog',
    templateUrl: '/html/dialog.component.html',
    directives: [NgClass]
})

export class DialogComponent {
    public messages: Message[] = [];
    public currentMessage: string;
    public currentPrice: string = '';
    public currentProfileData: ProfileData;
    public dialogId: number;
    public starterId: number;
    public token: string;
    public deal: any;
    public isoffer: any;
    public priceConfirmed: boolean = false;
    public priceBtnClassMap: any = {
        'btn-default': !this.priceConfirmed,
        'btn-success': this.priceConfirmed
    };
    public isDeal: boolean = false;
    private ngz: NgZone;
    private dialogInited: boolean = false;
    // private http: Http;

    constructor(ngz: NgZone) {
        socket = io(':3000');
        this.currentProfileData = window.jsonData.profile;
        this.dialogId = window.jsonData.dialogId;
        this.starterId = window.jsonData.starterId;
        this.token = window.jsonData.token;
        this.deal = window.jsonData.deal;
        this.isoffer = window.jsonData.isoffer;
        this.ngz = ngz;
        // this.http = http;

        this.initSockets();
        this.initDialog();
        // this.initToken();
        console.log('JSON DATA', window.jsonData, this.currentProfileData);
    }

    ngDoCheck () {
        // console.log('DO CHECK');
    }

    ngAfterContentInit() {
        // this.initStartMessages();
    }

    private initDialog = () => {
        socket.emit('init_dialog', this.dialogId, this.currentProfileData.id);

        if (this.deal) {
            this.currentPrice = this.deal.price;
            this.isDeal = true;
        }
    };

    private initToken = () => {
        socket.emit('token', this.token);
    };

    private initSockets = function() {
        socket.on('message', (message) => {
            console.log('MESSAGE FROM SERVER', message);
            
            this.ngz.run(() => {
                this.pushMessage(message);
            });
        });

        socket.on('messages', (messages) => {
            this.ngz.run(() => {
                this.pushMessages(messages);
            });
        });

        socket.on('system', (data) => {
            console.log('SERVER DATA', data);
            
            this.ngz.run(() => {
                this.systemMessage(data);
            });
        });

        socket.on('need_token', () => {
            this.initToken();
        });

        socket.on('token_success', () => {
            if (!this.dialogInited) {
                this.initDialog();
            }
        });

        socket.on('dialog_inited', () => {
            if (!this.dialogInited) {
                this.dialogInited = true;
                this.initStartMessages();
            }
        });

        socket.on('deal_price_sync', (price) => {
            this.ngz.run(() => {
                this.priceBtnClassMap['btn-success'] = false;
                this.priceBtnClassMap['btn-default'] = true;
                this.priceConfirmed = false;
                this.currentPrice = price;
            });
        });

        socket.on('deal_close', (price) => {
            this.ngz.run(() => {
                this.currentPrice = price;
                this.isDeal = true;
            });
        });
    };

    private initStartMessages = function() {
        // var uri = '/api/chat/messages/' + this.dialogId + '/1';
        // var ngz = this.ngz;
        // var pushMessage = this.pushMessage;

        socket.emit('old_messages', {
            dialog: this.dialogId,
            from: 1
        });

        // $.ajax(uri).done(function(data) {
        //     console.log(data);
        //     if (data.code == 0) {
        //             console.log('MESSAGES BRO WUT', data.data.messages);
        //             for (var i in data.data.messages) {
        //                 var message: Message = {
        //                     id: data.data.messages[i].id,
        //                     avatar: data.data.messages[i].avatar,
        //                     name: data.data.messages[i].name,
        //                     nameUrl: data.data.messages[i].nameUrl,
        //                     content: data.data.messages[i].content,
        //                     date: null,
        //                     type: 'message'
        //                 };
        //                 console.log('TRY PUSH', data.data.messages[i]);
        //                 pushMessage(message);
        //             }
        //         ngz.run(() => {
        //             console.log('NG RUN');
        //         });
        //     }
        // });

        // this.http.get(uri)
        //     .subscribe(
        //         data => console.log('DATA', data),
        //         err => console.log('ERR', err),
        //         () => console.log('Random Quote Complete')
        //     );
    };

    public clickContent = function(message) {
    };

    public priceChange = () => {
        console.log('PRICE', this.currentPrice);
        this.priceBtnClassMap['btn-success'] = false;
        this.priceBtnClassMap['btn-default'] = true;
        this.priceConfirmed = true;
        socket.emit('deal_price', this.currentPrice);
    };

    public priceConfirm = () => {
        this.priceBtnClassMap['btn-success'] = true;
        this.priceBtnClassMap['btn-default'] = false;
        this.priceConfirmed = true;
        socket.emit('deal_price_confirm', this.currentPrice);
    };

    public sendMessage = function() {
        if (!this.currentMessage) {
            return false;
        }

        var message: Message = {
            id: this.currentProfileData.id,
            avatar: this.currentProfileData.avatar,
            name: this.currentProfileData.name,
            nameUrl: this.currentProfileData.nameUrl,
            content: this.currentMessage,
            date: 'date',
            type: 'message',
            params: null
        };

        // socket.emit('message', message);
        socket.emit('send_message', {
            dialog: this.dialogId,
            profile: this.currentProfileData.id,
            message: this.currentMessage
        });
        // this.pushMessage(message);

        console.log('CURRENT MESSAGE', this.currentMessage);
        // socket.emit('message', this.currentMessage);
        this.currentMessage = '';
    };

    public systemMessage = function(data) {
        var message: Message = {
            id: null,
            avatar: null,
            name: null,
            nameUrl: null,
            content: data,
            date: null,
            type: 'system',
            params: null
        };

        this.pushMessage(message);
    };

    public pushMessage = function(message) {
        this.messages.push(message);
        this.scrollToBottom();
    };

    public pushMessages = function(messages) {
        for (var i in messages) {
            this.messages.push(messages[i]);
        }

        setTimeout(() => {
            this.scrollToBottom();
        }, 500);
    };

    public scrollToBottom = function() {
        $("html, body").animate({ scrollTop: $(document).height() }, 500);
    };

    public textKeyPress = function(event) {
        if (event.charCode == 13 && !event.shiftKey) {
            this.sendMessage();
            return false;
        }
    };
}

bootstrap(DialogComponent);
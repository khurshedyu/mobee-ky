import {Component} from 'angular2/core';
import {Injectable} from 'angular2/core';
import {DialogComponent} from './dialog.component';

@Component({
    selector: 'my-app',
    template: `
        <h1>My First Angular 2 App {{title}}</h1>
    `
})

//@Injectable()
export class AppComponent {
}
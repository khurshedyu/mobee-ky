<?php

return [
    'max' => [
        'numeric' => 'Не должно быть больше :max.',
        'file'    => 'Не должно быть больше, чем :max КБ.',
        'string'  => 'Не должно содержать больше :max символов.',
        'array'   => 'Не должно содержать больше :max элементов.',
    ],
    'min' => [
        'numeric' => 'Не должно быть меньше :min.',
        'file'    => 'Не должно быть меньше, чем :min КБ.',
        'string'  => 'Не должно содержать меньше :min символов.',
        'array'   => 'Не должно содержать меньше :min элементов.',
    ],
    'required' => 'Это поле не может быть пустым',
    'numeric' => 'Это поле должно быть числом.',
];
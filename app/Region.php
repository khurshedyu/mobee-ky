<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'region';
    protected $primaryKey = 'region_id';

    /**
     * Scopes
     */

    public function scopeByCountry($query, $id)
    {
        $query->where('country_id', $id);

        return $query;
    }

    /**
     * BELONGS TO
     */

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'country_id');
    }

    /**
     * HAS MANY
     */

    public function cities()
    {
        return $this->hasMany('App\City', 'region_id', 'region_id');
    }
}

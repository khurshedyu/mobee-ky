<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayTransaction extends Model
{
    const TYPE_NONE = 0;
    const TYPE_PAY_INVOICE = 1;
    const TYPE_BUY_SUPER = 2;

    static public function lastUserTransactions($id, $perPage = null)
    {
        $q = self::byUserId($id)
            ->sortLast();

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    public function typeString()
    {
        if ($this->type == self::TYPE_PAY_INVOICE) {
            return 'Зачисление средств';
        }

        if ($this->type == self::TYPE_BUY_SUPER) {
            return 'Оплата супераккаунта';
        }

        return '–';
    }

    /**
     * SCOPES
     */

    public function scopeByUserId($q, $id)
    {
        $q->where('user_id', $id);

        return $q;
    }

    public function scopeSortLast($q)
    {
        $q->orderBy('created_at', 'desc');

        return $q;
    }
}

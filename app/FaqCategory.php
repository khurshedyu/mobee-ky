<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    /**
     * SCOPES
     */

    public function scopeStart($query)
    {
        $query->with('items');

        return $query;
    }

    /**
     * HAS MANY
     */

    public function items()
    {
        return $this->hasMany('App\FaqItem', 'category_id', 'id');
    }
}

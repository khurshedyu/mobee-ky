<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RegisterRequest extends Model
{
    const STATUS_NEW = 0;
    const STATUS_ACTIVATED = 1;

    static public function createRequest($email, $password)
    {
        $expiredDate = Carbon::now()->addDays(7)->endOfDay();

        $regReq = new self;
        $regReq->email = $email;
        $regReq->password = $password;
        $regReq->key = md5(microtime()) . md5($email);
        $regReq->status = self::STATUS_NEW;
        $regReq->expired_at = $expiredDate;
        $regReq->save();

        return $regReq;
    }

    static public function findActiveByKey($key)
    {
        $regReq = self::where('key', $key)
            ->where('status', self::STATUS_NEW)
            ->whereRaw('expired_at > NOW()')
            ->first();

        return $regReq;
    }

    public function activated()
    {
        $this->status = self::STATUS_ACTIVATED;
        $this->save();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Super extends Model
{
    // const SUPER_PACK_24 = 5;
    // const SUPER_PACK_12 = 4;
    const SUPER_PACK_6 = 3;
    const SUPER_PACK_3 = 2;
    const SUPER_PACK_1 = 1;

    // const SUPER_PACK_COST_24 = 57600;
    // const SUPER_PACK_COST_12 = 33600;
    const SUPER_PACK_COST_6 = 19200;
    const SUPER_PACK_COST_3 = 10800;
    const SUPER_PACK_COST_1 = 4000;

    // const SUPER_PACK_MONTHS_24 = 24;
    // const SUPER_PACK_MONTHS_12 = 12;
    const SUPER_PACK_MONTHS_6 = 6;
    const SUPER_PACK_MONTHS_3 = 3;
    const SUPER_PACK_MONTHS_1 = 1;

    static public function createSuper($id, $userId)
    {
        $until = self::untilByUser($userId);
        $months = self::monthsBySuperId($id);
        $startDate = Carbon::now();

        if ($until['end_at']) {
            $startDate = $until['end_at'];
        }

        $startDate = Carbon::parse($startDate)->startOfDay();
        $endDate = Carbon::parse($startDate)->addMonths($months)->endOfDay();

        $super = new self;
        $super->start_at = $startDate;
        $super->end_at = $endDate;
        $super->user_id = $userId;
        $super->save();

        $user = User::findOrFail($userId);
        $user->super_until = $endDate;
        $user->save();

        return $super;
    }

    static public function untilByUser($userId)
    {
        $items = self::currentByUser($userId)
            ->orderBy('start_at', 'asc')
            ->get();

        $startAt = null;
        $endAt = null;
        $currentDay = Carbon::today();
        $percent = 0;
        $daysleft = 0;
        $minutesleft = 0;

        if (count($items)) {
            foreach ($items as $item) {
                $itemStartAt = new Carbon($item->start_at);
                $itemEndAt = new Carbon($item->end_at);

                if (!$startAt OR $startAt->timestamp > $itemStartAt->timestamp) {
                    $startAt = $itemStartAt;
                }

                if (!$endAt OR $endAt->timestamp < $itemEndAt->timestamp) {
                    $endAt = $itemEndAt;
                }
            }

            $secondsUsed = $startAt->diffInSeconds(Carbon::now());
            $secondsAll = $startAt->diffInSeconds($endAt);
            $percent = 100 - (100 / $secondsAll * $secondsUsed);
            $daysleft = round(($secondsAll - $secondsUsed) / 86400, 1);
            $minutesleft = round(($secondsAll - $secondsUsed) / 60);

            // Commented out, because days are not precise
            //$daysUsed = $startAt->diffInDays($currentDay);
            //$daysAll = $startAt->diffInDays($endAt);
            //$percent = 100 - (100 / $daysAll * $daysUsed);
        }

        return [
            'percent' => ceil($percent),
            'start_at' => $startAt,
            'end_at' => $endAt,
            'days' => $daysleft,
            'minutes' => $minutesleft,
            //'days' => round(($secondsAll - $secondsUsed) / 86400, 1),
            //'days' => ($currentDay !== null) ? $currentDay->diffInDays($endAt) : 0,
        ];
    }

    static public function monthsBySuperId($id)
    {
        $months = self::SUPER_PACK_MONTHS_1;

        switch ($id) {
            // case self::SUPER_PACK_24:
            //     $months = self::SUPER_PACK_MONTHS_24;
            //     break;
            // case self::SUPER_PACK_12:
            //     $months = self::SUPER_PACK_MONTHS_12;
            //     break;
            case self::SUPER_PACK_6:
                $months = self::SUPER_PACK_MONTHS_6;
                break;
            case self::SUPER_PACK_3:
                $months = self::SUPER_PACK_MONTHS_3;
                break;
            case self::SUPER_PACK_1:
                $months = self::SUPER_PACK_MONTHS_1;
                break;
        }

        return $months;
    }

    static public function costBySuperId($id)
    {
        $cost = self::SUPER_PACK_COST_1;

        switch ($id) {
            // case self::SUPER_PACK_24:
            //     $cost = self::SUPER_PACK_COST_24;
            //     break;
            // case self::SUPER_PACK_12:
            //     $cost = self::SUPER_PACK_COST_12;
            //     break;
            case self::SUPER_PACK_6:
                $cost = self::SUPER_PACK_COST_6;
                break;
            case self::SUPER_PACK_3:
                $cost = self::SUPER_PACK_COST_3;
                break;
            case self::SUPER_PACK_1:
                $cost = self::SUPER_PACK_COST_1;
                break;
        }

        return $cost;
    }

    /**
     * SCOPES
     */

    public function scopeCurrentByUser($q, $userId)
    {
        $q->where('user_id', $userId);
        // $q->whereRaw('start_at <= NOW()');
        $q->whereRaw('end_at >= NOW()');


        return $q;
    }
}

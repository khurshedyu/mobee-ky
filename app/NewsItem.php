<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsItem extends Model
{
    const ARCHIVE_NO = 0;
    const ARCHIVE_YES = 1;

    static public function lastPublished($perPage = null)
    {
        $q = self::published()
            ->last();

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    static public function getBesideNews($id, $max)
    {
        return self::published()
            ->without($id)
            ->last()
            ->take($max)
            ->get();
    }

    /**
     * SCOPES
     */

    public function scopeLast($query)
    {
        $query->orderBy('published_at', 'DESC');
        
        return $query;
    }

    public function scopeWithout($query, $id)
    {
        $query->where('id', '<>', $id);
        
        return $query;
    }

    public function scopePublished($query)
    {
        $query->where('archive', self::ARCHIVE_NO);
        $query->whereRaw('published_at < NOW()');
        
        return $query;
    }
}

<?php

namespace App;

use App\Components\Model;
use App\OrderRequest;
use Validator;
use DB;

class Order extends Model
{
    const IS_ORDER = 0;
    const IS_OFFER = 1;

    const STATUS_NEW = 0;
    const STATUS_PUBLIC = 1;
    const STATUS_BLOCK = 2;

    const ORDER_BY_RANDOM = 1;

    protected $fillable = [
        'title',
        'content',
        'price',
        'price_request',
        'only_super',
        'only_checked',
        'city_id',
        'category_id',
        'isoffer',
        'image_id',
    ];

    protected $defaultValues = [
        'price_request' => 0,
        'only_super' => 0,
        'only_checked' => 0,
    ];

    /**
     * Validators
     */

    static protected $validators = [
        'title' => 'required|max:255',
        'content' => 'required|min:10',
        'price' => 'numeric',
        'price_request' => 'boolean',
        'only_super' => 'boolean',
        'only_checked' => 'boolean',
        'city_id' => 'required|integer|min:1',
        'category_id' => 'required|integer|min:1',
        'isoffer' => 'required|integer',
        'image_id' => 'integer',
    ];

    static public function makeValidator($fields, $data, $options = [])
    {
        parent::$validators = self::$validators;
        return parent::makeValidator($fields, $data, $options);
    }

    /**
     * Methods
     */

    static public function getPublicOrder($id)
    {
        return self::where('id', $id)
            ->published()
            ->first();
    }

    static public function createOrder($data, $profileId)
    {
        $order = new self;
        $order->readData($data);
        $order->profile_id = $profileId;

        if (! (float) $order->price) {
            $order->price_request = 1;
            $order->price = 0;
        }

        $order->save();

        return $order;
    }

    static public function getOrdersByCategories($categoryIds, $filters = null, $params = null, $perPage = null)
    {
        $q = self::whereIn('category_id', $categoryIds);
        $q->published();

        if ($filters AND isset($filters['show'])) {
            if ($params AND isset($params['show_invert'])) {
                $q->parseShow($filters['show'], $params['show_invert']);
            } else {
                $q->parseShow($filters['show']);
            }
        }

        if ($filters) {
            $q->applyFilters($filters);
        }

        if ($params) {
            if (isset($params['order'])) {
                switch ($params['order']) {
                    case self::ORDER_BY_RANDOM:
                        $q->orderByRaw('RAND()');
                        break;
                    default:
                        $q->latest();
                        break;
                }
            } else {
                $q->latest();
            }
        } else {
            $q->latest();
        }

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    static public function getOrdersByProfile($profileId, $perPage = null)
    {
        $q = self::start()
            ->byProfile($profileId)
            ->isOrder();

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    static public function getOffersByProfile($profileId, $perPage = null)
    {
        $q = self::start()
            ->byProfile($profileId)
            ->isOffer();

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    public function updateOrder($data, $profileId)
    {
        $this->readData($data);
        $this->profile_id = $profileId;

        if (! (float) $this->price) {
            $this->price_request = 1;
            $this->price = 0;
        }

        $this->save();

        return $this;
    }

    public function getOrderRequests()
    {
        return OrderRequest::start()
            ->select(DB::raw('order_requests.*, (NOW() < users.super_until) as until'))
            ->rightJoin('profiles', 'profiles.id', '=', 'order_requests.profile_id')
            ->rightJoin('users', 'users.id', '=', 'profiles.user_id')
            ->byOrder($this->id)
            ->orderBy('until', 'DESC')
            ->orderBy('created_at', 'ASC')
            ->get();
    }

    public function imageUrl()
    {
        if ($this->image_id) {
            return '/i/orderimg/' . $this->image->path;
        }

        return config('app.default_bg_url');
    }

    public function statusTitle()
    {
        if ($this->status == self::STATUS_NEW) {
            return 'Новая запись';
        } else if ($this->status == self::STATUS_PUBLIC) {
            return 'Запись опубликована';
        } else if ($this->status == self::STATUS_BLOCK) {
            return 'Запись заблокирована';
        }

        return null;
    }

    public function statusIsNew()
    {
        return $this->status == self::STATUS_NEW;
    }

    public function statusIsPublic()
    {
        return $this->status == self::STATUS_PUBLIC;
    }

    public function statusIsBlock()
    {
        return $this->status == self::STATUS_BLOCK;
    }

    public function setStatusPublic()
    {
        $this->status = self::STATUS_PUBLIC;
    }

    public function setStatusBlock()
    {
        $this->status = self::STATUS_BLOCK;
    }

    public function typeIsOrder()
    {
        return !$this->isoffer;
    }

    public function typeIsOffer()
    {
        return !!$this->isoffer;
    }

    /**
     * BELONGS TO
     */

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'profile_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'city_id');
    }

    public function image()
    {
        return $this->belongsTo('App\File', 'image_id', 'id');
    }

    /**
     * HAS MANY
     */

    public function orderRequests()
    {
        return $this->hasMany('App\OrderRequest', 'order_id', 'id');
    }

    /**
     * Scopes
     */

    public function scopeStart($q)
    {
        $q->with('profile');

        return $q;
    }

    public function scopeApplyFilters($q, $filters)
    {
        return $q;
    }

    public function scopeLatest($q)
    {
        $q->orderBy('created_at', 'DESC');

        return $q;
    }

    public function scopePublished($q)
    {
        $q->where('status', self::STATUS_PUBLIC);

        return $q;
    }

    public function scopeIsOrder($q)
    {
        $q->where('isoffer', self::IS_ORDER);

        return $q;
    }

    public function scopeIsOffer($q)
    {
        $q->where('isoffer', self::IS_OFFER);

        return $q;
    }

    public function scopeByProfile($query, $profile)
    {
        $query->where('profile_id', $profile);

        return $query;
    }

    public function scopeParseShow($q, $show, $invert = false)
    {
        if ($show == 'orders') {
            $q->where('isoffer', $invert ? self::IS_OFFER : self::IS_ORDER);
        } else if ($show == 'offers') {
            $q->where('isoffer', $invert ? self::IS_ORDER : self::IS_OFFER);
        }

        return $q;
    }
}

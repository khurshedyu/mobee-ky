<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class File extends Model
{
    static public function saveUploadedFile($uploadedFile)
    {
        if (!$uploadedFile->isValid()) {
            return null;
        }

        $info = [
            'extension' => $uploadedFile->guessExtension(),
            'type' => $uploadedFile->getMimeType(),
            'size' => $uploadedFile->getClientSize(),
        ];

        $folder = substr(md5(date('Y-m-d')), 0, 16);
        $path = getcwd() . '/i/upload/' . $folder;
        $name = substr(md5(microtime() . 'Wie1Neec'), 0, 16); // Wie1Neec - Is a random string to make it difficult to hack
        $fullName = $name . '.' . $info['extension'];
        $fullPath = $path . '/' . $fullName;
        $publicPath = $folder . '/' . $fullName;

        if(!is_dir($path))
            mkdir($path);

        // Move uploaded file to destination path: $path with filename: $fullName
        $uploadedFile->move($path, $fullName);

        $file = new self;
        $file->path = $publicPath;
        $file->type = $info['type'];
        $file->size = $info['size'];
        $file->save();

        return $file;
    }
}

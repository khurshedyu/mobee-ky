<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqItem extends Model
{
    //

    /**
     * BELONGS TO
     */

    public function category()
    {
        return $this->belongsTo('App\FaqCategory', 'category_id', 'id');
    }
}

<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('orders-edit', function ($user, $order) {
            return in_array($order->profile_id, $user->profileIds());
        });

        $gate->define('order-create-request', function ($user, $order) {
            if ($order->only_super AND !$user->isSuper()) {
                return false;
            }

            foreach ($order->orderRequests as $orderRequest) {
                if (in_array($orderRequest->profile_id, $user->profileIds())) {
                    return false;
                }
            }
            
            return !in_array($order->profile_id, $user->profileIds());
        });

        $gate->define('order-request-create-dialog', function ($user, $orderRequest) {
            if ($orderRequest->dialog) {
                return false;
            }

            if (!in_array($orderRequest->order->profile_id, $user->profileIds())) {
                return false;
            }

            return !in_array($orderRequest->profile_id, $user->profileIds());
        });

        $gate->define('dialog-member', function ($user, $dialog) {
            if ($dialog->isOrderDialog()) {
                if (in_array($dialog->dialogable->profile_id, $user->profileIds())) {
                    return true;
                }

                if (in_array($dialog->dialogable->order->profile_id, $user->profileIds())) {
                    return true;
                }
            }

            return false;
        });
    }
}

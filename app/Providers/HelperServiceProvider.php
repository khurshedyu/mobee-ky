<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Helpers\HtmlHelper;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('htmlHelper', function() {
            return new HtmlHelper;
        });
    }
}

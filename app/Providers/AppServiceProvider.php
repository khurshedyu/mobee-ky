<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Profile;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Profile::saving(function ($profile) {
            $profile->phone = preg_replace("/[^0-9]/", '', $profile->phone);
            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

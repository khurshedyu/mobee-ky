<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    static public function checkConnection($from, $to)
    {
        return self::where(function ($q) use($from, $to) {
            $q->where('from_profile_id', $from);
            $q->where('to_profile_id', $to);
        })->orWhere(function ($q) use($from, $to) {
            $q->where('from_profile_id', $to);
            $q->where('to_profile_id', $from);
        })->first();
    }

    static public function getByProfile($profileId, $perPage = null)
    {
        $q = self::start()
            ->byProfile($profileId)
            ->orderBy('created_at', 'desc');

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    static public function getProfileConnections($id, $perPage = null)
    {
        $q = self::orderBy('created_at', 'desc')
            ->where('from_profile_id', $id)
            ->orWhere('to_profile_id', $id);

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    /**
     * SCOPES
     */

    public function scopeByProfile($q, $id)
    {
        $q->where('from_profile_id', $id)
            ->orWhere('to_profile_id', $id);

        return $q;
    }

    /**
     * BELONGS TO
     */

    public function scopeStart($q)
    {
        return $q;
    }

    public function deal()
    {
        return $this->belongsTo('App\Deal', 'deal_id', 'id');
    }

    public function fromProfile()
    {
        return $this->belongsTo('App\Profile', 'from_profile_id', 'id');
    }

    public function toProfile()
    {
        return $this->belongsTo('App\Profile', 'to_profile_id', 'id');
    }
}

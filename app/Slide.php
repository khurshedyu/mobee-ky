<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    /**
     * SCOPES
     */

    public function scopeToSLide($q)
    {
        $q->where('public', 1);
        $q->orderBy('weight', 'DESC');
        $q->orderBy('id', 'DESC');

        return $q;
    }
}

<?php

namespace App;

use App\Components\Model;
use Validator;

class Review extends Model
{
    const PARAM_BAD = 0;
    const PARAM_MIDDLING = 1;
    const PARAM_GOOD = 2;
    const PARAM_SUPER = 3;

    const REVIEW_ITEM_QUALITY = 'quality';
    const REVIEW_ITEM_TIME = 'time';
    const REVIEW_ITEM_CIVILITY = 'civility';

    const TYPE_DEFAULT = 0;
    const TYPE_OFFER = 1;

    const DICT_DEFAULT = 0;

    protected $fillable = [
        'param_quality',
        'param_time',
        'param_civility',
        'content',
    ];

    protected $defaultValues = [
        'param_quality' => self::PARAM_GOOD,
        'param_time' => self::PARAM_GOOD,
        'param_civility' => self::PARAM_GOOD,
        'content' => null,
    ];

    /**
     * Validators
     */

    static protected $validators = [
        'params_quality' => 'required|integer',
        'param_time' => 'required|integer',
        'param_civility' => 'required|integer',
        'content' => 'required|min:10',
    ];

    static public function makeValidator($fields, $data, $options = [])
    {
        parent::$validators = self::$validators;
        return parent::makeValidator($fields, $data, $options);
    }

    static private $dictionary = [
        self::DICT_DEFAULT => [
            self::TYPE_DEFAULT => [
                self::REVIEW_ITEM_QUALITY => [
                    'title' => 'Качество услуги',
                    'items' => [
                        self::PARAM_BAD => 'Плохо',
                        self::PARAM_MIDDLING => 'Удовлетворительно',
                        self::PARAM_GOOD => 'Хорошее или отличное',
                        self::PARAM_SUPER => 'Супер! Превзошли все ожидания!',
                    ],
                ],
                self::REVIEW_ITEM_TIME => [
                    'title' => 'Срок выполнения',
                    'items' => [
                        self::PARAM_BAD => 'Срок сорван',
                        self::PARAM_MIDDLING => 'Незначительное опоздание',
                        self::PARAM_GOOD => 'Точно в срок',
                        self::PARAM_SUPER => 'Супер! Раньше, чем договаривались!',
                    ],
                ],
                self::REVIEW_ITEM_CIVILITY => [
                    'title' => 'Уровень вежливости',
                    'items' => [
                        self::PARAM_BAD => 'Грубо',
                        self::PARAM_MIDDLING => 'Нейтрально',
                        self::PARAM_GOOD => 'Доброжелательно',
                        self::PARAM_SUPER => 'Супер! Вежливее не бывает!',
                    ],
                ],
            ],
            self::TYPE_OFFER => [
                self::REVIEW_ITEM_QUALITY => [
                    'title' => 'Объем оплаты',
                    'items' => [
                        self::PARAM_BAD => 'Не оплачено',
                        self::PARAM_MIDDLING => 'Ниже оговоренной суммы',
                        self::PARAM_GOOD => 'Точно, как договорились',
                        self::PARAM_SUPER => 'Супер! Больше, чем договаривались!',
                    ],
                ],
                self::REVIEW_ITEM_TIME => [
                    'title' => 'Срок оплаты',
                    'items' => [
                        self::PARAM_BAD => 'С большим опозданием',
                        self::PARAM_MIDDLING => 'Незначительное опоздание',
                        self::PARAM_GOOD => 'Точно в срок',
                        self::PARAM_SUPER => 'Супер! Раньше, чем договаривались!',
                    ],
                ],
                self::REVIEW_ITEM_CIVILITY => [
                    'title' => 'Уровень вежливости',
                    'items' => [
                        self::PARAM_BAD => 'Грубо',
                        self::PARAM_MIDDLING => 'Нейтрально',
                        self::PARAM_GOOD => 'Доброжелательно',
                        self::PARAM_SUPER => 'Супер! Вежливее не бывает!',
                    ],
                ],
            ],
        ],
    ];

    static public function reviewTitle($item, $isoffer = self::TYPE_DEFAULT, $dict = self::DICT_DEFAULT)
    {
        $dict = isset(self::$dictionary[$dict]) ? $dict : self::DICT_DEFAULT;
        $isoffer = isset(self::$dictionary[$dict][$isoffer]) ? $isoffer : self::TYPE_DEFAULT;

        return isset(self::$dictionary[$dict][$isoffer][$item]['title'])
            ? self::$dictionary[$dict][$isoffer][$item]['title']
            : 'Не указано';
    }

    static public function reviewItem($key, $item, $isoffer = self::TYPE_DEFAULT, $dict = self::DICT_DEFAULT)
    {
        $dict = isset(self::$dictionary[$dict]) ? $dict : self::DICT_DEFAULT;
        $isoffer = isset(self::$dictionary[$dict][$isoffer]) ? $isoffer : self::TYPE_DEFAULT;

        return isset(self::$dictionary[$dict][$isoffer][$item]['items'][$key])
            ? self::$dictionary[$dict][$isoffer][$item]['items'][$key]
            : 'Не указано';
    }

    static public function getReviewsByProfile($profileId, $perPage = null)
    {
        $q = self::start()
            ->byProfile($profileId)
            ->orderBy('created_at', 'desc');

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    static public function getReviewsByAuthor($profileId, $perPage = null)
    {
        $q = self::start()
            ->byAuthor($profileId)
            ->orderBy('created_at', 'desc');

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    static public function countByProfile($id)
    {
        return self::byProfile($id)
            ->count();
    }

    static public function countByAuthor($id)
    {
        return self::byAuthor($id)
            ->count();
    }

    static public function countByAuthorAndDeal($id, $dealId)
    {
        return self::byAuthor($id)
            ->byDeal($dealId)
            ->count();
    }

    public function qualityTitle()
    {
        return self::reviewItem(
            $this->param_quality,
            self::REVIEW_ITEM_QUALITY,
            $this->type,
            self::DICT_DEFAULT
        );
    }

    public function timeTitle()
    {
        return self::reviewItem(
            $this->param_time,
            self::REVIEW_ITEM_TIME,
            $this->type,
            self::DICT_DEFAULT
        );
    }

    public function civilityTitle()
    {
        return self::reviewItem(
            $this->param_civility,
            self::REVIEW_ITEM_CIVILITY,
            $this->type,
            self::DICT_DEFAULT
        );
    }

    public function maxRating()
    {
        return 6;
    }

    public function rating()
    {
        $count = ($this->param_quality * 2) + ($this->param_time * 1) + ($this->param_civility * 1);
        $rating = $count * 0.5;

        return $rating;
    }

    public function ratingPercent()
    {
        $rating = $this->rating();
        return 100 / $this->maxRating() * $rating;
    }

    public function isGoodRating()
    {
        return $this->rating() >= 4.0;
    }

    public function ratingTitle()
    {
        $rating = $this->rating();

        if ($rating < 2) {
            return 'Ужасно';
        } else if (2 <= $rating AND $rating < 4) {
            return 'Удовлетворительно';
        } else if (4 <= $rating AND $rating < 6) {
            return 'Хорошо';
        } else if (6 <= $rating) {
            return 'Суперположительно!';
        }

        return 'Не известно';
    }

    /**
     * SCOPES
     */

    public function scopeStart($q)
    {
        $q->with('deal', 'deal.dialog', 'deal.dialog.dialogable', 'deal.dialog.dialogable.order', 'author', 'profile');
        
        return $q;
    }

    public function scopeByAuthor($q, $id)
    {
        $q->where('author_id', $id);

        return $q;
    }

    public function scopeByProfile($q, $id)
    {
        $q->where('profile_id', $id);
        
        return $q;
    }

    public function scopeByDeal($q, $id)
    {
        $q->where('deal_id', $id);
        
        return $q;
    }

    /**
     * BELONGS TO
     */

    public function deal()
    {
        return $this->belongsTo('App\Deal', 'deal_id', 'id');
    }

    public function author()
    {
        return $this->belongsTo('App\Profile', 'author_id', 'id');
    }

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'profile_id', 'id');
    }
}

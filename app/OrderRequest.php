<?php

namespace App;

use App\Components\Model;
use Validator;

class OrderRequest extends Model
{
    protected $morphClass = 'order_request';

    protected $fillable = [
        'content',
        'order_id',
    ];

    /**
     * Validators
     */

    static protected $validators = [
    ];

    /**
     * BELONGS TO
     */

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'profile_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }

    /**
     * HAS MANY
     */

    public function dialog()
    {
        return $this->morphOne('App\Dialog', 'dialogable');
    }

    /**
     * SCOPES
     */

    public function scopeStart($query)
    {
        $query->with('profile');

        return $query;
    }

    public function scopeByOrder($query, $id)
    {
        $query->where('order_id', $id);

        return $query;
    }
}

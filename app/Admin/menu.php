<?php

Admin::menu()->url('/')->label('Start page')->icon('fa-dashboard');
Admin::menu('App\NewsItem')->label('Новости')->icon('fa-user');
Admin::menu('App\Page')->label('Страницы')->icon('fa-user');
Admin::menu('App\PageLink')->icon('fa-user');
Admin::menu('App\FaqCategory')->icon('fa-user');
Admin::menu('App\FaqItem')->icon('fa-user');
Admin::menu('App\Plus')->icon('fa-user');
Admin::menu('App\Section')->icon('fa-user');
Admin::menu('App\Category')->icon('fa-user');
Admin::menu('App\City')->icon('fa-user');
Admin::menu('App\Slide')->icon('fa-user');
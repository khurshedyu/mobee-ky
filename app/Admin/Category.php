<?php

Admin::model('App\Category')->title('Категории')->display(function ()
{
	$display = AdminDisplay::datatablesAsync();
	$display->with('categories', 'section', 'parent');
	$display->filters([
		Filter::related('category_id')->model('App\Category'),
		Filter::related('section_id')->model('App\Section'),
	]);
	$display->apply(function ($query)
	{
	    $query->orderBy('section_id', 'asc');
	    $query->orderBy('category_id', 'asc');
	});
	$display->columns([
		Column::string('title')->label('Заголовок'),
		Column::string('name')->label('Алиас'),
		// Column::string('content')->label('Content'),
		// Column::string('avatar_id')->label('Avatar_id'),
		// Column::count('categories')->label('Categories'),//->append(Column::filter('category_id')->model('App\Category')),
		Column::string('section.title')->label('Сфера')->append(Column::filter('section_id')),
		Column::string('parent.title')->label('Родительская кат.')->append(Column::filter('category_id')),
		Column::string('weight')->label('Вес'),
		Column::string('status')->label('Публикация'),
		// Column::datetime('updated_at')->label('Изменена')->format('d.m.Y H:i'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок'),
		FormItem::text('name', 'Алиас'),
		// FormItem::text('avatar_id', 'Аватар'),
		FormItem::select('section_id', 'Сфера')->model('App\Section')->display('title')->nullable(),
		FormItem::select('category_id', 'Родительская кат.')->model('App\Category')->display('title')->nullable(),
		FormItem::text('weight', 'Вес'),
		FormItem::checkbox('status', 'Доступна публично'),
		FormItem::ckeditor('content', 'Контент'),
	]);
	return $form;
});
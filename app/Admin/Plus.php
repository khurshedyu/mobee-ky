<?php

Admin::model('App\Plus')->title('Плюсики')->display(function ()
{
	$display = AdminDisplay::datatablesAsync();
	$display->with();
	$display->filters([

	]);
	$display->columns([
		Column::string('title')->label('Заголовок'),
		Column::string('name')->label('Ключ'),
		// Column::string('content')->label('Content'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок'),
		FormItem::text('name', 'Ключ'),
		FormItem::textarea('content', 'Контент'),
	]);
	return $form;
});
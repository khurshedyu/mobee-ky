<?php

Admin::model('App\Page')->title('Страницы')->display(function ()
{
	$display = AdminDisplay::datatablesAsync();
	$display->with();
	$display->filters([

	]);
	$display->apply(function ($query)
	{
	    $query->orderBy('id', 'desc');
	});
	$display->columns([
		Column::string('id')->label('#'),
		Column::string('title')->label('Заголовок'),
		Column::string('name')->label('Алиас'),
		// Column::string('content')->label('Content'),
		Column::string('archive')->label('В архиве'),
		Column::datetime('published_at')->label('Дата публикации')->format('d.m.Y H:i'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок'),
		FormItem::text('name', 'Алиас'),
		FormItem::ckeditor('content', 'Контент'),
		FormItem::timestamp('published_at', 'Дата публикации')->format('d.m.Y H:i'),//->seconds(true),
		FormItem::checkbox('archive', 'В архиве'),
	]);
	return $form;
});
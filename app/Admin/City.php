<?php

Admin::model('App\City')->title('Города')->display(function ()
{
	$display = AdminDisplay::datatablesAsync();
	$display->with('region', 'country');
	$display->apply(function ($query) {
	    $query->where('country_id', config('app.default_country_id'));
	});
	$display->filters([
		Filter::related('region_id')->model('App\Region'),
		Filter::related('country_id')->model('App\Country'),
	]);
	$display->columns([
		Column::string('city_id')->label('ID'),
		Column::string('country.name')->label('Страна')->append(Column::filter('country_id')),
		Column::string('region.name')->label('Регион')->append(Column::filter('region_id')),
		Column::string('name')->label('Название'),
		Column::string('name_en')->label('Название Eng.'),
		Column::string('public')->label('Публикация'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		// FormItem::text('country_id', 'Страна'),
		// FormItem::text('region_id', 'Регион'),
		FormItem::text('name', 'Название'),
		FormItem::text('name_en', 'Название Eng.'),
		FormItem::checkbox('public', 'Публикация'),
	]);
	return $form;
});
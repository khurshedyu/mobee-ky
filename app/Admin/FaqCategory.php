<?php

Admin::model('App\FaqCategory')->title('FAQ Категории')->display(function ()
{
	$display = AdminDisplay::datatablesAsync();
	$display->with('items');
	$display->filters([

	]);
	$display->apply(function ($query)
	{
	    $query->orderBy('id', 'desc');
	});
	$display->columns([
		Column::string('id')->label('#'),
		Column::string('title')->label('Заголовок'),
		Column::string('name')->label('Алиас'),
		Column::string('weight')->label('Вес'),
		// Column::string('description')->label('Описание'),
		Column::count('items')->label('Записей')->append(Column::filter('category_id')->model('App\FaqItem')),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок'),
		FormItem::text('name', 'Алиас'),
		FormItem::text('weight', 'Вес'),
		FormItem::textarea('description', 'Описание'),
	]);
	return $form;
});
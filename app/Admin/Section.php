<?php

Admin::model('App\Section')->title('Сферы деятельности')->display(function ()
{
	$display = AdminDisplay::datatablesAsync();
	$display->with();
	$display->filters([

	]);
	$display->columns([
		Column::string('title')->label('Заголовок'),
		Column::string('name')->label('Алиас'),
		Column::string('content')->label('Контент'),
		Column::string('weight')->label('Вес'),
		Column::string('status')->label('Статус'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок'),
		FormItem::text('name', 'Алиас'),
		// FormItem::text('avatar_id', 'Аватар'),
		FormItem::text('weight', 'Вес'),
		// FormItem::text('status', 'Статус'),
		FormItem::checkbox('status', 'Доступна публично'),
		FormItem::ckeditor('content', 'Контент'),
	]);
	return $form;
});
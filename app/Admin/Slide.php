<?php

Admin::model('App\Slide')->title('Слайдер')->display(function ()
{
	$display = AdminDisplay::datatables();
	$display->with();
	$display->filters([

	]);
	$display->columns([
		Column::image('image')->label('Изображение'),
		Column::string('title')->label('Заголовок'),
		// Column::string('content')->label('Контент'),
		Column::string('weight')->label('Вес'),
		Column::string('public')->label('Публикация'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок'),
		FormItem::image('image', 'Изображение'),
		FormItem::text('weight', 'Вес'),
		FormItem::ckeditor('content', 'Контент'),
		FormItem::checkbox('public', 'Публикация'),
	]);
	return $form;
});
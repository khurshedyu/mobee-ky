<?php

Admin::model('App\PageLink')->title('Меню в шапке')->display(function ()
{
	$display = AdminDisplay::datatablesAsync();
	$display->with();
	$display->filters([

	]);
	$display->apply(function ($query)
	{
	    $query->orderBy('visits', 'desc');
	});
	$display->columns([
		Column::string('id')->label('#'),
		Column::string('title')->label('Заголовок'),
		Column::string('name')->label('Алиас'),
		// Column::string('page_id')->label('Page_id'),
		Column::string('visits')->label('Просмотров'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок'),
		FormItem::text('name', 'Алиас'),
		// FormItem::text('page_id', 'Page'),
		FormItem::text('visits', 'Просмотров'),
	]);
	return $form;
});
<?php

Admin::model('App\FaqItem')->title('FAQ Записи')->display(function ()
{
	$display = AdminDisplay::datatablesAsync();
	$display->with('category');
	$display->filters([
		Filter::related('category_id')->model('App\FaqCategory'),
	]);
	$display->apply(function ($query)
	{
	    $query->orderBy('id', 'desc');
	});
	$display->columns([
		Column::string('id')->label('#'),
		Column::string('title')->label('Заголовок'),
		Column::string('name')->label('Алиас'),
		// Column::string('description')->label('Описание'),
		// Column::string('content')->label('Содержание'),
		Column::string('category.title')->label('Категория')->append(Column::filter('category_id')),
		Column::string('archive')->label('В архиве'),
	]);
	return $display;
})->createAndEdit(function ()
{
	$form = AdminForm::form();
	$form->items([
		FormItem::text('title', 'Заголовок'),
		FormItem::text('name', 'Алиас'),
		FormItem::select('category_id', 'Категория')->model('App\FaqCategory')->display('title')->nullable(),
		FormItem::ckeditor('description', 'Описание'),
		FormItem::ckeditor('content', 'Содержание'),
		FormItem::text('weight', 'Вес'),
		FormItem::checkbox('archive', 'В архиве'),
	]);
	return $form;
});
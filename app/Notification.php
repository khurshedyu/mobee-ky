<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use RestApiClientNodeJs;

class Notification extends Model
{
    const NOTIFICATIONS_ACTION_TYPE_ALL = 0;
    const NOTIFICATIONS_ACTION_TYPE_MY = 1;
    const NOTIFICATIONS_ACTION_TYPE_OTHERS = 2;

    const TYPE_NONE = 0;
    const TYPE_ADD_ORDER_REQUEST = 1;
    const TYPE_HELLO_NEW_USER = 2;

    const READ_NO = 0;
    const READ_YES = 1;

    const SELF_NO = 0;
    const SELF_YES = 1;

    static public function createNotification($type, $content, $userId, $self = false)
    {
        $n = new self;
        $n->self = $self ? self::SELF_YES : self::SELF_NO;
        $n->read = $self ? self::READ_YES : self::READ_NO;
        $n->type = $type;
        $n->user_id = $userId;
        $n->content = $content;
        $n->save();

        // Tell nodejs to inform user to get his notifications
        if(Auth::user()->id != $userId)
            RestApiClientNodeJs::tellUserToReloadNotifications($userId);

        return $n;
    }

    static public function noteAddOrderRequest($orderId, $requestId, $from, $to, $isoffer, $self = false)
    {
        $content = json_encode([
            'order_id' => $orderId,
            'request_id' => $requestId,
            'from_user_id' => $from,
            'to_user_id' => $to,
            'isoffer' => $isoffer,
        ]);

        $userId = $self ? $from : $to;

        return self::createNotification(self::TYPE_ADD_ORDER_REQUEST, $content, $userId, $self);
    }

    static public function noteHelloNewUser($userId)
    {
        $content = json_encode([
            null
        ]);

        return self::createNotification(self::TYPE_HELLO_NEW_USER, $content, $userId, true);
    }

    static public function userNotifications($id, $type, $perPage = null)
    {
        $query = self::byUser($id);
        $query->orderLast();
        
        if ($type === self::NOTIFICATIONS_ACTION_TYPE_MY) {
            $query->isSelf(self::SELF_YES);
        } else if ($type === self::NOTIFICATIONS_ACTION_TYPE_OTHERS) {
            $query->isSelf(self::SELF_NO);
        }

        return $perPage ? $query->paginate($perPage) : $query->get();
    }

    public function parseData()
    {
        $data = [
            'message' => $this->content,
            'url' => route('main::index'),
            'date' => $this->created_at,
        ];

        switch ($this->type) {
            case self::TYPE_ADD_ORDER_REQUEST:
                $content = json_decode($this->content, true);
                $isoffer = isset($content['isoffer']) AND $content['isoffer'];
                if ($this->self) {
                    $data['message'] = 'Вы оставили заявку в ' . ($isoffer ? 'предложении' : 'заказе') . ' #' . $content['order_id'];
                } else {
                    $data['message'] = 'В вашем ' . ($isoffer ? 'предложении' : 'заказе') . ' #' . $content['order_id'] . ' добавлена новая заявка';
                }
                $data['url'] = route('orders::view', ['id' => $content['order_id']]);
                break;
            case self::TYPE_HELLO_NEW_USER:
                $content = json_decode($this->content, true);
                $data['message'] = trans('messages.hello_new_user');
                $data['url'] = route('page::show', ['name' => 'welcome']);
                break;
        }

        return $data;
    }

    public function displayMessage()
    {
        return $this->parseData()['message'];
    }

    public function displayUrl()
    {
        return $this->parseData()['url'];
    }

    public function contentToArray()
    {
        return json_decode($this->content, true);
    }

    public function setRead($value = self::READ_YES)
    {
        $this->read = $value;
        $this->save();
    }

    /**
     * SCOPES
     */

    public function scopeOrderLast($q)
    {
        $q->orderBy('created_at', 'desc');
        $q->orderBy('id', 'desc');

        return $q;
    }

    public function scopeIsRead($q, $value = self::READ_YES)
    {
        $q->where('read', $value);

        return $q;
    }

    public function scopeIsSelf($q, $value = self::SELF_YES)
    {
        $q->where('self', $value);

        return $q;
    }

    public function scopeByUser($q, $userId)
    {
        $q->where('user_id', $userId);

        return $q;
    }
}

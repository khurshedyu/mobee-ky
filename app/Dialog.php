<?php

namespace App;

use App\Components\Model;
use App\OrderRequest;
use App\DialogMember;
use App\DialogItem;
use App\Deal;
use App\Connection;
use Validator;

class Dialog extends Model
{
    private $lastMessage;

    static public function createByOrderRequest($orderRequest)
    {
        $dialog = new self;
        $dialog->dialogable_id = $orderRequest->id;
        $dialog->dialogable_type = OrderRequest::getModel()->getMorphClass();
        $dialog->save();

        $dialogItem = new DialogItem;
        $dialogItem->profile_id = null;
        $dialogItem->dialog_id = $dialog->id;
        $dialogItem->type = DialogItem::TYPE_DIALOG_START;
        $dialogItem->content = null;
        $dialogItem->save();

        $members = [];
        $members[] = $orderRequest->profile_id;
        $members[] = $orderRequest->order->profile_id;

        foreach ($members as $member) {
            $dialogMember = new DialogMember;
            $dialogMember->dialog_id = $dialog->id;
            $dialogMember->profile_id = $member;
            $dialogMember->save();
        }

        return $dialog;
    }

    public function postMessage($content, $profileId)
    {
        $dialogItem = new DialogItem;
        $dialogItem->profile_id = $profileId;
        $dialogItem->dialog_id = $this->id;
        $dialogItem->type = DialogItem::TYPE_MESSAGE;
        $dialogItem->content = $content;
        $dialogItem->save();

        DialogMember::where('dialog_id', $this->id)->update([]);

        return $dialogItem;
    }

    public function createDeal($price)
    {
        $deal = new Deal;
        $deal->dialog_id = $this->id;
        $deal->price = $price;
        $deal->save();

        $dialogItem = new DialogItem;
        // $dialogItem->profile_id = $profileId;
        $dialogItem->dialog_id = $this->id;
        $dialogItem->type = DialogItem::TYPE_DEAL;
        $dialogItem->content = $deal->id;
        $dialogItem->save();

        if ($this->dialogable_type == 'order_request') {
            // $connection = new Connection;
            // $connection->deal_id = $deal->id;
            // $connection->from_profile_id = $this->dialogable->profile_id;
            // $connection->to_profile_id = $this->dialogable->order->profile_id;
            // $connection->save();
        }

        return $dialogItem;
    }

    public function isMember($profileId)
    {
        foreach ($this->members as $member) {
            if ($member->profile_id == $profileId) {
                return true;
            }
        }

        return false;
    }

    public function isOrderDialog()
    {
        return ($this->dialogable_type == OrderRequest::getModel()->getMorphClass());
    }

    public function avatarUrl()
    {
        return config('app.default_avatar_url');
    }

    public function title()
    {
        $typeTitle = $this->deal ? 'Сделка' : 'Обсуждение';
        $title = $typeTitle . ' # ' . $this->id;

        if ($this->isOrderDialog()) {
            $title = $typeTitle . ' @ ' . ($this->dialogable ? $this->dialogable->order->title : null);
        }

        return $title;
    }

    public function lastMessage()
    {
        if ($this->lastMessage) {
            return $this->lastMessage;
        }

        $this->lastMessage = $this->items()
            ->isMessage()
            ->orderLast()
            ->first();

        return $this->lastMessage;
    }

    public function getDialogItems()
    {
        return $this->items()
            ->orderBy('created_at', 'ASC')
            ->get();
    }

    public function getDialogActionItems()
    {
        return $this->items()
            ->with('dialog', 'dialog.deal')
            ->onlyActions()
            ->orderBy('created_at', 'ASC')
            ->get();
    }

    public function setOtherMessagesRead($profileId)
    {
        $this->items()
            ->where('type', DialogItem::TYPE_MESSAGE)
            ->where('profile_id', '<>', $profileId)
            ->update(['read' => DialogItem::READ_YES]);
    }

    /**
     * BELONGS TO
     */

    public function dialogable()
    {
        return $this->morphTo('dialogable');
    }

    /**
     * HAS ONE
     */

    public function deal()
    {
        return $this->hasOne('App\Deal', 'dialog_id', 'id');
    }

    /**
     * HAS MANY
     */

    public function items()
    {
        return $this->hasMany('App\DialogItem', 'dialog_id', 'id');
    }

    public function members()
    {
        return $this->hasMany('App\DialogMember', 'dialog_id', 'id');
    }

    /**
     * SCOPES
     */

    public function scopeStart($query)
    {
        return $query;
    }

    public function scopeByUser($q)
    {
        return $q;
    }
}

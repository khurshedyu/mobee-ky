<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Components\SessionWorker;

class VerifyEula
{
    protected $except = [
        '/license',
        '/logout',
        '/plus/*',
        '/webapi/username/check/*',
        '/uploadimg',
        // '/profile',
    ];

    public function handle($request, Closure $next, $guard = null)
    {
        if ($this->shouldPassThrough($request)) {
            return $next($request);
        }

        if (Auth::guard($guard)->check()) {
            $user = Auth::user();
            if (!$user->isLicensed()) {
                return redirect()->route('main::license');
            }

            if (!$this->shouldPassThrough($request, ['/profile'])) {
                $profile = SessionWorker::instance()->currentProfile();
                if (!$profile->city_id) {
                    return redirect()->route('profile::index');
                }
            }
        }

        return $next($request);
    }

    protected function shouldPassThrough($request, $excepts = null)
    {
        if (!$excepts) {
            $excepts = $this->except;
        }

        foreach ($excepts as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->is($except)) {
                return true;
            }
        }

        return false;
    }
}
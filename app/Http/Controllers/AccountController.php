<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Components\Payment;
use App\Components\AjaxResponse;
use App\PayInvoice;
use App\PayTransaction;
use App\Super;
use App\Notification;
use App\Page;
use App\ActionLog;
use Auth;
use Log;
use Parser;

class AccountController extends Controller
{
    public function billing()
    {
        return view('account.billing');
    }

    public function postBilling(Request $request)
    {
        $money = (float) $request->input('money', 0);
        $user = Auth::user();
        $minPay = config('app.kkb_min_pay');

        if ($money < $minPay) {
            return redirect()->route('account::billing')
                ->withErrors([
                    trans('messages.kkb_minpay', ['minpay' => $minPay]),
                ]);
        }

        $invoice = PayInvoice::make($money, $user->id);

        if (!$invoice OR !$invoice->id) {
            abort(403, 'Invoice error');
        }

        ActionLog::payInvoiceCreated($invoice->id, $user->id);

        return redirect()->route('account::billing::pay', ['id' => $invoice->id]);

        // $xml = Payment::initKKB($invoice->money, $invoice->id);

        // return view('layouts.payment', compact('xml'));
    }

    public function billingPay($id)
    {
        $user = Auth::user();
        $invoice = PayInvoice::findOrFail($id);

        if (!$invoice->ownedByUser($user->id)) {
            abort(403);
        }

        $payment = Payment::createAuthData($invoice->money, $invoice->id);

        return view('account.billing_pay', compact('payment', 'invoice'));
    }

    public function billingCheck($id)
    {
        $user = Auth::user();
        $invoice = PayInvoice::findOrFail($id);

        if (!$invoice->ownedByUser($user->id)) {
            abort(403);
        }

        $paymentCheck = Payment::checkOrderStatus($invoice->id);
        $paymentCheck->process();

        return view('account.billing_check', compact('paymentCheck', 'invoice'));
    }

    public function payInfo(Request $request)
    {
        $xml = $request->input('response', null);
        $data = Parser::xml($xml);

        $order = [
            'order_id' => $data['bank']['customer']['merchant']['order']['@attributes']['order_id'],
            'amount' => $data['bank']['customer']['merchant']['order']['@attributes']['amount'],
            'currency' => $data['bank']['customer']['merchant']['order']['@attributes']['currency'],
        ];

        $merchant = [
            'cert_id' => $data['bank']['customer']['merchant']['@attributes']['cert_id'],
            'name' => $data['bank']['customer']['merchant']['@attributes']['name'],
        ];

        $customer = $data['bank']['customer']['@attributes'];
        $payment = $data['bank']['results']['payment']['@attributes'];
        $payment['timestamp'] = $data['bank']['results']['@attributes']['timestamp'];

        // dd($customer);

        $paymentCheck = Payment::checkOrderStatus($order['order_id']);
        $paymentCheck->process();

        ActionLog::payInfo($data, $order['order_id']);

        // Log::info('PAY SUCCESS: ', $request->all());

        return AjaxResponse::make(null)
            ->codeSuccess()
            ->get();
    }

    public function billingPaySuccess($id)
    {
        // $user = Auth::user();
        // $invoice = PayInvoice::findOrFail($id);

        // $transaction = $invoice->success();

        // if ($transaction) {
        //     return redirect()->route('account::billing::history');
        // }

        // echo 'ERROR';
    }

    public function billingInvoices()
    {
        $user = Auth::user();
        $invoices = PayInvoice::lastUserInvoices(
            $user->id,
            config('app.payment_per_page'));

        return view('account.billing_invoices', compact('invoices'));
    }

    public function billingHistory()
    {
        $user = Auth::user();
        $transactions = PayTransaction::lastUserTransactions(
            $user->id,
            config('app.payment_per_page'));

        return view('account.billing_history', compact('transactions'));
    }

    public function super()
    {
        $user = Auth::user();

        return view('account.super', compact('user'));
    }

    public function superInfo()
    {
        $user = Auth::user();
        $page = Page::pageByName('account-super');

        return view('account.super_info', compact('user', 'page'));
    }

    public function superBuy($id)
    {
        $user = Auth::user();
        $canBuy = $user->canBuySuper($id);
        $cost = Super::costBySuperId($id);
        $months = Super::monthsBySuperId($id);

        return view('account.super_buy', compact('user', 'canBuy', 'cost', 'months'));
    }

    public function postSuperBuy($id)
    {
        $user = Auth::user();
        $canBuy = $user->canBuySuper($id);

        if (!$canBuy) {
            abort(403);
        }

        $super = $user->buySuper($id);
        ActionLog::userBuySuper($super->id, $user->id);

        return view('account.super_buy_success');
    }

    private function notifications($type)
    {
        $user = Auth::user();
        $notifications = Notification::userNotifications(
            $user->id,
            $type,
            config('app.notifications_per_page'));

        return view('account.notifications', compact('notifications', 'type'));
    }

    public function notificationsAll()
    {
        return $this->notifications(Notification::NOTIFICATIONS_ACTION_TYPE_ALL);
    }

    public function notificationsMy()
    {
        return $this->notifications(Notification::NOTIFICATIONS_ACTION_TYPE_MY);
    }

    public function notificationsOthers()
    {
        return $this->notifications(Notification::NOTIFICATIONS_ACTION_TYPE_OTHERS);
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use App\RegisterRequest;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Components\MelomanApi;
use App\Components\SessionWorker;
use Auth;
use Log;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            // 'password' => 'required|confirmed|min:6',
            'password' => 'required|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'meloman_id' => $data['meloman_id'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        Profile::createByUser($user, $data['meloman_data'] ? $data['meloman_data'] : null);

        return $user;
    }

    public function postLogin(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        // Start Meloman Auth

        $authByEmail = MelomanApi::execute('authByEmail', [
            'email' => $request->input('email', null),
            'pass' => sha1($request->input('password', null)),
        ]);

        Log::info('TRY LOGIN RESPONSE: ' . print_r($authByEmail, true));
        
        if ($authByEmail->statusCode == 404) {
            return redirect()->route('main::login')
                ->withInput($request->except('password'))
                ->withErrors([
                    'Такого Me ID не существует',
                ]);
        } else if ($authByEmail->statusCode != 200) {
            return redirect()->route('main::login')
                ->withInput($request->except('password'))
                ->withErrors([
                    'Ошибка авторизации',
                ]);
        }

        $authByEmailData = (array) json_decode($authByEmail->body);
        $user = User::byMelomanId($authByEmailData['id'])->first();

        if (!$user) {
            $melomanUser = MelomanApi::execute('info', [
                'user' => $authByEmailData['id'],
            ]);
            
            // dd($melomanUser);

            if ($melomanUser->statusCode != 200) {
                return redirect()->route('main::login')
                    ->withInput($request->except('password'))
                    ->withErrors([
                        'Ошибка получения данных от сервера',
                    ]);
            }

            $melomanUserData = (array) json_decode($melomanUser->body);

            // dd($melomanUserData);

            if ($user) {
                return redirect()->route('main::login')
                    ->withInput($request->except('password'))
                    ->withErrors([
                        'Ошибка авторизации. Обратитесь в поддержку. Код: 101',
                    ]);
            }

            $user = $this->create([
                'meloman_id' => $authByEmailData['id'],
                'email' => $request->input('email', null),
                'password' => $request->input('password', null),
                'meloman_data' => $melomanUserData,
            ]);
        }

        if ($user->password != bcrypt($request->input('password', null))) {
            $user->password = bcrypt($request->input('password', null));
            $user->save();
        }

        // End Meloman Auth

        if (Auth::guard($this->getGuard())->attempt($credentials, !$request->has('remember'))) {
            SessionWorker::instance()->clearCurrentProfile();
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles && ! $lockedOut) {
            $this->incrementLoginAttempts($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());
        $email = $request->input('email', null);
        // $password = $request->input('password', null);
        $password = sha1($request->input('password', null));

        Log::info('email', [$request->input('email', null)]);
        Log::info('password', [$request->input('password', null)]);

        $mobeeUser = User::byEmail($email)->first();
        $melomanEmailUser = MelomanApi::execute('getId', [
            'email' => $email,
        ]);

        if ($mobeeUser OR $melomanEmailUser->statusCode == 200) {
            return redirect()->route('main::register')
                ->withInput($request->except('password'))
                ->withErrors([
                    'Пользователь с таким адресом уже существует.',
                ]);
        }

        if ($validator->fails()) {
            return redirect()->route('main::register')
                ->withInput($request->except('password'))
                ->withErrors([
                    'Данные не прошли валидацию.',
                ]);
        }

        $regReq = RegisterRequest::createRequest($email, $password);

        Mail::send('emails.register_request', [
            'key' => $regReq->key
        ], function ($m) use ($regReq) {
            $m->from(env('MAIL_FROM', 'no-reply@mobee.kz'), 'Mobee.kz');
            $m->to($regReq->email);
            $m->subject('Активация на Mobee.kz');
        });

        return view('auth.register_request', compact('email'));
    }

    public function registerActivate($key)
    {
        // echo $key;
        $regReq = RegisterRequest::findActiveByKey($key);

        if (!$regReq) {
            return view('auth.register_activate', ['fail' => 'Код активации недействителен.']);
        }

        $email = $regReq->email;
        $password = $regReq->password;

        $mobeeUser = User::byEmail($email)->first();
        $melomanEmailUser = MelomanApi::execute('getId', [
            'email' => $email,
        ]);

        if ($mobeeUser OR $melomanEmailUser->statusCode == 200) {
            return view('auth.register_activate', ['fail' => 'Пользователь с таким адресом уже существует.']);
        }

        $register = MelomanApi::execute('register', [
            'email' => $email,
            'pass' => $password,
        ]);

        Log::info('TRY REGISTER RESPONSE: ' . print_r($register, true));

        $registerData = (array) json_decode($register->body);

        if ($register->statusCode == 409) {
            return view('auth.register_activate', ['fail' => 'Пользователь с таким адресом уже существует.']);
        }

        if ($register->statusCode != 201) {
            return view('auth.register_activate', ['fail' => 'Ошибка регистрации.']);
        }

        $melomanUser = MelomanApi::execute('info', [
            'user' => $registerData['id'],
        ]);

        Log::info('MELOMAN USER INFO: ' . print_r($melomanUser, true));
        
        // dd($melomanUser);

        if ($melomanUser->statusCode != 200) {
            return view('auth.register_activate', ['fail' => 'Ошибка получения данных от сервера.']);
        }

        $melomanUserData = (array) json_decode($melomanUser->body);

        // dd($melomanUserData);

        $this->create([
            'meloman_id' => $registerData['id'],
            'email' => $email,
            'password' => $password,
            'meloman_data' => $melomanUserData,
        ]);

        $regReq->activated();

        $credentials = [
            'email' => $email,
            'password' => $password,
        ];

        if (Auth::guard($this->getGuard())->attempt($credentials, true)) {
            SessionWorker::instance()->clearCurrentProfile();
        }

        // return view('auth.register_activate', ['fail' => 'Ошибка регистрации.']);

        return view('auth.register_activate');
    }

    public function logout()
    {
        Auth::guard($this->getGuard())->logout();
        SessionWorker::instance()->clearCurrentProfile();

        return redirect()->route('main::index');
    }
}

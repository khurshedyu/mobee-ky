<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ActionLog;
use App\Profile;
use App\Review;
use App\Plus;
use App\Connection;
use App\Notification;
use App\Page;
use App\Slide;
use App\File;
use App\Components\ActiveMenu;
use Auth;
use DB;
use Carbon\Carbon;
use Log;

class Main extends Controller
{
    public function index()
    {
        $slides = Slide::toSlide()->get();
        //var_dump($slides->toArray());exit;

        $days = [
            Carbon::now()->addDays(-6)->endOfDay(),
            Carbon::now()->addDays(-5)->endOfDay(),
            Carbon::now()->addDays(-4)->endOfDay(),
            Carbon::now()->addDays(-3)->endOfDay(),
            Carbon::now()->addDays(-2)->endOfDay(),
            Carbon::now()->addDays(-1)->endOfDay(),
            Carbon::now()->endOfDay(),
        ];

        $chartData = [];
        $htmlHelper = new \App\Helpers\HtmlHelper;

        // return $days;

        foreach ($days as $day) {
            $startDay = $day->copy()->startOfDay();

            $result = DB::connection('mysql')
                ->select("SELECT COUNT(id) as profiles, 
                    (SELECT COUNT(id) FROM orders WHERE isoffer = 0 AND created_at < ? AND created_at > ?) as orders, 
                    (SELECT COUNT(id) FROM orders WHERE isoffer = 1 AND created_at < ? AND created_at > ?) as offers, 
                    (SELECT COUNT(id) FROM deals WHERE created_at < ? AND created_at > ?) as deals 
                    FROM profiles WHERE created_at < ?", 
                    [
                        $day,
                        $startDay,
                        $day,
                        $startDay,
                        $day,
                        $startDay,
                        $day,
                    ]
                );

            if (count($result)) {
                $result[0]->date = $htmlHelper->formatDate($day, 'j M');
                $chartData[] = $result[0];
            }
        }

        $chartData = json_encode($chartData);

        // return $chartData;

        return view('main.index', compact('slides', 'chartData'));
    }

    public function landing()
    {
        return view('main.landing');
    }

    public function companies(Request $request)
    {
        $companies = Profile::start()
            ->isCompany()
            ->active()
            ->paginate(config('app.companies_per_page'));

        return view('main.companies', compact('companies'));
    }

    public function clients(Request $request)
    {
        $clients = Profile::start()
            ->isUser()
            ->active()
            ->paginate(config('app.clients_per_page'));

        return view('main.clients', compact('clients'));
    }

    public function profiles(Request $request)
    {
        ActiveMenu::instance()->setKey('main_profiles');
        $profiles = Profile::start()
            ->select(DB::raw('profiles.*, (NOW() < users.super_until) as until'))
            ->rightJoin('users', 'users.id', '=', 'profiles.user_id')
            ->orderBy('until', 'DESC')
            ->orderBy('profiles.rating', 'DESC')
            ->orderBy('profiles.created_at', 'ASC')
            ->where('profiles.city_id', '>', 0)
            ->whereIn('profiles.status', [
                Profile::STATUS_NEW,
                Profile::STATUS_ACTIVE,
            ])
            // ;
            ->paginate(config('app.clients_per_page'));

        // dd($profiles->toSql());

        return view('main.profiles', compact('profiles'));
    }

    public function license()
    {
        $user = Auth::user();

        if ($user->isLicensed()) {
            return redirect()->route('profile::index');
        }

        $page = Page::byName('license')->first();

        return view('main.license', compact('page'));
    }

    public function postLicense()
    {
        $user = Auth::user();

        if (!$user->isLicensed()) {
            $user->agreeWithLicense();
            Notification::noteHelloNewUser($user->id);
        }

        return redirect()->route('profile::index');
    }

    public function reviews()
    {
        ActiveMenu::instance()->setKey('main_reviews');
        $reviews = Review::start()
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return view('main.reviews', compact('reviews'));
    }

    public function connections()
    {
        ActiveMenu::instance()->setKey('main_connections');
        $connections = Connection::orderBy('created_at', 'desc')
            ->paginate(15);

        return view('main.connections', compact('connections'));
    }

    public function plus($name)
    {
        $plus = Plus::where('name', $name)->first();
        $helper = new \App\Helpers\HtmlHelper;
        $plus->content = $helper->parsePlus($plus->content);

        return $plus;
    }

    public function uploadImg(Request $request)
    {
        $user = Auth::user();

        Log::info('UPLOAD IMG');
        // Log::info('POST' . print_r($_POST, true));
        // Log::info('GET' . print_r($_GET, true));
        // Log::info('FILES' . print_r($_FILES, true));

        if ($request->hasFile('file') AND $request->file('file')->isValid()) {
            $mimeType = $request->file('file')->getMimeType();
            Log::info('MIME TYPE: ' . $mimeType);

            if (!in_array($mimeType, [
                'image/jpeg',
                'image/jpg',
                'image/png',
                'image/gif',
            ])) {
                return [
                    'error' => 1,
                ];
            }

            $file = File::saveUploadedFile($request->file('file'));
            $id = $file->id;

            ActionLog::logUploadImage($id, $user->id);

            return [
                'error' => 0,
                'data' => [
                    'id' => $id,
                    'path' => url('/i/upload/' . $file->path),
                ],
            ];
        }

        ActionLog::logFailUploadImage($user->id);

        return [
            'error' => 1,
        ];
    }
}

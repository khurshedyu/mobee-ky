<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Page;
use App\PageLink;

class PageController extends Controller
{
    public function show($name)
    {
        $page = Page::published()
            ->byName($name)
            ->first();

        if (!$page) {
            abort(404);
        }

        PageLink::where('name', $page->name)->increment('visits');

        return view('page.show', compact('page'));
    }
}

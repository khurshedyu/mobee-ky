<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FaqCategory;
use App\FaqItem;
use App\Page;
use App\Components\ActiveMenu;
use Validator;
use Mail;
use Auth;

class FaqController extends Controller
{
    public function __construct($foo = null)
    {
        ActiveMenu::instance()->setKey('faq_index');
    }

    public function index()
    {
        $categories = FaqCategory::start()
            ->orderBy('weight', 'DESC')
            ->get();

        $page = Page::byName('faq')->first();

        return view('faq.index', compact('categories', 'page'));
    }

    public function category($name)
    {
        $categories = FaqCategory::start()
            ->orderBy('weight', 'DESC')
            ->get();

        $category = FaqCategory::with('items')
            ->where('name', $name)
            ->first();

        if (!$category) {
            abort(404);
        }

        $items = $category->items()
            ->orderBy('weight', 'DESC')
            ->orderBy('updated_at', 'DESC')
            ->get();

        return view('faq.category', compact('categories', 'category', 'items'));
    }

    public function item($name)
    {
        $categories = FaqCategory::start()
            ->orderBy('weight', 'DESC')
            ->get();
            
        $item = FaqItem::where('name', $name)->first();

        if (!$item) {
            abort(404);
        }

        $incatItems = $item->category->items()
            ->orderBy('weight', 'DESC')
            ->orderBy('updated_at', 'DESC')
            ->get();

        return view('faq.item', compact('categories', 'item', 'incatItems'));
    }

    public function message()
    {
        return view('faq.message');
    }

    public function postMessage(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'message' => 'required|min:10',
            'message_type' => 'required|in:1,2,3',
        ]);

        if ($validator->fails()) {
            return redirect()->route('faq::message')
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $messageContent = $request->input('message');
        $messageType = 'Неизвестно';
        switch ($request->input('message_type')) {
            case 1:
                $messageType = 'Ошибка';
                break;
            case 2:
                $messageType = 'Вопрос';
                break;
            case 3:
                $messageType = 'Идея';
                break;
        }

        Mail::send('emails.user_help_message', [
            'messageContent' => $messageContent,
            'messageType' => $messageType,
            'user' => $user,
        ], function ($m) use ($user, $messageType) {
            $m->from($user->email);
            $m->to('support@mobee.kz');
            $m->subject('Поддержка: ' . $messageType);
        });

        return redirect()->route('faq::message')
            ->with('success', 'Обращение успешно отправлено!');
    }
}

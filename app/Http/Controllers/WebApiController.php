<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Components\SessionWorker;
use App\Dialog;
use App\Notification;
use App\Profile;
use Gate;
use Auth;

class WebApiController extends Controller
{
    const ERROR_NONE = 0;
    const ERROR_NOT_DIALOG_MEMBER = 1;
    const ERROR_DIALOG_NOT_FOUND = 2;

    /*

    private function response($data, $code)
    {
        return [
            'data' => $data,
            'code' => $code
        ];
    }

    public function chatSend(Request $request)
    {
        $id = $request->input('dialog', 0);
        $message = $request->input('message', null);
        $dialog = Dialog::start()->find($id);

        if (!$dialog) {
            return $this->response(['success' => false], self::ERROR_DIALOG_NOT_FOUND);
        }

        if (Gate::denies('dialog-member', $dialog)) {
            return $this->response(['success' => false], self::ERROR_NOT_DIALOG_MEMBER);
        }

        $profile = SessionWorker::instance()->currentProfile();
        $dialogItem = $dialog->postMessage($message, $profile->id);

        return $this->response(['success' => true, 'message' => $dialogItem->id], self::ERROR_NONE);
    }

    public function chatMessages(Request $request, $did, $id)
    {
        $dialog = Dialog::start()->find($did);

        if (!$dialog) {
            return $this->response(['success' => false], self::ERROR_DIALOG_NOT_FOUND);
        }

        if (Gate::denies('dialog-member', $dialog)) {
            return $this->response(['success' => false], self::ERROR_NOT_DIALOG_MEMBER);
        }

        $newItems = $dialog->items()
            ->with('profile')
            ->after($id)
            ->orderBy('created_at', 'ASC')
            ->get();

        $list = [];

        foreach ($newItems as $item) {
            $list[] = [
                'id' => $item->id,
                'avatar' => $item->profile->avatarUrl(),
                'name' => $item->profile->displayName(),
                'url' => $item->profile->linkName(),
                'content' => $item->content,
                'data' => $item->created_at,
            ];
        }

        return $this->response(['messages' => $list], self::ERROR_NONE);
    }

    public function notificationsCheck($id)
    {
        $user = Auth::user();
        $list = [];
        $html = new \App\Helpers\HtmlHelper;

        $notifications = Notification::byUser($user->id)
            ->where('id', '>', $id)
            ->isSelf(Notification::SELF_NO)
            ->orderLast()
            ->take(10)
            ->get();

        foreach ($notifications as $notification) {
            $list[] = [
                'id' => $notification->id,
                'data' => $notification->parseData(),
                'date' => $html->formatDateTime($notification->created_at),
            ];
        }

        return $list;
    }

    public function usernameCheck($name, $default)
    {
        $profile = Profile::where('name', $name)->first();

        $busy = false;
        $clean = false;

        if ($profile AND $profile->name != $default) {
            $busy = true;
        }

        if (preg_match("/^[a-zA-Z0-9_-]+$/", $name)) {
            $clean = true;
        }

        return $this->response([
            'busy' => $busy,
            'clean' => $clean,
        ], self::ERROR_NONE);
    }

    */
}

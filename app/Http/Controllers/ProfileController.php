<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Components\SessionWorker;
use App\Profile;
use App\City;
use App\Order;
use App\Dialog;
use App\DialogItem;
use App\DialogMember;
use App\Deal;
use App\UserToken;
use App\Review;
use App\Connection;
use App\Section;
use App\File;
use App\ActionLog;
use Auth;
use Gate;

class ProfileController extends Controller
{
    public function change(Request $request, $id)
    {
        SessionWorker::instance()->changeCurrentProfile($id);
        return redirect()->back();
    }

    public function index(Request $request)
    {
        $profile = SessionWorker::instance()->currentProfile();
        $cities = City::listByCountry(config('app.default_country_id'), true, true);
        $sections = Section::listAll();
        $entityTypes = Profile::entityTypes();
        $employeesList = Profile::employeesList();

        $profileView = 'profile.index';

        if ($profile->isCompany()) {
            $profileView = 'profile.index_company';
        }
        
        return view($profileView, compact('profile', 'cities', 'sections', 'entityTypes', 'employeesList'));
    }

    private function postIndexUser(Request $request, $profile)
    {
        $user = Auth::user();
        $validator = Profile::validateUserData($request->all(), $profile->id);

        if ($validator->fails()) {
            return redirect()->route('profile::index')
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $profile->update($request->all());
        ActionLog::profileUpdate($profile, $request->all(), $user->id);

        return redirect()->route('profile::index')
            ->with('success', 'Профиль успешно обновлен!');
    }

    private function postIndexCompany(Request $request, $profile)
    {
        $user = Auth::user();
        $validator = Profile::validateCompanyData($request->all(), $profile->id);

        if ($validator->fails()) {
            return redirect()->route('profile::index')
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $profile->update($request->all());
        ActionLog::profileUpdate($profile, $request->all(), $user->id);

        return redirect()->route('profile::index')
            ->with('success', 'Профиль успешно обновлен!');
    }

    public function postIndex(Request $request)
    {
        $profile = SessionWorker::instance()->currentProfile();

        if ($profile->isCompany()) {
            return $this->postIndexCompany($request, $profile);
        }

        return $this->postIndexUser($request, $profile);
    }

    public function view(Request $request, $name)
    {
        $currentPage = 'view';
        $profile = Profile::findProfile($name);

        if (!$profile) {
            return abort(404);
        }

        return view('profile.view', compact('profile', 'currentPage'));
    }

    public function viewOrders(Request $request, $name)
    {
        $currentPage = 'orders';
        $profile = Profile::findProfile($name);

        if (!$profile) {
            return abort(404);
        }

        $orders = Order::getOrdersByProfile($profile->id, config('app.orders_per_page'));

        return view('profile.view_orders', compact('profile', 'currentPage', 'orders'));
    }

    public function viewOffers(Request $request, $name)
    {
        $currentPage = 'offers';
        $profile = Profile::findProfile($name);

        if (!$profile) {
            return abort(404);
        }

        $orders = Order::getOffersByProfile($profile->id, config('app.orders_per_page'));

        return view('profile.view_offers', compact('profile', 'currentPage', 'orders'));
    }

    public function viewReviews(Request $request, $name)
    {
        $currentPage = 'reviews';
        $currentTab = 'about';
        $profile = Profile::findProfile($name);

        if (!$profile) {
            return abort(404);
        }

        $reviews = Review::getReviewsByProfile($profile->id, config('app.reviews_per_page'));
        $countAbout = Review::countByProfile($profile->id);
        $countMy = Review::countByAuthor($profile->id);

        return view('profile.view_reviews', compact(
            'profile',
            'reviews',
            'currentPage',
            'currentTab',
            'countAbout',
            'countMy'
        ));
    }

    public function viewReviewsMy(Request $request, $name)
    {
        $currentPage = 'reviews';
        $currentTab = 'my';
        $profile = Profile::findProfile($name);

        if (!$profile) {
            return abort('404');
        }
        
        $reviews = Review::getReviewsByAuthor($profile->id, config('app.reviews_per_page'));
        $countAbout = Review::countByProfile($profile->id);
        $countMy = Review::countByAuthor($profile->id);

        return view('profile.view_reviews', compact(
            'profile',
            'reviews',
            'currentPage',
            'currentTab',
            'countAbout',
            'countMy'
        ));
    }

    public function viewConnections(Request $request, $name)
    {
        $currentPage = 'connections';
        $profile = Profile::findProfile($name);

        if (!$profile) {
            return abort('404');
        }

        $connections = Connection::getByProfile($profile->id, config('app.connections_per_page'));

        return view('profile.view_connections', compact('profile', 'currentPage', 'connections'));
    }

    public function reviews(Request $request)
    {
        $currentPage = 'reviews';
        $currentTab = 'about';
        $profile = SessionWorker::instance()->currentProfile();

        if (!$profile) {
            return abort('404');
        }

        $reviews = Review::getReviewsByProfile($profile->id, config('app.reviews_per_page'));
        $countAbout = Review::countByProfile($profile->id);
        $countMy = Review::countByAuthor($profile->id);

        return view('profile.reviews', compact(
            'profile',
            'reviews',
            'currentPage',
            'currentTab',
            'countAbout',
            'countMy'
        ));
    }

    public function reviewsMy(Request $request)
    {
        $currentPage = 'reviews';
        $currentTab = 'my';
        $profile = SessionWorker::instance()->currentProfile();

        if (!$profile) {
            return abort('404');
        }
        
        $reviews = Review::getReviewsByAuthor($profile->id, config('app.reviews_per_page'));
        $countAbout = Review::countByProfile($profile->id);
        $countMy = Review::countByAuthor($profile->id);

        return view('profile.reviews', compact(
            'profile',
            'reviews',
            'currentPage',
            'currentTab',
            'countAbout',
            'countMy'
        ));
    }

    public function create()
    {
        $profile = new Profile;
        $cities = City::listByCountry(config('app.default_country_id'), true, true);
        $sections = Section::listAll(true);
        $entityTypes = Profile::entityTypes(true);
        $employeesList = Profile::employeesList(true);

        return view('profile.create', compact('profile', 'cities', 'sections', 'entityTypes', 'employeesList'));
    }

    public function postCreate(Request $request)
    {
        $user = Auth::user();
        $validator = Profile::validateCompanyData($request->all(), null);

        if ($validator->fails()) {
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $profile = Profile::createCompany($user);
        $profile->update($request->all());
        ActionLog::profileUpdate($profile, $request->all(), $user->id);

        SessionWorker::instance()->changeCurrentProfile($profile->id);

        return redirect()->route('profile::index')
            ->with('success', 'Профиль успешно создан!');
    }

    public function orders()
    {
        $profile = SessionWorker::instance()->currentProfile();
        $orders = Order::getOrdersByProfile($profile->id, config('app.orders_per_page'));

        return view('profile.orders', compact('orders'));
    }

    public function offers()
    {
        $profile = SessionWorker::instance()->currentProfile();
        $orders = Order::getOffersByProfile($profile->id, config('app.orders_per_page'));

        return view('profile.offers', compact('orders'));
    }

    public function deals()
    {
        $profile = SessionWorker::instance()->currentProfile();
        $dialogMember = DialogMember::getLastByProfile($profile->id);

        // return $dialogMember;

        return view('profile.deals', compact('dialogMember'));
    }

    public function dialog(Request $request, $id)
    {
        $user = Auth::user();
        $dialog = Dialog::start()->findOrFail($id);
        $profile = SessionWorker::instance()->currentProfile();

        if (!$dialog->dialogable) {
            abort(403, trans('messages.dialog_no_order_request'));
        }

        $starter = $dialog->dialogable->order->profile;

        if ($dialog->dialogable->order->isoffer) {
            $starter = $dialog->dialogable->profile;
        }

        if (Gate::denies('dialog-member', $dialog)) {
            abort(403);
        }

        $items = $dialog->getDialogItems();
        $lastItem = 0; //$items[count($items) - 1];
        $htmlHelper = new \App\Helpers\HtmlHelper;
        $dialog->setOtherMessagesRead($profile->id);

        $jsonData = json_encode([
            'dialogId' => $dialog->id,
            'starterId' => $starter->id,
            'profile' => $profile->getData(),
            'token' => UserToken::tokenByUserId($user->id),
            'deal' => $dialog->deal,
            'isoffer' => $dialog->dialogable->order->isoffer,
        ]);

        return view('profile.dialog', compact('dialog', 'items', 'lastItem', 'jsonData'));
    }

    public function history(Request $request, $id)
    {
        $user = Auth::user();
        $dialog = Dialog::start()->findOrFail($id);
        $profile = SessionWorker::instance()->currentProfile();

        if (Gate::denies('dialog-member', $dialog)) {
            abort(403);
        }

        $items = $dialog->getDialogActionItems();

        return view('profile.dialog_history', compact('dialog', 'items'));
    }

    public function dealReview(Request $request, $id)
    {
        $user = Auth::user();
        $dialog = Dialog::start()->findOrFail($id);
        $profile = SessionWorker::instance()->currentProfile();
        $deal = $dialog->deal;

        if (Gate::denies('dialog-member', $dialog)) {
            abort(403);
        }

        if (!$dialog->deal) {
            abort(403);
        }

        $author = $profile;
        $respondent = $dialog->dialogable->profile;
        $isoffer = (bool) $dialog->dialogable->order->isoffer;

        if ($author->id == $respondent->id) {
            $respondent = $dialog->dialogable->order->profile;
            $isoffer = !$isoffer;
        }

        $checkReview = Review::countByAuthorAndDeal($author->id, $deal->id);
        $starter = $dialog->dialogable->order->profile;

        return view('profile.deal_review', compact(
            'checkReview',
            'isoffer',
            'author',
            'starter',
            'respondent',
            'deal',
            'dialog'
        ));
    }

    public function postDealReview(Request $request, $id)
    {
        $user = Auth::user();
        $dialog = Dialog::start()->findOrFail($id);
        $profile = SessionWorker::instance()->currentProfile();
        $deal = $dialog->deal;

        if (Gate::denies('dialog-member', $dialog)) {
            abort(403);
        }

        if (!$dialog->deal) {
            abort(403);
        }

        $author = $profile;
        $respondent = $dialog->dialogable->profile;
        $isoffer = (bool) $dialog->dialogable->order->isoffer;

        if ($author->id == $respondent->id) {
            $respondent = $dialog->dialogable->order->profile;
            $isoffer = !$isoffer;
        }

        $checkReview = Review::countByAuthorAndDeal($author->id, $deal->id);

        if ($checkReview) {
            abort(403, trans('messages.deal_has_review'));
        }

        $starter = $dialog->dialogable->order->profile;

        $validator = Review::makeValidator([
            'param_quality',
            'param_time',
            'param_civility',
            'content',
        ], $request->all());

        if ($validator->fails()) {
            return redirect()->route('profile::deals::review', ['id' => $id])
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $review = new Review;
        $review->readData($request->all());
        $review->author_id = $author->id;
        $review->profile_id = $respondent->id;
        $review->deal_id = $deal->id;
        $review->type = $isoffer ? Review::TYPE_OFFER : Review::TYPE_DEFAULT;
        $review->save();

        $respondent->ratingChangeByReview($review->rating(), $review->maxRating());

        if ($review->isGoodRating() AND !Connection::checkConnection(
            $dialog->dialogable->profile_id,
            $dialog->dialogable->order->profile_id
        )) {
            $connection = new Connection;
            $connection->deal_id = $deal->id;
            $connection->from_profile_id = $dialog->dialogable->profile_id;
            $connection->to_profile_id = $dialog->dialogable->order->profile_id;
            $connection->save();
        }

        return redirect()->route('main::reviews')
            ->with('success', trans('messages.deal_review_success'));
    }

    public function dealClose($id)
    {
        $user = Auth::user();
        $profile = SessionWorker::instance()->currentProfile();
        $deal = Deal::findOrFail($id);
        $dialog = $deal->dialog;
        $performer = $dialog->dialogable->profile;

        if (Gate::denies('dialog-member', $dialog)) {
            abort(403);
        }

        if ($deal->closed()) {
            abort(403);
        }

        if (!$dialog->dialogable->order->isoffer AND $dialog->dialogable->order->profile_id != $profile->id) {
            abort(403);
        }

        if ($dialog->dialogable->order->isoffer AND $dialog->dialogable->profile_id != $profile->id) {
            abort(403);
        }

        return view('profile.deal_close', compact('performer', 'deal', 'dialog'));
    }

    public function postDealClose($id)
    {
        $user = Auth::user();
        $profile = SessionWorker::instance()->currentProfile();
        $deal = Deal::findOrFail($id);
        $dialog = $deal->dialog;
        $performer = $dialog->dialogable->profile;

        if (Gate::denies('dialog-member', $dialog)) {
            abort(403);
        }

        if ($deal->closed()) {
            abort(403);
        }

        if (!$dialog->dialogable->order->isoffer AND $dialog->dialogable->order->profile_id != $profile->id) {
            abort(403);
        }

        if ($dialog->dialogable->order->isoffer AND $dialog->dialogable->profile_id != $profile->id) {
            abort(403);
        }

        $deal->close();

        return redirect()->route('profile::deals::review', ['id' => $dialog->id]);
    }

    public function review($id)
    {
        $user = Auth::user();
        $deal = Deal::findOrFail($id);
        $dialog = $deal->dialog;
        $profile = SessionWorker::instance()->currentProfile();

        $performer = $dialog->dialogable->profile;

        if (Gate::denies('dialog-member', $dialog)) {
            abort(403);
        }

        return view('profile.review', compact('performer', 'deal'));
    }

    public function connections()
    {
        $user = Auth::user();
        $profile = SessionWorker::instance()->currentProfile();
        $connections = Connection::getProfileConnections($profile->id, config('app.connections_per_page'));

        return view('profile.connections', compact('connections'));
    }
}

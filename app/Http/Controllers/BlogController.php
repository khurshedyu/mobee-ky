<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\NewsItem;
use App\Components\ActiveMenu;

class BlogController extends Controller
{
    public function __construct($foo = null)
    {
        ActiveMenu::instance()->setKey('blog_index');
    }
    
    public function index()
    {
        $news = NewsItem::lastPublished(config('app.blog_items_per_page'));

        return view('blog.index', compact('news'));
    }

    public function view($id)
    {
        $item = NewsItem::findOrFail($id);
        $news = NewsItem::getBesideNews($id, config('app.blog_items_beside_max'));

        return view('blog.view', compact('item', 'news'));
    }
}

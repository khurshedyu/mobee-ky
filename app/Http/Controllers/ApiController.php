<?php

namespace App\Http\Controllers;

use Html;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Components\SessionWorker;
use App\Components\AjaxResponse;
use App\Dialog;
use App\DialogItem;
use App\Profile;
use App\User;
use App\UserToken;
use App\Notification;
use Gate;
use Auth;

class ApiController extends Controller
{
    const ERROR_NONE = 0;
    const ERROR_NOT_DIALOG_MEMBER = 1;
    const ERROR_DIALOG_NOT_FOUND = 2;
    const ERROR_PROFILE_NOT_FOUND = 3;
    const ERROR_USER_NOT_FOUND = 4;
    const ERROR_USER_TOKEN_PROFILE_OWN_FAIL = 5;

    private function response($data, $code)
    {
        return [
            'data' => $data,
            'code' => $code
        ];
    }

    public function chatMessages(Request $request, $did, $id)
    {
        $dialog = Dialog::start()->find($did);

        if (!$dialog) {
            return $this->response(['success' => false], self::ERROR_DIALOG_NOT_FOUND);
        }

        // if (Gate::denies('dialog-member', $dialog)) {
        //     return $this->response(['success' => false], self::ERROR_NOT_DIALOG_MEMBER);
        // }

        $newItems = $dialog->items()
            ->with('profile')
            ->after($id)
            ->orderBy('created_at', 'ASC')
            ->get();

        $list = [];
        $htmlHelper = new \App\Helpers\HtmlHelper;

        foreach ($newItems as $item) {
            if ($item->type == DialogItem::TYPE_MESSAGE) {
                $list[] = [
                    'id' => $item->id,
                    'avatar' => $item->profile->avatarUrl(),
                    'name' => $item->profile->displayName(),
                    'nameUrl' => $item->profile->linkName(),
                    'content' => $item->content,
                    'date' => $htmlHelper->formatDateTime($item->created_at, 'j F Y, в H:i:s'),
                    'type' => 'message',
                ];
            } else if ($item->type == DialogItem::TYPE_DEAL) {
                $deal = $item->getDeal();
                $list[] = [
                    'id' => $item->id,
                    'avatar' => null,
                    'name' => null,
                    'nameUrl' => $deal ? $deal->id : null,
                    'content' => $deal ? $htmlHelper->formatNum($deal->price, 0) : null,
                    'date' => $htmlHelper->formatDateTime($item->created_at, 'j F Y, в H:i:s'),
                    'type' => 'deal',
                    'params' => [
                        'closed' => $deal ? $deal->closed() : null,
                    ],
                ];
            }
        }

        // Get Dialog members and dialog information
        $dialoginfo = $dialog->members()->get(['dialog_id', 'profile_id'])->toArray();
        $members = [
            'dialog_id' => $dialoginfo[0]['dialog_id'],
            'members' => array_map(function($element) {
                return $element['profile_id'];
            }, $dialoginfo),
        ];

        return $this->response(['messages' => $list, 'members' => $members], self::ERROR_NONE);
    }

    public function chatSend(Request $request)
    {
        $dialogId = $request->input('dialog', null);
        $profileId = $request->input('profile', null);
        $message = $request->input('message', null);

        $dialog = Dialog::with('members')->find($dialogId);

        if (!$dialog) {
            return $this->response(null, self::ERROR_DIALOG_NOT_FOUND);
        }

        $profile = Profile::with('user')->find($profileId);

        if (!$profile) {
            return $this->response(null, self::ERROR_PROFILE_NOT_FOUND);
        }

        // foreach ($dialog->members as $member) {
        //     if (in_array($member->profile_id, $profile->user->profileIds())) {
        //         return $this->response(null, self::ERROR_NOT_DIALOG_MEMBER);
        //     }
        // }

        $item = $dialog->postMessage($message, $profile->id);
        $itemProfile = $item->profile;

        $htmlHelper = new \App\Helpers\HtmlHelper;

        $message = [
            'id' => $item->id,
            'avatar' => $profile->avatarUrl(),
            'name' => $profile->displayName(),
            'nameUrl' => $profile->linkName(),
            'content' => $item->content,
            'date' => $htmlHelper->formatDateTime($item->created_at, 'j F Y, в H:i:s'),
            'type' => 'message',
        ];

        return $this->response([
            'message' => $message,
        ], self::ERROR_NONE);
    }

    public function getUser(Request $request)
    {
        $id = $request->input('id', null);

        $token = UserToken::tokenByUserId($id);

        var_dump($token);
    }

    public function chatMember(Request $request, $did, $pid)
    {
        $token = $request->input('token');
        $user = UserToken::userByToken($token);

        if (!$user) {
            return $this->response(null, self::ERROR_USER_NOT_FOUND);
        }

        $dialog = Dialog::start()->find($did);


        if (!$dialog) {
            return $this->response(null, self::ERROR_DIALOG_NOT_FOUND);
        }

        if (!$user->ownsProfileId($pid)) {
            return $this->response(null, self::ERROR_USER_TOKEN_PROFILE_OWN_FAIL);
        }

        if (!$dialog->isMember($pid)) {
            return $this->response(null, self::ERROR_NOT_DIALOG_MEMBER);
        }

        // At this point we know that the user is right, so we search for his profile now
        $profile = Profile::with('user')->find($pid);

        $profileToSendToNodeJs = [
            'avatar' => $profile->avatarUrl(),
            'name' => $profile->displayName(),
            'nameUrl' => $profile->linkName(),
        ];

        return $this->response([
            'profile' => $profileToSendToNodeJs
        ], self::ERROR_NONE);
    }

    public function dealCreate(Request $request, $did, $pid)
    {
        $token = $request->input('token');
        $price = $request->input('price');
        $user = UserToken::userByToken($token);

        if (!$user) {
            return $this->response(null, self::ERROR_USER_NOT_FOUND);
        }

        $dialog = Dialog::start()->find($did);

        if (!$dialog) {
            return $this->response(null, self::ERROR_DIALOG_NOT_FOUND);
        }

        if (!$user->ownsProfileId($pid)) {
            return $this->response(null, self::ERROR_USER_TOKEN_PROFILE_OWN_FAIL);
        }

        if (!$dialog->isMember($pid)) {
            return $this->response(null, self::ERROR_NOT_DIALOG_MEMBER);
        }

        $dItem = $dialog->createDeal($price);
        $htmlHelper = new \App\Helpers\HtmlHelper;
        $deal = $dItem->getDeal();

        $message = [
            'id' => $dItem->id,
            'avatar' => null,
            'name' => null,
            'nameUrl' => $deal ? $deal->id : null,
            'content' => $deal ? $htmlHelper->formatNum($deal->price, 0) : null,
            'date' => $htmlHelper->formatDateTime($dItem->created_at, 'j F Y, в H:i:s'),
            'type' => 'deal',
            'params' => [
                'closed' => $deal->closed(),
            ],
        ];

        return $this->response([
            'message' => $message,
        ], self::ERROR_NONE);
    }

    public function notificationsCheck($id)
    {
        $user = Auth::user();
        $list = [];
        $html = new \App\Helpers\HtmlHelper;

        $notifications = Notification::byUser($user->id)
            ->where('id', '>', $id)
            ->isSelf(Notification::SELF_NO)
            ->orderLast()
            ->take(10)
            ->get();

        foreach ($notifications as $notification) {
            $list[] = [
                'id' => $notification->id,
                'data' => $notification->parseData(),
                'date' => $html->formatDateTime($notification->created_at),
            ];
        }

        return AjaxResponse::make($list)
            ->codeSuccess()
            ->get();

        // return $list;
    }

    public function usernameCheck($name, $default = null)
    {
        $profile = Profile::where('name', $name)->first();

        $busy = false;
        $clean = false;

        if($default)
        {
            if($profile AND $profile->name != $default)
            {
                $busy = true;
            }
        }
        else
        {
            if($profile)
            {
                $busy = true;
            }
        }

        if (preg_match("/^[a-zA-Z0-9_-]+$/", $name)) {
            $clean = true;
        }

        return AjaxResponse::make([
            'busy' => $busy,
            'clean' => $clean,
        ])
            ->codeSuccess()
            ->get();

        // return $this->response([
        //     'busy' => $busy,
        //     'clean' => $clean,
        // ], self::ERROR_NONE);
    }

    /**
     * Returns user unread messages count
     *
     * @return string json
     * @todo unused, delete if not needed
     */
    public function getUserUnreadMessages()
    {
        $newmessagescount = SessionWorker::instance()->currentProfile()->countNewMessages();
        return AjaxResponse::make(['count' => $newmessagescount])->codeSuccess()->get();
    }

    /**
     * Returns user last nofications
     *
     * @param int|Number $number Number of notifications
     * @return array
     */
    public function getUserLastNotifications($number = 3)
    {
        $notifications = Auth::user()->lastNotifications($number);

        // Prepare notifications for normal user
        foreach($notifications as $key => $notification)
        {
            $notifications[$key]['displayUrl'] = $notification->displayUrl();
            $notifications[$key]['displayMessage'] = $notification->displayMessage();
            $notifications[$key]['date'] = Html::formatDateTime($notification->created_at);
        }

        return AjaxResponse::make(['notifications' => $notifications])->codeSuccess()->get();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Components\Filters;
use App\Order;
use App\OrderRequest;
use App\Category;
use App\City;

class AdminController extends Controller
{
    public function makeFilters($params, $request)
    {
        $filtersConfig = [];

        foreach ($params as $param) {
            $param['value'] = $request->input($param['name'], $param['default']);
            $filtersConfig[] = $param;
        }

        $filters = new Filters($filtersConfig);

        return $filters;
    }

    public function index()
    {
        return view('admin.index');
    }

    public function orders(Request $request)
    {
        $filters = $this->makeFilters([
            [
                'type' => 'order',
                'name' => 'main_order',
                'label' => 'Сортировка',
                'default' => 1,
                'map' => [
                    1 => [
                        'title' => 'По дате (сначала новые)',
                        'value' => 'created_at DESC',
                    ],
                    2 => [
                        'title' => 'По дате (сначала старые)',
                        'value' => 'created_at ASC',
                    ],
                ],
            ],
            [
                'type' => 'select',
                'name' => 'status',
                'label' => 'Статус',
                'default' => 1,
                'map' => [
                    1 => [
                        'title' => 'Все',
                    ],
                    2 => [
                        'title' => 'Новые',
                        'value' => Order::STATUS_NEW,
                    ],
                    3 => [
                        'title' => 'Опубликованные',
                        'value' => Order::STATUS_PUBLIC,
                    ],
                    4 => [
                        'title' => 'Заблокированные',
                        'value' => Order::STATUS_BLOCK,
                    ],
                ],
            ],
            [
                'type' => 'text',
                'name' => 'id',
                'label' => 'ID заказа',
                'default' => '',
            ],
            [
                'type' => 'text',
                'name' => 'profile_id',
                'label' => 'ID пользователя',
                'default' => '',
            ],
        ], $request);

        $orders = Order::with('profile')
            ->applyFiltersAdmin($filters)
            ->paginate(10);

        return view('admin.orders', compact('orders', 'filters'));
    }

    public function ordersEdit($id)
    {
        $order = Order::findOrFail($id);
        $categoriesList = Category::withSectionsList(true);
        $cities = City::listByCountry(config('app.default_country_id'), true);

        return view('admin.orders_edit', compact('order', 'categoriesList', 'cities'));
    }

    public function ordersStatusPublic($id)
    {
        $order = Order::findOrFail($id);
        $order->setStatusPublic();
        $order->save();

        return redirect()->back();
    }

    public function ordersStatusBlock($id)
    {
        $order = Order::findOrFail($id);
        $order->setStatusBlock();
        $order->save();

        return redirect()->back();
    }

    public function ordersEditPost(Request $request, $id)
    {
        $validator = Order::makeValidator([
            'title',
            'content',
            'price',
            'price_request',
            'only_super',
            'only_checked',
            'city_id',
            'category_id',
            'isoffer',
        ], $request->all());

        if ($validator->fails()) {
            return redirect()->route('admin::orders::edit', ['id' => $id])
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $order = Order::findOrFail($id);
        $order->readData($request->all());
        $order->save();

        return redirect()->route('admin::orders::edit', ['id' => $id])
            ->with('success', 'Заказ успешно обновлен!');
    }

    public function ordersDelete($id)
    {
        $order = Order::findOrFail($id);
        $orderRequests = $order->orderRequests;

        foreach ($orderRequests as $orderRequest) {
            $dialog = $orderRequest->dialog;

            if ($dialog) {
                $deal = $dialog->deal;
                $items = $dialog->items;
                $members = $dialog->members;

                foreach ($members as $member) {
                    $member->delete();
                }

                if ($deal) {
                    $deal->delete();
                }
            }

            // $dialog->delete();
            $orderRequest->delete();
        }

        $order->delete();

        return redirect()->back();
    }
}

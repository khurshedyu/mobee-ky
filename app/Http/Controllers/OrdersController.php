<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Gate;
use Auth;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Components\SessionWorker;
use App\Components\AjaxResponse;
use App\Order;
use App\OrderRequest;
use App\City;
use App\Dialog;
use App\Notification;
use App\Category;
use App\Section;
use App\File;
use App\Page;
use App\ActionLog;

class OrdersController extends Controller
{
    public function index()
    {
        $orders = Order::start()
            ->orderBy('created_at', 'DESC')
            ->paginate(config('app.companies_per_page'));

        return view('orders.index', compact('orders'));
    }

    public function view(Request $request, $id)
    {
        $user = Auth::user();
        $profile = SessionWorker::instance()->currentProfile();

        // Get order in order to check if its published or not
        $order = Order::where('id', '=', $id)->first();

        // Get notification if there is any and set it to READ!
        $notification = Notification::ByUser($user->id)->IsRead(Notification::READ_NO)->get();

        // Go through all user notifications and get one which we should set as read
        foreach($notification as $notification)
        {
            if($id == $notification->contentToArray()['order_id'])
                $notification->setRead();
        }

        if(!$order)
        {
            abort(404, trans('messages.orders_view_404'));
        }
        elseif(!(bool)$order->status)
        {
            abort(404, trans('messages.orders_view_notpublished'));
        }

        $orderRequests = $order->getOrderRequests();

        return view('orders.view', compact('order', 'user', 'profile', 'orderRequests'));
    }

    public function postView(Request $request, $id)
    {
        $user = Auth::user();
        $profile = SessionWorker::instance()->currentProfile();
        $order = Order::getPublicOrder($id);

        if(!$order)
        {
            abort(404, trans('messages.orders_view_404'));
        }

        if(!$order->profile OR !$order->profile->user)
        {
            abort(403, trans('messages.orders_view_403'));
        }

        $orderRequest = new OrderRequest;
        $orderRequest->readData($request->all(), [
            'content',
            'profile_id',
        ]);

        if(!in_array($orderRequest->profile_id, $user->profileIds()))
        {
            return redirect()->back();
        }

        $orderRequest->order_id = $order->id;
        $orderRequest->save();

        // Notify myself
        Notification::noteAddOrderRequest(
            $order->id,
            $orderRequest->id,
            $user->id,
            $order->profile->user->id,
            $order->isoffer,
            true
        );

        // Notity other user
        Notification::noteAddOrderRequest(
            $order->id,
            $orderRequest->id,
            $user->id,
            $order->profile->user->id,
            $order->isoffer
        );

        ActionLog::orderPostRequest($order->id, $orderRequest->id, $user->id);

        return redirect()->back();
    }

    public function create()
    {
        return view('orders.create');
    }

    private function createAny(Request $request, $type)
    {
        $cities = City::listByCountry(config('app.default_country_id'), true);
        $profile = SessionWorker::instance()->currentProfile();
        $order = new Order;
        $order->isoffer = $type;
        $order->city_id = $profile->city_id;
        $orderCreate = Page::pageByName('order_create');

        $categoriesList = Category::withSectionsList(true);
        $rootCategories = Category::rootCategories();
        $sections = Section::listAll();
        // dd($categoriesList);

        return view('orders.create_any', compact(
            'cities',
            'profile',
            'order',
            'categoriesList',
            'rootCategories',
            'type',
            'sections',
            'orderCreate'));
    }

    public function createOrder(Request $request)
    {
        return $this->createAny($request, Order::IS_ORDER);
    }

    public function createOffer(Request $request)
    {
        return $this->createAny($request, Order::IS_OFFER);
    }

    public function postCreateOrder(Request $request)
    {
        return $this->postCreate($request);
    }

    public function postCreateOffer(Request $request)
    {
        return $this->postCreate($request);
    }

    public function postCreate(Request $request)
    {
        $user = Auth::user();
        $isoffer = $request->input('isoffer', 0);
        $validator = Order::makeValidator([
            'title',
            'content',
            'price',
            'price_request',
            'only_super',
            'only_checked',
            'city_id',
            'category_id',
            'isoffer',
        ], $request->all());

        if($validator->fails())
        {
            return redirect()->route('orders::create::' . ($isoffer ? 'offer' : 'order'))
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $profile = SessionWorker::instance()->currentProfile();
        $order = Order::createOrder(
            $request->all(),
            $profile->id);

        if(!$order)
        {
            return abort(403, trans('messages.orders_create_error'));
        }

        ActionLog::orderCreated($order->id, $user->id);

        return redirect()->route('orders::edit', ['id' => $order->id])
            ->with('success', trans('messages.orders_create_success'));
    }

    public function edit(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $profile = SessionWorker::instance()->currentProfile();
        $categoriesList = Category::withSectionsList(true);
        $rootCategories = Category::rootCategories();
        $sections = Section::listAll();

        if(Gate::denies('orders-edit', $order, $profile))
        {
            abort(403, trans('messages.orders_edit_403'));
        }

        $cities = City::listByCountry(config('app.default_country_id'), true);

        return view('orders.edit', compact(
            'order',
            'cities',
            'sections',
            'rootCategories',
            'categoriesList'));
    }

    public function postEdit(Request $request, $id)
    {
        $user = Auth::user();
        $profile = SessionWorker::instance()->currentProfile();
        $validator = Order::makeValidator([
            'title',
            'content',
            'price',
            'price_request',
            'only_super',
            'only_checked',
            'city_id',
            'category_id',
            'isoffer',
        ], $request->all());

        if($validator->fails())
        {
            return redirect()->route('orders::edit', ['id' => $id])
                ->withInput($request->all())
                ->withErrors($validator);
        }

        $order = Order::findOrFail($id);

        if(Gate::denies('orders-edit', $order, $profile))
        {
            abort(403, trans('messages.orders_edit_403'));
        }

        $order->updateOrder(
            $request->all(),
            $profile->id);

        ActionLog::orderEdited($order->id, $user->id);

        return redirect()->route('orders::edit', ['id' => $id])
            ->with('success', trans('messages.orders_update_success'));
    }

    public function process(Request $request, $id)
    {
        $orderRequest = OrderRequest::findOrFail($id);

        if(Gate::denies('order-request-create-dialog', $orderRequest))
        {
            abort(403);
        }

        $dialog = Dialog::createByOrderRequest($orderRequest);

        return redirect()->route('profile::deals::dialog', ['id' => $dialog->id]);
    }

    public function section(Request $request, $sectionname, $categoryname = null)
    {
        $section = Section::getSectionByName($sectionname);

        if(!$section)
        {
            abort(404);
        }

        $rootCategories = $section->getRootCategoriesOrdered();
        $categories = $section->categories;
        $category = null;

        if($categoryname)
        {
            $category = Category::getByName($categoryname);
        }

        $filters = [
            'show' => $request->input('show', 'all'),
        ];

        $categoryIds = [];
        if($category)
        {
            $categoryIds[] = $category->id;
            foreach($category->categories as $ct)
            {
                $categoryIds[] = $ct->id;
            }
        }
        else
        {
            foreach($categories as $ct)
            {
                $categoryIds[] = $ct->id;
            }
        }

        $orders = Order::getOrdersByCategories(
            $categoryIds,
            $filters,
            null,
            config('app.orders_per_page'));

        $widgetOrders = Order::getOrdersByCategories(
            $categoryIds,
            $filters,
            [
                'show_invert' => true,
                'order' => Order::ORDER_BY_RANDOM,
            ],
            config('app.orders_per_page'));

        return view('orders.sections', compact(
            'section',
            'orders',
            'widgetOrders',
            'category',
            'categories',
            'rootCategories',
            'sectionname',
            'categoryname',
            'filters'
        ));
    }

    public function category(Request $request, $name, $categoryname)
    {
        return $this->section($request, $name, $categoryname);
    }

    public function ordersection(Request $request, $id)
    {
        $data = [
            'categories' => [],
            'sub' => [],
        ];

        $categories = Category::listBySectionArray($id, 0, true);
        $data['categories'] = $categories;

        return AjaxResponse::make($data)
            ->codeSuccess()
            ->get();
    }

    public function ordersectioninfo(Request $request, $id)
    {
        $data = [
            'section_id' => 0,
            'categories' => [],
            'category_id' => 0,
            'sub' => [],
            'sub_id' => 0,
        ];

        $category = Category::where('id', $id)->first();

        if(!$category)
        {
            return AjaxResponse::make($data)
                ->codeSuccess()
                ->get();
        }

        $section = $category->section;
        $categories = Category::listBySectionArray($section->id, 0, true);
        $data['categories'] = $categories;

        if($category->isRoot())
        {
            $data['category_id'] = $category->id;
            $sub = Category::listByCategoryArray($category->id, true);
            $data['sub'] = $sub;
        }
        else
        {
            $parent = $category->parent;
            $data['category_id'] = $parent->id;
            $sub = Category::listByCategoryArray($parent->id, true);
            $data['sub'] = $sub;
            $data['sub_id'] = $category->id;
        }

        $data['section_id'] = $section->id;

        return AjaxResponse::make($data)
            ->codeSuccess()
            ->get();
    }
}

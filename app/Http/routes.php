<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::group(['as' => 'main::'], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'Main@index',
        ]);

        Route::get('/nomorelandings', [
            'as' => 'landing',
            'uses' => 'Main@landing',
        ]);

        Route::any('/payinfo', [
            'as' => 'payinfo',
            'uses' => 'AccountController@payInfo',
        ]);

        Route::get('/login', [
            'as' => 'login',
            'uses' => 'Auth\AuthController@getLogin',
        ]);

        Route::post('/login', [
            'uses' => 'Auth\AuthController@postLogin',
        ]);

        Route::get('/register', [
            'as' => 'register',
            'uses' => 'Auth\AuthController@getRegister',
        ]);

        Route::post('/register', [
            'uses' => 'Auth\AuthController@postRegister',
        ]);

        Route::get('/register/activate/{key}', [
            'as' => 'register::activate',
            'uses' => 'Auth\AuthController@registerActivate',
        ])->where('key', '[0-9a-zA-Z_-]+');

        Route::get('/logout', [
            'as' => 'logout',
            'uses' => 'Auth\AuthController@getLogout',
        ]);

        Route::get('/license', [
            'as' => 'license',
            'uses' => 'Main@license',
            'middleware' => ['auth'],
        ]);

        Route::post('/license', [
            'uses' => 'Main@postLicense',
            'middleware' => ['auth'],
        ]);

        Route::get('/connections', [
            'as' => 'connections',
            'uses' => 'Main@connections',
        ]);

        Route::get('/clients', [
            'as' => 'clients',
            'uses' => 'Main@clients',
        ]);

        Route::get('/companies', [
            'as' => 'companies',
            'uses' => 'Main@companies',
        ]);

        Route::get('/profiles', [
            'as' => 'profiles',
            'uses' => 'Main@profiles',
        ]);

        Route::get('/reviews', [
            'as' => 'reviews',
            'uses' => 'Main@reviews',
        ]);

        Route::get('/plus/{name}', [
            'as' => 'plus',
            'uses' => 'Main@plus',
        ])->where('name', '[0-9a-zA-Z_-]+');

        Route::post('/uploadimg', [
            'as' => 'uploadimg',
            'uses' => 'Main@uploadImg',
        ]);
    });

    Route::group(['prefix' => 'ads', 'as' => 'orders::'], function () {
        // Route::get('/', [
        //     'as' => 'index',
        //     'uses' => 'OrdersController@index',
        // ]);

        Route::get('/create', [
            'as' => 'create',
            'middleware' => ['auth'],
            'uses' => 'OrdersController@create',
        ]);

        // Route::post('/create', [
        //     'middleware' => ['auth'],
        //     'uses' => 'OrdersController@postCreate',
        // ]);

        Route::get('/create/order', [
            'as' => 'create::order',
            'middleware' => ['auth'],
            'uses' => 'OrdersController@createOrder',
        ]);

        Route::post('/create/order', [
            'middleware' => ['auth'],
            'uses' => 'OrdersController@postCreateOrder',
        ]);

        Route::get('/create/offer', [
            'as' => 'create::offer',
            'middleware' => ['auth'],
            'uses' => 'OrdersController@createOffer',
        ]);

        Route::post('/create/offer', [
            'middleware' => ['auth'],
            'uses' => 'OrdersController@postCreateOffer',
        ]);

        Route::get('/edit/{id}', [
            'as' => 'edit',
            'middleware' => ['auth'],
            'uses' => 'OrdersController@edit',
        ])->where('id', '[0-9]+');

        Route::post('/edit/{id}', [
            'middleware' => ['auth'],
            'uses' => 'OrdersController@postEdit',
        ])->where('id', '[0-9]+');

        Route::get('/process/{id}', [
            'as' => 'process',
            'middleware' => ['auth'],
            'uses' => 'OrdersController@process',
        ])->where('id', '[0-9]+');

        Route::get('/ordersection/{id}', [
            'as' => 'ordersection',
            'uses' => 'OrdersController@ordersection',
        ])->where('id', '[0-9]+');

        Route::get('/ordersectioninfo/{id}', [
            'as' => 'ordersectioninfo',
            'uses' => 'OrdersController@ordersectioninfo',
        ])->where('id', '[0-9]+');

        Route::get('/{id}', [
            'as' => 'view',
            'uses' => 'OrdersController@view',
        ])->where('id', '[0-9]+');

        Route::post('/{id}', [
            'middleware' => ['auth'],
            'uses' => 'OrdersController@postView',
        ])->where('id', '[0-9]+');

        Route::get('/{name}', [
            'as' => 'section',
            'uses' => 'OrdersController@section',
        ])->where(['name' => '[a-zA-Z_-]+']);

        Route::get('/{name}/{categoryname}', [
            'as' => 'category',
            'uses' => 'OrdersController@category',
        ])->where(['name' => '[a-zA-Z_-]+', 'categoryname' => '[a-zA-Z_-]+']);
    });

    Route::group(['prefix' => 'profile', 'as' => 'profile::'], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'ProfileController@index',
            'middleware' => ['auth'],
        ]);

        Route::post('/', [
            'uses' => 'ProfileController@postIndex',
            'middleware' => ['auth'],
        ]);

        Route::get('/create', [
            'as' => 'create',
            'uses' => 'ProfileController@create',
            'middleware' => ['auth'],
        ]);

        Route::post('/create', [
            'uses' => 'ProfileController@postCreate',
            'middleware' => ['auth'],
        ]);

        Route::get('/change/{id}', [
            'as' => 'change',
            'uses' => 'ProfileController@change',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::get('/orders', [
            'as' => 'orders',
            'uses' => 'ProfileController@orders',
            'middleware' => ['auth'],
        ]);

        Route::get('/offers', [
            'as' => 'offers',
            'uses' => 'ProfileController@offers',
            'middleware' => ['auth'],
        ]);

        Route::get('/deals', [
            'as' => 'deals',
            'uses' => 'ProfileController@deals',
            'middleware' => ['auth'],
        ]);

        Route::get('/deals/close/{id}', [
            'as' => 'deals::close',
            'uses' => 'ProfileController@dealClose',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::post('/deals/close/{id}', [
            'uses' => 'ProfileController@postDealClose',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::get('/dialog/{id}', [
            'as' => 'deals::dialog',
            'uses' => 'ProfileController@dialog',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        // Route::post('/dialog/{id}', [
        //     'uses' => 'ProfileController@postDialog',
        //     'middleware' => ['auth'],
        // ])->where('id', '[0-9]+');

        Route::get('/dialog/{id}/history', [
            'as' => 'deals::history',
            'uses' => 'ProfileController@history',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::get('/dialog/{id}/review', [
            'as' => 'deals::review',
            'uses' => 'ProfileController@dealReview',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::post('/dialog/{id}/review', [
            'uses' => 'ProfileController@postDealReview',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::get('/connections', [
            'as' => 'connections',
            'uses' => 'ProfileController@connections',
            'middleware' => ['auth'],
        ]);

        Route::get('/reviews', [
            'as' => 'reviews',
            'uses' => 'ProfileController@reviews',
            'middleware' => ['auth'],
        ]);

        Route::get('/reviews/my', [
            'as' => 'reviews::my',
            'uses' => 'ProfileController@reviewsMy',
            'middleware' => ['auth'],
        ]);

        Route::get('/review/{id}', [
            'as' => 'review',
            'uses' => 'ProfileController@review',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::post('/review/{id}', [
            'uses' => 'ProfileController@postReview',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::get('/visits', [
            'as' => 'visits',
            'uses' => 'ProfileController@visits',
            'middleware' => ['auth'],
        ]);

        Route::get('/support', [
            'as' => 'support',
            'uses' => 'ProfileController@support',
            'middleware' => ['auth'],
        ]);

        Route::get('/{name}/orders', [
            'as' => 'view::orders',
            'uses' => 'ProfileController@viewOrders',
            'middleware' => ['auth'],
        ])->where('name', '[0-9a-zA-Z_-]+');

        Route::get('/{name}/offers', [
            'as' => 'view::offers',
            'uses' => 'ProfileController@viewOffers',
            'middleware' => ['auth'],
        ])->where('name', '[0-9a-zA-Z_-]+');

        Route::get('/{name}/reviews', [
            'as' => 'view::reviews',
            'uses' => 'ProfileController@viewReviews',
            'middleware' => ['auth'],
        ])->where('name', '[0-9a-zA-Z_-]+');

        Route::get('/{name}/reviews/my', [
            'as' => 'view::reviews::my',
            'uses' => 'ProfileController@viewReviewsMy',
            'middleware' => ['auth'],
        ])->where('name', '[0-9a-zA-Z_-]+');

        Route::get('/{name}/connections', [
            'as' => 'view::connections',
            'uses' => 'ProfileController@viewConnections',
            'middleware' => ['auth'],
        ])->where('name', '[0-9a-zA-Z_-]+');

        Route::get('/{name}', [
            'as' => 'view',
            'uses' => 'ProfileController@view',
            'middleware' => ['auth'],
        ])->where('name', '[0-9a-zA-Z_-]+');
    });

    Route::group(['prefix' => 'account', 'as' => 'account::'], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'AccountController@index',
            'middleware' => ['auth'],
        ]);

        Route::get('/password', [
            'as' => 'password',
            'uses' => 'AccountController@password',
            'middleware' => ['auth'],
        ]);

        Route::get('/super', [
            'as' => 'super',
            'uses' => 'AccountController@super',
            'middleware' => ['auth'],
        ]);

        Route::get('/super/info', [
            'as' => 'super::info',
            'uses' => 'AccountController@superInfo',
            'middleware' => ['auth'],
        ]);

        Route::get('/billing', [
            'as' => 'billing',
            'uses' => 'AccountController@billing',
            'middleware' => ['auth'],
        ]);

        Route::post('/billing', [
            'uses' => 'AccountController@postBilling',
            'middleware' => ['auth'],
        ]);

        Route::get('/billing/pay/{id}', [
            'as' => 'billing::pay',
            'uses' => 'AccountController@billingPay',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::get('/billing/check/{id}', [
            'as' => 'billing::check',
            'uses' => 'AccountController@billingCheck',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        // Route::get('/billing/pay/success/{id}', [
        //     'as' => 'billing::pay::success',
        //     'uses' => 'AccountController@billingPaySuccess',
        //     'middleware' => ['auth'],
        // ])->where('id', '[0-9]+');

        Route::get('/billing/history', [
            'as' => 'billing::history',
            'uses' => 'AccountController@billingHistory',
            'middleware' => ['auth'],
        ]);

        Route::get('/billing/invoices', [
            'as' => 'billing::invoices',
            'uses' => 'AccountController@billingInvoices',
            'middleware' => ['auth'],
        ]);

        Route::get('/super', [
            'as' => 'super',
            'uses' => 'AccountController@super',
            'middleware' => ['auth'],
        ]);

        Route::get('/super/buy/{id}', [
            'as' => 'super::buy',
            'uses' => 'AccountController@superBuy',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::post('/super/buy/{id}', [
            'uses' => 'AccountController@postSuperBuy',
            'middleware' => ['auth'],
        ])->where('id', '[0-9]+');

        Route::get('/notifications', [
            'as' => 'notifications',
            'uses' => 'AccountController@notificationsAll',
            'middleware' => ['auth'],
        ]);

        Route::get('/notifications/my', [
            'as' => 'notifications::my',
            'uses' => 'AccountController@notificationsMy',
            'middleware' => ['auth'],
        ]);

        Route::get('/notifications/others', [
            'as' => 'notifications::others',
            'uses' => 'AccountController@notificationsOthers',
            'middleware' => ['auth'],
        ]);
    });

    Route::group(['prefix' => 'enterprise', 'as' => 'admin::', 'middleware' => ['auth']], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'AdminController@index',
        ]);

        Route::get('/orders', [
            'as' => 'orders',
            'uses' => 'AdminController@orders',
        ]);

        Route::get('/orders/edit/{id}', [
            'as' => 'orders::edit',
            'uses' => 'AdminController@ordersEdit',
        ]);

        Route::post('/orders/edit/{id}', [
            'uses' => 'AdminController@ordersEditPost',
        ]);

        Route::get('/orders/status/{id}/public', [
            'as' => 'orders::status::public',
            'uses' => 'AdminController@ordersStatusPublic',
        ]);

        Route::get('/orders/status/{id}/block', [
            'as' => 'orders::status::block',
            'uses' => 'AdminController@ordersStatusBlock',
        ]);

        Route::get('/orders/delete/{id}', [
            'as' => 'orders::delete',
            'uses' => 'AdminController@ordersDelete',
        ]);
    });

    Route::group(['prefix' => 'webapi', 'as' => 'webapi::'], function () {
        // Route::post('/chat/send', [
        //     'uses' => 'WebApiController@chatSend',
        //     'middleware' => ['auth'],
        // ]);

        // Route::get('/chat/messages/{did}/{id}', [
        //     'uses' => 'WebApiController@chatMessages',
        //     'middleware' => ['auth'],
        // ])->where(['did' => '[0-9]+', 'id' => '[0-9]+']);

        Route::get('/notifications/check/{id}', [
            'uses' => 'ApiController@notificationsCheck',
            'middleware' => ['auth'],
        ])->where(['id' => '[0-9]+']);

        Route::get('/username/check/{name}/{default?}', [
            'uses' => 'ApiController@usernameCheck',
            'middleware' => ['auth'],
        ]);
    });

    Route::group(['prefix' => 'page', 'as' => 'page::'], function () {
        Route::get('/{name}', [
            'as' => 'show',
            'uses' => 'PageController@show',
        ])->where(['name' => '[a-zA-Z0-9_-]+']);
    });

    Route::group(['prefix' => 'blog', 'as' => 'blog::'], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'BlogController@index',
        ]);

        Route::get('/{id}', [
            'as' => 'view',
            'uses' => 'BlogController@view',
        ])->where(['id' => '[0-9]+']);
    });

    Route::group(['prefix' => 'support', 'as' => 'faq::'], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'FaqController@index',
        ]);

        Route::get('/base/{name}', [
            'as' => 'category',
            'uses' => 'FaqController@category',
        ])->where(['name' => '[a-zA-Z0-9_-]+']);

        Route::get('/answer/{name}', [
            'as' => 'item',
            'uses' => 'FaqController@item',
        ])->where(['name' => '[a-zA-Z0-9_-]+']);

        Route::get('/message', [
            'as' => 'message',
            'middleware' => ['auth'],
            'uses' => 'FaqController@message',
        ]);

        Route::post('/message', [
            'as' => 'message',
            'middleware' => ['auth'],
            'uses' => 'FaqController@postMessage',
        ]);
    });
});

Route::group(['middleware' => ['api']], function () {
    Route::group(['prefix' => 'api', 'as' => 'api::'], function () {
        Route::post('/chat/send', [
            'uses' => 'ApiController@chatSend',
        ]);

        Route::get('/chat/messages/{did}/{id}', [
            'uses' => 'ApiController@chatMessages',
        ])->where(['did' => '[0-9]+', 'id' => '[0-9]+']);

        Route::get('/chat/member/{did}/{pid}', [
            'uses' => 'ApiController@chatMember',
        ])->where(['did' => '[0-9]+', 'pid' => '[0-9]+']);

        Route::post('/deal/create/{did}/{pid}', [
            'uses' => 'ApiController@dealCreate',
        ])->where(['did' => '[0-9]+', 'pid' => '[0-9]+']);

        Route::get('/chat/unreadmessages', [
            'uses' => 'ApiController@getUserUnreadMessages',
        ]);

        Route::get('/notifications/last/{number?}', [
            'uses' => 'ApiController@getUserLastNotifications',
        ])->where(['number' => '[0-9]+']);

    });
});
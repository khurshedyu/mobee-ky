<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    const STATUS_ARCHIVE = 0;
    const STATUS_PUBLISHED = 1;

    static public function getSectionByName($name)
    {
        return self::with('categories')
            ->byName($name)
            ->published()
            ->first();
    }

    static public function listAll($withNull = false)
    {
        $list = [];
        $items = self::published()
            ->weightOrder()
            ->get();

        if ($withNull) {
            $list[0] = 'Выберите сферу деятельности';
        }

        foreach ($items as $item) {
            $list[$item->id] = $item->title;
        }

        return $list;
    }

    public function listRoot($max = null)
    {
        $categories = [];

        $i = 0;
        foreach ($this->rootCategories as $cat) {
            $categories[] = $cat;
            $i++;

            if ($max AND $i >= $max) {
                break;
            }
        }

        return $categories;
    }

    public function getRootCategoriesOrdered()
    {
        return $this->rootCategories()
            ->orderBy('weight', 'DESC')
            ->orderBy('title', 'ASC')
            ->get();
    }

    /**
     * SCOPES
     */

    public function scopePublished($q)
    {
        $q->where('status', self::STATUS_PUBLISHED);

        return $q;
    }

    public function scopeWeightOrder($q)
    {
        $q->orderBy('weight', 'DESC');

        return $q;
    }

    public function scopeByName($q, $name)
    {
        $q->where('name', $name);

        return $q;
    }

    /**
     * HAS MANY
     */

    public function categories()
    {
        return $this->hasMany('App\Category', 'section_id', 'id');
    }

    public function rootCategories()
    {
        return $this->hasMany('App\Category', 'section_id', 'id')->whereRaw('section_id = categories.section_id AND (category_id IS NULL OR category_id = 0)')->orderBy('title', 'DESC');
    }
}

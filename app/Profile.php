<?php

namespace App;

use App\Components\Model;
use App\DialogMember;
use Carbon\Carbon;
use Validator;
use App\DialogItem;

class Profile extends Model
{
    const SEX_NONE = 0;
    const SEX_MALE = 1;
    const SEX_FEMALE = 2;

    const TYPE_USER = 1;
    const TYPE_COMPANY = 2;

    const STATUS_BANNED = -1;
    const STATUS_CLOSED = 0;
    const STATUS_NEW = 1;
    const STATUS_ACTIVE = 2;

    const ENTITY_TYPE_IP = 1;
    const ENTITY_TYPE_TOO = 2;

    protected $fillable = [
        'name',
        'prefix',
        'address',
        'firstname',
        'secondname',
        'lastname',
        'sex',
        'birth_date',
        'city_id',
        'entity_type',
        'employees',
        'phone',
        'section_id',
        'avatar_id',
    ];

    private $countDialogs = null;
    private $countDeals = null;

    static public function createByUser($user, $melomanData = null)
    {
        $profile = new self;
        $profile->user_id = $user->id;
        $profile->type = self::TYPE_USER;
        $profile->status = self::STATUS_ACTIVE;
        $profile->sex = self::SEX_NONE;
        $profile->birth_date = '0000-00-00';

        if ($melomanData) {
            $profile->name = $melomanData['nickname'];
            $profile->firstname = $melomanData['name'];
            $profile->secondname = $melomanData['secondname'];
            $profile->lastname = $melomanData['surname'];
            $profile->sex = $melomanData['gender'] ? 
                ($melomanData['gender'] == 'f' ? self::SEX_FEMALE : self::SEX_MALE) : 
                self::SEX_NONE;
            $profile->birth_date = $melomanData['birthdate'] ?
                $melomanData['birthdate'] :
                '0000-00-00';
        }

        $profile->save();

        return $profile;
    }

    static public function createCompany($user)
    {
        $profile = new self;
        $profile->user_id = $user->id;
        $profile->type = self::TYPE_COMPANY;
        $profile->status = self::STATUS_NEW;
        $profile->sex = self::SEX_NONE;
        $profile->birth_date = '0000-00-00';

        $profile->save();

        return $profile;
    }

    static public function listSex()
    {
        return [
            self::SEX_NONE => 'Не выбран',
            self::SEX_MALE => 'Мужчина',
            self::SEX_FEMALE => 'Женщина',
        ];
    }

    static public function entityTypes($withNull = false)
    {
        $list = [];

        if ($withNull) {
            $list[0] = 'Выберите тип бизнеса';
        }

        $list[self::ENTITY_TYPE_IP] = 'ИП';
        $list[self::ENTITY_TYPE_TOO] = 'ТОО';

        return $list;
    }

    static public function employeesList($withNull = false)
    {
        $list = [];

        if ($withNull) {
            $list[0] = 'Выберите количество сотрудников';
        }

        $list[10] = '1-10 чел.';
        $list[50] = '11-50 чел.';
        $list[200] = '51-200 чел.';
        $list[1000] = '201-1000 чел.';
        $list[10000] = '1 001-10 000 чел.';
        $list[100000] = '10 000 и больше чел.';

        return $list;
    }

    static public function findProfile($name)
    {
        $matches = null;
        $profile = null;

        $query = self::start()
         ->where('profiles.city_id', '>', 0);

        if (preg_match("/^id([0-9]+)$/", $name, $matches)) {
            $profile = $query->byId($matches[1])->first();
            // $profile = Profile::start()->byId($matches[1])->first();
        } else if (preg_match("/^[a-zA-Z0-9_-]+$/", $name, $matches)) {
            $profile = $query->byName($matches[0])->first();
            // $profile = Profile::start()->byName($matches[0])->first();
        }

        return $profile;
    }

    public function getData()
    {
        return [
            'id' => $this->id,
            'name' => $this->displayName(),
            'nameUrl' => $this->linkName(),
            'url' => route('profile::view', ['name' => $this->linkName()]),
            'avatar' => $this->avatarUrl(),
        ];
    }

    public function displayEntityType()
    {
        if ($this->entity_type == 1) {
            return 'ИП';
        }

        if ($this->entity_type == 2) {
            return 'ТОО';
        }

        return null;
    }

    public function displayName()
    {
        $displayName = 'Инкогнито';

        if ($this->firstname) {
            $displayName = $this->firstname;

            if ($this->lastname) {
                $displayName .= ' ' . $this->lastname;
            }
        } else if ($this->name) {
            $displayName = $this->name;
        }

        return $displayName;
    }

    public function linkName()
    {
        return !empty($this->name) ? $this->name : 'id' . $this->id;
    }

    public function profileTitle()
    {
        return ($this->type == self::TYPE_USER) ? 'Частное лицо' : 'Компания';
    }

    public function isCompany()
    {
        return $this->type == self::TYPE_COMPANY;
    }

    public function isChecked()
    {
        return (bool) $this->checked;
    }

    public function isSuper()
    {
        return $this->user AND $this->user->isSuper();
    }

    public function avatarUrl()
    {
        if ($this->avatar_id) {
            return '/i/avatars/' . $this->avatar->path;
        }

        return config('app.default_avatar_url');
    }

    public function bgUrl()
    {
        return config('app.default_bg_url');
    }

    public function countSuccessDeals()
    {
        return 0;
    }

    public function countDialogs()
    {
        if ($this->countDialogs === null) {
            $this->countDialogs = DialogMember::byProfile($this->id)
                ->count();
        }

        return $this->countDialogs;
    }

    public function countDeals()
    {
        if ($this->countDeals === null) {
            $dm = DialogMember::with('dialog', 'dialog.deal')
                ->byProfile($this->id)
                ->get();

            $this->countDeals = 0;

            foreach ($dm as $d) {
                if (isset($d->dialog) AND isset($d->dialog->deal)) {
                    $this->countDeals++;
                }
            }
        }

        return $this->countDeals;
    }

    public function countNewMessages()
    {
        $read = DialogItem::READ_NO;
        $types = [
            DialogItem::TYPE_MESSAGE,
        ];

        return DialogItem::select('dialog_items.id')
            ->rightJoin('dialog_members', 'dialog_members.dialog_id', '=', 'dialog_items.dialog_id')
            ->where('dialog_members.profile_id', $this->id)
            ->where('dialog_items.read', $read)
            ->where('dialog_items.profile_id', '<>', $this->id)
            ->whereIn('dialog_items.type', $types)
            ->count();
    }

    public function countComments()
    {
        $countAbout = Review::byProfile($this->id)
            ->orderBy('created_at', 'desc')
            ->count();

        $countMy = Review::byAuthor($this->id)
            ->orderBy('created_at', 'desc')
            ->count();

        return $countAbout + $countMy;
    }

    public function countReviews()
    {
        return $this->countComments();
    }

    public function countOrders()
    {
        return $this->orders()->isOrder()->count();
    }

    public function countOffers()
    {
        return $this->orders()->isOffer()->count();
    }

    public function countConnections()
    {
        $count = Connection::where('from_profile_id', $this->id)
            ->orWhere('to_profile_id', $this->id)
            ->count();

        return $count;
    }

    public function countVisits()
    {
        return 0;
    }

    public function countSupport()
    {
        return 0;
    }

    public function cityName()
    {
        return isset($this->city) ? $this->city->name : 'Не указан';
    }

    public function experience()
    {
        $createdAt = new Carbon($this->created_at);
        $now = new Carbon('NOW');
        $diff = $now->timestamp - $createdAt->timestamp;

        $secDay = 60 * 60 * 24;
        $secWeek = $secDay * 7;
        $secMonth = $secWeek * 4;

        if ($diff < $secDay) {
            return '1 день';
        } elseif ($diff < $secDay * 2) {
            return '2 дня';
        } elseif ($diff < $secDay * 3) {
            return '3 дня';
        } elseif ($diff < $secDay * 4) {
            return '4 дня';
        } elseif ($diff < $secDay * 5) {
            return '5 дней';
        } elseif ($diff < $secDay * 6) {
            return '6 дней';
        } elseif ($diff < $secWeek) {
            return '1 неделя';
        } elseif ($diff < $secWeek * 2) {
            return '2 недели';
        } elseif ($diff < $secWeek * 3) {
            return '3 недели';
        } elseif ($diff < $secMonth) {
            return '1 месяц';
        } elseif ($diff < $secMonth * 2) {
            return '2 месяца';
        } elseif ($diff < $secMonth * 3) {
            return '3 месяца';
        } elseif ($diff < $secMonth * 4) {
            return '4 месяца';
        } elseif ($diff < $secMonth * 5) {
            return '5 месяцев';
        } elseif ($diff < $secMonth * 6) {
            return 'Пол года';
        } elseif ($diff < $secMonth * 12) {
            return 'Больше полу года';
        }

        return 'Больше года';
    }

    private function userQuality()
    {
        $q = 0;

        if ($this->name) {
            $q += 5;
        }

        if ($this->firstname) {
            $q += 5;
        }

        if ($this->secondname) {
            $q += 5;
        }

        if ($this->lastname) {
            $q += 5;
        }

        $bd = explode('-', $this->birth_date);

        if ((int) $bd[0]) {
            $q += 5;
        }

        if ($this->sex) {
            $q += 5;
        }

        if ($this->city_id) {
            $q += 5;
        }

        if ($this->avatar_id) {
            $q += 5;
        }

        if ($this->phone) {
            $q += 10;
        }

        if ($this->checked) {
            $q += 50;
        }

        return $q;
    }

    private function companyQuality()
    {
        $q = 0;

        if ($this->city_id) {
            $q += 5;
        }

        if ($this->name) {
            $q += 5;
        }

        if ($this->section_id) {
            $q += 5;
        }

        if ($this->entity_type) {
            $q += 5;
        }

        if ($this->prefix) {
            $q += 5;
        }

        if ($this->firstname) {
            $q += 5;
        }

        $bd = explode('-', $this->birth_date);

        if ((int) $bd[0]) {
            $q += 5;
        }

        if ($this->employees) {
            $q += 5;
        }

        if ($this->address) {
            $q += 5;
        }

        if ($this->avatar_id) {
            $q += 5;
        }

        if ($this->phone) {
            $q += 10;
        }

        if ($this->checked) {
            $q += 40;
        }


        return $q;
    }

    public function quality()
    {
        return $this->isCompany() ? $this->companyQuality() : $this->userQuality();
    }

    public function ratingPlus($val)
    {
        $this->rating += $val;
        $this->save();
    }

    public function ratingMinus($val)
    {
        $this->rating -= $val;
        $this->save();
    }

    public function countRating()
    {
        $rating = $this->rating;

        if ($this->user->isSuper()) {
            $rating = $rating * ($rating > 0 ? 1.2 : 0.8);
        }

        return $rating;
    }

    public function ratingChangeByReview($value, $of)
    {
        $part = $of / 3;

        if ($value < $part) {
            $this->ratingMinus(100);
        } else if ($part <= $value AND $value < $part * 2) {
            $this->ratingMinus(10);
        } else if ($part * 2 <= $value AND $value < $of) {
            $this->ratingPlus(10);
        } else if ($of <= $value) {
            $this->ratingPlus(100);
        }
    }

    /**
     * Validators
     */

    static public function validateUserData($data, $id)
    {
        $validator = Validator::make($data, [
            'name' => 'string|regex:/^(?!id[0-9])[a-zA-Z0-9_-]+$/|min:5|max:255|unique:profiles,name,' . $id,
            'firstname' => 'required|max:255',
            'secondname' => 'max:255',
            'lastname' => 'required|max:255',
            'sex' => 'integer|in:0,1,2',
            'city_id' => 'required|integer|min:1',
            'birth_date' => 'date',

            'prefix' => 'max:255',
            'section_id' => 'integer|min:1',
            'employees' => 'integer',
            'phone' => 'max:255',
        ]);

        return $validator;
    }

    static public function validateCompanyData($data, $id)
    {
        $validator = Validator::make($data, [
            'name' => 'string|regex:/^[a-zA-Z0-9_-]+$/|min:5|max:255|unique:profiles,name,' . $id,
            'firstname' => 'required|max:255',
            'secondname' => 'max:255',
            'lastname' => 'max:255',
            'sex' => 'integer|in:0,1,2',
            'city_id' => 'required|integer|min:1',
            'birth_date' => 'date',

            'prefix' => 'max:255',
            'address' => 'max:255',
            'section_id' => 'required|integer|min:1',
            'employees' => 'integer',
            'phone' => 'max:255',
        ]);

        return $validator;
    }

    /**
     * BELONGS TO
     */

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'city_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function avatar()
    {
        return $this->belongsTo('App\File', 'avatar_id', 'id');
    }

    /**
     * HAS MANY
     */

    public function orders()
    {
        return $this->hasMany('App\Order', 'profile_id', 'id');
    }

    /**
     * SCOPES
     */

    public function scopeStart($query)
    {
        $query->with('city', 'city.region');

        return $query;
    }

    public function scopeById($query, $id)
    {
        $query->where('id', $id);

        return $query;
    }

    public function scopeByName($query, $name)
    {
        $query->where('name', $name);

        return $query;
    }

    public function scopeByUser($query, $user)
    {
        switch (gettype($user)) {
            case 'object':
                $query->where('user_id', $user->id);
                break;
            case 'array':
                $query->where('user_id', $user['id']);
                break;
            case 'integer':
            case 'string':
                $query->where('user_id', $user);
                break;
        }

        return $query;
    }

    public function scopeIsUser($query)
    {
        $query->where('type', self::TYPE_USER);

        return $query;
    }

    public function scopeIsCompany($query)
    {
        $query->where('type', self::TYPE_COMPANY);

        return $query;
    }

    public function scopeActive($query)
    {
        $query->where('status', self::STATUS_ACTIVE);

        return $query;
    }

    public function scopeNew($query)
    {
        $query->where('status', self::STATUS_NEW);

        return $query;
    }

    public function scopeActiveOrNew($query)
    {
        $query->whereIn('status', [self::STATUS_NEW, self::STATUS_ACTIVE]);

        return $query;
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 10/21/16
 * Time: 4:03 PM
 */

namespace App\Helpers;


class GeneralHelper
{
    /**
     * Generates wordendings depending on number
     *
     * @param int $number
     * @param array $wordarray
     * @return string
     */
    public static function generateRussianWordendingForNumbers($number, $wordarray)
    {
        $keys = array(2, 0, 1, 1, 1, 2);
        $mod = $number % 100;
        $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
        return $wordarray[$suffix_key];
    }

    /**
     * Get node js config, for some things
     * @see layout.blade.php file
     *
     * @return array
     */
    public static function getNodeJsConfig()
    {
        $content = file_get_contents(getcwd() . '/../.app.json');
        return json_decode($content, true);
    }
}
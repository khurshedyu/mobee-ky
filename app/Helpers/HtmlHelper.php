<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\PageLink;
use App\Section;
use App\Components\Table;

class HtmlHelper
{
    public function formatDate($date, $format = 'j F Y', $timezone = null)
    {
        $carbon = new Carbon($date);

        if ($timezone) {
            $carbon->setTimezone($timezone);
        }

        $string = $carbon->format($format);

        $string = str_replace([
            'January', 'February', 'March', 'April', 
            'May', 'June', 'July', 'August', 
            'September', 'October', 'November', 'December', 

            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec',
        ], [
            'января', 'февраля', 'марта', 'апреля', 
            'мая', 'июня', 'июля', 'августа', 
            'сентября', 'октября', 'ноября', 'декабря', 

            'янв', 'фев', 'мар', 'апр', 'мая', 'июн', 
            'июл', 'авг', 'сен', 'окт', 'ноя', 'дек',
        ], $string);

        return $string;
    }

    public function formatDateTime($date, $format = 'j F Y, в H:i')
    {
        $timezone = config('app.apptimezone');

        return $this->formatDate($date, $format, $timezone);
    }

    public function formatNum($value, $decimals = 2)
    {
        return number_format($value, $decimals, '.', ' ');
    }

    public function pages($count = 0, $max = 5, $filters = [])
    {
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $from = 1;
        $to = $count;

        if ($count > $max) {
            $from = $page - floor($max * 0.5);
            $to = $page + floor($max * 0.5);

            if ($from < 1) {
                $to -= $from;
                $to += 1;
                $from = 1;
            }

            if ($to > $count) {
                $from -= $to - $count;
                $to = $count;
            }
        }

        return view('html.pages', compact('count', 'page', 'max', 'from', 'to', 'filters'));
    }

    public function select($list = [], $value = null, $options = [])
    {
        return view('html.select', compact('list', 'value', 'options'));
    }

    public function formElement($title, $name, $value, $type = 'text', $options = [])
    {
        $view = 'text_field';

        if (!isset($options['id'])) {
            $options['id'] = substr(md5(microtime()), 0, 10);
        }

        switch ($type) {
            case 'text':
            case 'email':
            case 'password':
                $view = 'text_field';
                break;
            case 'textarea':
                $view = 'textarea_field';
                break;
            case 'date':
            case 'time':
                $view = 'date_field';
                break;
            case 'select':
                $view = 'select_field';
                break;
            case 'checkbox':
                $view = 'checkbox_field';
                break;
            case 'save':
                $view = 'save_button';
                break;
            case 'image_ajax':
                $view = 'image_ajax';
                break;
        }

        return view('html.' . $view, compact('title', 'name', 'value', 'type', 'options'));
    }

    public function textField($title, $name, $value, $options = [])
    {
        return $this->formElement($title, $name, $value, 'text', $options = []);
    }

    public function formGroup($title, $name, $value, $type, $options = [])
    {
        $element = $this->formElement($title, $name, $value, $type, $options);

        return view('html.form_group', compact('element', 'name', 'options'));
    }

    public function headerLinks()
    {
        return PageLink::headerLinks()->get();
    }

    public function subheaderLinks()
    {
        $links = Section::with('rootCategories')
            ->published()
            ->weightOrder()
            ->get();

        return $links;
    }

    public function sectionCategoryOrderOfferCheckLink($sectionname, $categoryname, $filters, $params = [])
    {
        $url = $categoryname 
            ? route('orders::category', ['name' => $sectionname, 'categoryname' => $categoryname]) 
            : route('orders::section', ['name' => $sectionname]);

        foreach ($params as $key => $param) {
            $filters[$key] = $param;
        }

        $urlFilters = http_build_query($filters);
        $url .= '?' . $urlFilters;

        return $url;
    }

    public function plus($key)
    {
        return '<span class="plus" data-key="' . $key . '"><i class="mobee-plus-1"></i></span>';
    }

    public function parsePlus($string)
    {
        $string = preg_replace("/\[plus:(.*?)\]/", '<span class="plus" data-key="$1"><i class="mobee-plus-1"></i></span>', $string);
        return $string;
    }

    public function t($string, $noEscape = false)
    {
        if (!$noEscape) {
            $string = e($string);
        }
        
        $string = $this->parsePlus($string);

        return $string;
    }

    public function table($items)
    {
        return new Table($items);
    }
}
<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Profile;
use App\PayTransaction;
use App\Super;
use App\Notification;

class User extends Authenticatable
{
    const UPDATE_BALANCE_NONE = 0;
    const UPDATE_BALANCE_PAY_INVOICE = 1;
    const UPDATE_BALANCE_BUY_SUPER = 2;

    private $_superUntil;
    private $_lastNotifications = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meloman_id',
        'email',
        'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    private $profileIds = null;

    public function scopeByMelomanId($query, $melomanId)
    {
        $query->where('meloman_id', $melomanId);

        return $query;
    }

    public function scopeByEmail($query, $email)
    {
        $query->where('email', $email);

        return $query;
    }

    public function isLicensed()
    {
        return (bool) $this->license;
    }

    public function isSuper()
    {
        $until = $this->superUntil();

        return (bool) $until['minutes'];
    }

    public function superUntil()
    {
        if (!$this->_superUntil) {
            $this->_superUntil = Super::untilByUser($this->id);
        }

        return $this->_superUntil;
    }

    public function canBuySuper($id)
    {
        $cost = Super::costBySuperId($id);

        return $this->balance >= $cost;
    }

    public function buySuper($id)
    {
        if (!$this->canBuySuper($id)) {
            return false;
        }

        $cost = Super::costBySuperId($id);
        $money = $cost * -1;
        $transaction = self::updateBalance(self::UPDATE_BALANCE_BUY_SUPER, [
            'money' => $money,
        ]);

        $super = Super::createSuper($id, $this->id);

        if ($super->id) {
            $transaction->content = $super->id;
            $transaction->save();
        } else {
            abort(403, 'Ошибка оплаты! Обратитесь в поддержку. КОД: ' . $transaction->id);
        }

        return $super;
    }

    public function agreeWithLicense()
    {
        $this->license = 1;
        $this->save();
    }

    public function profileIds()
    {
        if ($this->profileIds !== null) {
            return $this->profileIds;
        }

        $profiles = $this->profiles()->get();

        foreach ($profiles as $profile) {
            $this->profileIds[] = $profile->id;
        }

        return $this->profileIds;
    }

    public function ownsProfileId($profileId)
    {
        return in_array($profileId, $this->profileIds());
    }

    public function listProfiles()
    {
        $list = [];

        foreach ($this->profiles as $profile) {
            $list[$profile->id] = $profile->displayName();
        }

        return $list;
    }

    public function updateBalance($method, $params)
    {
        switch ($method) {
            case self::UPDATE_BALANCE_PAY_INVOICE:
                $this->balance += $params['invoice']->money;
                $this->save();

                $transaction = new PayTransaction;
                $transaction->money = $params['invoice']->money;
                $transaction->user_id = $this->id;
                $transaction->type = PayTransaction::TYPE_PAY_INVOICE;
                $transaction->content = $params['invoice']->id;
                $transaction->save();

                return $transaction;
                break;

            case self::UPDATE_BALANCE_BUY_SUPER:
                $this->balance += $params['money'];
                $this->save();

                $transaction = new PayTransaction;
                $transaction->money = $params['money'];
                $transaction->user_id = $this->id;
                $transaction->type = PayTransaction::TYPE_BUY_SUPER;
                $transaction->content = 0;
                $transaction->save();

                return $transaction;
                break;
        }

        return null;
    }

    public function lastNotifications($max = 3)
    {
        if ($this->_lastNotifications === null) {
            $this->_lastNotifications = Notification::byUser($this->id)
                ->orderLast()
                ->take($max)
                ->get();
        }

        return $this->_lastNotifications;
    }

    public function lastNotification()
    {
        $notifications = $this->lastNotifications();

        return count($notifications) ? $notifications[0] : null;
    }

    public function lastNotificationId()
    {
        $n = $this->lastNotification();

        return $n ? $n->id : 0;
    }

    /**
     * HAS MANY
     */

    public function profiles()
    {
        return $this->hasMany('App\Profile', 'user_id', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Components\Payment;
use App\PayData;
use App\User;

class PayInvoice extends Model
{
    const STATUS_NEW = 0;
    const STATUS_PAYD = 1;
    const STATUS_CANCELED = 2;

    static public function make($money, $userId)
    {
        $invoice = new self;
        $invoice->money = $money;
        $invoice->user_id = $userId;
        $invoice->status = self::STATUS_NEW;
        $invoice->save();

        return $invoice;
    }

    static public function lastUserInvoices($id, $perPage = null)
    {
        $q = self::byUserId($id)
            ->sortLast();

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    public function success()
    {
        if ($this->status != self::STATUS_NEW) {
            return false;
        }

        $this->status = self::STATUS_PAYD;
        $this->payd_at = DB::raw('NOW()');
        $this->save();

        $transaction = $this->user->updateBalance(User::UPDATE_BALANCE_PAY_INVOICE, [
            'invoice' => $this,
        ]);

        return $transaction;
    }

    public function apply($paymentCheck)
    {
        $response = $paymentCheck->getResponse();
        $invoiceId = $paymentCheck->invoiceId();

        $payData = PayData::byInvoiceId($invoiceId)
            ->first();

        if (!$payData) {
            $payData = new PayData;
            $payData->invoice_id = $invoiceId;
            $payData->amount = $response['amount'];
            $payData->currency = $response['currencycode'];
            $payData->bank_time = $response['timestamp'];
            $payData->reference = $response['reference'];
            $payData->cardhash = $response['cardhash'];
            $payData->approval_code = $response['approval_code'];
            $payData->msg = $response['msg'];
            $payData->secure = $response['secure'];
            $payData->card_bin = $response['card_bin'];
            $payData->c_hash = $response['c_hash'];
            $payData->payername = $response['payername'];
            $payData->payermail = $response['payermail'];
            $payData->payerphone = $response['payerphone'];
            $payData->save();
        }

        if (!$paymentCheck->isPayd()) {
            return false;
        }

        if (!$payData->isCompleted()) {
            $complete = Payment::completeOrder($invoiceId, $payData->reference, $payData->approval_code, $payData->amount, $payData->currency);

            if (!isset($complete['bank'])) {
                return false;
            }

            $response = $complete['bank']['response']['@attributes'];

            if ($response['code'] === '00') {
                $payData->completed = 1;
                $payData->completed_msg = $response['message'];
                $payData->save();
                return $this->success();
            }
        }

        return false;
    }

    public function isNew()
    {
        return ($this->status == self::STATUS_NEW);
    }

    public function isPayd()
    {
        return ($this->status == self::STATUS_PAYD);
    }

    public function isCanceled()
    {
        return ($this->status == self::STATUS_CANCELED);
    }

    public function typeString()
    {
        if ($this->isNew()) {
            return 'Заявка не обработана';
        }

        if ($this->isPayd()) {
            return 'Заявка оплачена';
        }

        if ($this->isCanceled()) {
            return 'Заявка отклонена';
        }

        return '–';
    }

    public function ownedByUser($id)
    {
        return $this->user_id == $id;
    }

    /**
     * SCOPES
     */

    public function scopeByUserId($q, $id)
    {
        $q->where('user_id', $id);

        return $q;
    }

    public function scopeSortLast($q)
    {
        $q->orderBy('created_at', 'desc');

        return $q;
    }

    /**
     * BELONGS TO
     */

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}

<?php

namespace App;

use App\Components\Model;
use Validator;
use DB;
use App\DialogItem;

class DialogMember extends Model
{
    static public function getLastByProfile($profileId, $perPage = null)
    {
        $q = self::start()
            ->latestByProfile($profileId)
            ->MyWithCount();

        return $perPage ? $q->paginate($perPage) : $q->get();
    }

    /**
     * BELONGS TO
     */

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'profile_id', 'id');
    }

    public function dialog()
    {
        return $this->belongsTo('App\Dialog', 'dialog_id', 'id');
    }

    /**
     * SCOPES
     */

    public function scopeStart($query)
    {
        $query->with('dialog', 'dialog.dialogable');

        return $query;
    }

    public function scopeLatestByProfile($query, $profileId)
    {
        $query->where('profile_id', $profileId);
        $query->orderBy('updated_at', 'DESC');

        return $query;
    }

    public function scopeMyWithCount($query)
    {
        $read = DialogItem::READ_NO;
        $types = implode(',', [
            DialogItem::TYPE_MESSAGE,
        ]);
        $query->select(DB::raw("dialog_members.*, (SELECT COUNT(dialog_items.id) FROM dialog_items WHERE dialog_members.dialog_id = dialog_items.dialog_id AND dialog_items.read = $read AND type IN($types) AND dialog_items.profile_id <> dialog_members.profile_id) as count_messages"));

        return $query;
    }

    public function scopeByProfile($query, $id)
    {
        $query->where('profile_id', $id);

        return $query;
    }
}

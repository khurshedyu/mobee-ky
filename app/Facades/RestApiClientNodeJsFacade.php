<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 10/31/16
 * Time: 1:57 PM
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class RestApiClientNodeJsFacade extends Facade {
    protected static function getFacadeAccessor() { return 'restapiclientnodejs'; }
}
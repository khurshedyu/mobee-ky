<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageLink extends Model
{
    /**
     * SCOPES
     */

    public function scopeHeaderLinks($query)
    {
        $query->orderBy('visits', 'DESC');

        return $query;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DialogItem;

class Deal extends Model
{
    const CLOSED_NO = 0;
    const CLOSED_YES = 1;

    public function closed()
    {
        return $this->closed == self::CLOSED_YES;
    }

    public function close()
    {
        $this->closed = self::CLOSED_YES;
        $this->save();

        if ($this->dialog) {
            $dialogItem = new DialogItem;
            $dialogItem->dialog_id = $this->dialog->id;
            $dialogItem->profile_id = null;
            $dialogItem->type = DialogItem::TYPE_DEAL_CLOSE;
            $dialogItem->content = $this->id;
            $dialogItem->save();
        }

        return $this;
    }

    /**
     * SCOPES
     */

    public function scopeIsOpened($q)
    {
        return $q->where('closed', self::CLOSED_NO);
    }

    public function scopeIsClosed($q)
    {
        return $q->where('closed', self::CLOSED_YES);
    }

    /**
     * BELONGS TO
     */

    public function dialog()
    {
        return $this->belongsTo('App\Dialog', 'dialog_id', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'country';
    protected $primaryKey = 'country_id';

    /**
     * HAS MANY
     */

    public function cities()
    {
        return $this->hasMany('App\City', 'region_id', 'region_id');
    }

    public function regions()
    {
        return $this->hasMany('App\Region', 'country_id', 'country_id');
    }
}

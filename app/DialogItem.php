<?php

namespace App;

use App\Components\Model;
use Validator;
use App\Deal;

class DialogItem extends Model
{
    const TYPE_SYSTEM = 0;
    const TYPE_MESSAGE = 1;
    const TYPE_DEAL = 2;
    const TYPE_DEAL_CLOSE = 3;
    const TYPE_DIALOG_START = 4;

    const READ_NO = 0;
    const READ_YES = 1;

    public function getDeal()
    {
        if ($this->type != self::TYPE_DEAL) {
            return null;
        }

        return Deal::where('id', $this->content)->first();
    }

    public function title()
    {
        $htmlHelper = new \App\Helpers\HtmlHelper;

        switch ($this->type) {
            case self::TYPE_DEAL:
                if ($this->dialog->deal) {
                    return 'Сделка заключена: ' . $htmlHelper->formatNum($this->dialog->deal->price, 0) . ' тг.';
                }

                return 'Сделка не заключена';
                break;
            case self::TYPE_DEAL_CLOSE:
                if ($this->dialog->deal) {
                    return 'Сделка закрыта: ' . $htmlHelper->formatNum($this->dialog->deal->price, 0) . ' тг.';
                }

                return 'Ошибка закрытия сделки';
                break;
            case self::TYPE_SYSTEM:
                return $this->content;
                break;
            case self::TYPE_DIALOG_START:
                return 'Диалог начат';
                break;
        }

        return $this->content;
    }

    /**
     * BELONGS TO
     */

    public function profile()
    {
        return $this->belongsTo('App\Profile', 'profile_id', 'id');
    }

    public function dialog()
    {
        return $this->belongsTo('App\Dialog', 'dialog_id', 'id');
    }

    /**
     * SCOPES
     */

    public function scopeOnlyActions($q)
    {
        $q->whereIn('type', [
            self::TYPE_DIALOG_START,
            self::TYPE_DEAL,
            self::TYPE_DEAL_CLOSE,
            self::TYPE_SYSTEM,
        ]);

        return $q;
    }

    public function scopeIsMessage($query)
    {
        $query->where('type', self::TYPE_MESSAGE);

        return $query;
    }

    public function scopeOrderLast($query)
    {
        $query->orderBy('created_at', 'DESC');

        return $query;
    }

    public function scopeAfter($query, $id)
    {
        $query->where('id', '>', $id);

        return $query;
    }
}

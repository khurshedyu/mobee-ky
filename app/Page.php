<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\HtmlHelper;

class Page extends Model
{
    const ARCHIVE_NO = 0;
    const ARCHIVE_YES = 1;

    static public function pageByName($name)
    {
        return self::byName($name)
            ->first();
    }

    static public function publicPageByName($name)
    {
        return self::byName($name)
            ->published()
            ->first();
    }

    public function contentFormatted()
    {
        $helper = new HtmlHelper;
        $content = $helper->t($this->content, true);

        return $content;
    }

    /**
     * SCOPES
     */

    public function scopeByName($query, $name)
    {
        $query->where('name', $name);

        return $query;
    }

    public function scopePublished($query)
    {
        $query->where('archive', self::ARCHIVE_NO);
        $query->whereRaw('published_at < NOW()');
        
        return $query;
    }
}

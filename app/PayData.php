<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayData extends Model
{
    public function isCompleted()
    {
        return (bool) $this->completed;
    }

    /**
     * SCOPES
     */

    public function scopeByInvoiceId($q, $id)
    {
        $q->where('invoice_id', $id);

        return $q;
    }
}

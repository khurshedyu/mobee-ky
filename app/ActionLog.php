<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PayInvoice;

class ActionLog extends Model
{
    const TYPE_ANY = 0;
    const TYPE_UPLOAD_IMAGE = 1;
    const TYPE_ORDER_CREATED = 2;
    const TYPE_ORDER_EDITED = 3;
    const TYPE_ORDER_REQUEST_CREATED = 4;
    const TYPE_PAY_INVOICE_CREATED = 5;
    const TYPE_PAY_INFO = 6;
    const TYPE_BUY_SUPER = 7;
    const TYPE_PROFILE_UPDATE = 8;

    static public function log($content, $type, $itemId, $userId)
    {
        $log = new self;
        $log->content = json_encode($content);
        $log->type = $type;
        $log->item_id = $itemId;
        $log->user_id = $userId;
        $log->save();

        return $log;
    }

    static public function logUploadImage($itemId, $userId)
    {
        $content = null;

        return self::log(
            $content,
            self::TYPE_UPLOAD_IMAGE,
            $itemId,
            $userId
        );
    }

    static public function logFailUploadImage($userId)
    {
        $content = null;
        $itemId = 0;
        
        return self::log(
            $content,
            self::TYPE_UPLOAD_IMAGE,
            $itemId,
            $userId
        );
    }

    static public function orderCreated($itemId, $userId)
    {
        $content = null;
        
        return self::log(
            $content,
            self::TYPE_ORDER_CREATED,
            $itemId,
            $userId
        );
    }

    static public function orderEdited($itemId, $userId)
    {
        $content = null;
        
        return self::log(
            $content,
            self::TYPE_ORDER_EDITED,
            $itemId,
            $userId
        );
    }

    static public function orderPostRequest($itemId, $orderRequestId, $userId)
    {
        $content = [
            'order_request_id' => $orderRequestId,
        ];
        
        return self::log(
            $content,
            self::TYPE_ORDER_REQUEST_CREATED,
            $itemId,
            $userId
        );
    }

    static public function payInvoiceCreated($itemId, $userId)
    {
        $content = null;
        
        return self::log(
            $content,
            self::TYPE_PAY_INVOICE_CREATED,
            $itemId,
            $userId
        );
    }

    static public function payInfo($data, $invoiceId)
    {
        $content = $data;
        $invoice = PayInvoice::where('id', $invoiceId)->first();

        if (!$invoice) {
            return null;
        }
        
        return self::log(
            $content,
            self::TYPE_PAY_INFO,
            $invoice->id,
            $invoice->user_id
        );
    }

    static public function userBuySuper($itemId, $userId)
    {
        $content = null;
        
        return self::log(
            $content,
            self::TYPE_BUY_SUPER,
            $itemId,
            $userId
        );
    }

    static public function profileUpdate($profile, $data, $userId)
    {
        $content = $data;
        $itemId = $profile->id;
        
        return self::log(
            $content,
            self::TYPE_PROFILE_UPDATE,
            $itemId,
            $userId
        );
    }
}

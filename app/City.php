<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'city';
    protected $primaryKey = 'city_id';
    public $timestamps = false;

    static public function listByCountry($id, $withRegion, $withNull = false)
    {
        $list = [];
        $query = self::start();
        $query->where('public', 1);
        $query->orderBy('name', 'ASC');

        if ($withRegion) {
            $query->with('region');
        }

        $cities = $query->byCountry($id)->get();

        if ($withNull) {
            $list[0] = 'Выберите город';
        }

        foreach ($cities as $city) {
            // $list[$city->city_id] = "{$city->name} ({$city->name_en})";
            $list[$city->city_id] = $city->name;
        }

        return $list;
    }

    /**
     * Scopes
     */

    public function scopeStart($query)
    {
        return $query;
    }

    public function scopeByRegion($query, $id)
    {
        $query->where('region_id', $id);

        return $query;
    }

    public function scopeByCountry($query, $id)
    {
        $query->where('country_id', $id);

        return $query;
    }

    /**
     * BELONGS TO
     */

    public function region()
    {
        return $this->belongsTo('App\Region', 'region_id', 'region_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'country_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserToken extends Model
{
    /**
     * Create user token by user_id or return one if exists
     *
     * @param $id
     * @return string
     */
    static public function tokenByUserId($id)
    {
        $userToken = self::where('user_id', $id)
            ->whereRaw('expired_at > NOW()')
            ->first();

        if ($userToken) {
            return $userToken->token;
        }

        $userToken = new self;
        $userToken->user_id = $id;
        $userToken->token = substr(md5(microtime()), 0, 10) . substr(md5($id), 0, 10);
        $userToken->expired_at = Carbon::today()->addDays(10);
        $userToken->save();

        return $userToken->token;
    }

    /**
     * Get user by token
     *
     * @param $token
     * @return null
     */
    static public function userByToken($token)
    {
        $userToken = self::with('user')
            ->where('token', $token)
            ->whereRaw('expired_at > NOW()')
            ->first();

        if ($userToken) {
            return $userToken->user;
        }

        return null;
    }

    static public function check($token, $id)
    {
        $user = self::userByToken($token);

        if (!$user) {
            return false;
        }

        return $user->id == $id;
    }

    /**
     * BELONGS TO
     */

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}

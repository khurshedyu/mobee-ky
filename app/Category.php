<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Section;

class Category extends Model
{
    const STATUS_PUBLISHED = 1;

    static public function withSectionsList($withNull = false)
    {
        $list = [];

        $sections = Section::with('rootCategories', 'rootCategories.categories')
            ->published()
            ->weightOrder()
            ->get();

        // dd($sections[2]->rootCategories[0]->categories);

        if ($withNull) {
            $list[0] = 'Укажите категорию';
        }

        foreach ($sections as $section) {
            foreach ($section->rootCategories as $rootCategory) {
                $list[$rootCategory->id] = $section->title . ' / ' . $rootCategory->title;
                foreach ($rootCategory->categories as $category) {
                    $list[$category->id] = $section->title . ' / ' . $rootCategory->title . ' / ' . $category->title;
                }
            }
        }

        return $list;
    }

    static public function listByCategory($id)
    {
        $categories = self::published()
            ->weightOrder();

        if ($id !== null) {
            if ($id) {
                $categories->where('category_id', $id);
            } else {
                $categories->whereRaw('(category_id = 0 OR category_id IS NULL)');
            }
        }
        
        return $categories->get();
    }

    static public function listByCategoryArray($id, $withNull = false)
    {
        $list = [];
        $categories = self::listByCategory($id);

        if ($withNull) {
            $list[0] = 'Укажите категорию';
        }

        foreach ($categories as $category) {
            $list[$category->id] = $category->title;
        }

        return $list;
    }

    static public function listBySection($id, $categoryId = null)
    {
        $categories = self::where('section_id', $id)
            ->published()
            ->weightOrder();

        if ($categoryId !== null) {
            if ($categoryId) {
                $categories->where('category_id', $categoryId);
            } else {
                $categories->whereRaw('(category_id = 0 OR category_id IS NULL)');
            }
        }

        return $categories->get();
    }

    static public function listBySectionArray($id, $categoryId = null, $withNull = false)
    {
        $list = [];
        $categories = self::listBySection($id, $categoryId);

        if ($withNull) {
            $list[0] = 'Укажите категорию';
        }

        foreach ($categories as $category) {
            $list[$category->id] = $category->title;
        }

        return $list;
    }

    static public function getByName($name)
    {
        return self::byName($name)
            ->first();
    }

    static public function rootCategories()
    {
        return self::listByCategory(0);
    }

    public function isRoot()
    {
        return !$this->category_id;
    }

    /**
     * SCOPES
     */

    public function scopePublished($q)
    {
        $q->where('status', self::STATUS_PUBLISHED);

        return $q;
    }

    public function scopeWeightOrder($q)
    {
        $q->orderBy('weight', 'DESC');

        return $q;
    }

    public function scopeBySection($q, $id)
    {
        $q->where('section_id', $id);

        return $q;
    }

    public function scopeByName($q, $name)
    {
        $q->where('name', $name);

        return $q;
    }

    public function categoriesOrdered()
    {
        return $this->categories()
            ->orderBy('weight', 'DESC')
            ->orderBy('title', 'ASC')
            ->get();
    }

    /**
     * BELONGS TO
     */

    public function section()
    {
        return $this->belongsTo('App\Section', 'section_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    /**
     * HAS MANY
     */

    public function categories()
    {
        return $this->hasMany('App\Category', 'category_id', 'id');
    }
}

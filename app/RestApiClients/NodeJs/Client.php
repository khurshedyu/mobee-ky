<?php
/**
 * Created by PhpStorm.
 * User: igor
 * Date: 10/31/16
 * Time: 1:47 PM
 */

namespace App\RestApiClients\NodeJs;

use App\ActionLog;
use App\Helpers\GeneralHelper;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Log;

class Client
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $apikey = 'AhQue8WoB2aiph8zeicahHooraf5goeh';

    /**
     * Initialize rest api client
     */
    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => GeneralHelper::getNodeJsConfig()['baseurl'] . ':3000/',
            'timeout' => 30,
            'verify' => false,
        ]);
    }

    /**
     * Tell nodejs server who should reload their notifications bar
     *
     * @param $userid
     */
    public function tellUserToReloadNotifications($userid)
    {
        try
        {
            $response = $this->client->get('/nodejs/reloadnotifications', [
                'query' => [
                    'userid' => $userid,
                    'apikey' => $this->apikey
                ]
            ]);
        }
        catch(ClientException $clientException)
        {
            switch($clientException->getCode())
            {
                default:
                    $logmessage = 'Call from ' . __CLASS__ . ' ' . __FUNCTION__ . ' -> ' . $clientException->getMessage();
                    Log::critical($logmessage);
                    break;
            }
        }
        catch(ConnectException $connectExceptions)
        {
            switch($connectExceptions->getCode())
            {
                default:
                    $logmessage = 'Call from ' . __CLASS__ . ' ' . __FUNCTION__ . ' -> ' . $connectExceptions->getMessage();
                    Log::critical($logmessage);
                    break;
            }
        }
    }
}
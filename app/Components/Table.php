<?php

namespace App\Components;

class Table
{
    private $items;
    private $columns;
    private $id;

    public function __construct($items = null) {
        $this->items = $items;
        $this->id = substr(md5(microtime()), 0, 10);

        return $this;
    }

    public function columns($columns)
    {
        $this->columns = $columns;

        return $this;
    }

    public function html()
    {
        $items = $this->items;
        $columns = $this->columns;
        $tableId = $this->id;

        return view('layouts._table', compact('items', 'columns', 'tableId'));
    }
}
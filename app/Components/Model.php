<?php

namespace App\Components;

use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations\Relation;
use Validator;

abstract class Model extends EloquentModel
{
    static protected $validators = [];
    protected $defaultValues = [];

    static protected function boot()
    {
        parent::boot();

        Relation::morphMap([
            \App\OrderRequest::getModel()->getMorphClass() => \App\OrderRequest::class,
        ]);
    }

    static public function makeValidator($fields, $data, $options = [])
    {
        $validators = [];
        $patterns = [];
        $replacements = [];

        foreach ($options as $key => $value) {
            $patterns[] = "/\{{$key}\}/";
            $replacements[] = $value;
        }

        foreach ($fields as $field) {
            if (!isset(self::$validators[$field])) {
                continue;
            }

            $validators[$field] = count($patterns) ?
                preg_replace($patterns, $replacements, self::$validators[$field]) :
                self::$validators[$field];
        }

        $validator = Validator::make($data, $validators);

        return $validator;
    }

    public function readData($data, $fields = [])
    {
        $fieldsToRead = count($fields) ? $fields : $this->fillable;

        foreach ($fieldsToRead as $field) {
            $this->$field = isset($data[$field]) ? 
                $data[$field] : 
                (isset($this->defaultValues[$field]) ? $this->defaultValues[$field] : null);
        }
    }

    public function scopeApplyFiltersAdmin($q, $filters)
    {
        foreach ($filters->filters() as $filter) {
            if ($filter['type'] == 'order') {
                $q->orderByRaw($filter['map'][$filter['value']]['value']);
            } else if ($filter['type'] == 'select') {
                if (isset($filter['map'][$filter['value']]['value'])) {
                    $q->where($filter['name'], $filter['map'][$filter['value']]['value']);
                }
            } else if ($filter['type'] == 'text') {
                if (isset($filter['value']) AND !empty($filter['value'])) {
                    $q->where($filter['name'], $filter['value']);
                }
            }
        }

        return $q;
    }
}
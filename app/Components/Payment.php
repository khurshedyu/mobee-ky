<?php

namespace App\Components;

use Illuminate\Support\ServiceProvider;
use App\Components\PaymentCheck;
use Parser;

class Payment extends ServiceProvider {

    static $URL = [
        'test' => 'https://testpay.kkb.kz/jsp/process/logon.jsp',
        'public' => 'https://epay.kkb.kz/jsp/process/logon.jsp'
    ];
    static $PRIVATE_KEY = false;
    static $INVERT = 0;
    static $BASE_ENCODE = true;

    /**
     * Register function
     */
    public function register() { }

    public static function templateKKB() {

        return '<merchant cert_id="[MERCHANT_CERTIFICATE_ID]" name="[MERCHANT_NAME]"><order order_id="[ORDER_ID]" amount="[AMOUNT]" currency="[CURRENCY]"><department merchant_id="[MERCHANT_ID]" amount="[AMOUNT]"/></order></merchant>';
    }

    public static function processTemplate($order_id = 0, $amount = 0, $currency = 398) {

        $merchant = self::templateKKB();

        $epay_env = ENV('KKB_ENV');

        if ($epay_env == 'public') {
            $config["MERCHANT_CERTIFICATE_ID"] = ENV('MERCHANT_CERTIFICATE_ID');
            $config["MERCHANT_NAME"] = ENV('MERCHANT_NAME');
            $config["MERCHANT_ID"] = ENV('MERCHANT_ID');
        }

        if ($epay_env == 'test') {
            $config["MERCHANT_CERTIFICATE_ID"] = ENV('MERCHANT_CERTIFICATE_ID_TEST');
            $config["MERCHANT_NAME"] = ENV('MERCHANT_NAME_TEST');
            $config["MERCHANT_ID"] = ENV('MERCHANT_ID_TEST');
        }


        $config["ORDER_ID"] = $order_id;
        $config["AMOUNT"] = $amount;
        $config["CURRENCY"] = $currency;

        foreach ($config as $key => $value) {
            $merchant = str_replace("[". $key ."]", $value, $merchant);
        }

        return $merchant;

    }

    public static function invert(){
        self::$INVERT = 1;
    }

    public static function sign($str = '')
    {

        $epay_env = ENV('KKB_ENV');

        if ($epay_env == 'public') {
            $crt = self::loadPrivateKey(ENV('CERT_NAME'), ENV('PRIVATE_KEY_PASS'));
        }

        if($epay_env == 'test') {
            $crt = self::loadPrivateKey(ENV('CERT_NAME_TEST'), ENV('PRIVATE_KEY_PASS_TEST'));
        }

        self::invert();

        if($crt['private_key'])
        {
            openssl_sign($str, $out, $crt['private_key']);
            if(self::$INVERT == 1) {
                $out = self::reverse($out);
            }
            return $out;
        }
    }

    public static function reverse($str = ''){
        return strrev($str);
    }

    public static function loadPrivateKey($file, $password = NULL) {

        $epay_env = ENV('KKB_ENV');

        // $filename = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'KKB' . DIRECTORY_SEPARATOR . $epay_env . DIRECTORY_SEPARATOR . $file;
        $filename = config('app.kkb_path') . DIRECTORY_SEPARATOR . $epay_env . DIRECTORY_SEPARATOR . $file;

        if(!is_file($filename))
        {
            echo "Key not found";
            exit;
        }

        $c = file_get_contents($filename);

        if($password) {
            $private_key = openssl_get_privatekey($c, $password) or die(openssl_error_string());
        } else {
            $private_key = openssl_get_privatekey($c) or die(openssl_error_string());
        }

        if(is_resource($private_key)){
            self::$PRIVATE_KEY = $private_key;
            return [
                'private_key' => $private_key,
                'content' => $c
            ];
        }

        return false;
    }


    public static function sign64($str){
        return base64_encode(self::sign($str));
    }

    public static function check_sign64($data, $str, $filename){
        return self::check_sign($data, base64_decode($str), $filename);
    }

    public static function check_sign($data, $str, $filename){

        if(self::$INVERT == 1)  $str = self::reverse($str);
        if(!is_file($filename)) return false;
        $public_key = file_get_contents($filename);

        return openssl_verify($data, $str, $public_key);
    }

    public static function initKKB($price, $id) {

        $template = self::processTemplate($id, $price);

        $result_sign = '<merchant_sign type="RSA">'. self::sign64($template) . '</merchant_sign>';

        $epay_env = ENV('KKB_ENV');
        $document = "<document>" . $template . $result_sign . "</document>";

        $xml['xml'] = base64_encode($document);
        $xml['url'] = self::$URL[$epay_env];

        return $xml;
    }

    static private function envParams($params = [])
    {
        $env = ENV('KKB_ENV');

        $allParams = [];

        switch ($env) {
            case 'public':
                $allParams = [
                    'merchant_cert_id' => ENV('MERCHANT_CERTIFICATE_ID'),
                    'merchant_name' => ENV('MERCHANT_NAME'),
                    'order_currency' => ENV('CURRENCY_CODE'),
                    'merchant_id' => ENV('MERCHANT_ID'),
                ];
                break;
            
            case 'test':
                $allParams = [
                    'merchant_cert_id' => ENV('MERCHANT_CERTIFICATE_ID_TEST'),
                    'merchant_name' => ENV('MERCHANT_NAME_TEST'),
                    'order_currency' => ENV('CURRENCY_CODE_TEST'),
                    'merchant_id' => ENV('MERCHANT_ID_TEST'),
                ];
                break;
        }

        foreach ($params as $key => $val) {
            $allParams[$key] = $val;
        }

        return $allParams;
    }

    static private function templateParams($template, $params)
    {
        foreach ($params as $key => $value) {
            $template = str_replace("[". $key ."]", $value, $template);
        }

        return $template;
    }

    static public function messageSign($message)
    {
        return '<merchant_sign type="RSA">' . self::sign64($message) . '</merchant_sign>';
    }

    static private function createDocument($message, $sign)
    {
        return "<document>" . $message . $sign . "</document>";
    }

    static public function createAuthData($price, $id)
    {
        $url = [
            'test' => 'https://testpay.kkb.kz/jsp/process/logon.jsp',
            'public' => 'https://epay.kkb.kz/jsp/process/logon.jsp',
        ];

        $params = self::envParams([
            'order_id' => $id,
            'amount' => $price,
        ]);

        $template = '<merchant cert_id="[merchant_cert_id]" name="[merchant_name]"><order order_id="[order_id]" amount="[amount]" currency="[order_currency]"><department merchant_id="[merchant_id]" amount="[amount]"/></order></merchant>';

        $message = self::templateParams($template, $params);
        $sign = self::messageSign($message);
        $document = self::createDocument($message, $sign);

        // dd($document);

        $xml = [
            'xml' => base64_encode($document),
            'url' => $url[ENV('KKB_ENV')],
        ];

        // dd($xml);
        return $xml;
    }

    static public function checkOrderStatus($id)
    {
        $url = [
            'test' => 'https://testpay.kkb.kz/jsp/remote/checkOrdern.jsp',
            'public' => 'https://epay.kkb.kz/jsp/remote/checkOrdern.jsp',
        ];

        $params = self::envParams([
            'order_id' => $id,
        ]);

        $template = '<merchant id="[merchant_id]"><order id="[order_id]"/></merchant>';

        $message = self::templateParams($template, $params);
        $sign = self::messageSign($message);
        $document = self::createDocument($message, $sign);

        $urlFull = $url[ENV('KKB_ENV')] . '?' . urlencode($document);

        $xml = file_get_contents($urlFull);
        $data = Parser::xml($xml);

        $paymentCheck = new PaymentCheck($xml);

        return $paymentCheck;
    }

    static public function completeOrder($id, $reference, $approval_code, $amount, $currency)
    {
        $url = [
            'test' => 'https://testpay.kkb.kz/jsp/remote/control.jsp',
            'public' => 'https://epay.kkb.kz/jsp/remote/control.jsp',
        ];

        $params = self::envParams([
            'order_id' => $id,
            'reference' => $reference,
            'approval_code' => $approval_code,
            'amount' => $amount,
            'currency' => $currency,
        ]);

        $template = '<merchant id="[merchant_id]"><command type="complete"/><payment reference="[reference]" approval_code="[approval_code]" orderid="[order_id]" amount="[amount]" currency_code="[currency]"/></merchant>';

        $message = self::templateParams($template, $params);
        $sign = self::messageSign($message);
        $document = self::createDocument($message, $sign);

        $urlFull = $url[ENV('KKB_ENV')] . '?' . urlencode($document);

        $xml = file_get_contents($urlFull);
        $xml = trim($xml);
        $data = Parser::xml($xml);

        return $data;
    }
}
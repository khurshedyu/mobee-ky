<?php

namespace App\Components;

class Filters
{
    private $filters;

    public function __construct($filters = []) {
        $this->filters = $filters;

        return $this;
    }

    public function filters()
    {
        return $this->filters;
    }

    public function values()
    {
        $values = [];

        foreach ($this->filters as $filter) {
            $values[$filter['name']] = $filter['value'];
        }

        return $values;
    }
}
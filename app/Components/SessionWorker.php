<?php

namespace App\Components;

use App\Profile;
use Auth;

class SessionWorker
{
    static private $instance;
    private $currentProfile;

    static public function instance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function profileById($profileId)
    {
        return Profile::find($profileId);
    }

    public function currentProfileId()
    {
        if (session()->has('current_profile_id')) {
            return session('current_profile_id');
        }

        $user = Auth::user();
        $profile = Profile::start()
            ->isUser()
            ->byUser($user)
            ->first();

        if (!$profile) {
            die('NO PROFILE FOR USER');
        }

        session()->put('current_profile_id', $profile->id);

        return session('current_profile_id');
    }

    public function currentProfile()
    {
        if ($this->currentProfile) {
            return $this->currentProfile;
        }

        $profileId = $this->currentProfileId();
        $this->currentProfile = $this->profileById($profileId);

        return $this->currentProfile;
    }

    public function changeCurrentProfile($id)
    {
        $user = Auth::user();
        $profile = Profile::start()
            ->byId($id)
            ->byUser($user)
            ->first();

        if (!$profile) {
            return false;
        }

        session()->regenerate();
        session()->put('current_profile_id', $profile->id);

        return session('current_profile_id');
    }

    public function clearCurrentProfile()
    {
        $this->currentProfile = null;
        return session()->forget('current_profile_id');
    }

    public function availableProfileList()
    {
        $user = Auth::user();
        $profiles = Profile::start()
            ->activeOrNew()
            ->byUser($user)
            ->get();

        $list = [];

        foreach ($profiles as $profile) {
            $list[] = [
                'title' => $profile->profileTitle(),
                'name' => $profile->displayName(),
                'id' => $profile->id,
            ];
        }

        return $list;
    }
}
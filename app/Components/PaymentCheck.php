<?php

namespace App\Components;

use App\Components\Payment;
use App\PayInvoice;
use Parser;

class PaymentCheck
{
    private $xml;
    private $data;

    public function __construct($xml)
    {
        $this->xml = $xml;
        $this->data = Parser::xml($xml);
    }

    public function getData()
    {
        return $this->data;
    }

    public function getResponse()
    {
        return $this->data['bank']['response']['@attributes'];
    }

    public function status()
    {
        if ($this->isCompleted()) {
            return 'Оплачен – средства зачислены';
        }

        return $this->isPayd() ? 'Оплачен – средства не зачислены' : 'Не оплачен';
    }

    public function isCompleted()
    {
        $response = $this->getResponse();
        $payment = $response['payment'];

        if ($payment !== 'true') {
            return false;
        }

        if ((int) $response['status'] === 2 AND (int) $response['result'] === 0) {
            return true;
        }

        return false;
    }

    public function isPayd()
    {
        $response = $this->getResponse();
        $payment = $response['payment'];

        if ($payment !== 'true') {
            return false;
        }

        if (((int) $response['status'] === 0 OR (int) $response['status'] === 2) AND (int) $response['result'] === 0) {
            return true;
        }

        return false;
    }

    public function invoiceId()
    {
        $response = $this->getResponse();

        return (int) $response['OrderID'];
    }

    public function process()
    {
        $invoiceId = (int) $this->data['bank']['merchant']['order']['@attributes']['id'];
        $invoice = PayInvoice::findOrFail($invoiceId);
        // $response = $this->data['bank']['response']['@attributes'];
        // $payment = $response['payment'];

        if ($this->isPayd() AND $invoice->isNew()) {
            $transaction = $invoice->apply($this);
            return $transaction;
        }

        return false;
    }
}
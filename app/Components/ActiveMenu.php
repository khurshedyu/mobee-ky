<?php

namespace App\Components;

use Auth;

class ActiveMenu
{
    static private $instance;

    static public function instance()
    {
        if (!self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    private $currentKey;

    public function setKey($key)
    {
        $this->currentKey = $key;
    }

    public function getKey()
    {
        return $this->currentKey;
    }

    public function active($key)
    {
        return $this->currentKey == $key ? 'active' : null;
    }
}
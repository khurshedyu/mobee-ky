<?php

namespace App\Components;

class AjaxResponse
{
    const CODE_SUCCESS = 200;
    const CODE_ERROR = 400;

    private $data;
    private $code;

    public function __construct($data, $code = null)
    {
        $this->setData($data);

        if ($code) {
            $this->setCode($code);
        }

        return $this;
    }

    static public function make($data, $code = null)
    {
        return new self($data, $code);
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    public function codeSuccess()
    {
        $this->setCode(self::CODE_SUCCESS);

        return $this;
    }

    public function codeError()
    {
        $this->setCode(self::CODE_ERROR);

        return $this;
    }

    private function response()
    {
        if (!$this->code) {
            $this->codeError();
        }

        return [
            'code' => $this->code,
            'data' => $this->data,
        ];
    }

    public function get()
    {
        return $this->response();
    }

    public function json()
    {
        return json_encode($this->response());
    }
}
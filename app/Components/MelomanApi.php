<?php

namespace App\Components;

use Illuminate\Support\ServiceProvider;
use anlutro\cURL\cURL;

class MelomanApi extends ServiceProvider {

    protected static $api = 'LS Rest Api';
    protected static $URL = null;
    protected static $PROTOCOL = 'http://';
    protected static $method = 'put';
    protected static $commandList = [

        'getId' => [
            'url' => '/customer/getIdByEmail',
            'method' => 'GET',
            'params' => [
                'email' => ''
            ]
        ],

        'authByEmail' => [
            'url' => '/customer/authByEmail',
            'method' => 'GET',
            'params' => [
                'email' => '',
                'pass' => ''
            ]
        ],

        'lsAccount' => [
            'url' => '/customer/{user}/lsAccount',
            'method' => 'GET',
            'params' => []
        ],

        'info' => [
            'url' => '/customer/{user}',
            'method' => 'GET',
            'params' => []
        ],

        'register' => [
            'url' => '/customer',
            'method' => 'POST',
            'params' => [
                'email' => '',
                'pass' => ''
            ]
        ],

        'resetPassword' => [
            'url' => '/customer/{user}/',
            'method' => 'PUT',
            'params' => [
                'email' => '',
                'password' => '',
            ]
        ],
        
        'userUpdate' => [
            'url' => '/customer/',
            'url_params' => ['user'],
            'method' => 'PUT',
            'params' => [
                'name' => '',
                'surname' => '',
                'secondname' => '',
                'email' => '',
                'mobile' => '',
                'birthdate' => '',
                'gender' => '',
                'telephone' => '',
                'region' => '',
                'post_index' => '',
                'city' => '',
                'district' => '',
                'address_street' => '',
                'address_home' => '',
                'address_flat' => '',
                'preferred_time_delivery' => '',
                'ls_email_offers' => '',
                'ls_sms_ballance' => '',
                'ls_sms_offers' => ''
            ]
        ]
    ];

    public static function execute($command, $params) {

        $command = self::$commandList[$command];

        if ($command) {
            return self::stream($command, $params);
        } else {
            return 'Command ' . $command . ' not found';
        }
    }

    public static function stream($command, $params) {

        $response = "";
        $curl = new cURL;

        $command['params']['key'] = ENV('MELOMAN_API_KEY', 'key');
        $link = [];
        $parser = explode('/', $command['url']);

        foreach ($parser as $i) {

            preg_match("/^{+([a-z0-9A-Z]+)+}$/", $i, $output);
            if (count($output) > 0) {
                if ($params[$output[1]]) {
                    $i = $params[$output[1]];
                }
            }
            $link[] = $i;
        }

        $apiUrl = ENV('MELOMAN_API_URL', 'lsapi.meloman.kz/lsgate/sndbx');

        $command['url'] = implode("/", $link);
        $url = self::$PROTOCOL . $apiUrl . $command['url'];

        $options =  array_merge($command['params'], $params);

        switch($command['method']) {

            case "PUT":
                $response = $curl->rawPut($url, $options);
                break;

            case "GET":
                $url = $curl->buildUrl($url, $options);
                $response = $curl->get($url);
            break;

            case "POST":
                $response = $curl->post($url, $options);
            break;

            default :
                $response = (object) array("statusCode" => "Method " . $command['method'] . " not found");
            break;

        };

        return $response;
    }

    public function register()
    {
        # code...
    }
}
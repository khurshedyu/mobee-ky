<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'meloman_id' => $faker->randomDigitNotNull,
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'license' => 0,
    ];
});

$factory->defineAs(App\User::class, 'licensed', function (Faker\Generator $faker) {
    return [
        'meloman_id' => $faker->randomDigitNotNull,
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'license' => 1,
        'balance' => 10000,
    ];
});

$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->domainWord,
        'firstname' => $faker->firstNameMale,
        'secondname' => $faker->firstNameFemale,
        'lastname' => $faker->lastName,
        'sex' => $faker->firstName,
        'type' => App\Profile::TYPE_USER,
        'status' => App\Profile::STATUS_ACTIVE,
        'checked' => 1,
        'city_id' => 15000,
        'phone' => $faker->phoneNumber,
        'address' => $faker->streetAddress,
        'birth_date' => $faker->date,
    ];
});

$factory->define(App\City::class, function (Faker\Generator $faker) {
    return [
        'city_id' => $faker->numberBetween(1000, 2000),
        'region_id' => $faker->numberBetween(1000, 2000),
        'country_id' => $faker->numberBetween(1000, 2000),
        'name' => $faker->city,
        'name_en' => $faker->city,
        'public' => 1,
    ];
});

$factory->define(App\Notification::class, function (Faker\Generator $faker) {
    return [
    ];
});
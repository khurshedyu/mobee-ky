<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDialogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dialogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dialogable_id')->unsigned()->nullable();
            $table->string('dialogable_type')->nullable();
            $table->timestamp('closed_at')->default('0000-00-00 00:00');
            $table->timestamp('created_at')->default('0000-00-00 00:00');
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dialogs');
    }
}

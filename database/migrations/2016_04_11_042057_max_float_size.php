<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MaxFloatSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('price');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->float('price', 15, 2)->after('isoffer');//->change();
        });



        Schema::table('deals', function (Blueprint $table) {
            $table->dropColumn('price');
        });

        Schema::table('deals', function (Blueprint $table) {
            $table->float('price', 15)->after('dialog_id');
        });



        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('balance');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->float('balance', 15)->after('email');
        });



        Schema::table('files', function (Blueprint $table) {
            $table->dropColumn('size');
        });

        Schema::table('files', function (Blueprint $table) {
            $table->float('size', 15)->after('type');
        });



        Schema::table('pay_invoices', function (Blueprint $table) {
            $table->dropColumn('money');
        });

        Schema::table('pay_invoices', function (Blueprint $table) {
            $table->float('money', 15)->after('id');
        });



        Schema::table('pay_transactions', function (Blueprint $table) {
            $table->dropColumn('money');
        });

        Schema::table('pay_transactions', function (Blueprint $table) {
            $table->float('money', 15)->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->float('price')->change();
        });

        Schema::table('deals', function (Blueprint $table) {
            $table->float('price')->change();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->float('balance')->change();
        });

        Schema::table('files', function (Blueprint $table) {
            $table->float('size')->change();
        });

        Schema::table('pay_invoices', function (Blueprint $table) {
            $table->float('money')->change();
        });

        Schema::table('pay_transactions', function (Blueprint $table) {
            $table->float('money')->change();
        });
    }
}

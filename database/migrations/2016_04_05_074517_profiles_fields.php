<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfilesFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->integer('section_id')->after('user_id');
            $table->integer('prefix_id')->after('user_id');
            $table->integer('employees')->after('user_id');
            $table->string('address')->after('user_id')->nullable();
            $table->string('phone')->after('user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('section_id');
            $table->dropColumn('prefix_id');
            $table->dropColumn('employees');
            $table->dropColumn('address');
            $table->dropColumn('phone');
        });
    }
}

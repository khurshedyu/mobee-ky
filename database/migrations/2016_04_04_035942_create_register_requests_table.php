<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('password');
            $table->string('key');
            $table->integer('status');
            $table->timestamp('expired_at')->default('0000-00-00 00:00');
            $table->timestamp('created_at')->default('0000-00-00 00:00');
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('register_requests');
    }
}

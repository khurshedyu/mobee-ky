<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('description')->nullable();
            $table->timestamp('start_at')->default('0000-00-00 00:00');
            $table->timestamp('end_at')->default('0000-00-00 00:00');
            $table->timestamp('created_at')->default('0000-00-00 00:00');
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('supers');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->float('money');
            $table->integer('user_id');
            $table->integer('type');
            $table->text('content')->nullable();
            $table->timestamp('created_at')->default('0000-00-00 00:00');
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pay_transactions');
    }
}

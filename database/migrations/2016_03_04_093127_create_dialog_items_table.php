<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDialogItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dialog_items', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content')->nullable();
            $table->integer('type')->nullable();
            $table->integer('profile_id')->nullable()->unsigned();
            $table->integer('dialog_id')->nullable()->unsigned();
            $table->timestamp('created_at')->default('0000-00-00 00:00');
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dialog_items');
    }
}

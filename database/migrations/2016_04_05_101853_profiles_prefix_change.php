<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProfilesPrefixChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('prefix_id');
            $table->string('prefix')->after('address')->nullable();
            $table->integer('entity_type')->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->integer('prefix_id')->after('user_id');
            $table->dropColumn('prefix');
            $table->dropColumn('entity_type');
        });
    }
}

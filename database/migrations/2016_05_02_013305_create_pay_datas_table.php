<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id');
            $table->float('amount', 15, 2);
            $table->integer('currency');
            $table->string('cardhash')->nullable();
            $table->string('card_bin')->nullable();
            $table->string('c_hash')->nullable();
            $table->string('msg')->nullable();
            $table->string('secure')->nullable();
            $table->string('payername')->nullable();
            $table->string('payermail')->nullable();
            $table->string('payerphone')->nullable();
            $table->integer('approval_code');
            $table->bigInteger('reference');
            $table->integer('completed');
            $table->string('completed_msg')->nullable();
            $table->timestamp('bank_time')->default('0000-00-00 00:00');
            $table->timestamp('created_at')->default('0000-00-00 00:00');
            $table->timestamp('updated_at')->default('0000-00-00 00:00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pay_datas');
    }
}

<?php

class JImage
{
    private $image;
    private $image_info;

    public function __construct($image_path)
    {
        if (!file_exists($image_path)) return;

        $this->image_info = getimagesize($image_path);

        switch($this->image_info['mime'])
        {
            case 'image/jpeg':
                $this->image = imagecreatefromjpeg($image_path);
                break;

            case 'image/gif':
                $this->image = imagecreatefromgif($image_path);
                break;

            case 'image/png':
                $this->image = imagecreatefrompng($image_path);
                break;
        }
    }

    private function getProportionalSize($my_width, $width, $height)
    {
        return floor($height / ($width / $my_width));
    }

    private function imageResize($params)
    {
        if (!isset($params['width']) AND !isset($params['height']))
        {
            return $this->image;
        }

        // Получаем реальные размеры изображения
        $params['real_width'] = imagesx($this->image);
        $params['real_height'] = imagesy($this->image);

        // Вычисляем недостающие размеры
        if (!isset($params['height']))
        {
            $params['height'] = $this->getProportionalSize($params['width'], $params['real_width'], $params['real_height']);
        }
        else if (!isset($params['width']))
        {
            $params['width'] = $this->getProportionalSize($params['height'], $params['real_height'], $params['real_width']);
        }

        // Получаем коэффициент изменения размера
        $k_width =  $params['width'] / $params['real_width'];
        $k_height = $params['height'] / $params['real_height'];
        $params['k'] = $k_width > $k_height ? $k_width : $k_height;
        $params['k'] = ceil($params['k'] * 1000) / 1000;

        // Получаем новые размеры изображения
        $params['item_width'] = $params['real_width'] * $params['k'];
        $params['item_height'] = $params['real_height'] * $params['k'];

        // $params['width'] = $params['item_width'];
        // Создаем пустое изображение по заданым размерам
        $image = imagecreatetruecolor($params['width'], $params['height']);
        imagefill($image, 0, 0, imagecolorallocate($image, 255, 255, 255));

        // Вычисляем отступы для центрирования изображения
        $width_offset = ceil($params['item_width'] - $params['width']) * 0.5 / $params['k'];
        $height_offset = ceil($params['item_height'] - $params['height']) * 0.5 / $params['k'];

        imagecopyresampled(
            $image,
            $this->image,
            0,
            0,
            $width_offset,
            $height_offset,
            $params['item_width'],
            $params['item_height'],
            $params['real_width'],
            $params['real_height']
        );

        return $image;
    }

    public function getImage($params = array(), $path)
    {
        $image = $this->imageResize($params);

        imagejpeg($image, $path, 90);
        imagedestroy($image);
    }
}
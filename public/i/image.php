<?php

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(-1);

include "JImage.php";

$uri = $_SERVER['REQUEST_URI'];
$matches = [];

if (!preg_match("/^\/i\/(avatars|original|orderimg)\/(.*?)$/", $uri, $matches)) {
    echo '404';
    return;
}

$dpi = 'original';
$w = 800;
$h = 0;

switch ($matches[1]) {
    case 'avatars':
        $dpi = 'avatars';
        $w = 200;
        $h = 200;
        break;
    case 'orderimg':
        $dpi = 'orderimg';
        $w = 800;
        $h = 600;
        break;
}

$file = getcwd() . '/upload/' . $matches[2];

if (!file_exists($file)) {
    echo 'no image';
    return;
}

$image = new JImage($file);
header('Content-Type: image/jpeg');

if ($dpi == 'original') {
    $image->getImage([], null);
} else {
    $params = ['width' => $w];
    if ($h) {
        $params['height'] = $h;
    }
    $image->getImage($params, null);
}

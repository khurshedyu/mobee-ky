System.register(['angular2/core', 'angular2/common', 'angular2/platform/browser'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, common_1, browser_1;
    var socket, DialogComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (browser_1_1) {
                browser_1 = browser_1_1;
            }],
        execute: function() {
            DialogComponent = (function () {
                // private http: Http;
                function DialogComponent(ngz) {
                    var _this = this;
                    this.messages = [];
                    this.currentPrice = '';
                    this.priceConfirmed = false;
                    this.priceBtnClassMap = {
                        'btn-default': !this.priceConfirmed,
                        'btn-success': this.priceConfirmed
                    };
                    this.isDeal = false;
                    this.dialogInited = false;
                    this.initDialog = function () {
                        socket.emit('init_dialog', _this.dialogId, _this.currentProfileData.id);
                        if (_this.deal) {
                            _this.currentPrice = _this.deal.price;
                            _this.isDeal = true;
                        }
                    };
                    this.initToken = function () {
                        socket.emit('token', _this.token);
                    };
                    this.initSockets = function () {
                        var _this = this;
                        socket.on('message', function (message) {
                            console.log('MESSAGE FROM SERVER', message);
                            _this.ngz.run(function () {
                                _this.pushMessage(message);
                            });
                        });
                        socket.on('messages', function (messages) {
                            _this.ngz.run(function () {
                                _this.pushMessages(messages);
                            });
                        });
                        socket.on('system', function (data) {
                            console.log('SERVER DATA', data);
                            _this.ngz.run(function () {
                                _this.systemMessage(data);
                            });
                        });
                        socket.on('need_token', function () {
                            _this.initToken();
                        });
                        socket.on('token_success', function () {
                            if (!_this.dialogInited) {
                                _this.initDialog();
                            }
                        });
                        socket.on('dialog_inited', function () {
                            if (!_this.dialogInited) {
                                _this.dialogInited = true;
                                _this.initStartMessages();
                            }
                        });
                        socket.on('deal_price_sync', function (price) {
                            _this.ngz.run(function () {
                                _this.priceBtnClassMap['btn-success'] = false;
                                _this.priceBtnClassMap['btn-default'] = true;
                                _this.priceConfirmed = false;
                                _this.currentPrice = price;
                            });
                        });
                        socket.on('deal_close', function (price) {
                            _this.ngz.run(function () {
                                _this.currentPrice = price;
                                _this.isDeal = true;
                            });
                        });
                    };
                    this.initStartMessages = function () {
                        // var uri = '/api/chat/messages/' + this.dialogId + '/1';
                        // var ngz = this.ngz;
                        // var pushMessage = this.pushMessage;
                        socket.emit('old_messages', {
                            dialog: this.dialogId,
                            from: 1
                        });
                        // $.ajax(uri).done(function(data) {
                        //     console.log(data);
                        //     if (data.code == 0) {
                        //             console.log('MESSAGES BRO WUT', data.data.messages);
                        //             for (var i in data.data.messages) {
                        //                 var message: Message = {
                        //                     id: data.data.messages[i].id,
                        //                     avatar: data.data.messages[i].avatar,
                        //                     name: data.data.messages[i].name,
                        //                     nameUrl: data.data.messages[i].nameUrl,
                        //                     content: data.data.messages[i].content,
                        //                     date: null,
                        //                     type: 'message'
                        //                 };
                        //                 console.log('TRY PUSH', data.data.messages[i]);
                        //                 pushMessage(message);
                        //             }
                        //         ngz.run(() => {
                        //             console.log('NG RUN');
                        //         });
                        //     }
                        // });
                        // this.http.get(uri)
                        //     .subscribe(
                        //         data => console.log('DATA', data),
                        //         err => console.log('ERR', err),
                        //         () => console.log('Random Quote Complete')
                        //     );
                    };
                    this.clickContent = function (message) {
                    };
                    this.priceChange = function () {
                        console.log('PRICE', _this.currentPrice);
                        _this.priceBtnClassMap['btn-success'] = false;
                        _this.priceBtnClassMap['btn-default'] = true;
                        _this.priceConfirmed = true;
                        socket.emit('deal_price', _this.currentPrice);
                    };
                    this.priceConfirm = function () {
                        _this.priceBtnClassMap['btn-success'] = true;
                        _this.priceBtnClassMap['btn-default'] = false;
                        _this.priceConfirmed = true;
                        socket.emit('deal_price_confirm', _this.currentPrice);
                    };
                    this.sendMessage = function () {
                        if (!this.currentMessage) {
                            return false;
                        }
                        var message = {
                            id: this.currentProfileData.id,
                            avatar: this.currentProfileData.avatar,
                            name: this.currentProfileData.name,
                            nameUrl: this.currentProfileData.nameUrl,
                            content: this.currentMessage,
                            date: 'date',
                            type: 'message',
                            params: null
                        };
                        // socket.emit('message', message);
                        socket.emit('send_message', {
                            dialog: this.dialogId,
                            profile: this.currentProfileData.id,
                            message: this.currentMessage
                        });
                        // this.pushMessage(message);
                        console.log('CURRENT MESSAGE', this.currentMessage);
                        // socket.emit('message', this.currentMessage);
                        this.currentMessage = '';
                    };
                    this.systemMessage = function (data) {
                        var message = {
                            id: null,
                            avatar: null,
                            name: null,
                            nameUrl: null,
                            content: data,
                            date: null,
                            type: 'system',
                            params: null
                        };
                        this.pushMessage(message);
                    };
                    this.pushMessage = function (message) {
                        this.messages.push(message);
                        this.scrollToBottom();
                    };
                    this.pushMessages = function (messages) {
                        var _this = this;
                        for (var i in messages) {
                            this.messages.push(messages[i]);
                        }
                        setTimeout(function () {
                            _this.scrollToBottom();
                        }, 500);
                    };
                    this.scrollToBottom = function () {
                        $("html, body").animate({ scrollTop: $(document).height() }, 300);
                    };
                    this.textKeyPress = function (event) {
                        if (event.charCode == 13 && !event.shiftKey) {
                            this.sendMessage();
                            return false;
                        }
                    };
                    socket = io(':3000');
                    this.currentProfileData = window.jsonData.profile;
                    this.dialogId = window.jsonData.dialogId;
                    this.starterId = window.jsonData.starterId;
                    this.token = window.jsonData.token;
                    this.deal = window.jsonData.deal;
                    this.isoffer = window.jsonData.isoffer;
                    this.ngz = ngz;
                    // this.http = http;
                    this.initSockets();
                    this.initDialog();
                    // this.initToken();
                    console.log('JSON DATA', window.jsonData, this.currentProfileData);
                }
                DialogComponent.prototype.ngDoCheck = function () {
                    // console.log('DO CHECK');
                };
                DialogComponent.prototype.ngAfterContentInit = function () {
                    // this.initStartMessages();
                };
                DialogComponent = __decorate([
                    core_1.Component({
                        selector: 'app-dialog',
                        templateUrl: '/html/dialog.component.html',
                        directives: [common_1.NgClass]
                    }), 
                    __metadata('design:paramtypes', [core_1.NgZone])
                ], DialogComponent);
                return DialogComponent;
            }());
            exports_1("DialogComponent", DialogComponent);
            browser_1.bootstrap(DialogComponent);
        }
    }
});

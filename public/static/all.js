var AjaxResponse = {
    isValid: function (res) {
        return (res.code !== undefined && res.code == 200);
    },
    run: function () {

    }
};

$(document).ready(function () {
    AjaxResponse.run();
});
var Api = {
    run: function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    }
};

$(document).ready(function () {
    Api.run();
});
$.fn.checkUsername = function () {
    var element = $(this);
    var label = element.parent().find('label');
    var lastKeyUp = Date.now();
    var lastUpdate = Date.now();
    var delay = 500;
    var defaultName = element.data('format');
    var queryCount = 0;

    var minSymbol = function () {
        label.find('span.check-username').remove();
        label.append('<span class="check-username check-username-busy">(минимум 5 символов)</span>');
        return;
    };

    element.on('keyup', function (e) {
        console.log('ELEMENT LENGTH', element.val().length);

        if (element.val().length < 5) {
            return minSymbol();
        }

        lastKeyUp = Date.now();

        setTimeout(function () {
            var now = Date.now();

            if ((now - lastKeyUp) > delay && (now - lastUpdate) > delay) {
                lastUpdate = Date.now();
                var value = element.val();
                console.log('GO', value);

                if (element.val().length < 5) {
                    return minSymbol();
                }

                label.find('span.check-username').remove();
                label.append('<span class="check-username check-username-process">(проверка)</span>');

                queryCount++;
                $.ajax('/webapi/username/check/' + value + '/' + defaultName)
                    .done(function (res) {
                        queryCount--;

                        if (queryCount) {
                            return;
                        }

                        label.find('span.check-username').remove();
                        if (AjaxResponse.isValid(res)) {
                            if (element.val().length < 5) {
                                return minSymbol();
                            }
                            
                            if (res.data.busy) {
                                label.append('<span class="check-username check-username-busy">(занят)</span>');
                            } else if (!res.data.clean) {
                                label.append('<span class="check-username check-username-busy">(недопустимые символы)</span>');
                            } else {
                                label.append('<span class="check-username check-username-free">(свободен)</span>');
                            }
                        } else {
                            label.append('<span class="check-username check-username-busy">(недопустимые символы)</span>');
                        }
                    })
                    .fail(function () {
                        queryCount--;
                        label.find('span.check-username').remove();
                        label.append('<span class="check-username check-username-busy">(недопустимые символы)</span>');
                    });
            }
        }, delay + 50);

        // console.log('CHANHGE', $(this).val());
    });
};
var Dashboard = {
    init: function () {
        var me = this;

        $('.dashboard-demo-button a').on('click', function (e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).html($(this).data('active'));
                $(this).parent().parent().find('.dashboard-noauth-main').hide();
                $(this).parent().parent().find('.dashboard-noauth-demo').show();
            } else {
                $(this).addClass('active');
                $(this).html($(this).data('unactive'));
                $(this).parent().parent().find('.dashboard-noauth-main').show();
                $(this).parent().parent().find('.dashboard-noauth-demo').hide();
            }
        });
    }, 
    run: function () {
        var me = this;

        me.init();
    }
};

$(document).ready(function () {
    Dashboard.run();
});
var Header = {
    bind: function () {
        var me = this;

        $('#header-links-more').on('click', function (e) {
            e.preventDefault();

            $('#meloman-list').toggle();

            // var content = $('#header-links-more-content');

            // Modal.sizeS('Все проекты', content.html());
        });

        $('#header-menu-more').on('click', function (e) {
            e.preventDefault();

            $('#menu-down').toggle();

            // alert('WUT');
            return false;

            // var liList = $(this).parent().parent().find('ul li a');

            // var content = '<div class="row"><div class="btn-group-vertical col-xs-12 col-sm-12" role="group">';
            // for (var i = 0; i < liList.length; i++) {
            //     var item = $(liList[i]);
            //     content += '<a href="' + item.attr('href') + '" class="btn btn-lg btn-default">' + item.html() + '</a>';
            // }
            // content +='</div></div>';

            // Modal.sizeS('Все сферы', content);
        });

        $('#links-more-btn').on('click', function (e) {
            e.preventDefault();

            $('#links-more-list').toggle();
        });

        // console.log('BIND LINKS UL RESIZE');
        WindowResize.subscribe(function (params) {
            // console.log('LINKS UL RESIZE');
            if (params.width < 1150) {
                $('#links-ul').hide();
                $('#links-more').show();
            } else {
                $('#links-ul').show();
                $('#links-more').hide();
            }
        });
    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    Header.run();
});
var ImageAjax = {
    imageUploaded: function (res, dataImg, dataHidden) {
        console.log('IMAGE UPLOADED', res, dataImg, dataHidden);

        if (res.error === undefined || res.error !== 0) {
            return false;
        }

        var imgId = '#' + dataImg;
        var imgField = $(imgId);
        
        var hiddenId = '#' + dataHidden;
        var hiddenField = $(hiddenId);

        hiddenField.val(res.data.id);
        
        imgField.attr('src', res.data.path);
        imgField.show();

        return true;
    },
    bind: function () {
        var me = this;

        $('.image-ajax-input').on('change', function (e) {
            if (!$(this)[0].files[0]) {
                return false;
            }

            var dataImg = $(this).data('img');
            var dataBtn = $(this).data('btn');
            var dataHidden = $(this).data('hidden');

            var formData = new FormData();
            formData.append('file', $(this)[0].files[0]);

            var btn = $('#' + dataBtn);
            btn.hide();

            $.ajax({
                url : '/uploadimg',
                type : 'POST',
                data : formData,
                processData: false,
                contentType: false
            }).done(function(res) {
                me.imageUploaded(res, dataImg, dataHidden);
            }).fail(function(res) {
            }).always(function(res) {
                console.log('IMG RESUEST END');
                btn.show();
            });
        });

        $('.image-ajax-btn').on('click', function (e) {
            e.preventDefault();

            var input = $('#' + $(this).data('id'));
            input.click();

            return false;
        });
    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    ImageAjax.run();
});
var Main = {
    priceRequestDisabler: function () {
        var element = $('#form_element_order_price_request_field');
        var field = $('#form_element_order_price_field');

        if (element.length && field.length) {
            if (element[0].checked) {
                field[0].disabled = true;
            } else {
                field[0].disabled = false;
            }
        }
    },
    orderCategorySelectItem: function (item) {
        var me = this;
        var box = $(item);
        var input = box.find('.order-category-select-input');
        var currentId = input.val();

        var updateCategories = function (categoryId) {
            box.loadBlockStart();

            $.ajax('/ads/ordersectioninfo/' + categoryId).done(function (res) {
                if (!AjaxResponse.isValid(res)) {
                    return false;
                }

                box.find('.order-category-select-section select').val(res.data.section_id);

                box.find('.order-category-select-category select').selectLoad(res.data.categories);
                box.find('.order-category-select-category select').val(res.data.category_id);
                box.find('.order-category-select-category').show();

                box.find('.order-category-select-category-sub select').selectLoad(res.data.sub);
                box.find('.order-category-select-category-sub select').val(res.data.sub_id);
                box.find('.order-category-select-category-sub').show();

                if (res.data.sub.length) {
                    box.find('.order-category-select-category-sub').hide();
                }
            }).fail(function (res) {
                alert('Ошибка загрузки категорий');
            }).always(function (res) {
                box.loadBlockStop();
            });
        };

        if (currentId) {
            updateCategories(currentId);
        }

        box.find('.order-category-select-section select').on('change', function () {
            if (!$(this)) {
                return false;
            }

            var select = $(this);
            var currentSection = select.val();

            input.val(0);

            box.loadBlockStart();

            $.ajax('/ads/ordersection/' + currentSection).done(function (res) {
                if (!AjaxResponse.isValid(res)) {
                    return false;
                }

                box.find('.order-category-select-category select').selectLoad(res.data.categories);
                box.find('.order-category-select-category select').val(0);
                box.find('.order-category-select-category').show();

                if (res.data.categories.length) {
                    box.find('.order-category-select-category').hide();
                }

                box.find('.order-category-select-category-sub select').selectLoad(res.data.sub);
                box.find('.order-category-select-category-sub select').val(0);
                box.find('.order-category-select-category-sub').hide();
            }).fail(function (res) {
                alert('Ошибка загрузки категорий');
            }).always(function (res) {
                box.loadBlockStop();
            });
        });

        box.find('.order-category-select-category select').on('change', function () {
            var select = $(this);
            var currentCategory = parseInt(select.val());

            console.log('CAT CHANGE', currentCategory);
            input.val(currentCategory);

            if (!currentCategory) {
                box.find('.order-category-select-category-sub').hide();
                return false;
            }

            updateCategories(currentCategory);
        });

        box.find('.order-category-select-category-sub select').on('change', function () {
            var select = $(this);
            var currentCategory = parseInt(select.val());

            if (!currentCategory) {
                currentCategory = box.find('.order-category-select-category select').val();
            }

            console.log('SUB CHANGE', currentCategory);
            input.val(currentCategory);

            // updateCategories(currentCategory);
        });
    },
    orderCategorySelect: function () {
        var me = this;
        var box = $('.order-category-select');

        for (var i = 0; i < box.length; i++) {
            me.orderCategorySelectItem(box[i]);
        }
    },
    bind: function () {
        var me = this;

        $('#main-slider').on('click', function (e) {
            e.preventDefault();

            var content = $('#main-slider-content');

            Modal.sizeL('Давайте знакомиться', content.html());
        });

        $('#form_element_order_price_request_field').on('change', function (e) {
            me.priceRequestDisabler();
        });

        me.orderCategorySelect();
    },
    run: function () {
        var me = this;

        me.bind();
        me.priceRequestDisabler();
    }
};

$(document).ready(function () {
    Main.run();
});
var Modal = {
    MODAL_TYPE_S: 's',
    MODAL_TYPE_M: 'm',
    MODAL_TYPE_L: 'l',
    loader: function (show) {
        if (show) {
            $('.mobeemodal-loader').show();
            $('.mobeemodals > ul').hide();
        } else {
            $('.mobeemodal-loader').hide();
            $('.mobeemodals > ul').show();
        }
    },
    checkBodyBlock: function () {
        var me = this;
        var container = $('#modals > ul > li');

        if (container.length) {
            $('body').addClass('blocked');
        } else {
            $('body').removeClass('blocked');
        }

        // console.log('CHECK', container.length);
    },
    showModal: function (type, title, content, options) {
        var me = this;
        var data = {};

        if (typeof options == 'object') {
            for (var key in options) {
                data[key] = options[key];
            }
        }

        var container = $('#modals > ul');
        var modal = $('#mobeemodal-template').clone().html();
        var li = $('<li></li>');

        // console.log('ASDASDASD', type);

        modal = Work.substr(modal, '[[title]]', title);
        modal = Work.substr(modal, '[[content]]', content);
        // console.log('MODAL', modal);

        modal = $(modal);
        modal.find('.mobeemodal-window').addClass('mobeemodal-window-' + type);
        // console.log('MODAL', modal);

        li.append(modal);
        container.prepend(li);

        me.checkBodyBlock();
    },
    sizeS: function (title, content, options) {
        var me = this;

        me.showModal(me.MODAL_TYPE_S, title, content, options);
    },
    sizeM: function (title, content, options) {
        var me = this;

        me.showModal(me.MODAL_TYPE_M, title, content, options);
    },
    sizeL: function (title, content, options) {
        var me = this;

        me.showModal(me.MODAL_TYPE_L, title, content, options);
    },
    bind: function () {
        var me = this;

        $('#modals > ul').on('click', 'li .mobeemodal-header-close-btn', function (e) {
            e.preventDefault();
            $(this).parent().parent().parent().parent().parent().remove();
            me.checkBodyBlock();
        });

        // WindowResize.subscribe(function (params) {
        //     $('.mobeemodal-bg').width(params.width);
        //     $('.mobeemodal-bg').height(params.height);
        //     $('.mobeemodals li').width(params.width);
        //     $('.mobeemodals li').height(params.height);
        // });

    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    Modal.run();
});
var Notification = {
    lastId: null,
    checkInterval: null,
    soundS: null,
    soundM: null,
    init: function () {
        var me = this;
        
        me.lastId = $('#dashboard-notifications').data('last');

        if (me.lastId) {
            // console.log('LAST ID', me.lastId);

            me.checkInterval = setTimeout(function () {
                // console.log('check by id', me.lastId);
                $.ajax('/webapi/notifications/check/' + me.lastId)
                    .done(function (res) {
                        if (!AjaxResponse.isValid(res)) {
                            return false;
                        }

                        var data = res.data;

                        if (data.length) {
                            me.soundS.play();
                        }

                        for (var i = 0; i < data.length; i++) {
                            if (data[i].id > me.lastId) {
                                me.lastId = data[i].id;
                            }

                            var item = '<li><div class="dashboard-notifications-item"><a href="#" class="closebtn"><i class="mobee-cancel-2"></i></a><a href="' + data[i].data.url + '" class="message">' + data[i].data.message + '</a><small class="blend-text">' + data[i].date + '</small></div></li>';
                            $('.widget-notifications ul').append(item);
                        }
                    })
                    .fail(function (data) {
                        console.log('FAIL', data);
                    });
            }, 3000);
        }

        me.soundS = new Audio('/static/sound/S.mp3');
        me.soundM = new Audio('/static/sound/M.mp3');
    },
    bind: function () {
        var me = this;

        $('.widget-notifications ul').on('click', '.closebtn', function (e) {
            e.preventDefault();
            $(this).parent().parent().remove();
        });
    },
    run: function () {
        // console.log('RUN NOTIFICATION');
        var me = this;

        me.init();
        me.bind();
    }
};

$(document).ready(function () {
    // Notification.run();
});
var Plus = {
    openPlus: function (key) {
        // console.log('OPEN PLUS', key);
        Modal.loader(true);
        $.ajax('/plus/' + key)
            .done(function (data) {
                Modal.loader(false);
                // console.log('GET PLUS', data);

                if (data.title && data.content) {
                    Modal.sizeM(data.title, data.content);
                }
            })
            .fail(function (data) {
                Modal.loader(false);
                // console.log('GET PLUS', data);
            });

        //Modal.sizeM(key, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,vquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
    },
    bind: function () {
        var me = this;

        $('body').on('click', '.plus', function (e) {
            e.preventDefault();
            var key = $(this).data('key');
            if (key) {
                me.openPlus(key);
            }
        });
    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    Plus.run();
});
var WindowResize = {
    subscribes: [],
    subscribe: function (callback) {
        var me = this;

        me.subscribes.push(callback);
    },
    run: function () {
        var me = this;
        var params = {
            width: $(window).width(),
            height: $(window).height()
        };

        for (var i = 0; i < me.subscribes.length; i++) {
            me.subscribes[i](params);
        }

        // console.log('WINDOW RESIZE RUN', params);
    }
};

$(document).ready(function () {
    WindowResize.run();
    $(window).resize(function () {
        WindowResize.run();
    });
});
var Work = {
    substr: function (string, key, val) {
        return string.split(key).join(val);
    }
};

$.fn.selectLoad = function (data) {
    var element = $(this);

    element.html('');

    for (var key in data) {
        element.append('<option value="' + key + '">' + data[key] + '</option>');
    }
};

$.fn.loadBlockStart = function (params) {
    var element = $(this);

    element.prepend('<div style="position:absolute; top:0; left:0; right:0; bottom:0; background:rgba(255,255,255,0.7); z-index:1000;" class="load-block"><div style="position:absolute; top:50%; left:0; right:0; text-align:center; margin-top:-20px; line-height:40px; color:#333;">Загрузка...</div></div>');
};

$.fn.loadBlockStop = function () {
    var element = $(this);

    element.find('.load-block').remove();
};
var Wrap = {
    select: function (wrap) {
        var me = this;
        
        $('#controls a').removeClass('active');
        $('#controls a#controls-' + wrap).addClass('active');
        $('#wrap .wrap-item').removeClass('wrap-active');
        $('#wrap #wrap-' + wrap).addClass('wrap-active');
    },
    bind: function () {
        var me = this;

        $('#controls a').on('click', function (e) {
            e.preventDefault();
            me.select($(this).data('wrap'));
        });

        $(".nano").nanoScroller();
    },
    run: function () {
        var me = this;

        me.bind();
    }
};

$(document).ready(function () {
    Wrap.run();
});
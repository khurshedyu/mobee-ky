<?php

return [
    'dashboard-usermenu' => [
        [
            'icon' => 'mobee-pencil-5',
            'title' => 'Заказы',
        ],
        [
            'icon' => 'mobee-shield',
            'title' => 'Сделки',
        ],
        [
            'icon' => 'mobee-link-4',
            'title' => 'Связи',
        ],
        [
            'icon' => 'mobee-book-3',
            'title' => 'Отзывы',
        ],
        [
            'icon' => 'mobee-chart-line',
            'title' => 'Посещения',
        ],
        [
            'icon' => 'mobee-chat-1',
            'title' => 'Поддержка',
        ],
    ],
];